<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/**
 * Promo Site Routes
 */


Route::get('/', function () {
    return view('landing.index');
});

Route::get('/industries', function () {
    return view('landing.industries');
});

Route::get('/demo', function () {
    return view('landing.demo');
});

Route::get('/reviews', function () {
    return view('landing.reviews');
});

Route::get('/pricing', function () {
    return view('landing.pricing');
});

Route::get('/gene-msj', function () {
    return Redirect::to('http://flapiapp.com/public/gene-msj');
});


/**
 * Customer Routes
 */

Route::group(['prefix' => 'payments'], function() {
    Route::get('/ipn', ['as' => 'payments.ipn.get', 'uses' => 'Admin\PaymentController@post']);
    Route::post('/ipn', ['as' => 'payments.ipn.post', 'uses' => 'Admin\PaymentController@post']);
    Route::get('/ipn', ['as' => 'payments.ipn.index', 'uses' => 'Admin\PaymentController@get']);
});


Route::get('/app', function () {
    return redirect()->route('login');
});

Route::group(['prefix' => 'app'], function(){


    Route::get('/layout-test/{id}', function ($id) {

        $layout = App\Models\Layouts\Layout::find($id);
        $layout->load('sections');

        return view('page-layouts.sample', compact('layout'));
    });

    /**
     * User Register
     */
    Route::as('users.register')->post('register','UsersController@store');

    Route::as('login')->get('login',['middleware' => ['guest'],function(){
        return view('login.index');
    }]);

    Route::as('logout')->get('logout',function(){
        Auth::logout();
        return redirect()->route('login');
    });

    //Route::get('login/{token}','Auth\AuthController@inviteLogin');

    /**
     * Settings Routes
     */

    Route::group(['prefix' => 'settings','middleware' => ['forceHTTPS','auth','adminOnly','logcheck']], function(){
        Route::as('settings.info')->get('/','SettingsController@index');
        Route::as('settings.info.store')->post('/store','SettingsController@store');
    });


    /**
     * Flatplan Routes
     */
    Route::as('flatplans.public.view')->get('public/{view_token}/{user_token}','FlatPlanController@publicView');

    Route::group(['prefix' => 'flat-plans','middleware' => ['forceHTTPS','auth','logcheck']], function(){
        Route::as('flatplans.index')->get('/','FlatPlanController@index');
        Route::as('flatplans.show')->get('/{slug}/v/{version}','FlatPlanController@show');
        Route::as('flatplans.duplicate')->get('duplicate/{flatplan_id}','FlatPlanController@duplicate');
        Route::as('flatplans.store')->post('/store','FlatPlanController@store');
        Route::as('flatplans.pdf')->get('pdf/{slug}/v/{version}','FlatPlanController@pdf');
        Route::as('flatplans.preview')->get('preview/{slug}/v/{version}','FlatPlanController@preview');
        Route::as('flatplans.status.toggle')->post('toggle-status','FlatPlanController@toggleLive');

    //    Route::as('flatplans.revert')->get('revert/{version_id}','FlatPlanController@revert');
    //    Route::as('flatplans.delete.version')->get('delete/version/{version_id}','FlatPlanController@deleteVersion');
    //    Route::as('flatplans.archive')->get('archive/{flat_plan_id}','FlatPlanController@archive');

        Route::as('flatplans.revert')->post('revert','FlatPlanController@revert');
        Route::as('flatplans.delete.version')->post('delete/version','FlatPlanController@deleteVersion');
        Route::as('flatplans.archive')->post('archive','FlatPlanController@archive');
    });


    Route::group(['prefix' => 'archive','middleware' => ['forceHTTPS','auth','logcheck']], function(){
        Route::as('flatplans.show.archive')->get('/{slug}/v/{version}','FlatPlanController@showArchive');
        Route::as('flatplans.archived')->get('/','FlatPlanController@archived');
    });


    /**
     * Backend Routes
     */

    Route::group(['prefix' => 'cp','middleware' => ['siteAdminsOnly']], function() {

        Route::get('/', ['as' => 'backend.index', 'uses' => 'Backend\AdminController@index']);
        Route::get('/login',['as'=>'backend.login','uses'=>'Backend\AdminController@login','middleware' => 'guest']);
        Route::get('/logout',['as'=>'backend.logout','uses'=>'Auth\AuthController@logout']);
        Route::post('/login-post',['as'=>'backend.login.validate','uses'=>'Auth\AuthController@login']);

        Route::group(['prefix' => 'companies'], function() {
            Route::get('/',                     ['as'=>'backend.companies.index','uses'=>'Backend\CompanyController@index']);
            Route::get('/create',               ['as'=>'backend.companies.create','uses'=>'Backend\CompanyController@create']);
            Route::post('/store',               ['as'=>'backend.companies.store','uses'=>'Backend\CompanyController@store']);
            Route::get('/edit/{id}',            ['as'=>'backend.companies.edit','uses'=>'Backend\CompanyController@edit']);
            Route::post('/update',              ['as'=>'backend.companies.update','uses'=>'Backend\CompanyController@update']);
            Route::get('/delete/{id}',          ['as'=>'backend.companies.delete','uses'=>'Backend\CompanyController@delete']);
        });

        Route::group(['prefix' => 'users'], function() {
            Route::get('/',                     ['as'=>'backend.users.index','uses'=>'Backend\UsersController@index']);
            Route::get('/create',               ['as'=>'backend.users.create','uses'=>'Backend\UsersController@create']);
            Route::post('/store',               ['as'=>'backend.users.store','uses'=>'Backend\UsersController@store']);
            Route::get('/edit/{id}',            ['as'=>'backend.users.edit','uses'=>'Backend\UsersController@edit']);
            Route::post('/update',              ['as'=>'backend.users.update','uses'=>'Backend\UsersController@update']);
            Route::get('/delete/{id}',          ['as'=>'backend.users.delete','uses'=>'Backend\UsersController@delete']);
        });

        Route::group(['prefix' => 'plans'], function() {
            Route::get('/',                     ['as'=>'backend.plans.index','uses'=>'Backend\PlanController@index']);
            Route::get('/create',               ['as'=>'backend.plans.create','uses'=>'Backend\PlanController@create']);
            Route::post('/store',               ['as'=>'backend.plans.store','uses'=>'Backend\PlanController@store']);
            Route::get('/edit/{id}',            ['as'=>'backend.plans.edit','uses'=>'Backend\PlanController@edit']);
            Route::post('/update',              ['as'=>'backend.plans.update','uses'=>'Backend\PlanController@update']);
            Route::get('/delete/{id}',          ['as'=>'backend.plans.delete','uses'=>'Backend\PlanController@delete']);
        });

        Route::group(['prefix' => 'subscriptions'], function() {
            Route::get('/',                     ['as'=>'backend.subscriptions.index','uses'=>'Backend\SubscriptionController@index']);
            Route::get('/create',               ['as'=>'backend.subscriptions.create','uses'=>'Backend\SubscriptionController@create']);
            Route::post('/store',               ['as'=>'backend.subscriptions.store','uses'=>'Backend\SubscriptionController@store']);
            Route::get('/edit/{id}',            ['as'=>'backend.subscriptions.edit','uses'=>'Backend\SubscriptionController@edit']);
            Route::post('/update',              ['as'=>'backend.subscriptions.update','uses'=>'Backend\SubscriptionController@update']);
            Route::get('/delete/{id}',          ['as'=>'backend.subscriptions.delete','uses'=>'Backend\SubscriptionController@delete']);
        });

    //    Route::post('/delete', ['as' => 'api.roles.delete', 'uses' => 'RolesController@delete']);
    });

});




/**
 * API Routes
 */

Route::group(['prefix' => 'api'], function(){

    /**
     * Roles
     */
    Route::group(['prefix' => 'roles'], function(){
        Route::post('/store',['as' => 'api.roles.store' , 'uses' =>'RolesController@store']);
        Route::post('/update',['as' => 'api.roles.update' , 'uses' =>'RolesController@update']);
        Route::get('/get',['as' => 'api.roles.get' , 'uses' =>'RolesController@get']);
        Route::post('/delete',['as' => 'api.roles.delete' , 'uses' =>'RolesController@delete']);
    });

    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function(){
        Route::get('/get',['as' => 'api.users.get' , 'uses' =>'UsersController@get']);
        Route::post('/invite',['as' => 'api.users.invite' , 'uses' =>'UsersController@invite']);
        Route::get('/toggleStatus',['as' => 'api.users.toggleStatus' , 'uses' =>'UsersController@toggleStatus']);
        Route::post('/update',['as' => 'api.users.update' , 'uses' =>'UsersController@update']);
        Route::get('/delete',['as' => 'api.users.delete' , 'uses' =>'UsersController@delete']);
        Route::get('/validateEmail',['as' => 'api.users.validateEmail' , 'uses' =>'UsersController@validateEmail']);
        Route::get('/validateRegisterCompany',['as' => 'api.users.validateRegisterCompany' , 'uses' =>'UsersController@validateRegisterCompany']);
        Route::get('/generate-csrf',['uses' =>'Customers\CompanyController@generateCSRF']);
    });

    Route::group(['prefix' => 'company'], function(){
        Route::post('/register', ['as'=>'company.register','uses' => 'Customers\CompanyController@register']);
    });

    /**
     * Contents
     */
    Route::group(['prefix' => 'contents'], function(){
        Route::get('/get',[          'as' => 'api.contents.get' ,          'uses' =>'ContentsController@get']);
        Route::get('/getForSelect',[ 'as' => 'api.contents.getForSelect',  'uses' =>'ContentsController@getForSelect']);
        Route::post('/store',[       'as' => 'api.contents.store' ,        'uses' =>'ContentsController@store']);
        Route::POST('/delete',[      'as' => 'api.contents.delete' ,       'uses' =>'ContentsController@delete']);
        Route::post('/update',[      'as' => 'api.contents.update' ,       'uses' =>'ContentsController@update']);


        Route::group(['prefix' => 'fields'], function(){
            Route::get('/get',['as' => 'api.contents.fields.get' , 'uses' =>'ContentsController@getFields']);
        });
    });


    /**
     * Content Fields
     */
    Route::group(['prefix' => 'fields'], function(){
        Route::post('/store', ['as' => 'api.fields.store' ,  'uses' =>'ContentFieldController@store']);
        Route::post('/update',['as' => 'api.fields.update' , 'uses' =>'ContentFieldController@update']);
        Route::get('/get',    ['as' => 'api.fields.get' ,    'uses' =>'ContentFieldController@get']);
        Route::post('/delete',['as' => 'api.fields.delete' , 'uses' =>'ContentFieldController@delete']);
    });


    /**
     * mediaTypes
     */
    Route::group(['prefix' => 'mediaTypes'], function(){
        Route::post('/store', ['as' => 'api.mediaTypes.store' ,  'uses' =>'MediaTypeController@store']);
        Route::post('/update',['as' => 'api.mediaTypes.update' , 'uses' =>'MediaTypeController@update']);
        Route::get('/get',    ['as' => 'api.mediaTypes.get' ,    'uses' =>'MediaTypeController@get']);
        Route::post('/delete',['as' => 'api.mediaTypes.delete' , 'uses' =>'MediaTypeController@delete']);
    });


    /**
     * formatTypes
     */
    Route::group(['prefix' => 'formatTypes'], function(){
        Route::post('/store', ['as' => 'api.formatTypes.store' ,  'uses' =>'FormatTypeController@store']);
        Route::post('/update',['as' => 'api.formatTypes.update' , 'uses' =>'FormatTypeController@update']);
        Route::get('/get',    ['as' => 'api.formatTypes.get' ,    'uses' =>'FormatTypeController@get']);
        Route::post('/delete',['as' => 'api.formatTypes.delete' , 'uses' =>'FormatTypeController@delete']);
    });

    /**
     * Content Fields
     */
    Route::group(['prefix' => 'pages'], function(){
        Route::get('/get',         ['as' => 'api.pages.get' ,    'uses' =>'FlatPlanPagesController@get']);
        Route::get('/delete',      ['as' => 'api.pages.delete' ,       'uses' =>'FlatPlanPagesController@delete']);
        Route::post('/reorder',    ['as' => 'api.pages.reorder' ,    'uses' =>'FlatPlanPagesController@reorder']);
        Route::post('/reorder/covers',    ['as' => 'api.pages.reorder.covers' ,    'uses' =>'FlatPlanPagesController@reorderCovers']);
        Route::post('/add',        ['as' => 'api.pages.add' ,    'uses' =>'FlatPlanPagesController@add']);
        Route::post('/merge',      ['as' => 'api.pages.merge' ,    'uses' =>'FlatPlanPagesController@merge']);
    });

    /**
     * Flat Plans
     */
    Route::group(['prefix' => 'fp'], function(){
        Route::post('/store',             ['as' => 'api.flatplans.store' ,          'uses' =>'FlatPlanController@store']);
        Route::post('/update',            ['as' => 'api.flatplans.update' ,         'uses' =>'FlatPlanController@update']);
        Route::get('/get',                ['as' => 'api.flatplans.get' ,            'uses' =>'FlatPlanController@get']);
        Route::post('/delete',            ['as' => 'api.flatplans.delete' ,         'uses' =>'FlatPlanController@delete']);
        Route::get('/assign-layout',      ['as' => 'api.flatplans.assign-layout' ,  'uses' =>'FlatPlanPagesController@assignLayout']);
        Route::get('/get-section',        ['as' => 'api.flatplans.get-section' ,    'uses' =>'FlatPlanPagesController@getSection']);
        Route::post('/update-section',    ['as' => 'api.flatplans.update-section' , 'uses' =>'FlatPlanSectionController@update']);
        Route::get('/create-version/{id}',['as' => 'api.flatplans.create-version' , 'uses' =>'FlatPlanController@createVersion']);
        Route::get('/create-hv/{id}/{v}', ['as' => 'api.flatplans.create-vh' ,      'uses' =>'FlatPlanController@createVersionFromHistory']);
        Route::post('/invite-users',      ['as' => 'api.flatplan.invite' ,          'uses' =>'FlatPlanController@inviteUsers']);
        Route::post('/validate',          ['as' => 'api.flatplan.validate' ,        'uses' =>'FlatPlanController@validateNames']);
        Route::get('/getViewers',         ['as' => 'api.flatplan.getViewers' ,      'uses' =>'FlatPlanController@getViewers']);
        Route::post('/getViewers',        ['as' => 'api.flatplan.getViewers' ,      'uses' =>'FlatPlanController@getViewers']);
    });


    /**
     * Templates
     */
    Route::group(['prefix' => 'templates'], function(){
        Route::get('/get/{id}',    ['as' => 'api.templates.get' ,    'uses' =>'TemplateController@get']);
    });

    /**
     * Company
     */
    Route::group(['prefix' => 'company'], function(){
        Route::get('/get-user-limit',['as' => 'api.company.getUserLimit' , 'uses' =>'Customers\CompanyController@getUserLimit']);
        Route::get('/toggle-autorenew/{SubscriptionReference}',['as' => 'api.company.toggleAutoRenew' , 'uses' =>'Customers\CompanyController@toggleAutoRenew']);
    });

    /**
     * History
     */
    Route::group(['prefix' => 'history'], function(){
        Route::get('/undo/{version_id}',['as' => 'api.history.undo' , 'uses' =>'HistoryController@undo']);
        Route::get('/redo/{version_id}',['as' => 'api.history.redo' , 'uses' =>'HistoryController@redo']);
    });

    /**
     * Options
     */
    Route::group(['prefix' => 'options'], function(){
        Route::get('/update',['as' => 'api.options.update' , 'uses' =>'OptionController@update']);
    });

});
