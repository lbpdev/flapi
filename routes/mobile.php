<?php


/**
 * Mobile API Routes
 */

/**
 *
 *  VERSION 1
 *
 */

Route::group(['prefix' => 'mobile-api/v1'], function(){

    Route::as('mobile.login')->post('/login','Mobile\AuthController@login');
    Route::as('mobile.logout')->post('/logout','Mobile\AuthController@logout');
    Route::as('mobile.reset-email')->get('/reset-email','Mobile\AuthController@resetEmail');
    Route::as('mobile.reset-email')->post('/reset-email','Mobile\AuthController@resetEmail');

    Route::as('mobile.reset-password')->get('/reset-password','Mobile\AuthController@resetPassword');
    Route::as('mobile.reset-password')->post('/reset-password','Mobile\AuthController@resetPassword');

    Route::group(['middleware'=>'auth.api'], function(){

        Route::as('mobile.tokens.refresh')->post('/refresh-token','Mobile\TokenController@get');
    });

    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function(){
        Route::as('mobile.users.name.update')->post('/update/name','Mobile\UserController@updateName');
        Route::as('mobile.users.password.update')->post('/update/password','Mobile\UserController@updatePw');
        Route::as('mobile.users.password.update-password')->post('/update-password','Mobile\PasswordController@updatePw');
    });

    /**
     * Comments
     */
    Route::group(['prefix' => 'device-tokens'], function(){
        Route::as('mobile.device-tokens.destroy')->post('/destroy','Mobile\DeviceTokenController@destroy');
        Route::as('mobile.device-tokens.store')->post('/store','Mobile\DeviceTokenController@store');
    });

});