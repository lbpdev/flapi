@extends('emails.main-new')

@section('email_body')
    <p>The scheduling app that will change your life and make your day as easy as pie.</p>
    <p>No more nagging over your shoulder, calling your extension a million times a day and interrupting your favourite song, sending you boring emails asking you if you have finally finished that endless task and generally making your life a drag.</p>
    <p>Your day will now look simple, clean and colourful. Bringing you satisfaction every time you hit the done button.</p>
    <p>Your superstar colleague has been good enough to send you this free invitation to join TapiApp.</p>

    <p><a href="{{ isset($token) ? (url('login') . '/'. $token) : '#' }}" style="color:#bf1c73;text-decoration: underline;">Click Here</a></p>
@endsection

