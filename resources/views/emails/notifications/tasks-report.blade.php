@extends('emails.notifications.master')

@section('email_body')
    <img src="http://tapiapp.com/app/public/images/email/Todays-Summary-Report.png" width="600" alt="Leading Brands Logo" title="Leading brands Logo"></a>
    <table>
        <tr>
            <td width="600" class="padd">&nbsp;</td>
        </tr>
    </table>
    <table style="width:100%;font-size:20px">
        <tr>
            <td width="24" class="padd">&nbsp;</td>
            <td style="font-size: 18px;">Name</td>
            <td style="font-size: 18px;text-align: right">Completed Tasks</td>
            <td width="24" class="padd">&nbsp;</td>
        </tr>
        @if(isset($users))
            @foreach($users as $user)
                @if($user['tasks'])
                    <tr>
                        <td width="24" class="padd">&nbsp;</td>
                        <td style="font-size: 18px;">
                            <a href="{{ route('calendar.week') }}?user={{$user['id']}}&department={{$user['department']}}&day={{$date}}">
                                {{ $user['name'] }}
                            </a>
                        </td>
                        <td style="font-size: 18px;text-align: right"> {{ $user['completed'] }} / {{ $user['tasks'] }}</td>
                        <td width="24" class="padd">&nbsp;</td>
                    </tr>
                @endif
            @endforeach
        @else
            <tr>
                <td width="24" class="padd">&nbsp;</td>
                <td style="font-size: 18px;">
                    <a href="#">
                        Gene Ellorin
                    </a>
                </td>
                <td style="font-size: 18px;text-align: right"> 3 / 4</td>
                <td width="24" class="padd">&nbsp;</td>
            </tr>
        @endif
        <tr><td>&nbsp;</td></tr>
    </table>
@endsection

