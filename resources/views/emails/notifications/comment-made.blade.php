@extends('emails.notifications.master')

@section('email_body')
    <br>
    <br>
    @if(isset($user))
        <p style="font-size: 18px">
            Hello. {{ $user->name }} wrote a comment <br>"{{ $comment }}".
        </p>
        <p style="font-size: 18px">
             <a href="{{ route('calendar.week') }}?user={{$user->id}}&department={{$department->id}}&day={{$date}}">Click to view the week.</a>
        </p>
    @else
        <p style="font-size: 18px">
            Hello. XxxxXxXxX wrote a comment <br>"XXXXxxX xxxxXx XXX X XX XXXXXxxxx Xx xXXXX".
        </p>
        <br>
        <p style="font-size: 18px">
            <a href="#">Click to view the week.</a>
        </p>
    @endif
@endsection

