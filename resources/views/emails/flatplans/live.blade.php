@extends('emails.main-new')

@section('email_body')
    <p>Flatplan {{ $flatplan['name'] }} is now live!</p>

    <p>Please <a href="{{ route('flatplans.preview',[$flatplan['slug'],$version]) }}">click here</a> to view it.</p>
@endsection

