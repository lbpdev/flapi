@extends('layout.login')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 col-md-offset-3 col-xs-offset-0" id="loginPage">
                <div class="panel panel-default"  id="login-panel">
                    <div class="panel-heading text-center padding-b-10">
                        <img src="{{ asset('public/img/Tapi-Logo-Dark.png') }}" width="190" alt="Flapi-Logo" title="Flapi Logo">
                    </div>
                    <div class="panel-body padding-t-0">
                        {{--@if (count($errors) > 0)--}}
                        {{--<div class="alert alert-danger">--}}
                        {{--<strong>Whoops!</strong> There were some problems with your input.<br><br>--}}
                        {{--<ul>--}}
                        {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        @if(!isset($user))
                            <form class="form-horizontal" role="form" method="POST" id="loginForm" action="{{ route('mobile.login') }}">
                                {!! csrf_field() !!}

                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <div class="alert-danger alert temp-hide" id="login-error-message">

                                        </div>
                                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Username">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <div class="col-md-12">
                                        <input type="password" class="form-control" name="password" placeholder="Password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group margin-b-0">
                                    <div class="col-md-12">
                                        <div class="col-md-5 padding-0 text-left">
                                            <a href="#">Stay logged in</a>  <input type="checkbox" name="remember" id="remember">
                                        </div>
                                        <div class="col-md-4 padding-0 text-right">
                                        </div>
                                        <div class="col-md-3 padding-0 text-right">
                                            <div class="btn btn-link padding-0">
                                                <button type="submit" id="loginBt" class="btn btn-primary">
                                                    Login
                                                </button>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </form>
                        @endif

                        <div class="form-group margin-b-0">
                            <div class="col-md-12">
                                <div class="row">
                                    @if(!isset($user))
                                        <div class="col-md-12 padding-0">
                                            <h4  class="padding-l-0 text-center margin-t-0" href="{{ url('/password/reset') }}">

                                                <a class="btn btn-link padding-l-0 padding-r-10 pull-left" href="#" id="termsBt">Terms of Use</a>
                                                <a class="{{ !isset($user) ? 'pull-right' : '' }} btn btn-link padding-l-0 padding-r-0" href="#" data-toggle="modal" data-target="#forgotEmail">Forgot Password</a>

                                                @if (isset($user))
                                                    <span class="link-divider"></span>
                                                    <a class="btn btn-link padding-l-0 " id="drop-bt" href="#">Register</a>
                                                @endif

                                            </h4>
                                        </div>
                                    @endif

                                    <div class="col-md-12 padding-t-40">
                                        <div class="row">
                                            <form class="form-horizontal" role="form" method="POST" id="register-form" action="{{ route('users.register') }}" {{ (isset($user) || Session::has('error')) ? 'style=height:500px !important' : '' }} >
                                                {!! csrf_field() !!}


                                                @if(Session::has('error'))
                                                    <div class="alert alert-danger margin-t-5">
                                                        {{ Session::get('error') }}
                                                    </div>
                                                @endif

                                                @if (isset($user))
                                                    <input type="hidden" value="{{ $user->id }}" name="user_id">
                                                @endif

                                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} {{ isset($user) ? 'temp-hide' : '' }}">

                                                    <div class="col-md-12">
                                                        <input type="hidden" class="form-control" name="username" required value="{{ isset($user) ? $user->email : old('name') }}" placeholder="Username">

                                                        @if (isset($error))
                                                            <span class="help-block">
                                                        <strong>{{ $error }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <p>Welcome {{ isset($user) ? $user->name : '' }},</p>
                                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} {{ isset($user) ? 'temp-hide' : '' }}">

                                                    <div class="col-md-12">
                                                        <input type="text" value="{{ isset($user) ? $user->name : '' }}" required class="form-control has-space" name="name" placeholder="Full Name">
                                                        @if ($errors->has('name'))
                                                            <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                                    <div class="col-md-12">
                                                        <input type="password" class="form-control" name="password" required placeholder="Create Password">

                                                        @if ($errors->has('password'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} {{ isset($user) ? 'temp-hide' : '' }}">

                                                    <div class="col-md-12">
                                                        <input type="text" value="{{ isset($user) ? $user->email : '' }}" required class="form-control" name="email"  placeholder="Email">

                                                        @if (isset($error['email']))
                                                            <span class="help-block">
                                                        <strong>{{ $error['email'] }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <div class="alert-danger alert temp-hide" id="register-error-message"></div>
                                                        <div class="col-md-7 padding-0 text-left terms">
                                                            By clicking on “Create Account” I accept the Terms of Service.<br>
                                                            Your privacy is protected.
                                                        </div>
                                                        <div class="col-md-5 padding-0 text-right">
                                                            <div class="btn btn-link padding-0">
                                                                <button type="submit" class="btn t-black btn-primary">
                                                                    Create Account
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 margin-t-10">
                                <div class="row text-center">
                                    @if (isset($user))
                                        <a class="btn btn-link padding-l-0 padding-r-10" href="#" id="termsBt">Terms of Use</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="bottom-logos">
                <img src="{{ asset('public/img/flapi-logo-white.png') }}" height="30">
                <span class="divider"></span>
                <img src="{{ asset('public/img/Tapi-Logo-White.png') }}" height="30">
                <span class="divider"></span>
                <img src="{{ asset('public/img/lb-logo-white.png') }}" height="30">
            </div>
        </div>
    </div>

    @include('terms')
    @include('login.modals.forgot-email')
    @include('login.modals.forgot-password')

@endsection

@section('js')

    @yield('forgot-password-js')
    @yield('forgot-email-js')

    <script>

        $('#termsBt').on('click', function(){
            $('#termsModal').modal('show');
        });

        var animFlag = 1;

        $('input').on({
            keydown: function(e) {
                if (e.which === 32 && !$(e.target).hasClass('has-space'))
                    return false;
                if (e.which === 13)
                    $(this).closest('form').trigger('submit');
            },
            change: function() {
                if(!$(this).hasClass('has-space'))
                    this.value = this.value.replace(/\s/g, "");
            }
        });

        $('#drop-bt').on('click',function(){
            if(animFlag){
                animFlag = 0;
                if($(this).find('i.active').length){
                    $(this).find('i').attr('class','drop-arrow inactive');
                    $('#register-form').animate({height:0}, 500 , function(){ animFlag = 1; });
                }
                else{
                    var height;

                    height = $('#register-form .form-group').eq(0).height();
                    fields = $('#register-form .form-group').length;
                    height = height*fields + 50;

                    $(this).find('i').attr('class','drop-arrow active');
                    $('#register-form').animate({height:height}, 500 , function(){ animFlag = 1; });
                }
            }
        });



        /** SEND CREATE REQUEST **/

        $('#remember').on('click',function(e){
            if(e.keyCode==13)
                $('#loginForm').trigger('submit');

            $('#loginForm input[type=password]').trigger('focus');
        });


        processFlag = 1;

        $('#loginForm').on('submit',function(e){
            var form = $(this);

            submitBt = $('#loginBt');
            $(submitBt).attr('disabled','disabled');

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {

                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200 && response.fromReset){
                            $('#changePassword').modal('show');
                            $('#changePassword #temp_password').val($('#loginForm input[type=password]').val());
                            $(submitBt).removeAttr('disabled');
                        }
                        else if(response.status==200)
                            window.location.reload();
                        else if(response.status==400){
                            $('#login-error-message').html(response.message).show();
                            $(submitBt).removeAttr('disabled');
                        } else {
                            $(submitBt).removeAttr('disabled');
                        }

                        processFlag = 1;
                    },
                    complete : function (){
                        $(submitBt).removeAttr('disabled');
                    }
                });
            }
        });


        $('#register-form').on('submit',function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {


                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){
                        if(response.status==200){
                            window.location.reload();
                        }
                        else
                            $('#register-error-message').html(response.message).show();
                        processFlag = 1;
                    },
                    done : function (){
                    }
                });
            }
        });

        $(document).ready(function(){
            centerAlignLoginPanel()
        });

        $(window).resize(function(){
            setTimeout(function(){
                centerAlignLoginPanel();
            },500);
        });

        function centerAlignLoginPanel(){
            $('#loginPage').height($(window).height());
            marginTop = $('#login-panel').outerHeight() / 2;
            marginLeft = $('#login-panel').outerWidth() / 2;

            $('#login-panel').css({
                'top' : '50%',
                'left' : '50%',
                'margin-top' : -marginTop,
                'margin-left' : -marginLeft,
                'position' : 'absolute'
            });
        }
    </script>
@endsection
