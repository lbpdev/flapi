@extends('layouts.app')

@section('page-title','Flat Plans')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}" />
    <style>
        #items-panel {
            width: 100%;
            float: left;
        }
        #sidebar {
            width: 25%;
            float: right;
            padding: 0 15px;
            margin-right: -100%;
        }
        #main {
            overflow: hidden;
        }

        #flatplanForm, #flatplanForm input,#flatplanForm label {
            color: #999;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile + label {
            font-size: 1.25em;
            font-weight: 700;
            color: white;
            background-color: black;
            display: inline-block;
        }

        .inputfile:focus + label,
        .inputfile + label:hover {

        }

        .inputfile + label {
            cursor: pointer; /* "hand" cursor */
            float: left;
            width: 100%;
            background-color: #fff;
            color: #bf1c73 !important;
            font-size: 10px;
            font-weight: 300;
        }
        .inputfile:focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }
        .inputfile + label * {
            pointer-events: none;
        }
        .js .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .no-js .inputfile + label {
            display: none;
        }
        .inputfile:focus + label,
        .inputfile.has-focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        #sidebar {
            padding: 0 30px;
        }

        .actions {
            margin-top: 5px;
            line-height: 16px;
            position: absolute;
            background-color: rgb(104, 101, 102);
            padding: 5px;
            bottom: 0;
            display: none;
            z-index:10;
            width: 100%;
        }

        .flatplan-item span.options {
            position: absolute;
            bottom: 0px;
            right: 9px;
            padding: 0;
            top: auto;
            color: #c03579;
            cursor: pointer;
            font-size: 20px;
            display: none;
            z-index: 5;
        }

        .flatplan-item:hover span.options  {
            display: block !important;
        }

        .flatplan-item span.options:hover  {
            font-size: 24px !important;
        }

        .flatplan-item .actions a {
            color: #dadada;
            font-size: 12px;
            float: right;
            text-align: right;
            width: 100%;
            line-height: 15px;
        }

        .flatplan-item .actions a:hover {
            color: #fff;
        }

        .flatplan-item.inactive {
            opacity: 0.5;
        }

    </style>
@endsection


@section('content')

    <div class="container relative">
        <div class="col-md-12 clearfix" id="main">
            <div class="row">
                <div class="clearfix" id="items-panel">

                    <div class="col-md-12 margin-t-10">
                        <div class="row">
                            Maximum active flat plans {{ count(Auth::user()->company->activeFlatPlans) }}/{{Auth::user()->company->maxFlatplans}}
                        </div>
                    </div>

                    @if(!isset($archivePage))
                        @if(Auth::user()->role->HasPermissions('modify-flat-plan')->count())
                            @if(!Auth::user()->company->maxFlatplansReached)
                                <div class="flatplan-item text-center add">
                                    <div class="item text-center add">
                                        <span class="plus-icon"></span>
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endif

                    @foreach($flatPlans as $flatplan)
                        @if($flatplan->isCurrentUserParticipant || Auth::user()->isAdmin)
                            @if($flatplan->currentVersion)
                                <a href="{{ route(( $flatplan->archived_at ? 'flatplans.show.archive' : 'flatplans.show'),[$flatplan->slug,$flatplan->currentVersion->version]) }}">
                            @endif
                        @endif
                            <div class="{{ !$flatplan->isCurrentUserParticipant ? 'inactive' : '' }} flatplan-item text-center" data-id="{{ $flatplan->id }}">
                                {!! $flatplan->issue && $flatplan->thumbnail ? '<p class="title">'.$flatplan->name.'</p>' : '' !!}

                                <div class="text-center item" data-id="{{ $flatplan->id }}" style="background-image:url({{$flatplan->thumbnail}})">
                                    <span>{{ $flatplan->thumbnail ? '' : $flatplan->name }}</span>
                                    {!! !$flatplan->thumbnail ? '<span class="issue">Issue:'. $flatplan->issue.'</span>' : '' !!}
                                </div>

                                <div class="actions">
                                    @if(!isset($archivePage))
                                        <a href="#" data-id="{{ $flatplan->id }}" class="editFP">Edit</a>
                                    @endif
                                        <a href="#" data-id="{{ $flatplan->id }}" class="deleteFP">Delete</a>

                                    @if(!Auth::user()->company->maxFlatplansReached && Auth::user()->isAdmin)
                                        <a href="{{ route('flatplans.duplicate',$flatplan->id) }}">Duplicate</a>
                                    @endif

                                </div>

                                @if(($flatplan->isCurrentUserParticipant || Auth::user()->isAdmin ) && in_array('modify-flat-plan', Auth::user()->permissionsArray))
                                    <span class="glyphicon glyphicon-option-horizontal options" aria-hidden="true"></span>
                                @endif

                                {!! $flatplan->issue && $flatplan->thumbnail ? '<p class="issue">Issue:'. $flatplan->issue.'</p>' : '' !!}
                            </div>

                        @if($flatplan->isCurrentUserParticipant || Auth::user()->isAdmin)
                            @if($flatplan->currentVersion)
                                </a>
                            @endif
                        @endif
                    @endforeach
                </div>

                @if(!isset($archivePage))
                <div class="clearfix border-left-gray" id="sidebar">
                    <h3 class="heading">New Flat Plan</h3>
                    <div class="row">
                        {!! Form::open(['route'=>'flatplans.store','id'=>'flatplanForm','class'=>'clearfix','files'=>true]) !!}

                        {!! Form::hidden('flatplan_id',0,['id'=>'flatplan_id']) !!}

                        <div class="col-md-12 text-left">
                            Title
                            {!! Form::input('text','flatplan[name]',null,['class'=>' form-control','placeholder'=>'Flatplan Name','id'=>'flatplanTitle']) !!}
                        </div>
                        <div class="col-md-12  text-left">
                            Issue #
                            {!! Form::input('text','flatplan[issue]',null,['class'=>' form-control']) !!}
                        </div>
                        <div class="col-md-12  text-left">
                            Upload Cover Image<br>
                            <img src="" class="cover-img" style="display: none;" width="90">
                            {!! Form::input('file','cover','',['id'=>'file','class'=>'inputfile form-control','placeholder'=>'Upload Cover Image']) !!}
                            <label for="file">Choose a file</label>
                        </div>
                        <div class="col-md-12  text-left">
                            <hr class="margin-t-0">
                            <div id="templateForm">
                                Media
                                {!! Form::select('meta[media_type_id]',$media, 1 ,['class'=>' selectpicker','title'=>'']) !!}
                                Format
                                {!! Form::select('meta[format_type_id]',$formats, 1 ,['class'=>' selectpicker','title'=>'']) !!}

                                <?php
                                    $paginations = [
                                        '0' => 'No Cover (Self cover)',
                                        '4' => 'Cover (4 pages)',
                                        '6' => 'Cover (6 pages)',
                                        '8' => 'Cover (8 pages)',
                                    ]
                                ?>

                                Pagination
                                {!! Form::select('meta[pagination]',$paginations, 4 ,['id'=>'paginationSelect','class'=>' selectpicker','title'=>'']) !!}

                                <?php
                                $styles = [
                                    'traditional' => 'Traditional',
                                    'modern'      => 'Modern'
                                ]
                                ?>

                                Flat Plan Style
                                {!! Form::select('meta[style]',$styles, 4 ,['class'=>' selectpicker','title'=>'']) !!}

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 padding-l-0">
                                            <?php
                                                $pages = [];
                                                $maxPages = Auth::user()->company->maxPages;
                                                for($x=2;$x<=$maxPages;$x+=2)
                                                    $pages[$x] = $x;
                                            ?>

                                            Inside Pages
                                            {!! Form::select('meta[inside_pages]',$pages,20,['class'=>'selectpicker','placeholder'=>'1']) !!}
                                        </div>

                                        <div class="col-md-6 padding-l-0">
                                            <div id="pageStart">
                                                First Page
                                                {!! Form::select('meta[start_page]',[3=>3,1=>1], 3 ,['class'=>' selectpicker','title'=>'']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 padding-r-0 text-left">
                                        Size
                                        <div class="col-md-12 padding-0">
                                            <div class="col-md-7 padding-0">
                                                {!! Form::input('number','meta[size_x]', null,['class'=>'numberOnly form-control','placeholder'=>'210','maxlength'=>3,'min'=>1]) !!}
                                            </div>
                                            <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                width
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-l-0 text-left">
                                        &nbsp;
                                        <div class="col-md-12 padding-0">
                                            <div class="col-md-7 padding-0">
                                                {!! Form::input('number','meta[size_y]', null,['class'=>'numberOnly form-control','placeholder'=>'290','maxlength'=>3,'min'=>1]) !!}
                                            </div>
                                            <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                height
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-r-0 text-left">
                                        <div class="col-md-12 padding-0">
                                            <div class="col-md-7 padding-0">
                                                {!! Form::input('text','meta[units]',null,['class'=>' form-control','placeholder'=>'mm']) !!}
                                            </div>
                                            <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                unit
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            Deadline
                            {!! Form::input('text','flatplan[deadline]','',['id'=>'deadline','class'=>'datepicker form-control','placeholder'=>'DD/MM/YYYY']) !!}
                        </div>
                        {!! Form::close() !!}

                        <div class="col-md-12 text-center">
                            {!! Form::submit('Next',['class'=>'inline-block float-none link-text pink','id'=>'selectUsersBt']) !!}
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div id="deleteModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog vertical-align-center" style="width: 300px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Delete Flatplan</h4>
                </div>
                <div class="modal-body text-center">
                    Are you sure you want to delete this?
              {{--<span class="deleteMessage">--}}
                  {{--<span class="counts">You currently have XX projects and XX tasks connected to this service</span>.--}}
              {{--</span>--}}
                    <br>
                    Please type "yes" to confirm.<br>
                    {!! Form::open(['route'=>'api.flatplans.delete']) !!}
                    <input type="hidden" name="id" id="deleteFlatplan" value="0">
                    <input id="deleteConfirmInput" class="text-center">
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer  text-center-important">
                    <button id="deleteTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>

    @include('flatplans.modals.all-users')

@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>

    @yield('all-user-js')

    <script>

        $('.datepicker').datepicker();

        var animating = false;
        var storeUrl = '{{ route('api.flatplans.store') }}';
        var updateUrl = '{{ route('api.flatplans.update') }}';

        $('#paginationSelect').on('change',function(){
            if($(this).val()!=0){
                $('#pageStart').show();
                $('#pageStart select').val(3);
            }
            else {
                $('#pageStart').hide();
                $('#pageStart select').val(1);
            }
        });

        $('.flatplan-item span.options').on('click',function(){
            $(this).closest('div').find('.actions').show();
        });

        $('.flatplan-item .actions').on('mouseleave',function(){
            $(this).hide();
        });

        function attachEditClickEvent(){
            $('.editFP').on('click',function(e){

                if(!animating){

                    hideSidebar();
                    id = $(this).attr('data-id');

                    $.ajax({
                        type: "GET",
                        url: appUrl+'/api/fp/get?id='+id,
                        success: function(response){

                            if(response.status==200) {
                                $('#flatplan_id').val(response.data.flatplan.id);

                                showSidebar();

                                $.each(response.data.flatplan, function(i,e){
                                    $('#sidebar form input[name="flatplan['+i+']"]').val(e);
                                    $('#sidebar form select[name="flatplan['+i+']"]').val(e).trigger('change');
                                });

                                $.each(response.data.meta, function(i,e){
                                    $('#sidebar form input[name="meta['+i+']"]').val(e);
                                    $('#sidebar form select[name="meta['+i+']"]').val(e).trigger('change');
                                });

                                if(response.data.cover)
                                    $('#sidebar form .cover-img').attr('src',response.data.cover).show();
                                else
                                    $('#sidebar form .cover-img').hide();

                                $('#sidebar form').attr('action',updateUrl);

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });
                }
            });
        }

        function deleteContent(){

            url = "{{ route('api.flatplans.delete') }}";
            data   = $('#deleteModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){
                        doAlert(response);
                        $('#deleteModal').modal('hide');

                        $('div[data-id='+$('#deleteFlatplan').val()+']').remove();
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        /** SEND DELETE REQUEST **/


        $('#deleteTrigger').on('click', function(){
            if($('#deleteConfirmInput').val()=="yes"){
                $('#deleteModal form').submit();
            } else {
                alert('Please follow the instructions');
            }
        });

        function attachDeleteEvent(){

            $('.deleteFP').on('click',function(e){

                if(!animating){

                    id = $(this).attr('data-id');

                    if(id>0){

                        $.ajax({
                            type: "GET",
                            url: '{{ route('api.flatplans.get') }}?id='+id,
                            success: function(response){
                                if(response.status==200){

                                    $('#deleteModal').modal('show');
                                    $('#deleteFlatplan').val(id);

                                } else {
                                    doAlert(response);
                                }

                            },
                            done : function (){
                                deleteProcessFlag = 1;
                            }
                        });

                    }
                }
            });
        }

        $('.flatplan-item.add').on('click',function(){
            showSidebar();
            $('#flatplan_id').val('');
            $('#flatplanForm')[0].reset();
            $('.cover-img').attr('src','').hide();
            $('#templateSelect').trigger('change');

            $('#sidebar form').attr('action',storeUrl);
        });

        function showSidebar(){
            if(!animating){
                animating = true;

                $('#items-panel').animate({
                    'width' : '75%'
                },200);

                $('#sidebar').animate({
                    'marginRight' : 0
                },200);

                setTimeout(function(){
                    animating = false;
                },250);
            }
        }

        function hideSidebar(){
            if(!animating){
                animating = true;

                $('#sidebar').animate({
                    'marginRight' : '-100%'
                },1);

                $('#items-panel').animate({
                    'width' : '100%'
                },1);

                setTimeout(function(){
                    animating = false;
                },5);
            }
        }

        function updateTemplateList(data){
            $('#templateSelect').html('');

            $.each(data,function(index,value){
                $('#templateSelect').append('<option value="'+index+'">'+value+'</option>');
            });

            $('#templateSelect').append('<option value=0>Template Name</option>');

            $('#templateSelect').selectpicker('refresh');
        }

        function addFlatplan(data){
            item =  '<div class="flatplan-item text-center" data-id="'+data['id']+'">' +
                        '<a href="'+appUrl+'/flatplans/'+data['id']+'">'+
                            '<div class="text-center item" data-id="'+data['id']+'" style="background-image:url('+data['cover']+')">'+
                                '<span>'+data['name']+'</span>'+
                            '</div>'+
                        '</a>' +
                        '<div class="actions">' +
                            '<a href="#" data-id="'+data['id']+'" class="editFP">Edit</a>'+
                            '<span class="divider"></span>'+
                            '<a href="#" data-id="'+data['id']+'" class="deleteFP">Delete</a>'+
                        '</div>'+
                    '</div>';

            $('#items-panel').append(item);
            attachDeleteEvent();
            attachEditClickEvent();
        }

        attachEditClickEvent();
        attachDeleteEvent();


        var inputs = document.querySelectorAll( '.inputfile' );

        Array.prototype.forEach.call( inputs, function( input )
        {
            var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

            input.addEventListener( 'change', function( e )
            {
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    label.innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });

            input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
            input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
        });


        $(".numberOnly").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl/cmd+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+C
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+X
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });


        $(window).load(function(){
            $('.container .alert').delay(3000).fadeOut(3000);
        });

        @if(Session::has('success'))
            doAlert({status:200,message:"{{ Session::get('success') }}" })
        @endif

        @if(Session::has('error'))
            doAlert({status:500,message:"{{ Session::get('error') }}" })
        @endif

        $('#deleteConfirmInput').bind('keypress', function(e){
            if ( e.keyCode == 13 ) {
                e.preventDefault();
                $('#deleteTrigger').trigger('click');
            }
        });

    </script>

    @yield('all-users-js')
@endsection
