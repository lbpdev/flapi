<div class="col-md-12">
    <div class="row">
        <div id="users-list" class="clearfix">
            <label class="margin-b-0">Users</label>
            <hr class="pull-left width-full">
            <div id="editing-list">
                <span>Editing</span>
                <ul class="list editting clearfix" id="users"> </ul>
            </div>

            <div id="online-list" style="display: none;">
                <span>Online</span>
                <ul class="list online clearfix" id="users"> </ul>
            </div>

            <div id="offline-list">
                @if($flatplan->view)
                <span>Offline</span>
                <ul class="list offline clearfix" id="users">
                    @foreach($flatplan->participants as $id=>$row)
                        <li data-id="{{$row->user->id}}" data-nickname="{{$row->user->name}}" >{{$row->user->name}}</li>
                    @endforeach
                </ul>
                @endif
            </div>

            <hr class="pull-left width-full">
        </div>
    </div>
</div>