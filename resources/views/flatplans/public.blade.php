@extends('layouts.app')

@section('page-title','Flat Plans')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/css/flatplan-page.css') }}" />

    <style>
        #editLayoutModal label {
            font-size: 10px;
            margin-bottom: 0;
            font-weight: 300;
            line-height: 20px;
        }
        #editLayoutModal p {
            font-size: 15px;
            line-height: 18px;
            color: #575757;
        }
        #editLayoutModal .form-group {
            border-bottom: 1px solid #ccc;
        }

        #layout-pages .layout-page {
            margin-top: 10px;
        }

        #contentType {
            float: left;
        }

        #contentTypeColor {
            display: block;
            border-radius: 50%;
            width: 12px;
            height: 12px;
            margin-top: 3px;
            margin-left: 5px;
            float: left;
        }
    </style>
@endsection


@section('content')
    <div id="edit-block" style="display: none;">Someone is editting this flatplan.</div>
    <div class="col-md-12 clearfix" id="flatplanView">
        @if($flatplan->view->is_live)
        <div class="col-md-12 clearfix" id="main" style="background-color: #f2f2f2">
            <div class="row">
                <div class="container info-panel">
                    <div class="col-md-3">
                        <p><label>Project Name: </label> {{ $version->flatplan->name }}</p>
                        <p><label>Issue Number: </label> {{ $version->flatplan->issue }}</p>
                    </div>
                    @if($version->flatplan->meta)
                        <div class="col-md-2">
                            <p><label>Media: </label> {{ $version->flatplan->meta->media ? $version->flatplan->meta->media->name : 'N/A' }}</p>
                            <p><label>Format: </label> {{ $version->flatplan->meta->format ? $version->flatplan->meta->format->name : 'N/A' }}</p>
                        </div>
                        <div class="col-md-2">
                            <p><label>Size: </label> {{ $version->flatplan->meta->size_x }}{{ $version->flatplan->meta->units }} x {{ $version->flatplan->meta->size_y }}{{ $version->flatplan->meta->units }}</p>
                            <p><label>Deadline: </label> {{ $version->flatplan->deadline }}</p>
                        </div>
                        <div class="col-md-3">
                            <p><label>Version: </label> {{ $version->version }}</p>
                            <p><label>Print Approved: </label> 1</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-12 clearfix padding-t-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div id="contents-list" class="clearfix {{ count($contents) ? '' : 'temp-hide' }}">
                            <label class="margin-b-0">Contents</label>
                            <ul class="list" id="contents">
                                @foreach($contents as $content)
                                    <li data-id="{{ $content->id }}">
                                        <div class="color-dot padding-l-0">
                                            <span style="background-color: {{ $content->color }};"></span>
                                            <a href="#">{{ $content->name }}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <hr class="pull-left width-full">
                        </div>
                        <div id="users-list" class="clearfix ">
                            <label class="margin-b-0">Online Users</label>
                            <ul class="list" id="users">
                            </ul>
                        </div>

                        {{--<a href="{{ route('api.history.redo',$version->id) }}" class="icon-link blue undo-blue width-full">Redo</a>--}}
                        <hr class="pull-left width-full">
                        <a href="#" class="icon-link download-pink"></a>
                        <a href="#" class="icon-link pdf-pink"></a>
                        <a href="#" class="icon-link print-pink"></a>
                        <hr class="pull-left width-full">
                    </div>

                    <div class="col-md-10 traditional" id="flatplan-pages">
                        <div class="wrapper clearfix">
                        @if($style=='modern')
                            @include('flatplans._pages_modern')
                        @else
                            @include('flatplans._pages')
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
            <h5 class="text-center">This flat plan is not live at the moment</h5>
        @endif
    </div>

    @include('flatplans.public-section')

@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>
    <script src="{{ asset('public/js/socket.io.js') }}"></script>

    @if($flatplan->view->is_live)
        <script>

            var nickname = '{{ $currentUser->user->name }}';
            var userId = '{{ $currentUser->user->id }}';
            var room = '{{ $version->id }}';
            var type = 'guest';

            <?php if(env('APP_ENV')=='live') { ?>
                    var socket = io('http://flapiapp.com:3001');
            <?php } else { ?>
                    var socket = io('http://localhost:3001');
            <?php } ?>

            socket.on("test-channel:App\\Events\\EventName", function(message){
                // increase the power everytime we load test route
            });

            socket.on("updateUsers", function(message){

                updateUsersList(message);
            });

            socket.on('connect', function(){
                socket.emit("subscribe", { userId : userId , room : room , nickname : nickname , clientId : socket.id, type : type } );
            });

            function updateUsersList(users){
                $('#users').html('');
                Object.keys(users).forEach(function(index) {
                    if(users[index].room=='flapi-room-'+room)
                        $('#users').append('<li>'+users[index].nickname+'</li>');
                });
            }

            function getRoomUsers(room){
                socket.emit("getActiveUsersInRoom", { room : 'flapi-room-'+room } );
            }

            socket.on('getActiveUsersInRoom', function(users){
                $('#users').html('');

                Object.keys(users).forEach(function(index) {
                    if(users[index].room=='flapi-room-'+room)
                        $('#users').append('<li>'+users[index].nickname+'</li>');
                });
            });

            function getActiveUsers(){
                socket.emit("getActiveUsers", { room: room });
            }

            function attachEditClickEvent(){
                $('#flatplan-pages .edit').unbind('click');
                $('#flatplan-pages .edit').on('click',function(){
                    id = $(this).attr('data-id');
                    el = $(this).closest('.page');
                    parent = $(this).closest('.pageHolder');
                    eq = $( '#'+parent.attr('id')+' .page .action.edit' ).index( $(this) );

                    addLoader(el);

                    $.ajax({
                        type: "GET",
                        url: appUrl+'/api/pages/get?id='+id,
                        success: function(response){

                            if(response.status==200) {
                                merged = false;
                                isEmpty = false;
                                clickedPage = 0;

                                $('#layout-pages .layout-page.merged').remove();
                                $('#layout-pages .layout-page').first().addClass('main');
                                $('#layout-pages .layout-page').first().find('.section').remove();
                                $('#layout-pages .layout-page').first().attr('data-pageid',response.data.id);

                                for(x=0;x<response.data.merged.length;x++){
                                    content = '';

                                    page = response.data.merged[x];
                                    copy = $('#flatplan-pages .page[data-id="'+page.id+'"]').children().clone();

                                    if($('#flatplan-pages .page[data-id="'+page.id+'"]').find('.section').length<1){
                                        isEmpty = true;
                                        content = '<div class="section" style="width:100%;height:100%;background-color: #eaeaea"></div>';
                                    }

                                    $('#layout-pages').append('<div class="merged layout-page '+(isEmpty ? 'empty' : 'notEmpty')+'" data-pageid="'+page.id+'" data-id=""></div>');

                                    layouts = $('#layout-pages .merged.layout-page[data-pageid='+page.id+']');

                                    layouts.html('').append(copy);
                                    layouts.find('.loader').remove();
                                    layouts.find('.action').remove();

                                    if(isEmpty)
                                        layouts.append(content);
                                    else
                                        layouts.find('.section').each(function(){
                                            $(this).find('.title').text($(this).attr('data-order'));
                                        });

                                    merged = true;
                                }

                                if(response.data.layout_id){
                                    applyLayout(response.data,false);
                                    $('#layout-selection').hide();
                                    $('#selectLayoutBt').hide();

                                    showSectionEdit();

                                    $('#selectLayoutBt').css('display','none !important');

                                } else {
                                    applyLayout(null,false);

                                    $('#selectLayoutBt').show();
                                    $('#selectLayoutBt').css('display','inline-block !important');
                                    showLayoutSelect();
                                }

                                $('#editLayoutModal').modal('show');

                                addChangeLayoutClickEvent();
                                addSectionClickEvent();

                                if(merged){
                                    $('#layout-pages .layout-page').first().hide();
                                    $('#layout-pages .layout-page').eq(eq+1).find('.section').first().trigger('click');
                                }
                                else {
                                    $('#layout-pages .layout-page').first().show();
                                    $('#layout-pages .layout-page .section').first().trigger('click');
                                }
                            }
                        },
                        complete : function (){
                            deleteProcessFlag = 1;
                            removeLoader(el);
                        }
                    });
                });
            }
            attachEditClickEvent();


            function applyLayout(page,global){
                parent = $('.layout-page.main');
                parent.html('');

                if(page){
                    if(global)
                        $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+'] .section').remove();

                    for(x=0;x<page.layout.sections.length;x++){
                        section = page.layout.sections[x];
                        sectionData = section.length ? section : [];

                        id = section['id'];
                        title = sectionData.length > 0 ? sectionData.data[0]['value'] : '';
                        left = section['left'] >=0 ? section['left'] : '';
                        right = section['right'] >=0 ? section['right'] : '';
                        topV = section['top'] >=0 ? section['top'] : '';
                        bottom = section['bottom'] >=0 ? section['bottom'] : '';
                        color = page.sections[x].content ? page.sections[x].content.color : '';

                        style = 'width:'+section['width']+'%;';
                        style += 'z-index:'+section['z_index']+';';
                        style += 'height:'+section['height']+'%;';
                        style += 'line-height:'+(( section['height'] / 100 ) * 80 )+'px;';
                        style += 'left:'+left +'%;';
                        style += 'top:'+topV+'%;';
                        style += 'right:'+right+'%;';
                        style += 'bottom:'+bottom+'%;';
                        style += 'background-color:'+color+';';
                        style += 'border-color:'+color;

                        if(global){
                            itemForPages = '<div data-order="'+section['order']+'" data-id="'+page.sections[x].id+'" data-sectionid="'+page.sections[x].id+'" data-contentid="'+page.layout.sections[x].content_id+'" class="section" style="'+style+'">' +
                                    '<span class="title">'+title+'</span>'+
                                    '</div>';

                            $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+']').removeClass('empty').prepend(itemForPages);
                        }

                        if(page.sections.length){
                            itemForLayout = '<div data-order="'+section['order']+'" data-id="'+page.layout.sections[x].id+'" data-sectionid="'+page.sections[x].id+'" data-contentid="'+page.layout.sections[x].content_id+'" class="section" style="'+style+'">' +
                                    '<span class="title">'+(x+1)+'</span>'+
                                    '</div>';

                            $('.layout-page.main').append(itemForLayout);
                        }
                    }

                } else {
                    item = '<div class="section" style="width:100%;height:100%;background-color: #eaeaea"></div>';
                    $('#layout-pages .layout-page.main').append(item);
                    $('#selectLayoutBt').hide();
//                $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+']').prepend(item).removeClass('empty');
                }

                addSectionClickEvent();
            }

            function updateContentList(contents){

                $('#contents').html('');

                if(contents.length>0){
                    $.each(contents, function(i,e){
                        el = '<li data-id="'+e.id+'">'+
                                '<div class="color-dot padding-l-0">' +
                                '<span style="background-color: '+e.color+';"></span>'+
                                '<a href="#">'+e.name+'</a>'+
                                '</div>'+
                                '</li>';

                        $('#contents').append(el);
                    });
                } else {
                    $('#contents-list').hide();
                }
            }
        </script>
    @endif

    @yield('public-section-js')
@endsection
