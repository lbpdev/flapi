@extends('layouts.app')

@section('page-title','Flat Plans')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}" />
    <style>
        #items-panel {
            width: 100%;
            float: left;
        }
        #sidebar {
            width: 25%;
            float: right;
            padding: 0 15px;
            margin-right: -100%;
        }
        #main {
            overflow: hidden;
        }

        #flatplanForm, #flatplanForm input,#flatplanForm label {
            color: #999;
        }

        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .inputfile + label {
            font-size: 1.25em;
            font-weight: 700;
            color: white;
            background-color: black;
            display: inline-block;
        }

        .inputfile:focus + label,
        .inputfile + label:hover {

        }

        .inputfile + label {
            cursor: pointer; /* "hand" cursor */
            float: left;
            width: 100%;
            background-color: #fff;
            color: #bf1c73 !important;
            font-size: 10px;
            font-weight: 300;
        }
        .inputfile:focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }
        .inputfile + label * {
            pointer-events: none;
        }
        .js .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        .no-js .inputfile + label {
            display: none;
        }
        .inputfile:focus + label,
        .inputfile.has-focus + label {
            outline: 1px dotted #000;
            outline: -webkit-focus-ring-color auto 5px;
        }

        #sidebar {
            padding: 0 30px;
        }

        .actions {
            margin-top: 5px;
            line-height: 16px;
            position: absolute;
            background-color: rgb(104, 101, 102);
            padding: 5px;
            bottom: 0;
            display: none;
            z-index:10;
            width: 100%;
        }

        .flatplan-item span.options {
            position: absolute;
            bottom: 0px;
            right: 9px;
            padding: 0;
            top: auto;
            color: #c03579;
            cursor: pointer;
            font-size: 20px;
            display: none;
            z-index: 5;
        }

        .flatplan-item:hover span.options  {
            display: block !important;
        }

        .flatplan-item span.options:hover  {
            font-size: 24px !important;
        }

        .flatplan-item .actions a {
            color: #dadada;
            font-size: 12px;
            float: right;
            text-align: right;
            width: 100%;
            line-height: 15px;
        }

        .flatplan-item .actions a:hover {
            color: #fff;
        }

        .flatplan-item.inactive {
            opacity: 0.5;
        }

    </style>
@endsection


@section('content')

    <div class="container relative">
        <div class="col-md-12 clearfix" id="main">
            <div class="row">
                <div class="clearfix" id="items-panel">

                    @foreach($flatPlans as $flatplan)
                        @if($flatplan->isCurrentUserParticipant)
                            @if($flatplan->view->is_live)
                                <a href="{{ route('flatplans.show',[$flatplan->slug,$flatplan->currentVersion->version]) }}">
                            @endif
                                <div class="flatplan-item text-center {{ !$flatplan->view->is_live ? 'inactive' : '' }}" data-id="{{ $flatplan->id }}">
                                    {!! $flatplan->issue && $flatplan->thumbnail ? '<p class="title">'.$flatplan->name.'</p>' : '' !!}

                                    <div class="text-center item" data-id="{{ $flatplan->id }}" style="background-image:url({{$flatplan->thumbnail}})">
                                        <span>{{ $flatplan->thumbnail ? '' : $flatplan->name }}</span>
                                        {!! !$flatplan->thumbnail ? '<span class="issue">Issue:'. $flatplan->issue.'</span>' : '' !!}
                                    </div>

                                    <div class="actions">
                                        @if(!isset($archivePage))
                                            <a href="#" data-id="{{ $flatplan->id }}" class="editFP">Edit</a>
                                        @endif
                                        <a href="#" data-id="{{ $flatplan->id }}" class="deleteFP">Delete</a>
                                        @if(!Auth::user()->company->maxFlatplansReached || Auth::user()->isAdmin)
                                            <a href="{{ route('flatplans.duplicate',$flatplan->id) }}">Duplicate</a>
                                        @endif
                                    </div>
                                    {!! $flatplan->issue && $flatplan->thumbnail ? '<p class="issue">Issue:'. $flatplan->issue.'</p>' : '' !!}

                                    @if(!$flatplan->view->is_live)
                                        <p style="color:red;font-size: 12px">Offline</p>
                                    @endif
                                </div>
                            @if($flatplan->view->is_live)
                                </a>
                            @endif
                        @endif
                    @endforeach
                </div>

                @if(!isset($archivePage))
                    <div class="clearfix border-left-gray" id="sidebar">
                        <h3 class="heading">New Flat Plan</h3>
                        <div class="row">
                            {!! Form::open(['route'=>'flatplans.store','id'=>'flatplanForm','class'=>'clearfix','files'=>true]) !!}

                            {!! Form::hidden('flatplan_id',0,['id'=>'flatplan_id']) !!}

                            <div class="col-md-12 text-left">
                                Title
                                {!! Form::input('text','flatplan[name]',null,['class'=>' form-control','placeholder'=>'Flatplan Name','id'=>'flatplanTitle']) !!}
                            </div>
                            <div class="col-md-12  text-left">
                                Issue #
                                {!! Form::input('text','flatplan[issue]',null,['class'=>' form-control']) !!}
                            </div>
                            <div class="col-md-12  text-left">
                                Upload Cover Image<br>
                                <img src="" class="cover-img" style="display: none;" width="90">
                                {!! Form::input('file','cover','',['id'=>'file','class'=>'inputfile form-control','placeholder'=>'Upload Cover Image']) !!}
                                <label for="file">Choose a file</label>
                            </div>
                            <div class="col-md-12  text-left">
                                <hr class="margin-t-0">
                                <div id="templateForm">
                                    Media
                                    {!! Form::select('meta[media_type_id]',$media, 1 ,['class'=>' selectpicker','title'=>'']) !!}
                                    Format
                                    {!! Form::select('meta[format_type_id]',$formats, 1 ,['class'=>' selectpicker','title'=>'']) !!}

                                    <?php
                                    $paginations = [
                                            '0' => 'No Cover (Self cover)',
                                            '4' => 'Cover (4 pages)',
                                            '6' => 'Cover (6 pages)',
                                            '8' => 'Cover (8 pages)',
                                    ]
                                    ?>

                                    Pagination
                                    {!! Form::select('meta[pagination]',$paginations, 4 ,['id'=>'paginationSelect','class'=>' selectpicker','title'=>'']) !!}

                                    <?php
                                    $styles = [
                                            'traditional' => 'Traditional',
                                            'modern'      => 'Modern'
                                    ]
                                    ?>

                                    Flat Plan Style
                                    {!! Form::select('meta[style]',$styles, 4 ,['class'=>' selectpicker','title'=>'']) !!}

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 padding-l-0">
                                                <?php
                                                $pages = [];
                                                $maxPages = Auth::user()->company->maxPages;
                                                for($x=2;$x<=$maxPages;$x+=2)
                                                    $pages[$x] = $x;
                                                ?>

                                                Inside Pages
                                                {!! Form::select('meta[inside_pages]',$pages,20,['class'=>'selectpicker','placeholder'=>'1']) !!}
                                            </div>

                                            <div class="col-md-6 padding-l-0">
                                                <div id="pageStart">
                                                    First Page
                                                    {!! Form::select('meta[start_page]',[3=>3,1=>1], 3 ,['class'=>' selectpicker','title'=>'']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 padding-r-0 text-left">
                                            Size
                                            <div class="col-md-12 padding-0">
                                                <div class="col-md-7 padding-0">
                                                    {!! Form::input('number','meta[size_x]', null,['class'=>'numberOnly form-control','placeholder'=>'210','maxlength'=>3,'min'=>1]) !!}
                                                </div>
                                                <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                    width
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-l-0 text-left">
                                            &nbsp;
                                            <div class="col-md-12 padding-0">
                                                <div class="col-md-7 padding-0">
                                                    {!! Form::input('number','meta[size_y]', null,['class'=>'numberOnly form-control','placeholder'=>'290','maxlength'=>3,'min'=>1]) !!}
                                                </div>
                                                <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                    height
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 padding-r-0 text-left">
                                            <div class="col-md-12 padding-0">
                                                <div class="col-md-7 padding-0">
                                                    {!! Form::input('text','meta[units]',null,['class'=>' form-control','placeholder'=>'mm']) !!}
                                                </div>
                                                <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                    unit
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                Deadline
                                {!! Form::input('text','flatplan[deadline]','',['id'=>'deadline','class'=>'datepicker form-control','placeholder'=>'DD/MM/YYYY']) !!}
                            </div>
                            {!! Form::close() !!}

                            <div class="col-md-12 text-center">
                                {!! Form::submit('Next',['class'=>'inline-block float-none link-text pink','id'=>'selectUsersBt']) !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div id="deleteModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog vertical-align-center" style="width: 300px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Delete Flatplan</h4>
                </div>
                <div class="modal-body text-center">
                    Are you sure you want to delete this?
                    {{--<span class="deleteMessage">--}}
                    {{--<span class="counts">You currently have XX projects and XX tasks connected to this service</span>.--}}
                    {{--</span>--}}
                    <br>
                    Please type "yes" to confirm.<br>
                    {!! Form::open(['route'=>'api.flatplans.delete']) !!}
                    <input type="hidden" name="id" id="deleteFlatplan" value="0">
                    <input id="deleteConfirmInput" class="text-center">
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer  text-center-important">
                    <button id="deleteTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>
@endsection
