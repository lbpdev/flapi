@extends('layouts.app-empty')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/css/flatplan-page.css') }}" />
    <style>
        .container {
            width: 100% !important;
        }

        #flatplanView .action {
            display: none !important;
        }

        img {
            position: relative;
            z-index: 99999;
        }
        @media print {
            body {-webkit-print-color-adjust: exact;}

            #flatplan-pages .page .section {
                z-index: 99999 !important;
            }
        }
    </style>
@endsection

@section('content')

    <div class="col-md-12 clearfix" id="flatplanView">
        <div class="row">
            <div class="col-md-12 clearfix" id="main" style="background-color: #f2f2f2">
                <div class="row">
                    <div class="container info-panel">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <p><label>Project Name: </label> {{ $version->flatplan->name }}</p>
                            <p><label>Issue Number: </label> {{ $version->flatplan->issue }}</p>
                        </div>
                        @if($version->flatplan->meta)
                            <div class="col-md-2 col-sm-3 col-xs-3">
                                <p><label>Media: </label> {{ $version->flatplan->meta->media ? $version->flatplan->meta->media->name : 'N/A' }}</p>
                                <p><label>Format: </label> {{ $version->flatplan->meta->format ? $version->flatplan->meta->format->name : 'N/A' }}</p>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-3">
                                <p><label>Size: </label> {{ $version->flatplan->meta->size_x }}{{ $version->flatplan->meta->units }} x {{ $version->flatplan->meta->size_y }}{{ $version->flatplan->meta->units }}</p>
                                <p><label>Deadline: </label> {{ $version->flatplan->deadline }}</p>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <p><label>Version: </label> {{ $version->version }}</p>
                                <p><label>Print Approved: </label> 1</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-12 clearfix padding-t-12">
                <div class="row">
                    <div class="container">
                        <div id="flatplan-pages">
                            @include('flatplans.show._pages')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection