<div class="col-md-12">
    <div class="row">
        <div class="pageHolder covers" id="coverPages">
            <?php
                $coverDivision = count($version->covers) / 2;
            ?>
            @foreach($version->covers as $index=>$page)
                @if($index<$coverDivision)
                    <div class="{{ $page->completed ? 'complete' : '' }} {{ $page->approved ? 'approved' : '' }} page ui-state-default {{ count($page->sections) ? ($page->layout ? '' : 'empty' ) : 'empty' }}" data-id="{{$page->id}}">
                        @include('flatplans.show.__page')
                        <span class="page-number">
                                <?php
                            switch($index){
                                case 0:
                                    echo 'FC'; break;
                                case 1:
                                    echo 'IFC'; break;
                                case ( count($version->covers) -2 ):
                                    echo 'IBC'; break;
                                case ( count($version->covers) -1 ):
                                    echo 'BC'; break;
                                default:
                                    echo 'Gatefold'; break;
                            }
                            ?>
                            </span>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="pageHolder" id="innerPages">

            <?php
                $index = $version->flatplan->meta ? $version->flatplan->meta->start_page : 3;
            ?>

            @foreach($version->innerPages as $page)

                <div class="{{ isset($is_public) ? 'edit' : '' }} {{ $page->completed ? 'complete' : '' }} {{ $page->approved ? 'approved' : '' }} page ui-state-default {{ count($page->sections) ? ($page->layout ? '' : 'empty' ) : 'empty' }}" data-id="{{$page->id}}">
                    @include('flatplans.show.__page')
                    <span class="page-number">{{ $index }}</span>
                </div>

                <?php
                    $index++;
                ?>
            @endforeach
        </div>

        <div class="pageHolder covers" id="coverPagesBack">
            @foreach($version->covers as $index=>$page)
                @if($index>=$coverDivision)
                    <div class="{{ isset($is_public) ? 'edit' : '' }} {{ $page->completed ? 'complete' : '' }} {{ $page->approved ? 'approved' : '' }} page ui-state-default {{ count($page->sections) ? ($page->layout ? '' : 'empty' ) : 'empty' }}" data-id="{{$page->id}}">
                        @include('flatplans.show.__page')
                        <span class="page-number">
                            <?php
                                switch($index){
                                    case 0:
                                        echo 'FC'; break;
                                    case 1:
                                        echo 'IFC'; break;
                                    case ( count($version->covers) -2 ):
                                        echo 'IBC'; break;
                                    case ( count($version->covers) -1 ):
                                        echo 'BC'; break;
                                    default:
                                        echo 'Gatefold';
                                }
                            ?>
                        </span>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
