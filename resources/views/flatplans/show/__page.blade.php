@if($page->layout)
    @foreach($page->sections as $section)
        @if($section)
            <?php
            $layout = 'z-index:'.$section->layout->z_index.';';
            $layout .= 'width:'.$section->layout->width.'%;';
            $layout .= ' height:'. $section->layout->height.'%;';
            $layout .= ' line-height:'. (( $section->layout->height / 100 ) * 80 ).'px;';
            $layout .= ' right:'. ( $section->layout->right>=0 ? $section->layout->right.'%;' : ';');
            $layout .= ' left:'.($section->layout->left>=0 ? $section->layout->left.'%;' : ';');
            $layout .= ' top:'.($section->layout->top>=0 ? $section->layout->top.'%;' : ';');
            $layout .= ' bottom:'.($section->layout->bottom>=0 ? $section->layout->bottom.'%;' : ';');
            $layout .= ' background-color:'. ($section->content ? $section->content['color'].';' : ';');
            ?>
            <div class="section" data-order="{{ $section->layout->order }}" data-id="{{$section->id}}" data-sectionid="{{ $section->id }}"  data-contentid="{{ $section->content ? $section->content->id : '' }}" style=" {{$layout}}; ">
                <span class="title">{{ count($section->data) ? $section->title : '' }}</span>
            </div>
        @endif
    @endforeach
@endif
@if(!isset($is_public))
    <span data-id="{{$page->id}}" class="action delete"></span>
    <span data-id="{{$page->id}}" class="action move"></span>
    <span data-id="{{$page->id}}" class="action merge"></span>
    <span data-id="{{$page->id}}" class="action edit"></span>
@endif