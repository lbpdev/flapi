

    <div class="col-md-12">
        <div class="row">
            <div class="covers pageHolder" id="coverPages">
                @foreach($version->covers as $index=>$page)
                    <div class="{{ $page->completed ? 'complete' : '' }} {{ $page->approved ? 'approved' : '' }} page ui-state-default {{ count($page->sections) ? ($page->layout ? '' : 'empty' ) : 'empty' }}" data-id="{{$page->id}}">
                        @include('flatplans.show.__page')
                            <span class="page-number">
                                <?php
                                switch($index){
                                    case 0:
                                        echo 'FC'; break;
                                    case 1:
                                        echo 'IFC'; break;
                                    case ( count($version->covers) -2 ):
                                        echo 'IBC'; break;
                                    case ( count($version->covers) -1 ):
                                        echo 'BC'; break;
                                    default:
                                        echo 'Gatefold'; break;
                                }
                                ?>
                            </span>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="pageHolder" id="innerPages">
                <?php
                    $index = $version->flatplan->meta ? $version->flatplan->meta->start_page : 3;
                ?>
                @foreach($version->innerPages as $page)
                    <div class="{{ $page->completed ? 'complete' : '' }} {{ isset($is_public) ? 'edit' : '' }} {{ $page->approved ? 'approved' : '' }} page ui-state-default {{ count($page->sections) ? ($page->layout ? '' : 'empty' ) : 'empty' }}" data-id="{{$page->id}}">
                        @include('flatplans.show.__page')
                        <span class="page-number">{{ $index }}</span>
                    </div>
                    <?php
                        $index++;
                    ?>
                @endforeach
            </div>
        </div>
    </div>