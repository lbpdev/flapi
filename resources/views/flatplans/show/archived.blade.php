@extends('layouts.app')

@section('page-title','Flat Plans')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/css/flatplan-page.css') }}" />

    <style>
        #flatplan-pages .page .action {
            display: none !important;
        }
    </style>
@endsection


@section('content')
    <div id="edit-block" style="display: none;">Someone is editing this flatplan.</div>
    <div class="clearfix" id="flatplanView">
        <div class="col-md-12 clearfix" id="main" style="background-color: #f2f2f2">
            <div class="row">
                <div class="container info-panel">
                    <div class="col-md-3">
                        <p><label>Project Name: </label> {{ $version->flatplan->name }}</p>
                        <p><label>Issue Number: </label> {{ $version->flatplan->issue }}</p>
                    </div>
                    @if($version->flatplan->meta)
                        <div class="col-md-2">
                            <p><label>Media: </label> {{ $version->flatplan->meta->media ? $version->flatplan->meta->media->name : 'N/A' }}</p>
                            <p><label>Format: </label> {{ $version->flatplan->meta->format ? $version->flatplan->meta->format->name : 'N/A' }}</p>
                        </div>
                        <div class="col-md-2">
                            <p><label>Size: </label> {{ $version->flatplan->meta->size_x }}{{ $version->flatplan->meta->units }} x {{ $version->flatplan->meta->size_y }}{{ $version->flatplan->meta->units }}</p>
                            <p><label>Deadline: </label> {{ $version->flatplan->deadline }}</p>
                        </div>
                        <div class="col-md-3">
                            <p><label>Version: </label> {{ $version->version }}</p>
                            <p><label>Print Approved: </label> No</p>
                        </div>
                        <div class="col-md-2 text-right">
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-12 clearfix padding-t-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div id="contents-list" class="clearfix {{ count($contents) ? '' : 'temp-hide' }}">
                            <label class="margin-b-0">Contents</label>
                            <ul class="list" id="contents">
                                @foreach($contents as $content)
                                    <li data-id="{{ $content->id }}">
                                        <div class="color-dot padding-l-0">
                                            <span style="background-color: {{ $content->color }};"></span>
                                            <a href="#">{{ $content->name }}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <hr class="pull-left width-full">
                        </div>

                        {{--<a href="{{ route('api.history.redo',$version->id) }}" class="icon-link blue undo-blue width-full">Redo</a>--}}
                        <a href="#" class="icon-link download-pink"></a>
                        <a href="#" class="icon-link user-pink"></a>
                        <a target="_blank" href="{{ route('flatplans.pdf',[$flatplan->slug,$version->version]) }}" id="makePDF" class="icon-link pdf-pink"></a>
                        <a target="_blank" href="{{ route('flatplans.preview',[$flatplan->slug,$version->version]) }}?print=true" class="icon-link print-pink"></a>
                        <hr class="pull-left width-full">

                        {{--                        <a class="link-text pink" href="{{ route('flatplans.public.view',[$version->view->token,$version->view->users[0]->token]) }}">View as Public</a>--}}

                    </div>

                    <div class="col-md-10 traditional" id="flatplan-pages">
                        @if($style=='modern')
                            @include('flatplans.show._pages_modern')
                        @else
                            @include('flatplans.show._pages')
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>
    <script src="{{ asset('public/js/socket.io.js') }}"></script>
    <script src="https://html2canvas.hertzen.com/build/html2canvas.js"></script>

    <script>

        function attachEditClickEvent(){
            $('#flatplan-pages .page .action.edit').unbind('click');
            $('#flatplan-pages .page .action.edit').on('click',function(){
                id = $(this).attr('data-id');
                el = $(this).closest('.page');
                parent = $(this).closest('.pageHolder');
                eq = $( '#'+parent.attr('id')+' .page .action.edit' ).index( $(this) );

                addLoader(el);

                $.ajax({
                    type: "GET",
                    url: appUrl+'/api/pages/get?id='+id,
                    success: function(response){

                        if(response.status==200) {
                            merged = false;
                            isEmpty = false;
                            clickedPage = 0;

                            $('.layout-page.main').attr('data-pageid',response.data.id);

                            $('#layout-pages .layout-page.merged').remove();
                            $('#layout-pages .layout-page').eq(0).addClass('main');

                            for(x=0;x<response.data.merged.length;x++){
                                content = '';

                                page = response.data.merged[x];
                                copy = $('#flatplan-pages .page[data-id="'+page.id+'"]').children().clone();

                                if($('#flatplan-pages .page[data-id="'+page.id+'"]').find('.section').length<1){
                                    isEmpty = true;
                                    content = '<div class="section" style="width:100%;height:100%;background-color: #eaeaea"></div>';
                                }

                                $('#layout-pages').append('<div class="merged layout-page '+(isEmpty ? 'empty' : '')+'" data-pageid="'+page.id+'" data-id=""></div>');

                                layouts = $('#layout-pages .merged.layout-page[data-pageid='+page.id+']');

                                layouts.html('').append(copy);
                                layouts.find('.loader').remove();
                                layouts.find('.action').remove();

                                if(isEmpty)
                                    layouts.append(content);
                                else
                                    layouts.find('.section').each(function(){
                                        $(this).find('.title').text($(this).attr('data-order'));
                                    });

                                if(id == page.id){
                                    $('#layout-pages .layout-page.main').removeClass('main');
                                }

                                layouts.append('<a href="#" class="change-layout">Change layout</a>');

                                merged = true;
                            }

                            if(response.data.layout_id){
                                applyLayout(response.data,false);
                                $('#layout-selection').hide();
                                $('#selectLayoutBt').hide();
                                $('#section-edit').show();
                                $('#selectLayoutBt').css('display','none !important');
                                $('.layout-page.main').append('<a href="#" class="change-layout">Change layout</a>');
                            } else {
                                applyLayout(null,false);
                                $('#layout-selection').show();
                                $('#section-edit').hide();
                                $('#selectLayoutBt').show();
                                $('#selectLayoutBt').css('display','inline-block !important');
                            }


                            $('#editLayoutModal').modal('show');

                            addChangeLayoutClickEvent();
                            addSectionClickEvent();

                            if(merged){
                                $('#layout-pages .layout-page').first().hide();
                                layouts.find('.section').first().trigger('click');
                            }
                            else{
                                $('#layout-pages .layout-page').first().show();
                                $('#layout-pages .layout-page .section').first().trigger('click');
                            }
                        }
                    },
                    complete : function (){
                        deleteProcessFlag = 1;
                        removeLoader(el);
                    }
                });
            });
        }
        attachEditClickEvent();


        function addChangeLayoutClickEvent(){
            $('#layout-pages .change-layout').unbind('click');
            $('#layout-pages .change-layout').on('click', function(){
                $('.layout-page.main').removeClass('main');
                $(this).closest('.layout-page').addClass('main');
                $('#section-edit').hide();
                $('#layout-selection').show();
                $('#selectLayoutBt').show();
            });
        }
        attachMoveEvent();

        function applyLayout(page,global){
            parent = $('.layout-page.main');
            parent.html('');

            if(page){
                if(global)
                    $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+'] .section').remove();

                for(x=0;x<page.layout.sections.length;x++){
                    section = page.layout.sections[x];
                    sectionData = section.length ? section : [];

                    id = section['id'];
                    title = sectionData.length > 0 ? sectionData.data[0]['value'] : '';
                    left = section['left'] >=0 ? section['left'] : '';
                    right = section['right'] >=0 ? section['right'] : '';
                    topV = section['top'] >=0 ? section['top'] : '';
                    bottom = section['bottom'] >=0 ? section['bottom'] : '';
                    color = page.sections[x].content ? page.sections[x].content.color : '';

                    style = 'width:'+section['width']+'%;';
                    style += 'z-index:'+section['z_index']+';';
                    style += 'height:'+section['height']+'%;';
                    style += 'line-height:'+(( section['height'] / 100 ) * 80 )+'px;';
                    style += 'left:'+left +'%;';
                    style += 'top:'+topV+'%;';
                    style += 'right:'+right+'%;';
                    style += 'bottom:'+bottom+'%;';
                    style += 'background-color:'+color+';';
                    style += 'border-color:'+color;

                    if(global){
                        itemForPages = '<div data-order="'+section['order']+'" data-id="'+page.sections[x].id+'" data-sectionid="'+page.sections[x].id+'" data-contentid="'+page.layout.sections[x].content_id+'" class="section" style="'+style+'">' +
                                '<span class="title">'+title+'</span>'+
                                '</div>';

                        $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+']').removeClass('empty').prepend(itemForPages);
                    }

                    if(page.sections.length){
                        itemForLayout = '<div data-order="'+section['order']+'" data-id="'+page.layout.sections[x].id+'" data-sectionid="'+page.sections[x].id+'" data-contentid="'+page.layout.sections[x].content_id+'" class="section" style="'+style+'">' +
                                '<span class="title">'+(x+1)+'</span>'+
                                '</div>';

                        $('.layout-page.main').append(itemForLayout);
                        $('.layout-page.main').append('<a href="#" class="change-layout">Change layout</a>');
                    }
                }
            } else {
                item = '<div class="section" style="width:100%;height:100%;background-color: #eaeaea"></div>';
                $('#layout-pages .layout-page.main').append(item);
                $('#selectLayoutBt').hide();
                $('#layout-pages .layout-page.main').append('<a href="#" class="change-layout">Change layout</a>');
//                $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+']').prepend(item).removeClass('empty');
            }

            addSectionClickEvent();
        }

        function updateContentList(contents){

            $('#contents').html('');

            if(contents.length>0){
                $.each(contents, function(i,e){
                    el = '<li data-id="'+e.id+'">'+
                            '<div class="color-dot padding-l-0">' +
                            '<span style="background-color: '+e.color+';"></span>'+
                            '<a href="#">'+e.name+'</a>'+
                            '</div>'+
                            '</li>';

                    $('#contents').append(el);
                });
            } else {
                $('#contents-list').hide();
            }
        }
    </script>

    @yield('layout-js')
    @yield('addpage-js')
    @yield('mergepage-js')
    @yield('editflatplan-js')
    @yield('history-js')
    @yield('users-js')
@endsection
