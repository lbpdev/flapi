@extends('layouts.app')

@section('page-title','Flat Plans')

@section('style')
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/plugins/jquery-ui/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/css/flatplan-page.css') }}" />

    <style>
        @if(!in_array('modify-merge-pages',$permissions))
            #flatplan-pages .page .action.merge {
                display: none !important;
            }
        @endif
        @if(!in_array('modify-delete-pages',$permissions))
            #flatplan-pages .page .action.delete {
                display: none !important;
            }
        @endif
        @if(!in_array('modify-flat-plan',$permissions))
            #flatplan-pages .page .action.edit {
                display: none !important;
            }
        @endif
        @if(!in_array('modify-move-pages',$permissions))
            #flatplan-pages .page .action.move {
                display: none !important;
            }
        @endif
        @if(!in_array('view-title',$permissions))
            #section-form .title {
                display: none !important;
            }
        @endif
        @if(!in_array('view-design-complete',$permissions))
            #section-form .title {
                display: none !important;
            }
        @endif
        @if(!in_array('view-design-complete',$permissions))
            #section-form .design-complete {
                display: none !important;
            }
        @endif
        @if(!in_array('view-article-approved',$permissions))
            #section-form .article-approved {
                display: none !important;
            }
        @endif
        @if(!in_array('view-contact-number',$permissions))
            #section-form .contact-number {
                display: none !important;
            }
        @endif
        @if(!in_array('view-contact-email',$permissions))
            #section-form .contact-email {
                display: none !important;
            }
        @endif
        @if(!in_array('view-content-private-fields',$permissions))
            #section-form .private-field {
                display: none !important;
            }
        @endif
        @if(!in_array('view-content-public-fields',$permissions))
            #section-form .public-field {
                display: none !important;
            }
        @endif
        @if(!in_array('modify-page-layouts',$permissions))
            .page.empty .action.edit {
                display: none !important;
            }
            .layout-page .change-layout {
                display: none !important;
            }
            #saveContent {
                display: none !important;
            }
        @endif

        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control
        {
            opacity: 0.6;
        }

        .form-group {
            margin-bottom: 8px;
        }

        #editLayoutModal input[type="radio"] + label {
            line-height: 20px !important;
            margin-left: 10px !important;
        }

        #section-form span {
            min-width: 120px;
            display: inline-block;
        }

        #section-form textarea {
            resize: none;
        }

        #orderNumber {
            width: 5%;
            height: 28px;
            line-height: 28px;
        }

        #contentTypeSelection .bootstrap-select {
            width: 95% !important;
        }

        #layout-pages {
            max-height: 350px;
            overflow-y: scroll;
        }

        .pageHolder.covers .action.move {
            display: none !important;
        }

        #editFlatplanBt {
            line-height: 24px;
        }

    </style>
@endsection


@section('content')
    <div id="edit-block" style="display: none;">Someone is editing this flatplan.</div>
    <div class="clearfix" id="flatplanView">
        <div class="col-md-12 clearfix" id="main" style="background-color: #f2f2f2">
            <div class="row">
                <div class="container info-panel">
                    <div class="col-md-3">
                        <p><label>Project Name: </label> {{ $version->flatplan->name }}</p>
                        <p><label>Issue Number: </label> {{ $version->flatplan->issue }}</p>
                    </div>
                    @if($version->flatplan->meta)
                        <div class="col-md-2">
                            <p><label>Media: </label> {{ $version->flatplan->meta->media ? $version->flatplan->meta->media->name : 'N/A' }}</p>
                            <p><label>Format: </label> {{ $version->flatplan->meta->format ? $version->flatplan->meta->format->name : 'N/A' }}</p>
                        </div>
                        <div class="col-md-2">
                            <p><label>Size: </label> {{ $version->flatplan->meta->size_x }}{{ $version->flatplan->meta->units }} x {{ $version->flatplan->meta->size_y }}{{ $version->flatplan->meta->units }}</p>
                            <p><label>Deadline: </label> {{ $version->flatplan->deadline }}</p>
                        </div>
                        <div class="col-md-2">
                            <p><label>Version: </label> {{ $version->version }}</p>
                            <p><label>Print Approved: </label> {{ $version->flatplan->meta->print_approved ? 'Yes' : 'No' }}</p>
                        </div>
                        <div class="col-md-3 text-right">

                            <p class="text-left"><label>Pages: </label> <span id="currentPageCount">{{ count($version->pages) }}</span>/{{ Auth::user()->company->maxPages }}</p>

                            @if(Auth::user()->isAdmin)
                                <p>
                                    <a href="#" class="pull-right link-text pink" id="editFlatplanBt">Edit Project Details</a>
                                </p>
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-12 clearfix padding-t-12">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div id="contents-list" class="clearfix {{ count($contents) ? '' : 'temp-hide' }}">
                            <label class="margin-b-0">Contents</label>
                            <ul class="list" id="contents">
                                @foreach($contents as $content)
                                    <li data-id="{{ $content->id }}">
                                        <div class="color-dot padding-l-0">
                                            <span style="background-color: {{ $content->color }};"></span>
                                            <a href="#">{{ $content->name }}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <hr class="pull-left width-full">
                        </div>

                        @if(in_array('modify-add-pages',$permissions))
                            <a href="#" id="addPagesBt" data-id="{{ $version->id }}" class="icon-link blue add-blue width-full">Add Page</a>
                            <hr class="pull-left width-full">
                        @endif

                        {{--<a href="{{ route('api.history.undo',$version->id) }}" class="icon-link blue undo-blue width-full">Undo</a>--}}
                        @if(count($versions))
                            <a href="#" id="showHistory" class="icon-link blue history-blue width-full">History</a>
                            <hr class="pull-left width-full">
                        @endif
                        {{--<a href="{{ route('api.history.redo',$version->id) }}" class="icon-link blue undo-blue width-full">Redo</a>--}}
                        <a href="#" class="icon-link download-pink"></a>

                        @if(in_array('modify-participants',$permissions))
                            <a href="#" class="icon-link user-pink" id="selectUsersBt"></a>
                        @endif

                        <a target="_blank" href="{{ route('flatplans.pdf',[$flatplan->slug,$version->version]) }}" id="makePDF" class="icon-link pdf-pink"></a>
                        <a target="_blank" href="{{ route('flatplans.preview',[$flatplan->slug,$version->version]) }}?print=true" class="icon-link print-pink"></a>
                        <hr class="pull-left width-full">

                        @if(in_array('modify-create-version',$permissions))
                            <a class="link-text pink" href="{{ route('api.flatplans.create-version',$version->id) }}">Save as new version</a>
                            <hr class="pull-left width-full">
                        @endif


                        @if(in_array('modify-status',$permissions))
                            <a class="link-text {{ $flatplan->view->is_live ? 'live' : '' }}" id="go-live-bt" href="#">{{ $flatplan->view->is_live ? 'Go offline' : 'Go Live' }}</a>
                        @endif
{{--                        <a class="link-text pink" href="{{ route('flatplans.public.view',[$version->view->token,$version->view->users[0]->token]) }}">View as Public</a>--}}

                        @include('flatplans.templates.online-users')
                    </div>

                    <div class="col-md-10 traditional" id="flatplan-pages">
                        <div class="wrapper clearfix">
                            @if($flatplan->meta ? $flatplan->meta->style=='modern' : 'traditional')
                                @include('flatplans.show._pages_modern')
                            @else
                                @include('flatplans.show._pages')
                            @endif
                        </div>

                        @if(in_array('modify-flat-plan',$permissions))
                            <div class="col-md-12" id="flatplanActions">
                                <div class="row text-right">
                                    <div class="pull-right">
                                        <a data-type="archive" data-action="{{ route('flatplans.archive') }}" data-id="{{$flatplan->id}}" href="#" class="no-text icon-link archive"></a>
                                        <a data-type="revert" data-action="{{ route('flatplans.revert') }}" data-id="{{$version->id}}" href="#" class="no-text icon-link revert"></a>
                                        <a data-type="{{ count($versions) ? 'deleteVersion' : 'deleteFlatplan' }}" href="#" data-action="{{ route('flatplans.delete.version') }}" data-id="{{$version->id}}" class="no-text icon-link trash"></a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="deletePageModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
        <div class="modal-dialog vertical-align-center" style="width: 300px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Delete Page</h4>
                </div>
                <div class="modal-body text-center">
                    Are you sure you want to delete page?
                </div>
                <div class="modal-footer  text-center-important">
                    <button id="deletePageTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </div>

        </div>
    </div>

    @include('flatplans.show.modals.layout')
    @include('flatplans.show.modals.add-page')
    @include('flatplans.show.modals.merge-page')
    @include('flatplans.show.modals.edit-flatplan')
    @include('flatplans.show.modals.history')
    @include('flatplans.modals.all-users')
    @include('flatplans.show.modals.invite-form')
    @include('flatplans.show.modals.live')
    @include('flatplans.show.modals.confirm-modal')
    @include('flatplans.show.modals.kicked')

@endsection

@section('js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>
    <script src="{{ asset('public/js/socket.io.js') }}"></script>
    <script src="https://html2canvas.hertzen.com/build/html2canvas.js"></script>

    <script>

        var startPage = '{{$version->flatplan->meta->start_page}}';

        // 30 Minutes Idle Time
        var idleTime = 1800000;

        @if(env('APP_ENV')!='local')
            var nickname = '{{ Auth::user()->name }}';
            var user_id = '{{ Auth::user()->id }}';
            var room = '{{ $version->id }}';
            var type = 'employee';

            <?php if(env('APP_ENV')=='live') { ?>
                var socket = io('http://flapiapp.com:3001');
            <?php } else { ?>
                var socket = io('http://localhost:3001');
            <?php } ?>

            socket.on("updateUsers", function(message){

                if(!message[socket.id].editting){
                    $('#edit-block').show();
                    $('#flatplanView .page .action').remove();
                    $('#addPagesBt').remove();
                }

                updateUsersList(message);
            });

            socket.on('connect', function(){
                socket.emit("subscribe", {
                    room : room ,
                    nickname : nickname ,
                    clientId : socket.id ,
                    userId : user_id,
                    type : type
                } );
            });

            socket.on('kicked', function(){
            });

            socket.on('statusUpdate', function(data){
            });

            function updateUsersList(users){
                $('#users').html('');

                $('.list.offline li').show();
                Object.keys(users).forEach(function(index) {
                    if(users[index].room=='flapi-room-'+room){
                        active = false;

                        if(users[index].editting){
                            $('.list.editting').append('<li data-id="'+users[index].userId+'" data-nickname="'+users[index].nickname+'" data-client-id="'+users[index].clientId+'">'+users[index].nickname+'</li>');
                            active = true;

                            $('#edit-block').removeClass('free').html('Someone else is currently editing this flatplan.');


                            if(users[index].userId == user_id){
                                var inactivityTime = function () {
                                    var t;
                                    window.onload = resetTimer;
                                    // DOM Events
                                    document.onmousemove = resetTimer;
                                    document.onkeypress = resetTimer;

                                    function logout() {
                                        socket.emit("kicked", { room : room, user_id : user_id });
                                        $('#modalKicked').modal('show');
                                    }

                                    function resetTimer() {
                                        clearTimeout(t);
                                        t = setTimeout(logout, idleTime);
                                        // 1000 milisec = 1 sec
                                    }
                                };
                                inactivityTime();
                            }
                        }
                        else {

                            if($('.list.online li[data-nickname="'+users[index].nickname+'"]').length<1)
                                $('.list.online').append('<li data-id="'+users[index].userId+'" data-nickname="'+users[index].nickname+'" data-client-id="'+users[index].clientId+'">'+users[index].nickname+'</li>');

                            $('#online-list').show();
                            active = true;
                        }

                        if(active){
                            target = $('#users.offline li[data-nickname="'+users[index].nickname+'"]');

                            if(target.attr('data-nickname') == users[index].nickname)
                                $('.list.offline li[data-nickname="'+users[index].nickname+'"]').hide();

                        }

                        $('.list.online li').each(function(){
                            if(users[$(this).attr('data-client-id')]==undefined){
                                $('.list.online li[data-client-id="'+$(this).attr('data-client-id') +'"]').remove();
                            }
                        });
                    }
                });

            }

            function getRoomUsers(room){
                socket.emit("getActiveUsersInRoom", { room : 'flapi-room-'+room } );
            }

            socket.on('getActiveUsersInRoom', function(users){
                $('#users').html('');

                Object.keys(users).forEach(function(index) {
                    if(users[index].room=='flapi-room-'+room)
                        $('#users.online').append('<li>'+users[index].nickname+'</li>');
                });
            });

            socket.on('noOneIsEditting', function(users){
                $('#edit-block').addClass('free').html('The person editing this flatplan has finished. Click <a href="">here</a> to take over.');
            });

            function getActiveUsers(){
                socket.emit("getActiveUsers", { room: room });
            }

        @endif
//        $('#makePDF').on('click',function(e){
//
//            e.preventDefault();
//
//            html2canvas($('#flatplan-pages'), {
//                onrendered: function(canvas) {
//                    canvas.id = 'Do';
//
//                    document.getElementById('flatplan-pages').appendChild(canvas);
//
////                        $("#Do").width(780);
//
//                    html2canvas($("#Do"), {
//                        onrendered: function(canvas) {
//
//                            setpixelated(canvas.getContext('2d'));
//                            var imgData = canvas.toDataURL(
//                                    'image/png');
//
//                            var doc = new jsPDF('p', 'mm');
//                            doc.addImage(imgData, 'PNG', 1, 1);
////                                doc.save('Report-'+moment().format('D-MM-Y')+'.pdf');
//                            window.open(doc, '_blank', 'fullscreen=yes');
//
////                                window.open(doc);
//
//                            $("#Do").remove();
//                        },
//                    });
//                },
//                background : '#fff',
//            });
//        });


        function attachEditClickEvent(){
            $('#flatplan-pages .edit').unbind('click');
            $('#flatplan-pages .edit').on('click',function(){

                $('#layout-pages').animate({scrollTop: 0});

                id = $(this).attr('data-id');
                el = $(this).closest('.page');
                parent = $(this).closest('.pageHolder');
                eq = $( '#'+parent.attr('id')+' .page .action.edit' ).index( $(this) );

                addLoader(el);

                $.ajax({
                    type: "GET",
                    url: appUrl+'/api/pages/get?id='+id,
                    success: function(response){

                        if(response.status==200) {
                            merged = false;
                            isEmpty = false;
                            clickedPage = 0;

                            $('#layout-pages .layout-page.merged').remove();
                            $('#layout-pages .layout-page').first().addClass('main');
                            $('#layout-pages .layout-page').first().find('.section').remove();
                            $('#layout-pages .layout-page').first().attr('data-pageid',response.data.id);

                            for(x=0;x<response.data.merged.length;x++){
                                content = '';

                                page = response.data.merged[x];
                                copy = $('#flatplan-pages .page[data-id="'+page.id+'"]').children().clone();

                                if($('#flatplan-pages .page[data-id="'+page.id+'"]').find('.section').length<1){
                                    isEmpty = true;
                                    content = '<div class="section" style="width:100%;height:100%;background-color: #eaeaea"></div>';
                                }

                                $('#layout-pages').append('<div class="merged layout-page '+(isEmpty ? 'empty' : 'notEmpty')+'" data-pagenumber="'+page.order+'" data-pageid="'+page.id+'" data-id=""></div>');

                                layouts = $('#layout-pages .merged.layout-page[data-pageid='+page.id+']');

                                layouts.html('').append(copy);
                                layouts.find('.loader').remove();
                                layouts.find('.action').remove();

                                if(isEmpty)
                                    layouts.append(content);
                                else
                                    layouts.find('.section').each(function(){
                                        $(this).find('.title').text($(this).attr('data-order'));
                                    });

                                layouts.append('<a href="#" class="change-layout">Change layout</a>');

                                merged = true;
                            }

                            if(response.data.layout_id){
                                applyLayout(response.data,false);
                                $('#layout-selection').hide();
                                $('#selectLayoutBt').hide();

                                showSectionEdit();

                                $('#selectLayoutBt').css('display','none !important');
                                $('.layout-page.main').append('<a href="#" class="change-layout">Change layout</a>');

                            } else {
                                applyLayout(null,false);

                                $('#selectLayoutBt').show();
                                $('#selectLayoutBt').css('display','inline-block !important');
                                showLayoutSelect();
                            }

                            $('#editLayoutModal').modal('show');

                            addChangeLayoutClickEvent();
                            addSectionClickEvent();

                            if(merged){
                                $('#layout-pages .layout-page').first().hide();

                                setTimeout(function(){
                                    $('#layout-pages .layout-page[data-pagenumber="'+(eq+1)+'"]').find('.section').first().trigger('click');
                                },100);
                            }
                            else {
                                $('#layout-pages .layout-page').first().show();
                                $('#layout-pages .layout-page .section').first().trigger('click');
                            }

                            setTimeout(function(){
                                $('#layout-pages').animate({scrollTop: $('#layout-pages .layout-page.main').offset().top - 350});
                            },500);

                        }
                    },
                    complete : function (){
                        deleteProcessFlag = 1;
                        removeLoader(el);
                    }
                });
            });
        }
        attachEditClickEvent();


        function addChangeLayoutClickEvent(){
            $('#layout-pages .change-layout').unbind('click');
            $('#layout-pages .change-layout').on('click', function(){
                $('.layout-page.main').removeClass('main');
                $(this).closest('.layout-page').addClass('main');

                showLayoutSelect();
                $('#selectLayoutBt').show();
            });
        }

        addChangeLayoutClickEvent();


        function attachMoveEvent(){
            $( "#innerPages" ).sortable({
                placeholder: "page ui-state-highlight",
                items: ".page:not(.no-drag)",
                helper: 'clone',
                handle: ".move",
                start : function(event,ui){
                },
                stop : function(event,ui){
//                    if($(ui.item[0]).closest('#innerPages').hasClass('covers'))
                        reorder('innerPages');
                }
            });
            $( "#innerPages" ).disableSelection();
        }
        attachMoveEvent();


        function attachDeleteClickEvent(){
            $('#flatplan-pages .page .action.delete').unbind('click');
            $('#flatplan-pages .page .action.delete').on('click',function(){
                id = $(this).attr('data-id');
                el = $(this).closest('.page');
                addLoader(el);

                $('#deletePageTrigger').attr('data-id',id);
                $('#deletePageModal').modal('show');
                removeLoader(el);
            });
            attachDeleteTiggerEvent();
        }
        attachDeleteClickEvent();


        function attachDeleteTiggerEvent(){
            $('#deletePageTrigger').unbind('click');
            $('#deletePageTrigger').on('click',function(){
                id = $(this).attr('data-id');
                el = $("#flatplan-pages .page[data-id="+id+"]");

                $.ajax({
                    type: "GET",
                    url: appUrl+'/api/pages/delete?id='+id,
                    success: function(response){

                        if(response.status==200) {
                            el.remove();
                            updateContentList(response.data.contents);
                            reorder('innerPages');
                        }

                        doAlert(response);
                        $('#currentPageCount').text($('#flatplan-pages .page').length);


                        $('#deletePageModal').modal('hide');
                    },
                    complete : function (){
                        deleteProcessFlag = 1;
                    }
                });
                reorder('innerPages');
            });
        }
        attachDeleteTiggerEvent();

        function attachMergeClickEvent(){
            $('#flatplan-pages .page .action.merge').unbind('click');
            $('#flatplan-pages .page .action.merge').on('click',function(){


                $('#mergePage').html('');
                $('#mergeTo').html('');

                parent = $(this).closest('.pageHolder');
                index = parent.find(".page .action.merge" ).index( $(this) ) + 1;

                page_type = parent.attr('id');

                $('#mergePageForm input[name=id]').val({{ $version->id }});
                $('#mergePageForm input[name=page_type]').val(page_type);

                $('#mergePageModal').modal('show');
                currentPages = parent.find('.page').length;

                for(x=1;x<=currentPages;x++){
                    $('#mergePage').append('<option value="'+x+'">'+( startPage > 1 ? x+2 : x )+'</option>');
                    $('#mergeTo').append('<option value="'+x+'">'+( startPage > 1 ? x+2 : x )+'</option>');
                }

                $('#mergePage').val(index).selectpicker('refresh');
                $('#mergeTo').val(index+1).selectpicker('refresh');
            });
        }
        attachMergeClickEvent();

        function applyLayout(page,global){
            parent = $('.layout-page.main');
            parent.html('');

            if(page){
                if(global)
                    $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+'] .section').remove();

                for(x=0;x<page.layout.sections.length;x++){
                    section = page.layout.sections[x];
                    sectionData = section.length ? section : [];

                    id = section['id'];
                    title = sectionData.length > 0 ? sectionData.data[0]['value'] : '';
                    left = section['left'] >=0 ? section['left'] : '';
                    right = section['right'] >=0 ? section['right'] : '';
                    topV = section['top'] >=0 ? section['top'] : '';
                    bottom = section['bottom'] >=0 ? section['bottom'] : '';
                    color = page.sections[x].content ? page.sections[x].content.color : '';

                    style = 'width:'+section['width']+'%;';
                    style += 'z-index:'+section['z_index']+';';
                    style += 'height:'+section['height']+'%;';
                    style += 'line-height:'+(( section['height'] / 100 ) * 80 )+'px;';
                    style += 'left:'+left +'%;';
                    style += 'top:'+topV+'%;';
                    style += 'right:'+right+'%;';
                    style += 'bottom:'+bottom+'%;';
                    style += 'background-color:'+color+';';
//                    style += 'border-color:'+color;

                    if(global){
                        itemForPages = '<div data-order="'+section['order']+'" data-id="'+page.sections[x].id+'" data-sectionid="'+page.sections[x].id+'" data-contentid="'+page.layout.sections[x].content_id+'" class="section" style="'+style+'">' +
                           '<span class="title">'+title+'</span>'+
                            '</div>';

                        $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+']').removeClass('empty').prepend(itemForPages);
                    }

                    if(page.sections.length){
                        itemForLayout = '<div data-order="'+section['order']+'" data-id="'+page.sections[x].id+'" data-sectionid="'+page.sections[x].id+'" data-contentid="'+page.layout.sections[x].content_id+'" class="section" style="'+style+'">' +
                               '<span class="title">'+(x+1)+'</span>'+
                                '</div>';

                        $('.layout-page.main').append(itemForLayout);
                    }
                }

                $('.layout-page.main').append('<a href="#" class="change-layout">Change layout</a>');
            } else {
                item = '<div class="section" style="width:100%;height:100%;background-color: #eaeaea"></div>';
                $('#layout-pages .layout-page.main').append(item);
                $('#selectLayoutBt').hide();
                $('#layout-pages .layout-page.main').append('<a href="#" class="change-layout">Change layout</a>');
//                $('#flatplan-pages .page[data-id='+parent.attr('data-pageid')+']').prepend(item).removeClass('empty');
            }

            addSectionClickEvent();
        }

        function reorder(targetItem){
            index = 1;
            fill = startPage == 1 ? 0 : 2;

            data = {};

            if(targetItem=='coverPages')
                target = $('.covers .page');
            else
                target = $('#'+targetItem+' .page');

            target.each(function(){

                data[index] = [];

                data[index] = {
                    'id' : $(this).attr('data-id'),
                    'order' : index
                };

                if(targetItem=='coverPages'){
                    if(index==1)
                        $(this).find('.page-number').text('FC');

                    else if(index==2)
                        $(this).find('.page-number').text('IFC');

                    else if(index==$('.covers .page').length - 1)
                        $(this).find('.page-number').text('IBC');

                    else if(index==$('.covers .page').length)
                        $(this).find('.page-number').text('BC');
                    else
                        $(this).find('.page-number').text('Gatefold');
                }
                else {
                    $(this).find('.page-number').text(index+fill);
                }

                index++;
            });


            $.ajax({
                type: "POST",
                url: appUrl+'/api/pages/reorder',
                data: { 'data' : data } ,
                success: function(response){
                },
                done : function (){
                    processFlag = 1;
                }
            });

        }

        function updateContentList(contents){

            $('#contents').html('');

            if(contents.length>0){
                $.each(contents, function(i,e){
                    el = '<li data-id="'+e.id+'">'+
                            '<div class="color-dot padding-l-0">' +
                                '<span style="background-color: '+e.color+';"></span>'+
                                '<a href="#">'+e.name+'</a>'+
                            '</div>'+
                        '</li>';

                    $('#contents').append(el);
                });
            } else {
                $('#contents-list').hide();
            }
        }
    </script>

    @yield('layout-js')
    @yield('addpage-js')
    @yield('mergepage-js')
    @yield('editflatplan-js')
    @yield('history-js')
    @yield('users-js')
    @yield('all-users-js')
    @yield('go-live-js')
    @yield('confirm-modal-js')
@endsection
