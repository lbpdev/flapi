<div id="editFlatplanModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:300px">
        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
                <h4 class="modal-title">Edit Project Details</h4>
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-left" id="section-edit">
                            {!! Form::open(['route'=>'api.flatplans.update','id'=>'editFlatplanForm']) !!}
                            {!! Form::hidden('flatplan_id',$version->flatplan->id,['id'=>'flatplan_id']) !!}

                            <div class="col-md-12 text-left">
                                Title
                                {!! Form::input('text','flatplan[name]',$version->flatplan->name,['class'=>' form-control','placeholder'=>'Flatplan Name']) !!}
                            </div>
                            <div class="col-md-12  text-left">
                                Issue #
                                {!! Form::input('text','flatplan[issue]',$version->flatplan->issue,['class'=>' form-control','placeholder'=>'1']) !!}
                            </div>
                            <div class="col-md-12  text-left">
                                <hr>
                                Media
                                {!! Form::select('meta[media_type_id]',$media, $version->flatplan->meta->media_type_id,['class'=>' selectpicker','title'=>'']) !!}

                                Format
                                {!! Form::select('meta[format_type_id]',$formats,$version->flatplan->meta->format_type_id,['class'=>' selectpicker','title'=>'']) !!}

                                <?php
                                $styles = [
                                        'traditional' => 'Traditional',
                                        'modern'      => 'Modern'
                                ]
                                ?>

                                <div class="col-md-6 padding-l-0">
                                    <div id="pageStart">
                                        First Page
                                        {!! Form::select('meta[start_page]',[3=>3,1=>1], $version->flatplan->meta ? $version->flatplan->meta->start_page : 3 ,['class'=>' selectpicker','title'=>'']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6 padding-0">
                                    Flat Plan Style
                                    {!! Form::select('meta[style]',$styles, $version->flatplan->meta->style ,['class'=>' selectpicker','title'=>'']) !!}
                                </div>

                                <div class="row">
                                    <div class="col-md-6 padding-r-0 text-left">
                                        Size
                                        <div class="col-md-12 padding-0">
                                            <div class="col-md-7 padding-0">
                                                {!! Form::input('text','meta[size_x]',$version->flatplan->meta->size_x,['class'=>' form-control','placeholder'=>'210']) !!}
                                            </div>
                                            <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                width
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-l-0 text-left">
                                        &nbsp;

                                        <div class="col-md-12 padding-0">
                                            <div class="col-md-7 padding-0">
                                                {!! Form::input('text','meta[size_y]',$version->flatplan->meta->size_y,['class'=>' form-control','placeholder'=>'290']) !!}
                                            </div>
                                            <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                height
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 padding-r-0 text-left">
                                        <div class="col-md-12 padding-0">
                                            <div class="col-md-7 padding-0">
                                                {!! Form::input('text','meta[units]',$version->flatplan->meta->units,['class'=>' form-control','placeholder'=>'mm']) !!}
                                            </div>
                                            <div class="col-md-5 padding-r-0 padding-l-5 font-10">
                                                unit
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                Deadline
                                {!! Form::input('text','flatplan[deadline]',$version->flatplan->deadline,['class'=>'datepicker form-control','placeholder'=>'1']) !!}


                                <?php
                                $prints = [
                                        '0' => 'No',
                                        '1' => 'Yes'
                                ]
                                ?>

                                <div class="col-md-6 padding-l-0">
                                    <div id="pageStart">
                                        Print Approved
                                        {!! Form::select('meta[print_approved]',$prints, $version->flatplan->meta ? $version->flatplan->meta->print_approved : 0 ,['class'=>' selectpicker','title'=>'']) !!}
                                    </div>
                                </div>

                                <div class="col-md-12 text-center">
                                    {!! Form::submit('Save',['class'=>'float-none link-text pink']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('editflatplan-js')
    <script>

        $('#editFlatplanBt').on('click',function(){

            $('#editFlatplanForm input[name=id]').val($(this).attr('data-id'));

            $('#editFlatplanModal').modal('show');

            currentPages = $('#sortable .page').length;
        });

        $('#editFlatplan').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){
                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

    </script>
@endsection