<div id="modalKicked" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog vertical-align-center" style="width:260px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
                <h4 class="modal-title">Inactivity</h4>
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            You have been idle for more than 30 minutes. Click here to reconnect.<br>
                            <a href="{{ Request::url() }}">Click here to reconnect.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>