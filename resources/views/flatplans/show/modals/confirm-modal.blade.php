<!-- Modal -->
<div id="confirmModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content text-center">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="message"> Are you sure you want to delete this? </div>
                Type "yes" to confirm.<br>
                {!! Form::open() !!}
                    <input type="hidden" id="confirmId" name="id">
                    <input id="confirmInput" class="text-center">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer text-center-important">
                <button id="confirmTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>

    </div>
</div>

@section('confirm-modal-js')
    <script>
        /** SEND DELETE REQUEST **/

        $('#confirmTrigger').on('click', function(){
            if($('#confirmInput').val()=="yes"){
                $('#confirmModal form').submit();
                $('#deleteContentModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });

        $('#confirmInput').bind('keypress', function(e){
            if ( e.keyCode == 13 ) {
                e.preventDefault();
                $('#confirmTrigger').trigger('click');
            }
        });

        $('#flatplanActions .icon-link').on('click',function (e) {
            e.preventDefault();

            modal = $('#confirmModal');
            actionType = $(this).attr('data-type');
            actionURL = $(this).attr('data-action');
            id = $(this).attr('data-id');
            modal.find('form').attr('action',actionURL);

            switch(actionType){
                case 'archive':
                    $('#confirmId').val(id);
                    modal.find('.modal-title').html('Archive Flatplan');
                    modal.find('.message').html('Are you sure you want to archive this flatplan??');
                    break;
                case 'revert':
                    $('#confirmId').val(id);
                    modal.find('.modal-title').html('Reset Version');
                    modal.find('.message').html('Are you sure you want to reset this flatplan? All data and layout information for this version will be lost.');
                    break;
                case 'deleteVersion':
                    $('#confirmId').val(id);
                    modal.find('.modal-title').html('Delete Version');
                    modal.find('.message').html('Are you sure you want to delete this version? This cannot be undone. Once this flatplan has been deleted you will revert back to an older version.');
                    break;
                case 'deleteFlatplan':
                    $('#confirmId').val(id);
                    modal.find('.modal-title').html('Delete Flatplan');
                    modal.find('.message').html('Are you sure you want to delete this flatplan? This cannot be undone.');
                    break;
            }


            modal.modal('show');
        });

    </script>
@endsection