        <!-- Modal -->
<div id="goLiveModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content text-center">
            <div class="modal-header">
                <h4 class="modal-title">Go <span class="live-status">{{ $flatplan->view->is_live ? 'Offline' : 'Live' }}</span></h4>
            </div>
            <div class="modal-body">
                Are you sure you want to go <span class="live-status">{{ $flatplan->view->is_live ? 'Offline' : 'Live' }}</span>?<br>
                Please type "yes" to go <span class="live-status">{{ $flatplan->view->is_live ? 'Offline' : 'Live' }}</span>.<br>

                {!! Form::open(['route'=>'flatplans.status.toggle','id'=>'goLiveForm']) !!}
                <input type="hidden" name="id" value="{{$flatplan->view->id}}">
                <input id="goLiveConfirmInput" class="text-center">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer text-center-important">
                <button id="goLiveTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>

    </div>
</div>

@section('go-live-js')
    <script>

        /** SEND DELETE REQUEST **/

        $('#goLiveTrigger').on('click', function(e){
            if($('#goLiveConfirmInput').val()=="yes"){
                var form = $('#goLiveForm');
                processFlag = 1;

                e.preventDefault();
                e.stopPropagation();

                data = form.serialize();
                url = form.attr('action');

                if(processFlag) {
                    processFlag = 0;

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function(response){

                            if(response.status==200) {

                                if(response.data){
                                    $('#go-live-bt').text('Go Offline').addClass('live');
                                    $('.live-status').text('offline');
                                }
                                else{
                                    $('#go-live-bt').text('Go Live').removeClass('live');
                                    $('.live-status').text('live');
                                }

                                $('#goLiveModal').modal('hide');
                            }

                            doAlert(response);
                        },
                        done : function (){
                            processFlag = 1;
                        }
                    });
                }

            } else {
                alert('Please follow the instructions');
            }
        });

        $('#go-live-bt').on('click',function(){
            $('#goLiveModal').modal('show');
        });

    </script>
@endsection