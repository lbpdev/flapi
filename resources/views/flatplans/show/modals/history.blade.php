<div id="historyModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:900px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
                <h4 class="modal-title">Version</h4>
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-left">
                            <div class="col-md-1 padding-l-0">
                                Version
                            </div>
                            <div class="col-md-1 padding-l-0">
                                {!! Form::select('version_id',$versions, 0 ,['class'=>'form-control selectpicker','id'=>'versionSelect']) !!}
                            </div>
                            <iframe width="100%" height="500" src="" id="versionViewer"></iframe>
                            <a class="link-text pink" id="historyApplyBt" href="">Apply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('history-js')
    <script>

        $('#showHistory').on('click',function(){
            $("#versionSelect").val($("#versionSelect option:first").val()).trigger('change');;
            $('#historyModal').modal('show');
        });

        $('#versionSelect').on('change',function(){
            $('#versionViewer').attr('src','{{ route('flatplans.index') }}/preview/{{$version->flatplan->slug}}/v/'+$(this).val());

            $('#historyApplyBt').attr('href','{{ url('api/fp/create-hv') }}/{{$version->flatplan->id}}'+'/'+$(this).val());

        });

        $('#editFlatplan').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){
                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

    </script>
@endsection