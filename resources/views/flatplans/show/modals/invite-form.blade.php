<div id="inviteModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:900px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-left">
                            {!! Form::open(['id'=>'updateParticipants','route'=>'api.flatplan.invite','class'=>'pull-left width-full']) !!}
                            {!! Form::hidden('flatplan_id',$flatplan->id) !!}
                            {!! Form::hidden('version_id',$version->id) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
