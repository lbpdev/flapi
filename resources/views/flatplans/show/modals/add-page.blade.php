<div id="addPageModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:260px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
                <h4 class="modal-title">Add Pages</h4>
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-left">
                            {!! Form::open(['route'=>'api.pages.add','id'=>'addPageForm']) !!}
                            {!! Form::hidden('id',$version->id) !!}
                            <?php
                                $page_types = [
                                    'coverPages' => 'Cover Page',
                                    'innerPages'  => 'Inside Page'
                                ]
                            ?>

                            <div id="addPagesFields" class="row">
                                <div class="col-md-5 padding-r-0">
                                    Type of page:
                                </div>
                                <div class="col-md-7">
                                    {!! Form::select('page_type',$page_types, 'innerPages' ,['class'=>'selectpicker','title'=>'Select Content','id'=>'addPageType']) !!}
                                </div>


                                <div class="col-md-8">
                                    Number of pages:
                                </div>
                                <div class="col-md-4">
                                    {!! Form::select('pages',[], 1 ,['class'=>' selectpicker','id'=>'addPageAmount']) !!}
                                </div>

                                <div id="insertAfterGroup">
                                    <div class="col-md-8">
                                        Insert after page:
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::select('after',[], 1 ,['class'=>' selectpicker','id'=>'addPageIn']) !!}
                                    </div>
                                </div>

                                <div class="col-md-12 text-center">
                                    {!! Form::submit('Save',['class'=>'float-none link-text pink margin-b-0']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('addpage-js')
    <script>
        var fp_style = '{{ $flatplan->meta ? $flatplan->meta->style : 'traditional' }}';
        var maxPages = {{Auth::user()->company->maxPages}};

        $('#addPagesBt').on('click',function(){

            $('#addPageForm input[name=id]').val($(this).attr('data-id'));

            $('#addPageModal').modal('show');

            updatePageSelection();
        });

        function updatePageSelection(){
            target = $('#addPageType').val();

            currentPages = $('#flatplan-pages .page').length;

            if(currentPages>={{Auth::user()->company->maxPages}}){
                $('#addPagesFields').hide();
                if(!$('#addPageForm p').length)
                    $('#addPageForm').append('<p class="text-center">You have reached the maximum pages allowed per flatplan.</p>');
            } else {
                $('#addPagesFields').show();
                $('#addPageForm').find('p').remove();
            }


            $('#addPageIn').html('');

            for(x=startPage;x<=currentPages;x++){
                $('#addPageIn').append('<option value="'+x+'">'+x+'</option>');
            }

            $('#addPageIn').selectpicker('refresh');

            availablePages = maxPages - currentPages;

            $('#addPageAmount').html('');
            $('#addPageAmount').append('<option value="'+1+'">'+1+'</option>');

            for(x=2;x<=availablePages;x+=2){
                $('#addPageAmount').append('<option value="'+x+'">'+x+'</option>');
            }

            $('#addPageAmount').selectpicker('refresh');

            $('#insertAfterGroup').show();

            if(target=='coverPages')
                $('#insertAfterGroup').hide();

            $('#currentPageCount').text($('#flatplan-pages .page').length);
        }


        $('#addPageType').on('change',function(){
            updatePageSelection();
        });

        $('#addPageForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            newPages = response.data;

                            target = newPages[0].is_cover ? 'coverPages' : 'innerPages';

                            if(fp_style=='traditional'){

                                    row = '';
                                    for(x=0;x<newPages.length;x++){
                                        row += '<div class="page ui-state-default empty" data-id="'+newPages[x].id+'">' +
                                                '<span data-id="'+newPages[x].id+'" class="action delete"></span>' +
                                                '<span data-id="'+newPages[x].id+'" class="action move ui-sortable-handle"></span> ' +
                                                '<span data-id="'+newPages[x].id+'" class="action merge"></span>' +
                                                '<span data-id="'+newPages[x].id+'" class="action edit"></span>' +
                                                '<span class="page-number">'+newPages[x].order+'</span>' +
                                                '</div>';
                                    }

                                    if(target=='innerPages'){
                                        if($('#'+target+' .page').length)
                                            $('#'+target+' .page').eq( parseInt(newPages[0].order) - 1 ).after(row);
                                        else
                                            $('#'+target).append(row);
                                    } else {
                                        $('#coverPages .page').eq( Math.floor(($('#coverPages .page').length / 2 ) - 1) ).after(row);
                                    }

                            } else {

                                for(x=0;x<newPages.length;x++){
                                    row = '';

                                    row += '<div class="page ui-state-default empty" data-id="'+newPages[x].id+'">' +
                                            '<span data-id="'+newPages[x].id+'" class="action delete"></span>' +
                                            '<span data-id="'+newPages[x].id+'" class="action move ui-sortable-handle"></span> ' +
                                            '<span data-id="'+newPages[x].id+'" class="action merge"></span>' +
                                            '<span data-id="'+newPages[x].id+'" class="action edit"></span>' +
                                            '<span class="page-number">Gatefold</span>' +
                                            '</div>';

                                    if(target=='innerPages'){
                                        if($('#innerPages .page').length)
                                            $('#innerPages .page').eq( parseInt(newPages[0].order) - 1 ).after(row);
                                        else
                                            $('#innerPages').append(row);
                                    }
                                    else if(x<(newPages.length/2)){
                                        $('#coverPages').append(row);
                                    }
                                    else{
                                        $('#coverPagesBack').prepend(row);
                                    }
                                }
                            }

                            attachEditClickEvent();
                            attachDeleteClickEvent();
                            attachMergeClickEvent();
                            attachMoveEvent();
                            $('#currentPageCount').text($('#flatplan-pages .page').length);

                            reorder(target);

                            $('#addPageModal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

    </script>
@endsection