<div id="mergePageModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:260px">
        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
                <h4 class="modal-title">Merge Pages</h4>
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 text-left" id="section-edit">
                            {!! Form::open(['route'=>'api.pages.merge','id'=>'mergePageForm']) !!}
                            {!! Form::hidden('id') !!}
                            {!! Form::hidden('page_type') !!}

                            <div class="col-md-5 padding-0 line"> Merge Pages :</div>

                            <div class="col-md-3 padding-0">{!! Form::select('page',[], 1 ,['class'=>' selectpicker','id'=>'mergePage']) !!}</div>

                            <div class="col-md-1 padding-0 line text-center">to</div>

                            <div class="col-md-3 padding-0">{!! Form::select('to',[], 1 ,['class'=>' selectpicker','id'=>'mergeTo']) !!}</div>

                            <div class="col-md-12 text-center">
                                {!! Form::submit('Save',['class'=>'float-none link-text pink margin-b-0']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('mergepage-js')
    <script>

        $('#mergePageForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            newPages = response.data.pages;
                            type = response.data.type;

                            for(x=0;x<newPages.length;x++){

                                targetReplace = $('#'+type+' .page').eq(newPages[x].order - 1);

                                targetDuplicate = $('#'+type+' .page').eq($('#mergePage').val() - 1).clone();

                                targetDuplicate.attr('data-id',newPages[x].id);
                                targetDuplicate.find('.action').attr('data-id',newPages[x].id);


                                sectionIndex = 0;
                                targetDuplicate.find('.section').each(function(){
                                    $(this).attr('data-id',newPages[x].sections[sectionIndex].id);
                                    $(this).attr('data-sectionid',newPages[x].sections[sectionIndex].id);
                                    sectionIndex++;
                                });

                                $(targetReplace).replaceWith(targetDuplicate);
                                $(targetReplace).removeClass('empty');

                                reorder(type);
                            }

                            attachEditClickEvent();
                            attachDeleteClickEvent();
                            attachMergeClickEvent();
                            attachMoveEvent();
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

    </script>
@endsection