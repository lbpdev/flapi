<div id="editLayoutModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 500px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix  text-center">
                <h4 class="modal-title">Edit Page Layout</h4>
            </div>
            <div class="modal-body clearfix width-full padding-l-0 padding-r-0">

                <input type="hidden" name="id" value="0">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2 sidebar text-left">
                            <div class="row text-center"style="line-height: 20px;">
                                Page <br>Layout
                                <div id="layout-pages">
                                    <div class="layout-page main" data-id="">
                                        <div class="section" style="width:100%; height: 100%;"></div>
                                        <a href="#" class="change-layout">Change layout</a>
                                    </div>
                                </div>
                                <br>
                                {{--<a href="#" class="link-text pink" id="selectLayoutBt">Next</a>--}}
                                {!! Form::hidden('layout_id',0,['id'=>'selectedLayout']) !!}
                            </div>
                        </div>
                        <div class="col-md-9 text-left pull-left border-left-gray" id="section-edit">
                            <div id="contentTypeSelection" class=" pull-left col-md-12">
                                <div class="row" style="display: none">
                                    <div class=" pull-left padding-r-10" id="orderNumber"></div>
                                    {!! Form::select('content_id',$contentList, 0 ,['class'=>'width-auto pull-left selectpicker','title'=>'Select Content','id'=>'contentSelect']) !!}
                                </div>
                                <div class="row">
                                    <label>Section</label>
                                    <p style="margin:0">
                                        <p id="contentType"></p>
                                        <span id="contentTypeColor"></span>
                                    </p>
                                </div>
                            </div>
                            {!! Form::open(['route'=>'api.flatplans.update-section','class'=>' col-md-12 pull-left','id'=>'sectionForm']) !!}
                            <div class="row">
                                {!! Form::hidden('section_id',0,['id'=>'section_edit_id']) !!}
                                <div id="section-form" class="relative">

                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('public-section-js')
    <script>

        function addChangeLayoutClickEvent(){
            $('#layout-pages .change-layout').unbind('click');
            $('#layout-pages .change-layout').on('click', function(){
                $('.layout-page.main').removeClass('main');
                $(this).closest('.layout-page').addClass('main');

                showLayoutSelect();
                $('#selectLayoutBt').show();
            });
        }

        $('#selectLayoutBt').on('click', function(){
            $(this).hide();
            $('#layout-selection').hide();
            $('#section-edit').show();

            id = $('.layout-page.main').attr('data-pageid');
            layout_id = $('#selectedLayout').val();

            $.ajax({
                type: "GET",
                url: appUrl+'/api/fp/assign-layout?id='+ id +'&layout_id='+layout_id,
                success: function(response){
                    if(response.status==200) {
                        $('.layout-page.main').attr('data-pageid',response.data.id);

                        if(response.data.layout_id){
                            applyLayout(response.data,true);
                        }

                        $('.layout-page.main .section').first().trigger('click');
                        $('#contentSelect').val(0).trigger('change');

                        addChangeLayoutClickEvent();
                        showSectionEdit();

                        $('#editLayoutModal').modal('show');
                    }
                },
                done : function (){
                    deleteProcessFlag = 1;
                }
            });
        });

        function showSectionEdit(){
            $('#layout-selection').hide();

            setTimeout(function(){
                $('#editLayoutModal .sidebar').css('width','100px');
                $('#section-edit').css('width','370px').show();
                $('#editLayoutModal .modal-dialog').css('width','500px');
            },100);
        }

        function showLayoutSelect(){
            $('#section-edit').hide();
            setTimeout(function(){
                $('#editLayoutModal .sidebar').css('width','130px');
                $('#layout-selection').css('width','630px').show();
                $('#editLayoutModal .modal-dialog').css('width','760px');
            },100);

        }

        function addSelectLayoutClick(){
            $('.selectLayoutBt').on('click', function(){
                $(this).hide();
                $('#layout-selection').hide();
                $('#section-edit').show();

                id = $('.layout-page.main').attr('data-pageid');
                layout_id = $('#selectedLayout').val();

                $.ajax({
                    type: "GET",
                    url: appUrl+'/api/fp/assign-layout?id='+ id +'&layout_id='+layout_id,
                    success: function(response){
                        if(response.status==200) {
                            $('.layout-page.main').attr('data-pageid',response.data.id);

                            if(response.data.layout_id){
                                applyLayout(response.data,true);
                            }

                            $('.layout-page.main .section').first().trigger('click');
                            $('#contentSelect').val(0).trigger('change');

                            addChangeLayoutClickEvent();
                            showSectionEdit();

                            $('#editLayoutModal').modal('show');
                        }
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            });
        }


        $('#contentSelect').on('change',function(){
            $('#section-form').html('');

            $('#editLayoutModal .modal-dialog').css('width','500px');

            if($(this).val()==0)
                $('#saveContent').hide();
            else
                $('#saveContent').show();

            elem = $('#section-form');

            addLoader(elem,'white','small');

            section_id = $('.layout-page.main .section.selected').attr('data-sectionid');

            $('#section_edit_id').val(section_id);

            $.ajax({
                type: "GET",
                url: appUrl+'/api/contents/fields/get?id='+$(this).val()+'&section_id='+section_id,
                success: function(response){

                    if(response.status==200) {

                        $('.layout-page.main .selected').css('background-color',response.data.color+' !important');

                        $('#flatplan-pages .section[data-id='+section_id+']').css('background-color',response.data.color+' !important');
                        $('#flatplan-pages .page .section[data-sectionid='+section_id+']').css('background-color',response.data.color+' !important');
                        $('#contentType').text(response.data.name);
                        $('#contentTypeColor').css('background-color',response.data.color+' !important');

                        $.each(response.data.fields, function(i,e){
                            if(e){
                                if(e.value){
                                    el = '<div class="form-group '+e.slug+' '+(e.type.slug == 'boolean' ? 'margin-b-4 margin-t-4' : '' )+'">';

                                    if(e.type.slug == 'boolean')
                                        el += '<label>'+e.name+'</label><p>'+(e.value ? 'Yes' : 'No')+'</p>';
                                    else {
                                        el += '<label>'+e.name+'</label><p>'+e.value+'</p>';
                                    }

                                    el += '</div>';

                                    $('#section-form').append(el);
                                }
                            }
                        });

                        updateContentList(response.data.contents);

                        checkDesignComplete();
                        $('#contents-list').show();

                        $('.layout-page.main .selected').attr('data-id',response.data.id);

                        $('#contentSelect').closest('.bootstrap-select').css('border','1px solid '+response.data.color);
                    }
                },
                complete : function (){
                    deleteProcessFlag = 1;
                    removeLoader(elem);
                }
            });

            $('#templateName').val($(this).find('option[value='+$(this).val()+']').text());
        });


        $('#contentSelect').prepend('<option value="0">Select Content</option>').selectpicker('refresh');

        function addSectionClickEvent(){

            $('#layout-pages .layout-page .section').unbind('click');
            $('#layout-pages .layout-page .section').on('click', function(){
                $('#layout-pages .selected').removeClass('selected');
                $('#layout-pages .main').removeClass('main');

                $(this).addClass('selected');
                $('#orderNumber').text($(this).attr('data-order')+'.');

                $(this).closest('.layout-page').addClass('main');

                section_id = $(this).attr('data-sectionid');
                content_id = $(this).attr('data-cotentid');

                if($(this).closest('.layout-page').hasClass('empty'))
                    $(this).closest('.layout-page').find('.change-layout').trigger('click');

                if(section_id){
                    $.ajax({
                        type: "GET",
                        url: appUrl+'/api/fp/get-section?id='+section_id,
                        success: function(response){

                            if(response.status==200) {
                                $('#layout-selection').hide();
                                $('#section-edit').show();
                                $('#contentSelect').val(response.data.content_id).trigger('change');
                            }
                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });
                } else {
                    $('#contentSelect').val(0).trigger('change');
                }
            });
        }


        $('#sectionForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            target = $('#flatplan-pages .page[data-id='+response.data.page.id+']');

                            $('.section[data-id="'+response.data.section.flat_plan_section_id+'"]').find('.title').html(response.data.section.value);

                            response.data.page.approved ? target.addClass('approved') : target.removeClass('approved');
                            response.data.page.completed ? target.addClass('complete') : target.removeClass('complete');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        function checkDesignComplete(){
            if($('#design-complete-yes').length){
                if(!$('#design-complete-yes').is(':checked')){
                    $('#article-approved-no').prop("checked", true);
                    $('#article-approved-no').attr('checked','checked');
                    $('.article-approved input').attr('disabled','disabled');
                }
                else
                    $('.article-approved input').each(function(){
                        this.removeAttribute('disabled');
                    });
            }
        }

        $('#layout-selection .layout-page').on('click',function(){

            $('#layout-selection .layout-page.selected').removeClass('selected');
            $(this).addClass('selected');

            $('#selectedLayout').val($(this).attr('data-id'));

            $('.layout-page.main').html($(this).html());
            $('.layout-page.main').append('<a href="#" class="selectLayoutBt">Save layout</a>');

            addSectionClickEvent();
            addSelectLayoutClick();

            $('#selectLayoutBt').show();
        });
    </script>
@endsection