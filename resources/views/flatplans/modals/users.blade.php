<div id="usersModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:300px">

        <!-- Modal content-->
        <div class="modal-content clearfix">

            <div class="modal-header clearfix">
                <h4 class="modal-title">Invite</h4>
            </div>
            <div class="modal-body clearfix width-full text-left invite-list">
                {!! Form::open(['id'=>'rolesEditForm','route'=>'api.flatplan.invite','class'=>'pull-left width-full']) !!}
                {!! Form::hidden('version_id',$version->id) !!}
                <div class="col-md-12  text-left">
                    <div class="row">
                        Employees
                        <ul class="">
                            @foreach($users['employees'] as $id=>$user)
                                <?php
                                    $is_viewer = '';

                                    if($version->view)
                                        $is_viewer = in_array($id,$version->view->employeeUserIds);
                                ?>
                                <li>
                                    <p>
                                        {{ Form::input('checkbox','employees[]',$id,[($is_viewer ? 'checked=checked' : '' ),'class'=>'form-control','id'=>'field-radio-'.$id]) }}
                                        <label for="field-radio-{{$id}}">{{$user}}</label>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-12  text-left">
                    <div class="row">
                        Guests
                        <ul class="">
                            @foreach($users['public'] as $id=>$user)
                                <?php
                                    $is_viewer = '';

                                    if($version->view)
                                        $is_viewer = in_array($id,$version->view->guestUserIds);
                                ?>
                                <li>
                                    <p>
                                        {{ Form::input('checkbox','guests[]',$id,[($is_viewer ? 'checked=checked' : '' ),'class'=>'form-control','id'=>'public-radio-'.$id]) }}
                                        <label for="public-radio-{{$id}}">{{$user}}</label>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix pull-left width-full">
                {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>

@section('users-js')
    <script>
        $('.user-pink').on('click',function(){
            $('#usersModal').modal('show');
        });
    </script>
@endsection