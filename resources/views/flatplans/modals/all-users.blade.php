<div id="usersModal" class="modal fade" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:400px">

        @inject('userService','App\Services\UserService')

        <?php
            $users['employees'] = $userService->getAllEmployees();
            $users['public'] = $userService->getAllGuests();
            $roles = $userService->getAllRoles();
        ?>
        <!-- Modal content-->
        <div class="modal-content clearfix">

            <div class="modal-header clearfix">
                <h4 class="modal-title">Invite</h4>
            </div>
            <div class="modal-body clearfix width-full text-left invite-list">
                <div class="col-md-12  text-left employees">
                    <div class="row">
                        Employees
                        <ul class="">
                            @foreach($users['employees'] as $id=>$user)
                                <?php
                                    $is_viewer = '';
                                ?>
                                <li class="clearfix">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <p>
                                                {{ Form::input('checkbox','employees['.$id.'][user_id]',$id,[($is_viewer ? 'checked=checked' : '' ),'class'=>'form-control','id'=>'field-radio-'.$id]) }}
                                                <label for="field-radio-{{$id}}">{{$user}}</label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row role">
                                            {!! Form::select('role_id',$roles, $userService->getRoleById($id) ,['class'=>' selectpicker']) !!}
                                            {{ Form::hidden('employees['.$id.'][role_id]',$userService->getRoleById($id),['class'=>'roleInput']) }}
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-12  text-left guests">
                    <div class="row">
                        Clients
                        <ul class="">
                            @foreach($users['public'] as $id=>$user)
                                <?php
                                    $is_viewer = '';
                                ?>
                                <li>
                                    <p>
                                        {{ Form::input('checkbox','guests[]',$id,[($is_viewer ? 'checked=checked' : '' ),'class'=>'form-control','id'=>'public-radio-'.$id]) }}
                                        <label for="public-radio-{{$id}}">{{$user}}</label>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix width-full text-center">
                <button class="btn btn-default" id="saveFlatplan">Save</button>
            </div>
        </div>
    </div>
</div>

@section('all-users-js')
    <script>
        $('#saveFlatplan').on('click',function(){

            submitBt = $(this);
            $(submitBt).attr('disabled','disabled');

            if($('#flatplanForm').length){
                $('.invite-list').clone().appendTo('#flatplanForm');

                // Find disabled inputs, and remove the "disabled" attribute
                var disabled = $('#flatplanForm').find(':input:disabled').removeAttr('disabled');
                $('#flatplanForm').submit();

                // re-disabled the set of inputs that you previously enabled
                disabled.attr('disabled','disabled');
            } else {
                $('.invite-list').clone().appendTo('#updateParticipants');
                $('#updateParticipants').submit();
            }
        });

        $('#selectUsersBt').on('click',function(){

                submitBt = $(this);
                $(submitBt).attr('disabled','disabled');

                // Find disabled inputs, and remove the "disabled" attribute
                var disabled = $('#flatplanForm').find(':input:disabled').removeAttr('disabled');

                // serialize the form
                var serialized = $('#flatplanForm').serialize();

                // re-disabled the set of inputs that you previously enabled
                disabled.attr('disabled','disabled');

            if($('#flatplanForm').length){
                method = 'POST';
                url = appUrl+'/api/fp/validate';
            } else {
                method = 'GET';
                url = appUrl+'/api/fp/getViewers';
                serialized = { 'flatplan_id' : {{ isset($flatplan) ? $flatplan->id  : 0 }} }
            }

            $.ajax({
                type: method,
                data : serialized,
                url: url,
                success: function(response){

                    if(response.status==200) {

                        data = response.data;

                        if(data){
                            participants = data.participants;
                            guests = data.view.users;

                            for(x=0;x<participants.length;x++){
                                targetEl = $('.invite-list input[value="'+participants[x].user_id+'"]');

                                targetEl.attr('checked',true);

                                targetEl.closest('li').find('select[name="role_id"]').val(participants[x].role_id).trigger('change');
                            }

                            for(x=0;x<guests.length;x++){
                                targetEl = $('#public-radio-'+guests[x].viewable_id);
                                targetEl.attr('checked',true);
                            }
                        }

                        $('#usersModal').modal('show');
                        $('#usersModal .selectpicker').trigger('change');

                    } else {
                        doAlert(response);
                    }

                },
                complete : function (){
                    deleteProcessFlag = 1;
                    $(submitBt).removeAttr('disabled');
                }
            });

        });

        $('.employees .selectpicker').on('change',function(){
            $(this).closest('.role').find('.roleInput').val($(this).val());
        });
    </script>
@endsection