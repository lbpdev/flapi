
<!-- Modal -->
<div id="deleteUserModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:300px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body text-center">
                Are you sure you want to delete this? Please type "yes" to confirm.<br>
                <input id="deleteUserConfirmInput" class="text-center">
            </div>
            <div class="modal-footer text-center-important">
                <button id="deleteUserTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>