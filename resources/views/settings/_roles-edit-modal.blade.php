<div id="editRoleModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width:500px">

        <!-- Modal content-->
        <div class="modal-content clearfix">

            <div class="modal-header clearfix">
                <h4 class="modal-title">Edit Role</h4>
            </div>
            <div class="modal-body clearfix width-full text-left">
                {!! Form::open(['id'=>'rolesEditForm','route'=>'api.roles.update','class'=>'pull-left width-full']) !!}

                @if(isset($company_id))
                    {!! Form::hidden('company_id',$company_id) !!}
                @endif

                <input type="hidden" name="id" value="0">
                <div class="col-md-6  text-left">
                    <div class="row">
                        Roles
                        <input type="text" class="form-control" style="text-align: left !important;" name="name" placeholder="Add a Role" value="" required="required">
                    </div>
                </div>
                <div class="col-md-2 padding-r-0  text-left">
                    &nbsp;
                    <div class="small margin-b-7 relative" id="edit_icon_select_wrap">
                        {!! Form::select('icon',$userIcons, 0 ,['id'=>'edit_icon_select','class'=>' selectpicker','title'=>'']) !!}
                        <img src="" height="22" id="edit_role_icon">
                    </div>
                </div>

                <div class="col-md-6  text-left">
                    <div class="row">
                        What user is allowed to view:
                        <ul class="permission-list clearfix  text-left" id="viewPermissions">
                            @foreach($permissions['view'] as $index=>$permission)
                                <li>
                                    <p>
                                        {{ Form::input('checkbox','views[]',$permission->id,['class'=>'form-control','id'=>'edit-permission-radio-'.$permission->id]) }}
                                        <label for="edit-permission-radio-{{$permission->id}}">{{$permission->name}}</label>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-6  text-left">
                    <div class="row">
                        What user is allowed to modify:
                        <ul class="permission-list clearfix  text-left" id="modifyPermissions">
                            @foreach($permissions['modify'] as $permission)
                                <li>
                                    <p>
                                        {{ Form::input('checkbox','modifies[]',$permission->id,['class'=>'form-control','id'=>'edit-permission-radio-'.$permission->id]) }}
                                        <label for="edit-permission-radio-{{$permission->id}}">{{$permission->name}}</label>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
            <div class="modal-footer clearfix pull-left width-full">
                {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>