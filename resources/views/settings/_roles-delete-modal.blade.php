<div id="deleteRoleModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Delete Role</h4>
            </div>
            <div class="modal-body text-center">
                Are you sure you want to delete this?
              <span class="deleteMessage">
                  <span class="counts">You currently have XX projects and XX tasks connected to this service</span>.
              </span>
                <br>
                Please type "yes" to confirm.<br>
                {!! Form::open(['route'=>'api.roles.delete']) !!}
                <input type="hidden" name="id" value="0">
                <input id="deleteRoleConfirmInput" class="text-center">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer  text-center-important">
                <button id="deleteRoleTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>

    </div>
</div>
