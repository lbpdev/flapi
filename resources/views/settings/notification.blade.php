
<hr>
<div class="row">
    <div class="row">
        <div class="col-md-3">
            {!! Form::open(['id'=>'notificationsForm','route'=>'notifications.settings.store']) !!}

            <input type="hidden" name="company_id" value="{{ Auth::user()->company->id }}">
            <div class="col-md-12">
                <?php
                    $now = \Carbon\Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d');
                ?>
                <div class="form-group relative">
                    Daily Tasks Completion Check
                    <input type="text" class="timepicker form-control pull-left" style="width: 190px" value="{{ isset($options['incomplete-tasks']) ? \Carbon\Carbon::parse($now.' '.$options['incomplete-tasks'])->setTimezone(Auth::user()->company->timezone)->format('H:i') : '9:00' }}" name="notifications[incomplete-tasks]" >
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group relative">
                    Daily Schedule Reminder
                    <input type="text" class="timepicker form-control pull-left" style="width: 190px" value="{{ isset($options['today-tasks']) ? \Carbon\Carbon::parse($now.' '.$options['today-tasks'])->setTimezone(Auth::user()->company->timezone)->format('H:i') : '9:00' }}" name="notifications[today-tasks]" >
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group relative">
                    Daily Summary Report
                    <input type="text" class="timepicker form-control pull-left" style="width: 190px" value="{{ isset($options['tasks-report']) ? \Carbon\Carbon::parse($now.' '.$options['tasks-report'])->setTimezone(Auth::user()->company->timezone)->format('H:i') : '9:00' }}" name="notifications[tasks-report]" >
                </div>
            </div>

            <div class="col-md-12">
                {!! Form::submit('Update',['class'=>'t-pink link-bt link-color margin-t-0 margin-b-10 pull-left']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-2">
            @include('pages.settings.reports')
        </div>
    </div>
</div>


@section('notifications-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script>
        $('.timepicker').timepicker({
            timeFormat: 'H:mm',
            interval: 15,
            minTime: '1',
            maxTime: '11:50pm',
            startTime: '1:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $('#reportsInterval').on('change',function(){
            $('#week-interval').hide();
            $('#month-interval').hide();
            $('#year-interval').hide();

            if($(this).val()=='w')
                $('#week-interval').show();
            else if($(this).val()=='m')
                $('#month-interval').show();
            else if($(this).val()=='y')
                $('#year-interval').show();

        });
    </script>
@endsection