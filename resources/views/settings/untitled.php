<div id="editClientModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            {!! Form::open(['id'=>'clientEditForm','route'=>'api.clients.update','class'=>'pull-left width-full']) !!}
            <div class="modal-header clearfix">
                <h4 class="modal-title">Edit Client</h4>
            </div>
            <div class="modal-body clearfix width-full">

                <input type="hidden" name="id" value="0">
                <div class="col-md-12">
                    <div class="row">
                        Name
                        <div class="form-group relative">
                            <input type="text" class="form-control margin-b-5" name="name" placeholder="Name" value="" required="required">
                            <input id="clientEditColor" class="form-control" name='color' value='#3355cc'/>
                        </div>
                    </div>
                </div>

            </div>

                <div class="modal-footer clearfix pull-left width-full">
                    {!! Form::submit('Update',['class'=>'createbt btn btn-default margin-b-0']) !!}
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>