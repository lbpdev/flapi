<div id="editContentModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            {!! Form::open(['id'=>'contentEditForm','route'=>'api.contents.update','class'=>'pull-left width-full']) !!}

            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif

            <div class="modal-header clearfix">
                <h4 class="modal-title">Edit Content</h4>
            </div>
            <div class="modal-body clearfix width-full">

                <input type="hidden" name="id" value="0">
                <div class="col-md-12">
                    <div class="row">
                        <div class="color-label">
                            <p class="pull-left">Name</p>
                            <p class="pull-right"><input id="contentEditColor" class="form-control" name='color' value='#3355cc'/></p>
                        </div>
                        <div class="form-group relative pull-left width-full">
                            <input type="text" class="form-control margin-b-5" name="name" placeholder="Name" value="" required="required" maxlength="30">
                        </div>

                        <div class="col-md-12 text-left">
                            <div class="row">
                                <ul class="radio-list clearfix" id="editContentTypeList">
                                    @foreach($contentFields as $index=>$field)
                                        <li>
                                            <p>
                                                {{ Form::input('checkbox','fields[]',$field->id,['class'=>'form-control','id'=>'edit-content-radio-'.$field->id]) }}
                                                <label for="edit-content-radio-{{$field->id}}">
                                                    {{$field->name}}
                                                    @if($field->company_id !=0)
                                                        ({{ $field->is_public ? "Public" : 'Private' }})
                                                    @endif
                                                </label>
                                            </p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="modal-footer clearfix pull-left width-full">
                {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>