<div id="editFormatTypeModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content clearfix">
            {!! Form::open(['id'=>'editFormatTypeForm','route'=>'api.formatTypes.update','class'=>'pull-left width-full']) !!}
            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif

            <div class="modal-header clearfix">
                <h4 class="modal-title">Edit Format Type</h4>
            </div>
            <div class="modal-body clearfix width-full">
                <input type="hidden" name="id" value="0">
                <div class="row">
                    <div class="col-md-12">
                        Name
                        <div class="form-group relative">
                            <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Format Type Name" value="" maxlength="30">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer clearfix pull-left width-full">
                {!! Form::submit('Save',['class'=>'createbt btn btn-default margin-b-0']) !!}
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

            {!! Form::close() !!}
        </div>

    </div>
</div>