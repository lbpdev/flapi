@section('fields-style')
@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'fieldsForm','route'=>'api.fields.store']) !!}

            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif

            <div class="col-md-4">
                Content Fields
                <div class="form-group relative">
                    <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Content Field" value="" maxlength="30">
                </div>
            </div>

            <div class="col-md-2 padding-l-0">
                &nbsp;
                <div class="small">
                    <select name="is_public" class=" selectpicker">
                        <option value="1">Public</option>
                        <option value="0">Private</option>
                    </select>
                </div>
            </div>

            <div class="col-md-2 padding-r-0 padding-l-0">
                &nbsp;
                <div class="small">
                    {!! Form::select('content_field_type_id',$fieldTypes, 1 ,['class'=>' selectpicker']) !!}
                </div>
            </div>

            <div class="col-md-2 padding-r-0">
                &nbsp;
                <div class="small">
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-12 margin-b-10">
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-md-12">
            <div class="alert row-alert" style="display: none"></div>
            <div class="col-md-12">
                <ul class="list" id="fieldList">
                    @if(count($fields))
                        @foreach($fields as $id=>$name)
                            <li>
                                <div class="pull-left t24">
                                    <p class=margin-b-0>{{ $name }}</p>
                                </div>
                                @if(!in_array($id,[1,2,3,4]))
                                    <div class="margin-l-10 pull-left divider-left">
                                        <a href="#" data-fieldid="{{ $id }}" class="edit-field link-text padding-r-0">Edit</a>
                                    </div>
                                    <div class="margin-l-10 pull-left divider-left">
                                        <a href="#" data-fieldid="{{ $id }}" class="delete-field delete-icon link-icon"></a>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
@include('settings._fields-delete')

        <!-- Modal -->
@include('settings._fields-edit')

@section('fields-js')
    <script>

        $('#holidate').daterangepicker({
            datepickerOptions : {
                numberOfMonths : 2,
                maxDate: null,
                minDate: null
            },
            presetRanges: [],
            presets: { dateRange: "Date Range" }
        }).on('change', function(e){
            dateRange = (JSON.parse(e.currentTarget.value));
            $(this).closest('div').find('.from').val(moment(dateRange.start).format('MM/DD/Y'));
            $(this).closest('div').find('.to').val(moment(dateRange.end).format('MM/DD/Y'));
        });

        /** SEND CREATE REQUEST **/

        $('#fieldsForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){


                        if(response.status==200) {
                            refreshFieldList(response.data);
                            $(form).find('input[type=text]').val('');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        /** SEND DELETE REQUEST **/


        $('#deleteFieldTrigger').on('click', function(){
            if($('#deleteFieldConfirmInput').val()=="yes"){
                deleteField();
                $('#deleteFieldModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /** SEND UPDATE REQUEST **/

        $('#fieldEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshFieldList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshFieldList(data){
            $('#fieldList').html('');
            $('#contentTypeList').html('');
            $('#editContentTypeList').html('');

            if(data.length) {
                for(var x=0;x<data.length;x++){
                    el =
                            '<li>'+
                                '<div class="pull-left t24">'+
                                    '<p class=margin-b-0>'+data[x]['name']+'</p>'+
                                '</div>';


                    if($.inArray(data[x]['slug'],['title','article-approved','design-complete','description'])<0){
                         el +=  '<div class="margin-l-10 pull-left divider-left">'+
                                    '<a href="#" data-fieldid="'+data[x]['id']+'" class="edit-field link-text padding-r-0">Edit</a>'+
                                '</div>'+
                                '<div class="margin-l-10 pull-left divider-left">'+
                                    '<a href="#" data-fieldid="'+data[x]['id']+'" class="delete-field delete-icon link-icon"></a>'+
                                '</div>';
                    }

                    el +=    '</li>';

                    $('#fieldList').append(el);

                    el = '<li>'+
                            '<p>'+
                                '<input class="form-control" id="field-radio-'+data[x]['id']+'" name="fields[]" type="checkbox" value="'+data[x]['id']+'">'+
                                '<label for="field-radio-'+data[x]['id']+'">'+data[x]['name'];

                    if(data[x]['company_id']!=0)
                        el += data[x]['is_public'] ? ' (Public)' : ' (Private)';

                    el +=    '</p>'+
                         '</li>';

                    $('#contentTypeList').append(el);

                    el = '<li>'+
                            '<p>'+
                            '<input class="form-control" id="edit-content-radio-'+data[x]['id']+'" name="fields[]" type="checkbox" value="'+data[x]['id']+'">'+
                            '<label for="edit-content-radio-'+data[x]['id']+'">'+data[x]['name'];

                    if(data[x]['company_id']!=0)
                        el += data[x]['is_public'] ? ' (Public)' : ' (Private)';

                    el +=    '</p>'+
                            '</li>';

                    $('#editContentTypeList').append(el);
                }
                attachDeleteFieldEvent();
                attachEditFieldEvent();

                $('#fieldList').closest('.row').find('.alert').hide();
            }
            else {
                $('#fieldList').closest('.row').find('.alert').removeClass('temp-hide').show();
            }
        }


        function deleteField(){

            url = "{{ route('api.fields.delete') }}";
            data   = $('#deleteFieldModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.fields.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshFieldList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteFieldEvent(){
            $('.delete-field').on('click', function(e){

                e.preventDefault();

                fieldId = $(this).attr('data-fieldid');

                if(fieldId>0){
                    $('#deleteFieldModal').modal('show');
                    $('#deleteField').val(fieldId);
                }
            });
        }

        function attachEditFieldEvent(){
            $('.edit-field').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-fieldid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.fields.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){
                                $('#editFieldModal').modal('show');

                                $.each(response.data.field, function(i,e){
                                    $('#editFieldModal input[name='+i+']').val(e);
                                    $('#fieldEditForm select[name='+i+']').val(e).trigger('change');
                                });


                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteFieldEvent();
        attachEditFieldEvent();
    </script>
@endsection