<!-- Modal -->
<div id="deleteContentModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content text-center">
            <div class="modal-header">
                <h4 class="modal-title">Delete Content</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this? <br>
                  <span class="deleteMessage">
                      <span class="counts">You currently have XX projects and XX tasks connected to this deliverable</span>.
                      <br>By deleting this deliverable, all items mentioned above will also be deleted.
                      <br><b>This cannot be undone.</b>
                  </span>
                <br>
                Please type "yes" to confirm.<br>
                {!! Form::open(['route'=>'api.contents.delete']) !!}
                    <input type="hidden" id="deleteContent" name="id">
                    <input id="deleteContentConfirmInput" class="text-center">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer text-center-important">
                <button id="deleteContentTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>

    </div>
</div>