<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'formatTypesForm','route'=>'api.formatTypes.store']) !!}
            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif
            <div class="col-md-4 padding-r-0">
                Format Types
                <div class="form-group relative">
                    <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Format Type" value="" maxlength="30">
                </div>
            </div>

            <div class="col-md-2 padding-r-0">
                &nbsp;
                <div class="small">
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-12 margin-b-10">
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-md-12">
            <div class="alert row-alert" style="display: none"></div>
            <div class="col-md-12">
                <ul class="list" id="formatTypeList">
                    @if(count($formatTypes))
                        @foreach($formatTypes as $formatType)
                            <li>
                                <div class="pull-left t24">
                                    <p class=margin-b-0>{{ $formatType->name }}</p>
                                </div>
                                @if($formatType->company_id != 0)
                                    <div class="margin-l-10 pull-left divider-left">
                                        <a href="#" data-formatTypeid="{{ $formatType->id }}" class="edit-formatType link-text padding-r-0">Edit</a>
                                    </div>
                                    <div class="margin-l-10 pull-left divider-left">
                                        <a href="#" data-formatTypeid="{{ $formatType->id }}" class="delete-formatType delete-icon link-icon"></a>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
@include('settings._format-types-delete')
@include('settings._format-types-edit')

@section('format-types-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#formatTypesForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshFormatTypeList(response.data);
                            $(form).find('input[type=text]').val('');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        /** SEND DELETE REQUEST **/


        $('#deleteFormatTypeTrigger').on('click', function(){
            if($('#deleteFormatTypeConfirmInput').val()=="yes"){
                deleteFormatType();
                $('#deleteFormatTypeModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /** SEND UPDATE REQUEST **/

        $('#editFormatTypeForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshFormatTypeList(response.data);
                            $(form).closest('.modal').modal('hide');
                            $(form).trigger('reset');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshFormatTypeList(data){
            $('#formatTypeList').html('');

            if(data.length) {
                for(var x=0;x<data.length;x++){
                    el =
                            '<li>'+
                            '<div class="pull-left t24">'+
                            '<p class=margin-b-0>'+data[x]['name']+'</p>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-formatTypeid="'+data[x]['id']+'" class="edit-formatType link-text padding-r-0">Edit</a>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-formatTypeid="'+data[x]['id']+'" class="delete-formatType delete-icon link-icon"></a>'+
                            '</div>'+
                            '</li>';

                    $('#formatTypeList').append(el);
                }
                attachDeleteFormatTypeEvent();
                attachEditFormatTypeEvent();

                $('#formatTypeList').closest('.row').find('.alert').hide();
            }
            else {
                $('#formatTypeList').closest('.row').find('.alert').removeClass('temp-hide').show();
            }
        }


        function deleteFormatType(){

            url = "{{ route('api.formatTypes.delete') }}";
            data   = $('#deleteFormatTypeModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.formatTypes.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshFormatTypeList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteFormatTypeEvent(){
            $('.delete-formatType').on('click', function(e){

                e.preventDefault();

                formatTypeId = $(this).attr('data-formatTypeid');

                if(formatTypeId>0){
                    $('#deleteFormatTypeModal').modal('show');
                    $('#deleteFormatType').val(formatTypeId);
                }
            });
        }

        function attachEditFormatTypeEvent(){
            $('.edit-formatType').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-formatTypeid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.formatTypes.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){

                                $('#editFormatTypeModal').modal('show');

                                $.each(response.data.formatType, function(i,e){
                                    $('#editFormatTypeModal input[name='+i+']').val(e);
                                });


                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteFormatTypeEvent();
        attachEditFormatTypeEvent();
    </script>
@endsection