<div class="col-md-12">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-12">
        <ul id="contentList">
            @if(count($contents))
                @foreach($contents as $content)
                    <li>
                        <div class="color-dot padding-l-0">
                            <span style="background-color: {{ $content->color }};"></span>
                            <a href="#">{{ $content->name }}</a>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-contentid="{{ $content->id }}" class="edit-content link-text padding-r-0">Edit</a>
                        </div>
                        <div class="margin-l-10 pull-left divider-left">
                            <a href="#" data-contentid="{{ $content->id }}" class="delete-content delete-icon link-icon"></a>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>