<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'mediaTypesForm','route'=>'api.mediaTypes.store']) !!}
            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif
            <div class="col-md-4 padding-r-0">
                Media Types
                <div class="form-group relative">
                    <input type="text" class="form-control margin-b-5" name="name" placeholder="Add Media Type" value="" maxlength="30">
                </div>
            </div>

            <div class="col-md-2 padding-r-0">
                &nbsp;
                <div class="small">
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-12 margin-b-10">
                {!! Form::close() !!}
            </div>
        </div>

        <div class="col-md-12">
            <div class="alert row-alert" style="display: none"></div>
            <div class="col-md-12">
                <ul class="list" id="mediaTypeList">
                    @if(count($mediaTypes))
                        @foreach($mediaTypes as $mediaType)
                            <li>
                                <div class="pull-left t24">
                                    <p class=margin-b-0>{{ $mediaType->name }}</p>
                                </div>
                                @if($mediaType->company_id != 0)
                                    <div class="margin-l-10 pull-left divider-left">
                                        <a href="#" data-mediaTypeid="{{ $mediaType->id }}" class="edit-mediaType link-text padding-r-0">Edit</a>
                                    </div>
                                    <div class="margin-l-10 pull-left divider-left">
                                        <a href="#" data-mediaTypeid="{{ $mediaType->id }}" class="delete-mediaType delete-icon link-icon"></a>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
@include('settings._media-types-delete')
@include('settings._media-types-edit')

@section('media-types-js')
    <script>

        /** SEND CREATE REQUEST **/

        $('#mediaTypesForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshMediaTypeList(response.data);
                            $(form).find('input[type=text]').val('');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        /** SEND DELETE REQUEST **/


        $('#deleteMediaTypeTrigger').on('click', function(){
            if($('#deleteMediaTypeConfirmInput').val()=="yes"){
                deleteMediaType();
                $('#deleteMediaTypeModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /** SEND UPDATE REQUEST **/

        $('#editMediaTypeForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshMediaTypeList(response.data);
                            $(form).closest('.modal').modal('hide');
                            $(form).trigger('reset');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshMediaTypeList(data){
            $('#mediaTypeList').html('');

            if(data.length) {
                for(var x=0;x<data.length;x++){
                    el =
                            '<li>'+
                            '<div class="pull-left t24">'+
                            '<p class=margin-b-0>'+data[x]['name']+'</p>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-mediaTypeid="'+data[x]['id']+'" class="edit-mediaType link-text padding-r-0">Edit</a>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-mediaTypeid="'+data[x]['id']+'" class="delete-mediaType delete-icon link-icon"></a>'+
                            '</div>'+
                            '</li>';

                    $('#mediaTypeList').append(el);
                }
                attachDeleteMediaTypeEvent();
                attachEditMediaTypeEvent();

                $('#mediaTypeList').closest('.row').find('.alert').hide();
            }
            else {
                $('#mediaTypeList').closest('.row').find('.alert').removeClass('temp-hide').show();
            }
        }


        function deleteMediaType(){

            url = "{{ route('api.mediaTypes.delete') }}";
            data   = $('#deleteMediaTypeModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.mediaTypes.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshMediaTypeList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }

        function attachDeleteMediaTypeEvent(){
            $('.delete-mediaType').on('click', function(e){

                e.preventDefault();

                mediaTypeId = $(this).attr('data-mediaTypeid');

                if(mediaTypeId>0){
                    $('#deleteMediaTypeModal').modal('show');
                    $('#deleteMediaType').val(mediaTypeId);
                }
            });
        }

        function attachEditMediaTypeEvent(){
            $('.edit-mediaType').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-mediaTypeid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.mediaTypes.get') }}?id='+stageId,
                        success: function(response){


                            if(response.status==200){

                                $('#editMediaTypeModal').modal('show');

                                $.each(response.data.mediaType, function(i,e){
                                    $('#editMediaTypeModal input[name='+i+']').val(e);
                                });


                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteMediaTypeEvent();
        attachEditMediaTypeEvent();
    </script>
@endsection