@section('department-style')
@endsection

<hr>
<div class="row">
    <div class="row">
        <div class="col-md-8">
            {!! Form::open(['id'=>'contentsForm','route'=>'api.contents.store']) !!}

            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif

            <div class="col-md-12">
                Contents <span class="info-icon"  data-toggle="tooltip" title="Deliverables are products or services that you deliver to your customers. For example, an advertising agency will provide print adverts, campaigns, TV ads, radio ads, etc. "></span>
                <div class="form-group">
                    <input type="text" class="form-control margin-b-5 pull-left" style="width: 190px" name="name" placeholder="Add a Content" value="" required="required" maxlength="30">
                    <input id="contentColor" class="form-control" name='color' value='#3355cc'/>
                    {!! Form::submit('Add',['class'=>'link-bt t-pink link-color margin-b-0 divider-left']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <ul class="radio-list clearfix" id="contentTypeList">
                    @foreach($contentFields as $index=>$field)
                        <li>
                            <p>
                                {{ Form::input('checkbox','fields[]',$field->id,['class'=>'form-control','id'=>'field-radio-'.$field->id]) }}
                                <label for="field-radio-{{$field->id}}">
                                    {{$field->name}}
                                    @if($field->company_id !=0)
                                         ({{ $field->is_public ? "Public" : 'Private' }})
                                    @endif
                                </label>
                            </p>
                        </li>
                    @endforeach
                </ul>
            </div>


            <div class="col-md-12">
                {!! Form::close() !!}
            </div>

            <div class="col-md-6">
                <div class="alert alert-danger {{ !count($contents) ? '' : 'temp-hide' }}">Please create at least one content (for example, brochure, blog post, HTML mailer, report, etc.)</div>
            </div>
        </div>

        @include('settings._contents-list')

    </div>
</div>


<!-- Modal -->
@include('settings._contents-delete')

        <!-- Modal -->
@include('settings._contents-edit')

@section('contents-js')
    <script>


        $("#contentColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#contentColor').val(color.toHexString()).attr('value',color.toHexString()); // #ff0000
            }
        }).val(colors[0]).attr('value',colors[0]);

        $("#contentEditColor").spectrum({
            showPaletteOnly: true,
            showPalette:true,
            hideAfterPaletteSelect:true,
            color: colors[0],
            palette: [
                colors
            ],
            change: function(color) {
                $('#contentEditColor').val(color.toHexString()).attr('value',color.toHexString()); // #ff0000
            }
        }).val(colors[0]).attr('value',colors[0]);



        /** SEND CREATE REQUEST **/

        $('#contentsForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshContentList(response.data);
                            $(form).find('input[type=text]').val('');
                        }

                        doAlert(response);

                        $(submitBt).removeAttr('disabled');
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });

        /** SEND UPDATE REQUEST **/

        $('#contentsEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshStageList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /** SEND UPDATE REQUEST **/

        $('#contentEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response){

                        if(response.status==200) {
                            refreshContentList(response.data);
                            $(form).trigger('reset');
                            $(form).closest('.modal').modal('hide');
                        }

                        doAlert(response);
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshContentList(data){
            $('#contentList').html('');

            if(data.length) {
                for(var x=0;x<data.length;x++){

                    el =
                        '<li>'+
                            '<div class="color-dot padding-l-0">'+
                                '<span style="background-color: '+data[x]['color']+';"></span>'+
                                '<a href="#">'+data[x]['name']+'</a>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                                '<a href="#" data-contentid="'+data[x]['id']+'" class="edit-content link-text padding-r-0">Edit</a>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                                '<a href="#" data-contentid="'+data[x]['id']+'" class="delete-content delete-icon link-icon"></a>'+
                            '</div>'+
                        '</li>';

                    $('#contentList').append(el);
                }
                attachDeleteContentEvent();
                attachEditContentEvent();

                $('#contentList').closest('.row').find('.alert').hide();
                checkRequirements();
            }
            else {
                $('#contentList').closest('.row').find('.alert').removeClass('temp-hide').show();
                checkRequirements();
            }
        }


        function deleteContent(){

            url = "{{ route('api.contents.delete') }}";
            data   = $('#deleteContentModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.contents.delete') }}',
                    data: data,
                    success: function(response){


                        if(response.status==200){
                            refreshContentList(response.data);
                        }

                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        }



        /** SEND DELETE REQUEST **/


        $('#deleteContentTrigger').on('click', function(){
            if($('#deleteContentConfirmInput').val()=="yes"){
                deleteContent();
                $('#deleteContentModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });

        function attachDeleteContentEvent(){
            $('.delete-content').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-contentid');

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.contents.get') }}?id='+stageId,
                        success: function(response){
                            if(response.status==200){

//                                tasksCount = response.data.tasksCount;
//                                taskS = tasksCount.length == 1 ? '' : 's' ;
//                                projectsCount = response.data.projectsCount;
//                                projectS = projectsCount.length == 1 ? '' : 's' ;

//                                if(tasksCount || projectsCount){
//                                    $('#deleteContentModal .deleteMessage .counts').html('You currently have <b>'+projectsCount+' project'+projectS+'</b> and <b>'+tasksCount+' task'+taskS+'</b> connected to this deliverable.');
//                                    $('#deleteContentModal .deleteMessage').show();
//                                }
//                                else
//                                    $('#deleteContentModal .deleteMessage').hide();

                                $('#deleteContentModal').modal('show');
                                $('#deleteContent').val(stageId);

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });

                }
            });
        }

        function attachEditContentEvent(){
            $('.edit-content').on('click', function(e){

                e.preventDefault();

                stageId = $(this).attr('data-contentid');

                $('#editContentTypeList input[name="fields[]"]').each(function(i,e){
                    $(e).prop('checked',false);
                });

                if(stageId>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.contents.get') }}?id='+stageId,
                        success: function(response){

                            if(response.status==200){
                                $('#editContentModal').modal('show');

                                $('#contentEditColor').attr('value', response.data.content.color); // #ff0000
                                $('#editContentModal .sp-preview-inner').css('background-color', response.data.content.color); // #ff0000

                                $.each(response.data.content.fields, function(i,e){
                                    $('#editContentModal input[id=edit-content-radio-'+e.id+']').prop('checked',true);
                                });

                                $.each(response.data.content, function(i,e){
                                    $('#editContentModal input[name='+i+']').val(e);
                                });

                            } else {
                                doAlert(response);
                            }

                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachDeleteContentEvent();
        attachEditContentEvent();
    </script>
@endsection