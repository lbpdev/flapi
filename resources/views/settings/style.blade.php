<hr>
<div class="row">
    <div class="row">
        <div class="col-md-6">
            @inject('optionService','App\Services\OptionService')

            <?php
                $option = $optionService->getOption('flatplan_style');
            ?>
            <div class="col-md-6">
                Flatplan Style
                <ul class="list margin-t-10">
                     <li>
                         {{ Form::input('radio','flatplan-style','traditional',[ ($option ? ( $option->value == 'traditional' ? 'checked=checked' : '' ) : '' ), 'data-id'=>Auth::user()->company->id,'class'=>'select-fp-style form-control','id'=>'style-radio-1']) }}
                         <label for="style-radio-1">Traditional</label>
                     </li>
                     <li>
                        {{ Form::input('radio','flatplan-style','modern',[ ($option ? ( $option->value == 'modern' ? 'checked=checked' : '' ) : '' ), 'data-id'=>Auth::user()->company->id,'class'=>'select-fp-style form-control','id'=>'style-radio-2']) }}
                        <label for="style-radio-2">Modern</label>
                     </li>
                </ul>
            </div>
        </div>

    </div>
</div>

@section('select-style-js')
    <script>
        deleteProcessFlag = 1;
        $('.select-fp-style').on('click', function(e){

            style = $(this).val();

            if(deleteProcessFlag){
                deleteProcessFlag = 0;

                $.ajax({
                    type: "GET",
                    url: '{{ route('api.options.update') }}?value='+style+'&name=flatplan_style',
                    success: function(response){
                        if(response.status==200){

                        } else {
                            doAlert(response);
                        }
                    },
                    complete : function (){
                        deleteProcessFlag = 1;
                    }
                });
            }
        });
    </script>
@endsection