<hr>
<div class="row">
    <div class="row">
        <div class="col-md-6">
            {!! Form::open(['id'=>'rolesForm','route'=>'api.roles.store']) !!}

            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif

            <div class="col-md-6">
                Roles
                <input type="text" class="form-control" name="name" placeholder="Add a Role" value="" required="required">
            </div>
            <div class="col-md-2 padding-r-0">
                &nbsp;
                <div class="small margin-b-7 relative" id="icon_select_wrap">
                    {!! Form::select('icon',$userIcons, 0 ,['id'=>'icon_select','class'=>' selectpicker','title'=>'']) !!}
                    <img src="" height="22" id="role_icon">
                </div>
            </div>

            <div class="col-md-12">
                What user is allowed to view:
                <ul class="radio-list clearfix" id="viewPermissions">
                @foreach($permissions['view'] as $index=>$permission)
                    <li>
                        <p>
                            {{ Form::input('checkbox','views[]',$permission->id,['class'=>'form-control','id'=>'permission-radio-'.$permission->id]) }}
                            <label for="permission-radio-{{$permission->id}}">{{$permission->name}}</label>
                        </p>
                    </li>
                @endforeach
                </ul>
            </div>
            <div class="col-md-12">
                What user is allowed to modify:
                <ul class="radio-list clearfix margin-b-0" id="modifyPermissions">
                @foreach($permissions['modify'] as $permission)
                    <li>
                        <p>
                            {{ Form::input('checkbox','modifies[]',$permission->id,['class'=>'form-control','id'=>'permission-radio-'.$permission->id]) }}
                            <label for="permission-radio-{{$permission->id}}">{{$permission->name}}</label>
                        </p>
                    </li>
                @endforeach
                </ul>
            </div>

            <div class="col-md-3 text-left margin-t-5">
                {!! Form::submit('Create',['class'=>'link-bt link-color margin-b-20 t-pink']) !!}
                {!! Form::close() !!}
            </div>
        </div>

        @include('settings._roles-list')
        @include('settings._roles-edit-modal')
        @include('settings._roles-delete-modal')
    </div>
</div>

@section('roles-js')
    <script>

        refreshRadioEvent();

        var appUrl = '{{ url('/') }}';

        $('#icon_select').on('change', function(){
            $('#role_icon').attr('src','{{ url('public/img/icons/user-icons') }}/'+$(this).val());
        }).trigger('change');

        $(window).load(function(){
            $('#icon_select option').each(function(i,e){
                $('#icon_select_wrap li span.text').eq(i).append('<img height="20" src="'+appUrl+'/public/img/icons/user-icons/'+$(this).val()+'">');
            });
        });

        /** SEND CREATE REQUEST **/

        $('#rolesForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        refreshRolesList(response.data);
                        refreshRolesSelect(response.data)
                        $(form).find('input[type=text]').val('');
                        refreshRadioEvent();
                        attachEditRoleEvent();
                        attachDeleteUserEvent;
                    }

                    doAlert(response);
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });

        /** SEND CREATE REQUEST **/

        $('#rolesEditForm').on('submit', function(e){
            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();

            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){
                    if(response.status==200) {
                        refreshRolesList(response.data);
                        refreshRolesSelect(response.data)
                        refreshRadioEvent();
                        attachEditRoleEvent();
                        attachDeleteRoleEvent();

                        $(form).trigger('reset');
                        $(form).closest('.modal').modal('hide');
                    }

                    doAlert(response);
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });

        $('#deleteRoleTrigger').on('click', function(){
            if($('#deleteRoleConfirmInput').val()=="yes"){
                deleteRole(); 
                $('#deleteRoleModal').modal('hide');
            } else {
                alert('Please follow the instructions');
            }
        });


        /*********************************** FUNCTIONS **************************/

        function refreshRolesList(data){
            $('#rolesList').html('');      

            for(var x=0;x<data.length;x++){

                el = '<li>'+
                     '<div class="pull-left t24">'+
                        '<img src="'+appUrl+'/public/img/icons/user-icons/'+data[x]['icon']+'" height="22" class="margin-r-5">'+
                        '<p class="margin-b-0">'+data[x]['name']+'</p>'+
                      '</div>';

                if($.inArray(data[x]['slug'],['admin','client'])<0)
                {

                    el += '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-id="'+data[x]['id']+'" class="edit-role link-text padding-r-0">Edit</a>'+
                            '</div>'+
                            '<div class="margin-l-10 pull-left divider-left">'+
                            '<a href="#" data-id="'+data[x]['id']+'" class="delete-role delete-icon link-icon"></a>'+
                          '</li>';

                }

                $('#rolesList').append(el);
            }
        }

        function refreshRolesSelect(data){
            $('#role_select').find('option').remove();

            for(var x=0;x<data.length;x++){
                el = '<option value="'+data[x]['id']+'">'+data[x]['name']+'</option>';

                $('#role_select').append(el);
            }

            $('.selectpicker').selectpicker('refresh');
        }

        function refreshRadioEvent(){
            $('#rolesList input[type=radio]').on('click', function(){
                $('#deleteRole').attr('data-roleid', $(this).val());
            });
        }

        function deleteRole(){
            url = "{{ route('api.roles.delete') }}";
            data   = $('#deleteRoleModal form').serialize();
            deleteProcessFlag = 1;

            if(deleteProcessFlag) {
                deleteProcessFlag = 0;

                $.ajax({
                    type: "POST",
                    url: '{{ route('api.roles.delete') }}',
                    data: data,
                    success: function(response){

                        if(response.status==200){
                            $('#deleteRoleModal input[name=id]').val(roleId);
                            refreshRadioEvent();
                            refreshRolesList(response.data);
                            refreshRolesSelect(response.data);
                        }


                        doAlert(response);
                    },
                    done : function (){
                        deleteProcessFlag = 1;
                    }
                });

            } 
        }

        $('#edit_icon_select').on('change', function(){
            $('#edit_role_icon').attr('src','{{ url('public/img/icons/user-icons') }}/'+$(this).val());
        }).trigger('change');

        function attachEditRoleEvent(){
            $('.edit-role').on('click', function(e){

                e.preventDefault();

                id = $(this).attr('data-id');

                if(id>0){

                    $.ajax({
                        type: "GET",
                        url: '{{ route('api.roles.get') }}?id='+id,
                        success: function(response){

                            if(response.status==200){

                                $('#editRoleModal').modal('show');

                                $('#editRoleModal input[name=name]').val(response.data.name);
                                $('#editRoleModal input[name=id]').val(response.data.id);
                                $('#editRoleModal select[name=icon]').val(response.data.icon).selectpicker('refresh').trigger('change');

                                $.each(response.data.permissions, function(i,e){
                                    $('#editRoleModal input[id=edit-permission-radio-'+e.id+']').prop('checked',true);
                                });

                            } else {
                                doAlert(response);
                            }

                            $('#edit_icon_select option').each(function(i,e){
                                $('#edit_icon_select_wrap li span.text').eq(i).append('<img height="20" src="'+appUrl+'/public/img/icons/user-icons/'+$(this).val()+'">');
                            });
                        },
                        done : function (){
                            deleteProcessFlag = 1;
                        }
                    });


                }
            });
        }

        attachEditRoleEvent();

        function attachDeleteRoleEvent(){

            /** SEND DELETE REQUEST **/

            $('.delete-role').on('click', function(e){
                e.preventDefault();

                roleId = $(this).attr('data-id');

                if(roleId>0){
                    $('#deleteRoleModal').modal('show');
                    $('#deleteRoleModal input[name=id]').val(roleId);
                }
                else
                    alert('Please select a role to delete.');
            });
        }

        attachDeleteRoleEvent();

    </script>
@endsection