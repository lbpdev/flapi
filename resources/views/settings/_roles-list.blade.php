<div class="col-md-12">
    <div class="alert row-alert" style="display: none"></div>
    <div class="col-md-12">
        <ul id="rolesList" class="list">
            @if(count($roles))
                @foreach($roles as $index=>$role)
                    <li>
                        <div class="pull-left t24">
                            <img src="{{ asset('public/img/icons/user-icons').'/'.$role['role']['icon'] }}" height="22" class="margin-r-5">
                            <p class="margin-b-0">{{ $role['role']['name'] }}</p>
                        </div>
                        <!-- Make Admin and Client Role Permanent -->
                        @if(!in_array($role['role']['id'],[1,5]))
                            <div class="margin-l-10 pull-left divider-left">
                                <a href="#" data-id="{{ $role['role']['id'] }}" class="edit-role link-text padding-r-0">Edit</a>
                            </div>
                            <div class="margin-l-10 pull-left divider-left">
                                <a href="#" data-id="{{ $role['role']['id'] }}" class="delete-role delete-icon link-icon"></a>
                            </div>
                        @endif
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>