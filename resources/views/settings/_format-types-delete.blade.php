<!-- Modal -->
<div id="deleteFormatTypeModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">

        <!-- Modal content-->
        <div class="modal-content text-center">
            <div class="modal-header">
                <h4 class="modal-title">Delete Format Type</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this? Please type "yes" to confirm.<br>
                {!! Form::open(['route'=>'api.formatTypes.delete']) !!}
                    <input type="hidden" id="deleteFormatType" name="id">
                    <input id="deleteFormatTypeConfirmInput" class="text-center">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer text-center-important">
                <button id="deleteFormatTypeTrigger" class="createbt btn btn-default confirmBt">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>

        </div>

    </div>
</div>