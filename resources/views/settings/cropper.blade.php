<div id="cropperModal" class="modal fade delete" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Delete Task Type</h4>
            </div>
            <div class="modal-body text-center">
                <img src="" id="crop-image" width="100%">
            </div>
            <div class="modal-footer  text-center-important">
                <button class="btn btn-default" id="saveCrop">Done</button>
            </div>

        </div>

    </div>
</div>
