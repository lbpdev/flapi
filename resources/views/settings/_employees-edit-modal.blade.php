<div id="editUserModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 300px">
        <!-- Modal content-->
        <div class="modal-content clearfix">
        {!! Form::open(['id'=>'userEditForm','route'=>'api.users.update','class'=>'']) !!}
            @if(isset($company_id))
                {!! Form::hidden('company_id',$company_id) !!}
            @endif
          <div class="modal-header clearfix">
            <h4 class="modal-title">Edit User</h4>
          </div>
          <div class="modal-body clearfix width-full">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" value="0">
                    Name:
                    <input type="text" class="form-control" name="name" placeholder="Full Name" maxlength="30" value="" required="required">
                    Role:
                    <div class="small margin-b-7">
                        {!! Form::select('role_id',$roleList, null ,['id'=>'edit_user_role_select','class'=>'selectpicker']) !!}
                    </div>
                    Email:
                    <input type="text" class="form-control" name="email" maxlength="30" placeholder="Email Address" value="" required="required">


                </div>
            </div>

          </div>
          <div class="modal-footer clearfix pull-left width-full">
                {!! Form::submit('Save',['id'=>'sendInvite','class'=>'btn btn-default margin-tb-0']) !!}
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        {!! Form::close() !!}
        </div>
      </div>
</div>