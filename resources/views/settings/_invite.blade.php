<hr>
<div class="row">
    {!! Form::open(['id'=>'inviteForm','route'=>'api.users.invite']) !!}

    @if(isset($company_id))
        {!! Form::hidden('company_id',$company_id) !!}
    @endif

    <div class="row">
        <div class="col-md-5">
            @inject('subService','App\Services\SubscriptionService')

            <?php  $subscription = $subService->getSubscription(); ?>

            <div class="col-md-6">
                {{--Invite New Staff <span id="user-limit">(User Limit: {{ count(Auth::user()->company->activeUsers) }}/{{ $subscription->plan->max_users }})</span>--}}
                Invite New Staff
                <input type="text" class="form-control" name="name" placeholder="Full Name" value="" required="required">
            </div>
            <div class="col-md-6 padding-r-0 padding-l-0">
                &nbsp;
                {!! Form::select('role_id',$roleList, null ,['id'=>'role_select','class'=>'form-control selectpicker']) !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-5">
            <div class="col-md-6">
                <input type="text" class="form-control margin-tb-0" name="email" placeholder="Email Address" value="" required="required">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class=" padding-r-0" id="inviteBt">
                <div class="pull-right">
                      {!! Form::submit('Send Invite',['id'=>'sendInvite','class'=>'pull-right t-pink padding-r-0 link-bt link-color margin-tb-0 margin-t-n10']) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

    <!-- Modal -->
    <div id="deleteModal" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Session Extension</h4>
          </div>
          <div class="modal-body">
            Are you sure you want to delete this? Please type "yes" to confirm.
            <input id="deleteConfirmInput">
          </div>
          <div class="modal-footer">
                <button id="deleteRoleTrigger" class="createbt btn btn-default">Confirm</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>

        </div>

      </div>
    </div>

@section('invite-js')
    <script>

        $('#role_select').on('change',function(){
            if($(this).val()==2)
                $('#invite-scheduler-for').show();
            else
                $('#invite-scheduler-for').hide();
        });

        $('#role_select').val(1).trigger('change');

        if($('.col-user .employee').length < 1)
            $('#inviteForm').find('.alert').show();

        /** SEND CREATE REQUEST **/

        $('#inviteForm').on('submit', function(e){

            submitBt = $(this).find('input[type=submit]');
            $(submitBt).attr('disabled','disabled');

            var form = $(this);
            processFlag = 1;

            e.preventDefault();
                e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            if(processFlag) {
                processFlag = 0;

                $.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function(response){

                    if(response.status==200) {
                        $(form).trigger("reset");

                        employeeClass =  response.data.role[0].slug;
                        activeClass =  response.data.is_active==1 ? 'active' : '';
                        activity =  response.data.is_active==1 ? 'Deactivate' : 'Activate';

                        element =
                                '<div class="col-md-12 padding-5-10 padding-l-0 margin-b-3" id="user-row-'+response.data.id+'">'+
                                    '<div class="pull-left padding-r-5">'+
                                        '<img class="role-icon '+ employeeClass +' active" src="{{ asset('public/img/icons/user-icons/') }}/'+response.data.role[0].icon+'" height="18">'+
                                '</div>'+
                                '<div class="pull-left">'+
                                '<p>'+response.data.name+'</p>'+
                                '</div>'+
                                '<div class="pull-left">'+
                                '<a href="#" data-userid="'+response.data.id+'" class="divider-left link-text edit-user">Edit</a>'+
                                '<a href="#" data-userid="'+response.data.id+'" class="divider-left toggle-user link-text">'+activity+'</a>'+
                                '<a href="#" data-userid="'+response.data.id+'" class="divider-left delete-user link-text">Delete</a>'+
                                '<p class="last-login"></p>'+
                                '</div>'+
                                '</div>';

                        $('#employeesList-'+response.data.role[0].id).find('.placeholder').remove();
                        $('#employeesList-'+response.data.role[0].id).append(element);

                        refreshUserLimit();
                    }

                  doAlert(response);
                  attachDeleteUserEvent();
                  attachEditUserEvent();
                  attachToggleUserEvent();


                  if($('.col-user .employee').length){
                      $('#inviteForm').find('.alert').removeClass('temp-hide').hide();
                      checkRequirements();
                  }
                  else
                      checkRequirements();


                  $(submitBt).removeAttr('disabled');
                  },
                  done : function (){
                    processFlag = 1;
                  }
                });
            }
        });


        function refreshUserLimit(){

            $.ajax({
                type: "GET",
                url: '{{ route('api.company.getUserLimit') }}?id={{ Auth::user()->company->id }}',
                success: function(response){


                    if(response.max_users)
                        $('#user-limit').html('(User Limit: '+ response.current_users +'/'+ response.max_users+')');

                    checkRequirements();
                },
                done : function (){
                    processFlag = 1;
                }
            });
        }


    </script>
@endsection
