@extends('backend.master')


@section('custom-css')
    <style>
        hr {
            display: inline-block;
            width: 100%;
        }

        .col-md-8 {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ isset($options['name']) ? $options['name'] : '' }}
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">

        <div class="col-md-12 relative">

            @include('pages.settings._info')
            <h3>Holidays</h3>
            @include('pages.settings._holidays')
            <h3>Departments</h3>
            @include('pages.settings._department')
            <h3>Employees</h3>
            @include('pages.settings._employees')
            <h3>Services</h3>
            @include('pages.settings._stages')
            <h3>Formats</h3>
            @include('pages.settings._formats')
            <h3>Clients</h3>
            @include('pages.settings._clients')
            <hr>

        </div>
    </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/js/country_city.js') }}"></script>
    <script>
        $('.daterangepicker').daterangepicker({
            datepickerOptions : {
                numberOfMonths : 2,
                maxDate: null,
                minDate: null
            },
            presetRanges: [],
            presets: { dateRange: "Date Range" }
        }).on('change', function(e){
            dateRange = (JSON.parse(e.currentTarget.value));
            $(this).closest('div').find('.hidden .project-start').val(moment(dateRange.start).format('MM/DD/Y'));
            $(this).closest('div').find('.hidden .project-end').val(moment(dateRange.end).format('MM/DD/Y'));
            $(this).closest('div').find('.project-start').first().val(moment(dateRange.start).format('MM/DD/Y') + ' - ' +moment(dateRange.end).format('MM/DD/Y'));

            attachDatePickerListeners(moment(dateRange.end).format('MM/DD/Y'),moment(dateRange.start).format('MM/DD/Y'));
        });

        var colors = JSON.parse('{!! $colors !!}');
        var requirements = 0;
        var workHours = {{ isset($options['workhours']) ? ( $options['workhours'] ? $options['workhours'] : false ) : 8  }};


    </script>

    @yield('info-js')
    @yield('roles-js')
    @yield('invite-js')
    @yield('department-js')
    @yield('employees-js')
    @yield('stages-js')
    @yield('formats-js')
    @yield('clients-js')
    @yield('keys-js')
    @yield('holidays-js')

    <script>
        $(window).load(function(){
            $('.modal.delete input').keypress(function(e) {

                if(e.which == 13) {
                    e.preventDefault();
                    e.stopPropagation();

                    $(this).closest('.modal-dialog').find('.confirmBt').trigger('click');
                }

            });
        });

        $('.modal.delete').on('shown.bs.modal',function(){
            $(this).find('input.text-center').val('');
        });

        var globalAlert = $('#globalAlert');

        function doAlert(response){
            $(globalAlert).clearQueue().finish();

            if(response.status==200)
                globalAlert.addClass('alert-success').text(response.message).show();
            else
                globalAlert.addClass('alert-danger').text(response.message).show();

            setTimeout(function(){
                globalAlert.fadeOut(500,function(){ $(this).attr('class','alert row-alert').text(''); });
            },4000);
        }

        function checkRequirements(){
            return true;
        }

    </script>
@endsection











