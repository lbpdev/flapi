@extends('backend.master')

@section('custom-css')

@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Users
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            @if(Session::has('message'))
                <div class="col-md-12">
                    <div class="row pull-left alert alert-{{ Session::get('alert') }}">
                        {{ Session::get('message') }}
                    </div>
                </div>
            @endif

            <h4>{{ $data->name }}</h4>
            <div class="row">
                <div class="col-md-5">
                    {!! Form::open(['id'=>'userEditForm','route'=>'backend.users.update','class'=>'']) !!}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    Name:
                    <input type="text" class="form-control" name="name" placeholder="Full Name" maxlength="30" value="{{ $data->name }}" required="required">
                    Role:
                    <div class="small margin-b-7">
                        {!! Form::select('role_id',$roleList, $data->role->id ,['id'=>'role_select','class'=>'selectpicker']) !!}
                    </div>
                    Email:
                    <input type="text" class="form-control" name="email" maxlength="30" placeholder="Email Address" value="{{ $data->email }}" required="required">

                    {!! Form::submit('Save',['id'=>'sendInvite','class'=>'btn btn-default margin-tb-0']) !!}
                    {!! Form::close() !!}
                </div>
                <div class="col-md-6 col-md-offset-1" id="user-row-{{$data->id}}">
                    <p>Email: {{ $data->email }}</p>
                    <p>Status: {{ $data->is_active ? 'Active' : 'Disabled' }}</p>
                    <p>Company: {{ $data->company->name }}</p>
                    <p>Role: {{ $data->role->name }}</p>
                    <p>Last Login: {{ $data->lastLogin ? $data->lastLogin->created_at->format('d-m-Y h:i') : 'N/A' }}</p>
                    <a href="#" data-userid="{{ $data->id }}" class="toggle-user link-text">{{ $data->is_active ? 'Deactivate' : 'Activate' }}</a>
                    <a class="divider-left link-text" onclick="return confirm('Are you sure you want to delete user?')" href="{{ route('backend.users.delete',$data->id) }}">Delete</a>
                </div>
            </div>

        </div>
    </div>

</section>
<!-- /.content -->

@include('settings._employees-delete')

@endsection

@section('custom-js')
    <script src="{{ asset('public/admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('table').DataTable({
            "order": [[ 3, "desc" ]],
            "pageLength": 14,
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        function toggleUser(user_id){

            url = "{{ route('api.users.toggleStatus') }}";
            toggleProcessFlag = 1;

            if(toggleProcessFlag) {
                toggleProcessFlag = 0;

                if(user_id){
                    $.ajax({
                        type: "GET",
                        url: url+'?user_id='+user_id,
                        success: function(response){

                            if(response.status==200){

                                if(response.data>0){
                                    $('#user-row-'+user_id).find('.toggle-user').html('Deactivate');
                                    $('#user-row-'+user_id).find('img.role-icon').addClass('active');
                                    $('#user-row-'+user_id).removeClass('inactive');
                                }
                                else{
                                    $('#user-row-'+user_id).find('.toggle-user').html('Activate');
                                    $('#user-row-'+user_id).find('img.role-icon').removeClass('active');
                                    $('#user-row-'+user_id).addClass('inactive');
                                }
                            }

                            doAlert(response);

                            if($('#departments .employee.active').length < 1){
                                $('#inviteForm').find('.alert').show();
                                checkRequirements();
                            } else{
                                $('#inviteForm').find('.alert').hide();
                                checkRequirements();
                            }
                            refreshUserLimit();
                        },
                        done : function (){
                            toggleProcessFlag = 1;
                        }
                    });
                } else {
                }
            }
        }

        function attachToggleUserEvent(){
            $('.toggle-user').unbind('click');
            $('.toggle-user').on('click', function(e){
                e.preventDefault();
                userid = $(this).attr('data-userid');

                if(userid)
                    toggleUser(userid);
            });
        }

        attachToggleUserEvent();

    </script>
@endsection