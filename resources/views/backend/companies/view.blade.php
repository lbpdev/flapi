@extends('backend.master')


@section('custom-css')
    <style>
        hr {
            display: inline-block;
            width: 100%;
        }

        .col-md-8 {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ isset($options['name']) ? $options['name'] : '' }}
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">

        <div class="col-md-12 relative">
            @include('settings._info')
            <h3>Roles</h3>
            @include('settings._roles')
            <h3>Invite</h3>
            @include('settings._invite')
            <h3>Users</h3>
            @include('settings._employees')
            <h3>Contents</h3>
            @include('settings._contents')
            <h3>Fields</h3>
            @include('settings._fields')
            <h3>Media Types</h3>
            @include('settings._media-types')
            <h3>Format Types</h3>
            @include('settings._format-types')
            <hr>
        </div>
    </div>
    </div>
</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/js/country_city.js') }}"></script>
    <script src="{{ asset('public/plugins/bgrins-spectrum/spectrum.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js') }}"></script>
    <script src='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.js') }}'></script>
    <script src='{{ asset('public/js/jquery.Jcrop.min.js') }}'></script>
    <script>

        $('.daterangepicker').daterangepicker({
            datepickerOptions : {
                numberOfMonths : 2,
                maxDate: null,
                minDate: null
            },
            presetRanges: [],
            presets: { dateRange: "Date Range" }
        }).on('change', function(e){
            dateRange = (JSON.parse(e.currentTarget.value));
            $(this).closest('div').find('.hidden .project-start').val(moment(dateRange.start).format('MM/DD/Y'));
            $(this).closest('div').find('.hidden .project-end').val(moment(dateRange.end).format('MM/DD/Y'));
            $(this).closest('div').find('.project-start').first().val(moment(dateRange.start).format('MM/DD/Y') + ' - ' +moment(dateRange.end).format('MM/DD/Y'));

            attachDatePickerListeners(moment(dateRange.end).format('MM/DD/Y'),moment(dateRange.start).format('MM/DD/Y'));
        });

        var colors = JSON.parse('{!! $colors !!}');
        var requirements = 0;
        var workHours = {{ isset($options['workhours']) ? ( $options['workhours'] ? $options['workhours'] : false ) : 8  }};

        function checkRequirements(){

//            if( $('#roles .employee.active').length && $('#formatList li').length){
//                $('#main-alert').hide();
//                $('#sched-link').removeClass('temp-hide').show();
//                $('#report-link').removeClass('temp-hide').show();
//            } else {
//                $('#main-alert').show();
//                $('#sched-link').addClass('temp-hide').hide();
//                $('#report-link').addClass('temp-hide').hide();
//            }
        }

        checkRequirements();

        $('#termsBt').on('click', function(){
            $('#termsModal').modal('show');
        });

        $('#employees').on('click', function(){
            $('#plansModal').modal('show');
        });

        //        fixAlert();
    </script>

    @yield('info-js')
    @yield('roles-js')
    @yield('invite-js')
    @yield('employees-js')
    @yield('notifications-js')
    @yield('contents-js')
    @yield('fields-js')
    @yield('select-style-js')
    @yield('media-types-js')
    @yield('format-types-js')

    <script>
        $(window).load(function(){
            $('.modal.delete input').keypress(function(e) {

                if(e.which == 13) {
                    e.preventDefault();
                    e.stopPropagation();

                    $(this).closest('.modal-dialog').find('.confirmBt').trigger('click');
                }

            });

            $('#reportsInterval').trigger('change');
        });

        $('.modal.delete').on('shown.bs.modal',function(){
            $(this).find('input.text-center').val('');
        });

    </script>

@endsection











