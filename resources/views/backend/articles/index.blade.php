@extends('admin.master')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.css">
@endsection

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ isset($category) ? $category->name : 'Articles' }}
        <small>Control panel</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            <a href="{{ route('admin.articles.create', isset($category) ? ['category'=>$category->id,'cat_name'=>$category->name] : '') }}">+ Add New</a>
            @if(Session::has('success'))
                <div class="col-md-12 alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <table width="100%" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th>Category</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{ $article->title }}</td>
                            <td>{{ $article->subtitle }}</td>
                            <td>{{ $article->categoryName }}</td>
                            <td>{{ $article->created_at->format('M d Y H:m') }}</td>
                            <td>{{ $article->updated_at->format('M d Y H:m') }}</td>
                            <td>
                                <a href="{{ route('admin.articles.edit',$article->id) }}">Edit</a>
                                |
                                <a href="{{ route('admin.articles.delete',$article->id) }}">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th>Category</th>
                    <th>Date Created</th>
                    <th>Date Updated</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

</section>
<!-- /.content -->

@endsection

@section('custom-js')
    <script src="{{ asset('public/admin') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('public/admin') }}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $('table').DataTable({
            "order": [[ 3, "desc" ]],
            "pageLength": 14,
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    </script>
@endsection