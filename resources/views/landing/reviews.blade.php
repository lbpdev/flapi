@extends('layouts.landing')

@section('css')
<style>
    .container.page a {
        color: #333 !important;
        text-decoration: none;
        font-size: 17px !important;
        text-shadow: none;
        font-weight: bold;
    }

    .container.page p {
        font-size: 17px;
        line-height: 20px;
    }

    .container.page h2 {
        margin: 0 !important;
        margin-top: 20px !important;
    }
</style>

@endsection

@section('content')
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="black-bg clearfix" id="slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-8 col-md-offset-2 col-lg-offset-0 col-sm-12 slider clearfix col">
                <div class="text-center vcenter">
                    <h2>TapiApp is making an impact in the world of scheduling. </h2>
                    <p>See what others are saying about us.</p>
                    <a href="{{ url('/pricing') }}#register-panel"><button>Start Today</button></a>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 clearfix relative col">
                <img src="{{ asset('public/landing/img/v2/Industries-banner.png') }}" class="bottom" width="650" alt="Banner" title="Tapi Banner">
            </div>
        </div>
    </div>
</div>

<section class="container page clearfix padding-b-40">
  <div class="row relative">
    <h2 class="text-center">FinancesOnline</h2>
      <div class="col-md-6 text-center col">
          <div class="row">
            <img src="{{ asset('public/landing/img/v2/finance-online.png') }}" class="pull-right shadow" width="100%">
            <br>
            <br>
            <img src="{{ asset('public/landing/img/v2/risingstar.png') }}">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="{{ asset('public/landing/img/v2/userexperience2017.png') }}">
          </div>
      </div>
      <div class="col-md-6 text-center padding-lr-40 clearfix col">
          <div class="clearfix">

            <p class="margin-t-40"><a href="http://www.tapiapp.com/" target="_blank">TapiApp</a> went from an idea into something we knew we had to build when we could not find a simple, easy-to-use and fully customisable scheduling solution. Like most teams we were forced to use Excel to make spreadsheets that scrolled endlessly and made moving schedules around cumbersome and frustrating, as well as extremely time-consuming.</p>
 
                <p>Other scheduling software was expensive, old fashioned, out of date or so over the top it was more suitable for enterprise-level large corporates, and not made for small- to medium-sized teams that need something that is well-designed, clean, modern and customisable.</p>
                 
                <p>Shortly after the launch of TapiApp, we are pleased to announce it has received its first two awards from one of the leading business review directories, <a href="http://www.financesonline.com/" target="_blank">FinancesOnline</a>.</p>
                 
                <p>This is an exciting accolade for a software solution that is still in its infancy, and has been recognized by a team of business and software experts.</p>
                
                <p>In the TapiApp <a href="https://reviews.financesonline.com/p/tapiapp/#review" target="_blank">review</a>, the site's SaaS and B2B experts evaluated the product for its contribution to the online scheduling space, and gave it a Rising Star 2017 Award, which highlights the potential of TapiApp and how the design and features are at the forefront of online scheduling.</p>
                 
                <p>In addition to this, the app received the Great Experience 2017 Award demonstrating how simple TapiApp is to use and how it can make the scheduling experience quick and easy for the user.</p>
                 
                <p>TapiApp will continue to develop updates and help users around the world schedule their projects without having to cut and paste endless cells in spreadsheets and fix formatting issues, making scheduling a task users enjoy and can work with quickly and intuitively.</p>

                <p>TapiApp was distinguished for giving users an unlimited possibility to create projects and add clients, as well as for offering one of the simplest interfaces to be found in the FinancesOnline directory. Experts also paid attention to our time monitoring features, and included TapiApp on the platform’s list of most <a href="https://financesonline.com/project-management-software-reviews-15-popular-applications/" target="_blank">popular project management solutions</a> for this year.</p>

          </div>
      </div>
  </div>
</section>
@endsection