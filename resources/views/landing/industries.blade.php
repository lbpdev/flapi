@extends('layouts.landing')

@section('content')
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="black-bg clearfix" id="slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 slider clearfix col">
                <div class="text-center vcenter">
                    <h2>TapiApp can be customised to work with just about any industry that schedules time for teams and employees</h2>
                    <p>See how easy scheduling can be</p>
                    <a href="{{ url('/pricing') }}"><button class="dark">Start Today</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bluegreen-bg clearfix full-width">
    <section class="container" id="ideal-way">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12 text-center padding-lr-40 clearfix">
                <h2 class="margin-t-0 margin-b-10">Why Flapi?</h2>
                <p>Whether you’re a freelancer, events management company or small marketing agency, TapiApp can help you get things done. We all work from to-do lists and have deadlines to meet, but how you manage these can make a huge difference to your workflow. Streamline the process and reduce the time you spend scheduling and planning.</p>
                <p>Some of the industries we serve include:</p>
            </div>
        </div>
    </section>
</div>

<div id="industry" class="clearfix">
    <div class="col-lg-12 col-md-12 text-center padding-b-15 padding-t-20 bg-white" id="industryFigs">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="clearfix industries-wrap" style="display: inline-block">
                        <div class="col-md-4 col-lg-4 margin-b-20">
                            <div class="fig orange">
                                <h2 class="margin-t-0 margin-b-10">Graphic Design</h2>
                                <p>Perhaps you have a day working solely on one project but the next is full of small edits and finishing touches. Using TapiApp can help you get everything done without missing any important last minute jobs.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 margin-b-20">
                            <div class="fig lime">
                                <h2 class="margin-t-0 margin-b-10">Marketing</h2>
                                <p>A daily checklist probably already sits on your desk, but going online saves the fuss of printing, editing and reprinting paper lists when more work comes in. Prioritise the most urgent tasks and keep clients happy.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 margin-b-20">
                            <div class="fig dark">
                                <h2 class="margin-t-0 margin-b-10">Events</h2>
                                <p>So much goes into organising an event, it can be difficult to keep up with all demands, whether they’re from the client or other departments. Keep everyone happy by organising everyone’s day and completing tasks on time.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 margin-b-20">
                            <div class="fig blue">
                                <h2 class="margin-t-0 margin-b-10">Accountants</h2>
                                <p>Although accountants benefit from relatively high levels of flexibility, this can make staying on top of things challenging. Organise your week and set tasks for each day to keep yourself on schedule and ready for new reports and tasks to come in.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 margin-b-20">
                            <div class="fig fig bluegreen">
                                <h2 class="margin-t-0 margin-b-10">Publishers</h2>
                                <p>With a whole range of contributing departments and individuals, not mentioning external companies and clients, publishers using TapiApp can schedule tasks to create the most seamless workflow.</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 margin-b-20">
                            <div class="fig red">
                                <h2 class="margin-t-0 margin-b-10">Developers</h2>
                                <p>With projects that can span weeks or even months, TapiApp can help to visualise deadlines or break projects down into smaller and more doable tasks for web and app developers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 padding-t-40">
                        Because TapiApp is fully customisable, it's suitable for anyone with production requirements.<br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

