@extends('layouts.landing')

@section('css')
<style>

video::-internal-media-controls-download-button {
    display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
</style>
@endsection

@section('content')

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="dark-bg" id="hiw-video" style="">
    <div class="container text-center padding-t-40">
        <h1>How it Works</h1>
        <p>Watch the TapiApp demo below for an idea of the functions and customisation tools available.</p>
        <video width="960" height="600" controls autoplay>
          <source src="videos/TapiApp.mp4" type="video/mp4">
        Your browser does not support the video tag.
        </video>
        <br>
        <br>
        <p><a href="https://tapiapp.com/pricing#register-panel" style="font-weight: 600">Log in</a> to access the full range of How To Videos</p>
        <br>
        <br>
    </div>
</div>

@endsection
