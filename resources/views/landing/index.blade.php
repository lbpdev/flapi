@extends('layouts.landing')

@section('content')

        <!-- Main jumbotron for a primary marketing message or call to action -->
<div class="dark-bg" id="slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-md-offset-2 col-lg-offset-2 col-sm-12 slider clearfix col">
                <div class="text-center caption">
                    <h2>Creating and sharing publication flatplans has never been easier</h2>
                    <!-- <a href="videos/TapiApp.mp4" class="html5lightbox" data-width="1280" data-height="720"><button>Play Video</button></a> -->
                    <a href="{{ url('/pricing') }}#register-panel"><button>Free Trial</button></a>
                </div>
            </div>
            {{--<div class="col-lg-8 col-md-12 col-sm-12 clearfix relative col">--}}
                {{--<img src="{{ asset('public/landing/img/v2/Tapi-Screen.png') }}" class="bottom" width="650" alt="Banner" title="Tapi Banner">--}}
            {{--</div>--}}
        </div>
    </div>
</div>

<section class="container" id="ideal-way">
    <!-- Example row of columns -->
    <div class="row relative">
        <div class="col-md-7 text-center col">
            <div class="row">
                <img src="{{ asset('public/flapielements/Screenshot-1.png') }}" class="pull-right" width="100%">
            </div>
        </div>
        <div class="col-md-5 text-center padding-lr-40 clearfix col">
            <div class="vcenter clearfix">
                <h2 class="margin-t-0">Make it Your Own</h2>
                <p>FlapiApp is a powerful online tool that allows you to create, move and edit pages in your flatplan.</p>

                <a href="{{ url('/pricing') }}#register-panel"><button class="start">Free Trial</button></a>
            </div>
        </div>
    </div>
</section>

<div id="views">
    <!-- Example row of columns -->
    <div class="col-md-6 text-center padding-lr-40 gray-bg padding-tb-40 border-box clearfix overflow-hidden col">
        <div class="col-md-7 text-center col-md-offset-4 col-sm-offset-0 col-xs-offset-0">
            <h2 class="margin-t-40">Fully customisable</h2>
            <p>Because each publication is unique in its page layout, FlapiApp allows users to customise pages to match their requirements.</p>
            <a href="{{ url('/pricing') }}#register-panel"><button class="start white">Start Today</button></a>
        </div>
    </div>
    <div class="col-md-6 text-center green-bg padding-tb-40 overflow-hidden col">
        <div class="row">
            <div class="bg-bars clearfix">
            </div>

            <div class="col-md-9 text-center">
                <div class="row">
                    <img src="{{ asset('public/flapielements/Screenshot-2.png') }}" class="pull-left margin-l-40" width="630">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="easy">
    <!-- Example row of columns -->
    <div class="col-md-7 text-center bg-image col">
        <div class="row">
        </div>
    </div>
    <div class="col-md-5 text-center padding-lr-40  col">
        <div class="clearfix  vcenter">
            <h2 class="margin-t-0">Online ease</h2>
            <p>Once flatplans have been created, they can be shared with various members of the publishing team, and all they need is their web browser to see changes and ammendments in real time.</p>
            <a target="_blank" href="{{ route('login') }}"><button class="start">Login</button></a>
        </div>
    </div>
</div>


<div id="customization" class="clearfix">
    <!-- Example row of columns -->
    <div class="col-md-6 text-center padding-lr-40 gray-bg padding-tb-40 border-box overflow-hidden col">
        <div class="col-md-7 text-center col-md-offset-4 text">
            <div class="clearfix   vcenter">
                <h2 class="margin-t-10">Small learning curve</h2>
                <p>Using FlapiApp, you will have your first flatplan up and running within a few minutes, and maintaining couldn’t be easier. The days of cutting and pasting cells in Excel or using other complicated software are over. </p>
                <button class="start white">Start Today</button>
            </div>
        </div>
    </div>
    <div class="col-md-6 text-center pink-bg padding-tb-40 col overflow-hidden">
        <div class="row">
            <div class="bg-bars clearfix">
            </div>
            <div class="col-md-9 text-center">
                <div class="row">
                    <img src="{{ asset('public/flapielements/Screenshot-3.png') }}" class="pull-left margin-l-40" width="630">
                </div>
            </div>
        </div>
    </div>
</div>

<section class="container nojs" id="contact">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Contact us</h2>
        </div>
        <div class="col-md-12 col-md-6 col-md-offset-3 text-center">
            <p>We are always here to help and listen. <br>Feel free to send us a question or suggestion <br>and we will get back to you within three days.</p>
        </div>
        <div class="col-md-6 col-md-offset-3 text-center margin-t-30" >

            <?php
            if(isset($_GET['contact_success']) && $_GET['contact_success']){
            ?>
            <div class="alert alert-success"><p>Thank you for your interest in Tapi App. We will get back to you shortly.</p></div>
            <?php
            } else {
            ?>

            <form id="contactForm" action="mail.php" method="POST">
                <div class="col-md-6"><input name="fname" type="text" required="required" class="form-control" placeholder="First Name"></div>
                <div class="col-md-6"><input name="lname" type="text"  required="required" class="form-control" placeholder="Last Name"></div>
                <div class="col-md-12"><input name="email" type="email" required="required" class="form-control" placeholder="Email"></div>

                <?php
                $types = ['Sales','IT Support','General Queries','Complaint','Press & Media'];
                ?>

                <div class="col-md-12 margin-tb-4">
                    <select name="subject" id="workdays" class="form-control selectpicker" title="Subject">
                        <?php
                        foreach ($types  as $type)
                            echo '<option value="'.$type.'">'.$type.'</option>';
                        ?>
                    </select>
                </div>
                <div class="col-md-12"><textarea name="message" class="form-control" required="required" rows="6" placeholder="Message"></textarea></div>
                <div class="col-md-12 text-center" style="padding-top:3px;">
                    <div class="g-recaptcha" data-sitekey="6LfwfR0UAAAAAK11x-V8wRA6aOBSwJ3lziiMrgfx"></div>
                </div>
                <input type="submit" name="submit" value="Submit">
            </form>

            <?php
            }
            ?>
        </div>
    </div>
</section>
@endsection
