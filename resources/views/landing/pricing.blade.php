@extends('layouts.landing')

@section('content')
        <!-- Main jumbotron for a primary marketing message or call to action -->
<div class="pricing relative" id="slider">
    <div class="col-lg-5 col-md-12 slider clearfix col left">
        <div class="col-lg-5 col-md-12 col-md-offset-0 col-lg-offset-6 slider clearfix col left">
            <div class="text-center vcenter">
                <h2>Choose Your Plan</h2>
                <p>Sign up and upgrade at any time</p>
                <a href="#register-panel"><button>Start Today</button></a>
            </div>
        </div>
    </div>
    <div class="col-lg-7 col-md-7 col-sm-12 clearfix relative col right">
    </div>
</div>

<div class="container padding-t-40 padding-b-40 text-center pricing">
    <div class="row">
        <br>
        <br>
        <p class="intro">We offer a range of plans to suit all business sizes. If you sign up as an early adopter you will be exempt from any subscription fee increases for 12 months.</p>
        <br>
        <div class="clearfix" id="pricing-table">
            <div class="col-md-12 col-sm-6 col-xs-12  text-center ">
                <div class="row circles">
                    <div class="col-md-3 col-sm-3 col-xs-12  text-center col pink">
                        <div class="row">
                            <a href="#">
                                <div class="circle pink">
                                    <div class="color-box orange-light">
                                        <span class="number">5</span>
                                        <span class="text">users</span><br>
                                        -
                                        <br>
                                        <span class="text">5 Flatplans</span>
                                        <span class="text small">(can be for multiple publications)</span>
                                        <span class="text small">Maximum 80+4  pages per flatplan</span>
                                    </div>
                                    <div class="color-box orange">
                                        <span class="price"><span class="dollar">$</span>15</span><br>
                                        <span class="text">per month</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12  text-center col blue">
                        <div class="row">
                            <a href="#">
                                <div class="circle">
                                    <div class="color-box lime-light">
                                        <span class="number">10</span>
                                        <span class="text">users</span><br>
                                        -
                                        <br>
                                        <span class="text">5 Flatplans</span>
                                        <span class="text small">(can be for multiple publications)</span>
                                        <span class="text small">Maximum 160 + 4 pages per flatplan</span>
                                    </div>
                                    <div class="color-box lime">
                                        <span class="price"><span class="dollar">$</span>25</span><br>
                                        <span class="text">per month</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12  text-center col green">
                        <div class="row">
                            <a href="#">
                                <div class="circle">
                                    <div class="color-box blue-light">
                                        <span class="number">15</span>
                                        <span class="text">users</span><br>
                                        -
                                        <br>
                                        <span class="text">5 Flatplans</span>
                                        <span class="text small">(can be for multiple publications)</span>
                                        <span class="text small">Maximum 240 + 4 pages per flatplan</span>
                                    </div>
                                    <div class="color-box blue">
                                        <span class="price"><span class="dollar">$</span>35</span><br>
                                        <span class="text">per month</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12  text-center col purple">
                        <div class="row">
                            <a href="#">
                                <div class="circle">
                                    <div class="color-box green-light">
                                        <span class="number">20</span>
                                        <span class="text">users</span><br>
                                        -
                                        <br>
                                        <span class="text">5 Flatplans</span>
                                        <span class="text small">(can be for multiple publications)</span>
                                        <span class="text small">Maximum 400+4 pages per flatplan</span>
                                    </div>
                                    <div class="color-box green">
                                        <span class="price"><span class="dollar">$</span>60</span><br>
                                        <span class="text">per month</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12  text-center col">
                <div class="row">
                    <h1 style="margin-top: 60px;margin-bottom: 30px">All Plans Come With <br>These Features</h1>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <ul class="features text-left">
                            <li><span class="check"></span> <p>Drag and drop</p></li>
                            <li><span class="check"></span> <p>Real-time sharing</p></li>
                            <li><span class="check"></span> <p>Autosave</p></li>
                            <li><span class="check"></span> <p>No need to use Excel or other software</p></li>
                            <li><span class="check"></span> <p>Online access. Anytime, anywhere.</p></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <ul class="features text-left">
                            <li><span class="check"></span> <p>Synopsis and notes per page</p></li>
                            <li><span class="check"></span> <p>Multiple user levels</p></li>
                            <li><span class="check"></span> <p>Customise teams and roles</p></li>
                            <li><span class="check"></span> <p>Set user permissions</p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <a href="https://tapiapp.com/#contact" class="pink"><p>For more than 35 users please contact us for a custom plan. </p></a>
        <!--    <a href="http://tapiapp.com/app/register"><button class="start">START FREE TRIAL</button></a>-->

        <div class="col-md-10 col-md-offset-1" id="register-panel">
            <div class="row">
                <h2 class="margin-b-20 margin-t-20">Start Free Trial Now</h2>

                <p class="intro">30 day free trial. Five user plan including you. Unlimited projects and tasks. No credit card required. No obligation. By starting a free trial you are agreeing to the Terms of Use.</p>
                {!! Form::open(['route'=>'company.register']) !!}

                <input type="hidden" name="csrf_token" id="csrf_token">
                <div class="col-md-8 col-md-offset-2">

                    <input type="text" class="form-control" name="user[name]" placeholder="Full Name" value="" required="required">
                    <input type="email" class="form-control" name="user[email]" placeholder="Email Address (This will be your username)" value="" required="required">
                    <input type="password" class="form-control" name="user[password]" placeholder="Password" value="" required="required">
                    <input type="password" class="form-control" name="user[password_confirm]" placeholder="Confirm Password" value="" required="required">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <select class="form-control selectpicker" id="country" name="option[country]"><option value="" selected="selected">Choose Country</option></select>
                            </div>
                        </div>
                        <div class="col-md-6" style="display:none;">
                            <div class="">
                                <select class="form-control selectpicker" id="city" name="option[city]"><option value="" disabled selected="selected">Choose City</option></select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12" style=" font-size: 12px;text-align: left; line-height: 16px;">
                            Company Name
                            <input type="text" class="form-control margin-t-0" name="option[name]" placeholder="Please enter the name of your company"  required="required" value="">
                        </div>
                        <div class="col-md-6" style="display:none;font-size: 12px;text-align: left; line-height: 16px;">

                            <?php
                            $daysOfWeek = [
                                    'Mon' => 'Mon',
                                    'Tue' => 'Tue',
                                    'Wed' => 'Wed',
                                    'Thu' => 'Thu',
                                    'Fri' => 'Fri',
                                    'Sat' => 'Sat',
                                    'Sun' => 'Sun'
                            ]
                            ?>
                            Work Days
                            <select name="workdays[]" style="display:none;" id="workdays" multiple="multiple" class="form-control selectpicker " title="Work Days">
                                <?php
                                foreach ($daysOfWeek  as $day)
                                    echo '<option '.( ( $day!="Sat" || $day!="Sun" ) ? 'selected' : '' ).' value="'.$day.'">'.$day.'</option>';
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6" style=" line-height: 16px;">
                            <?php

                            $timezones = [
                                    '(UTC-11:00) Midway Island' => 'Pacific/Midway',
                                    '(UTC-11:00) Samoa' => 'Pacific/Samoa',
                                    '(UTC-10:00) Hawaii' => 'Pacific/Honolulu',
                                    '(UTC-09:00) Alaska' => 'US/Alaska',
                                    '(UTC-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles',
                                    '(UTC-08:00) Tijuana' => 'America/Tijuana',
                                    '(UTC-07:00) Arizona' => 'US/Arizona',
                                    '(UTC-07:00) Chihuahua' => 'America/Chihuahua',
                                    '(UTC-07:00) La Paz' => 'America/Chihuahua',
                                    '(UTC-07:00) Mazatlan' => 'America/Mazatlan',
                                    '(UTC-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain',
                                    '(UTC-06:00) Central America' => 'America/Managua',
                                    '(UTC-06:00) Central Time (US &amp; Canada)' => 'US/Central',
                                    '(UTC-06:00) Guadalajara' => 'America/Mexico_City',
                                    '(UTC-06:00) Mexico City' => 'America/Mexico_City',
                                    '(UTC-06:00) Monterrey' => 'America/Monterrey',
                                    '(UTC-06:00) Saskatchewan' => 'Canada/Saskatchewan',
                                    '(UTC-05:00) Bogota' => 'America/Bogota',
                                    '(UTC-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern',
                                    '(UTC-05:00) Indiana (East)' => 'US/East-Indiana',
                                    '(UTC-05:00) Lima' => 'America/Lima',
                                    '(UTC-05:00) Quito' => 'America/Bogota',
                                    '(UTC-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic',
                                    '(UTC-04:30) Caracas' => 'America/Caracas',
                                    '(UTC-04:00) La Paz' => 'America/La_Paz',
                                    '(UTC-04:00) Santiago' => 'America/Santiago',
                                    '(UTC-03:30) Newfoundland' => 'Canada/Newfoundland',
                                    '(UTC-03:00) Brasilia' => 'America/Sao_Paulo',
                                    '(UTC-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
                                    '(UTC-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
                                    '(UTC-03:00) Greenland' => 'America/Godthab',
                                    '(UTC-02:00) Mid-Atlantic' => 'America/Noronha',
                                    '(UTC-01:00) Azores' => 'Atlantic/Azores',
                                    '(UTC-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
                                    '(UTC+00:00) Casablanca' => 'Africa/Casablanca',
                                    '(UTC+00:00) Edinburgh' => 'Europe/London',
                                    '(UTC+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich',
                                    '(UTC+00:00) Lisbon' => 'Europe/Lisbon',
                                    '(UTC+00:00) London' => 'Europe/London',
                                    '(UTC+00:00) Monrovia' => 'Africa/Monrovia',
                                    '(UTC+00:00) UTC' => 'UTC',
                                    '(UTC+01:00) Amsterdam' => 'Europe/Amsterdam',
                                    '(UTC+01:00) Belgrade' => 'Europe/Belgrade',
                                    '(UTC+01:00) Berlin' => 'Europe/Berlin',
                                    '(UTC+01:00) Bern' => 'Europe/Berlin',
                                    '(UTC+01:00) Bratislava' => 'Europe/Bratislava',
                                    '(UTC+01:00) Brussels' => 'Europe/Brussels',
                                    '(UTC+01:00) Budapest' => 'Europe/Budapest',
                                    '(UTC+01:00) Copenhagen' => 'Europe/Copenhagen',
                                    '(UTC+01:00) Ljubljana' => 'Europe/Ljubljana',
                                    '(UTC+01:00) Madrid' => 'Europe/Madrid',
                                    '(UTC+01:00) Paris' => 'Europe/Paris',
                                    '(UTC+01:00) Prague' => 'Europe/Prague',
                                    '(UTC+01:00) Rome' => 'Europe/Rome',
                                    '(UTC+01:00) Sarajevo' => 'Europe/Sarajevo',
                                    '(UTC+01:00) Skopje' => 'Europe/Skopje',
                                    '(UTC+01:00) Stockholm' => 'Europe/Stockholm',
                                    '(UTC+01:00) Vienna' => 'Europe/Vienna',
                                    '(UTC+01:00) Warsaw' => 'Europe/Warsaw',
                                    '(UTC+01:00) West Central Africa' => 'Africa/Lagos',
                                    '(UTC+01:00) Zagreb' => 'Europe/Zagreb',
                                    '(UTC+02:00) Athens' => 'Europe/Athens',
                                    '(UTC+02:00) Bucharest' => 'Europe/Bucharest',
                                    '(UTC+02:00) Cairo' => 'Africa/Cairo',
                                    '(UTC+02:00) Harare' => 'Africa/Harare',
                                    '(UTC+02:00) Helsinki' => 'Europe/Helsinki',
                                    '(UTC+02:00) Istanbul' => 'Europe/Istanbul',
                                    '(UTC+02:00) Jerusalem' => 'Asia/Jerusalem',
                                    '(UTC+02:00) Kyiv' => 'Europe/Helsinki',
                                    '(UTC+02:00) Pretoria' => 'Africa/Johannesburg',
                                    '(UTC+02:00) Riga' => 'Europe/Riga',
                                    '(UTC+02:00) Sofia' => 'Europe/Sofia',
                                    '(UTC+02:00) Tallinn' => 'Europe/Tallinn',
                                    '(UTC+02:00) Vilnius' => 'Europe/Vilnius',
                                    '(UTC+03:00) Baghdad' => 'Asia/Baghdad',
                                    '(UTC+03:00) Kuwait' => 'Asia/Kuwait',
                                    '(UTC+03:00) Minsk' => 'Europe/Minsk',
                                    '(UTC+03:00) Nairobi' => 'Africa/Nairobi',
                                    '(UTC+03:00) Riyadh' => 'Asia/Riyadh',
                                    '(UTC+03:00) Volgograd' => 'Europe/Volgograd',
                                    '(UTC+03:30) Tehran' => 'Asia/Tehran',
                                    '(UTC+04:00) Abu Dhabi' => 'Asia/Muscat',
                                    '(UTC+04:00) Baku' => 'Asia/Baku',
                                    '(UTC+04:00) Moscow' => 'Europe/Moscow',
                                    '(UTC+04:00) Muscat' => 'Asia/Muscat',
                                    '(UTC+04:00) St. Petersburg' => 'Europe/Moscow',
                                    '(UTC+04:00) Tbilisi' => 'Asia/Tbilisi',
                                    '(UTC+04:00) Yerevan' => 'Asia/Yerevan',
                                    '(UTC+04:30) Kabul' => 'Asia/Kabul',
                                    '(UTC+05:00) Islamabad' => 'Asia/Karachi',
                                    '(UTC+05:00) Karachi' => 'Asia/Karachi',
                                    '(UTC+05:00) Tashkent' => 'Asia/Tashkent',
                                    '(UTC+05:30) Chennai' => 'Asia/Calcutta',
                                    '(UTC+05:30) Kolkata' => 'Asia/Kolkata',
                                    '(UTC+05:30) Mumbai' => 'Asia/Calcutta',
                                    '(UTC+05:30) New Delhi' => 'Asia/Calcutta',
                                    '(UTC+05:30) Sri Jayawardenepura' => 'Asia/Calcutta',
                                    '(UTC+05:45) Kathmandu' => 'Asia/Katmandu',
                                    '(UTC+06:00) Almaty' => 'Asia/Almaty',
                                    '(UTC+06:00) Astana' => 'Asia/Dhaka',
                                    '(UTC+06:00) Dhaka' => 'Asia/Dhaka',
                                    '(UTC+06:00) Ekaterinburg' => 'Asia/Yekaterinburg',
                                    '(UTC+06:30) Rangoon' => 'Asia/Rangoon',
                                    '(UTC+07:00) Bangkok' => 'Asia/Bangkok',
                                    '(UTC+07:00) Hanoi' => 'Asia/Bangkok',
                                    '(UTC+07:00) Jakarta' => 'Asia/Jakarta',
                                    '(UTC+07:00) Novosibirsk' => 'Asia/Novosibirsk',
                                    '(UTC+08:00) Beijing' => 'Asia/Hong_Kong',
                                    '(UTC+08:00) Chongqing' => 'Asia/Chongqing',
                                    '(UTC+08:00) Hong Kong' => 'Asia/Hong_Kong',
                                    '(UTC+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
                                    '(UTC+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
                                    '(UTC+08:00) Perth' => 'Australia/Perth',
                                    '(UTC+08:00) Singapore' => 'Asia/Singapore',
                                    '(UTC+08:00) Taipei' => 'Asia/Taipei',
                                    '(UTC+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator',
                                    '(UTC+08:00) Urumqi' => 'Asia/Urumqi',
                                    '(UTC+09:00) Irkutsk' => 'Asia/Irkutsk',
                                    '(UTC+09:00) Osaka' => 'Asia/Tokyo',
                                    '(UTC+09:00) Sapporo' => 'Asia/Tokyo',
                                    '(UTC+09:00) Seoul' => 'Asia/Seoul',
                                    '(UTC+09:00) Tokyo' => 'Asia/Tokyo',
                                    '(UTC+09:30) Adelaide' => 'Australia/Adelaide',
                                    '(UTC+09:30) Darwin' => 'Australia/Darwin',
                                    '(UTC+10:00) Brisbane' => 'Australia/Brisbane',
                                    '(UTC+10:00) Canberra' => 'Australia/Canberra',
                                    '(UTC+10:00) Guam' => 'Pacific/Guam',
                                    '(UTC+10:00) Hobart' => 'Australia/Hobart',
                                    '(UTC+10:00) Melbourne' => 'Australia/Melbourne',
                                    '(UTC+10:00) Port Moresby' => 'Pacific/Port_Moresby',
                                    '(UTC+10:00) Sydney' => 'Australia/Sydney',
                                    '(UTC+10:00) Yakutsk' => 'Asia/Yakutsk',
                                    '(UTC+11:00) Vladivostok' => 'Asia/Vladivostok',
                                    '(UTC+12:00) Auckland' => 'Pacific/Auckland',
                                    '(UTC+12:00) Fiji' => 'Pacific/Fiji',
                                    '(UTC+12:00) International Date Line West' => 'Pacific/Kwajalein',
                                    '(UTC+12:00) Kamchatka' => 'Asia/Kamchatka',
                                    '(UTC+12:00) Magadan' => 'Asia/Magadan',
                                    '(UTC+12:00) Marshall Is.' => 'Pacific/Fiji',
                                    '(UTC+12:00) New Caledonia' => 'Asia/Magadan',
                                    '(UTC+12:00) Solomon Is.' => 'Asia/Magadan',
                                    '(UTC+12:00) Wellington' => 'Pacific/Auckland',
                                    '(UTC+13:00) Nuku\'alofa' => 'Pacific/Tongatapu'
                            ];

                            foreach ($timezones as $key => $value) {
                                $tz[$value] = $key;
                            }
                            ?>
                            <div class="" style="display:none;font-size: 12px;text-align: left; line-height: 16px;">
                                Timezone
                                <select name="option[timezone]" id="employees" class="form-control selectpicker" title="Timezone">
                                    <?php
                                    foreach ($tz  as $value=>$item)
                                        echo '<option '.($value=='Europe/London'? 'selected' : '').' value="'.$value.'">'.$item.'</option>';
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" style="display:none;font-size: 12px;text-align: left; line-height: 16px;">
                            <?php
                            for($x=1;$x<25;$x++)
                                $workHours[$x] = $x;
                            ?>
                            Work Hours
                            <div class="row" >
                                <div class="col-md-12">
                                    <div class="small">
                                        <select name="workhours" id="workhours_from" class="form-control selectpicker" title="Work Hours">
                                            <?php
                                            foreach ($workHours  as $workHour)
                                                echo '<option '.($workHour==8? 'selected' : '').' value="'.$workHour.'">'.$workHour.'</option>';
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="" style=" font-size: 12px;text-align: left; line-height: 16px;">
                        <?php
                        $employeeSelection = [
                                '0' => 'Free Trial (5 Users)',
                                '1' => '5 Users ($15/month)',
                                '2' => '10 Users ($25/month)',
                                '3' => '15 Users ($35/month)',
                                '4' => '20 Users ($60/month)'
                        ]
                        ?>
                        Plan
                        <div class="">
                            <select name="subscription" id="employees" class="form-control selectpicker">
                                <?php
                                foreach ($employeeSelection  as $value=>$item)
                                    echo '<option '.($value==0? 'selected' : '').' value="'.$value.'">'.$item.'</option>';
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2 relative margin-t-10">
                    <input class="pull-left" type="checkbox" id="termsRadio">&nbsp;<span href="#" class="pull-left" id="termsField">&nbsp; I have read and accept the terms of use. </span>
                    <p class="error" style="font-size: 16px; color: #c40000;">{{ Session::has('error') ? Session::get('error') : '' }}</p>
                </div>
                <div class="col-md-12 text-center">
                    <input value="Register" type="submit" class="link-bt start">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>

@endsection

@section('js')

<script src="https://tapiapp.com/app/public/js/country_city.js"></script>
<script>

    $('form').on('submit',function(e){

        $('.error').html('');

        if(!$('#termsRadio').is(":checked")){
            $('#register-panel .error').html('Please make sure you have read the terms of use and ticked the checkbox above.').show();
            e.preventDefault();
            e.stopPropagation();

            return false;
        } else {
            $('#termsField').css({ 'background-color' : 'transparent'});
            $('#termsError').hide();
        }

        if(!$('form').hasClass('valid')){
            e.preventDefault();
            e.stopPropagation();
        }

        password = $($('form').find('input[type=password]')[0]).val();
        cpassword = $($('form').find('input[type=password]')[1]).val();
        email = $('form').find('input[type=email]').val();
        name = $('form').find('input[name="option[name]"]').val();

        if(password==cpassword){
            $('#register-panel .error').html('');

            if(!$('form').hasClass('valid')){
                $.ajax({
                    type: "GET",
                    url: '{{ route('api.users.validateRegisterCompany') }}?email='+email+'&name='+name+'&password='+password,
                    success: function(response){
                        if(response.status==500) {

                            $('#register-panel .error').html(response.data.message).show();

                            return false;

                        } else {
                            $('form').addClass('valid');
                            $('form').submit();
                        }
                    },
                    done : function (){
                        processFlag = 1;
                    }
                });
            }
        } else {
            $('#register .error').html('Passwords do not match.');
        }

        return true;
    });

    $('#country').val('United Kingdom').trigger("change");

    $('#termsBt').on('click', function(){
        $('#termsModal').modal('show');
    });

    $('#termsField').on('click', function(e){
        $('#termsRadio').trigger('click');
    });

</script>


@endsection
