
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center width-700">

                <!-- Modal content-->
                <div class="modal-content clearfix" id="termsModal">
                    <div class="modal-header clearfix padding-l-30">
                        <h4 class="modal-title">TERMS OF USE - Tapi App</h4>
                    </div>
                    <div class="modal-body clearfix width-full padding-30 text-left" >
                    <p><strong>ACCEPTANCE OF TERMS OF USE</strong></p>

                    <p>The services that TapiApp (www.tapiapp.com) provides to &ldquo;Users&rdquo; is subject to the Terms of Use (&ldquo;ToU&quot;) set out below. </p>

                    <p>Leading Brands, as owners of TapiApp, reserve the right to update, change or modify the ToU at any time without notice. The most current version of the ToU can be reviewed by clicking on the &quot;Terms of Use&quot; link located at the bottom of www.tapiapp.com.</p>

                    <p>This Agreement establishes the terms and conditions that apply to the use of www.tapiapp.com by User. By using Services [as defined in DESCRIPTION OF SERVICES], User agrees to comply with all of the terms and conditions. The right to use Services is dedicated to User and is not transferable to any other person or entity, unless otherwise permitted by the ToU. User is responsible for all use of User's account and for ensuring that all use of User's Account complies fully with the provisions of this Agreement.</p>

                    <p>Leading Brands shall have the right at any time to change or discontinue any aspect or feature of &ldquo;Services&rdquo;, including, but not limited to, content, design, accessibility, and equipment needed for access or use. </p>

                    <p></p>

                    <p><strong>DESCRIPTION OF SERVICES</strong></p>

                    <p>www.tapiapp.com provides Users with access to an online production and scheduling software system (&quot;Services&quot;). Services, including any updates, enhancements, new features, and/or the addition of any new Web properties, are subject to the ToU. Users may test the site using a free, no obligation trial period (for a maximum of 30 days) prior to upgrading to a paid subscription. </p>

                    <p><strong>EQUIPMENT</strong></p>
                    <p> User shall be responsible for obtaining and maintaining all computer and mobile phone hardware, and software such as web browsers, to access and use &ldquo;Services&rdquo;. Services provided by www.tapiapp.com will only be available by modern, up-to-date web browsers such as FireFox, Safari and Chrome. If Users attempt to access www.tapiapp.com using old, out-of-date versions, or browsers such as Internet Explorer, it is not guaranteed that all Services will work.</p>

                    <p><strong>USER CONDUCT</strong></p>

                    <ol type="A">
                         <li>User shall use www.tapiapp.com for lawful and business purposes only. User shall not post or transmit any material through www.tapiapp.com that violates or infringes in any way upon the rights of others, which is unlawful, threatening, abusive, defamatory, invasive of privacy or publicity rights, vulgar, obscene, profane or otherwise objectionable, or which encourages conduct that would constitute a criminal offense, give rise to civil liability or otherwise violate any law. Any conduct by a User that restricts or inhibits any other User from using www.tapiapp.com will not be permitted.</li>

                         <li>www.tapiapp.com contains copyrighted material and other proprietary information, including, but not limited to, text, images and code, and the entire contents of www.tapiapp.com are copyrighted as a collective work under governing copyright laws. User acknowledges that it does not acquire any ownership rights by using &ldquo;Services&rdquo; or by subscribing to a paid plan.</li>
                    </ol>

                    <p><strong>USE OF SERVICES</strong></p>

                    <p>&ldquo;Services&rdquo; may contain email, calendars and/or other messages or communication facilities designed to enable User to communicate with others (each a &quot;Communication Service&rdquo;; collectively &quot;Communication Services&rdquo;). User agrees to use the Communication Services only to post, send and receive messages and material that are proper and, when applicable, related to the particular Communication Service. By way of example, and not as a limitation, User agrees that when using the Communication Services, User will not: </p>

                    <ul>
                    <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others&#8232;</li>
                    <li>Publish, post, distribute or disseminate any inappropriate, profane, defamatory, obscene, indecent or unlawful topic, name, material or information&#8232;</li>
                    <li>Use any material or information, including images or photographs, which are made available through the Services in any manner that infringes any copyright, trademark, patent, trade secret, or other proprietary right of any party&#8232;</li>
                    <li>Restrict or inhibit any other user from using the Communication Services&#8232;</li>
                    <li>Violate any code of conduct or other guidelines which may be applicable for any particular Communication Service&#8232;</li>
                    <li>Harvest or otherwise collect information about others, including e-mail addresses and personal data&#8232;</li>
                    <li>Violate any applicable laws or regulations&#8232;</li>
                    <li>Create a false identity for the purpose of misleading others</li>
                    </ul>

                    <p> www.tapiapp.com has no obligation to monitor Communication Services. However, the site reserves the right to review materials posted via Communication Services and to remove any materials using its sole discretion. www.tapiapp.com also reserves the right to terminate User&rsquo;s access to any or all of the Communication Services at any time, without notice, for any reason it deems valid. The site also reserves the right at all times to disclose any information as it deems necessary to satisfy any applicable law, regulation, legal process or governmental request.</p>

                    <p><strong>MEMBER ACCOUNT, PASSWORD, AND SECURITY</strong></p>

                    <p>Access to Services requires User to create an account. User must complete the registration process by inputting current, complete and accurate information as prompted by the registration form. User must select a password, and the email address will function as the username. User is entirely responsible for maintaining the confidentiality of User&rsquo;s password and account. Furthermore, User is entirely responsible for any and all activities that occur under User&rsquo;s account. User agrees to notify Leading Brands immediately of any unauthorised use of User&rsquo;s account or any other breach of security. Leading Brands will not be liable for any loss that User may incur as a result of someone else using User&rsquo;s password or account, either with or without User&rsquo;s knowledge. User could, however, be held liable for losses incurred by Leading Brands or another party due to someone else using User&rsquo;s password. User may not use anyone else's account at any time, without the permission of the account holder. </p>

                    <p></p>

                    <p><strong>DISCLAIMER OF WARRANTY &amp; LIMITATION OF LIABILITY</strong></p>

                    <ol type="A">
                         <li>User expressly agrees that use of &ldquo;Services&rdquo; is solely at user's risk.</li>

                         <li>www.tapiapp.com is provided on an &quot;as is&quot; basis without warranties of any kind, either express or implied, including, but not limited to, warranties of title or implied warranties of merchantability or fitness for a particular purpose, other than those warranties which are implied by and incapable of exclusion, restriction or modification under the laws applicable to this agreement.</li>

                         <li>This disclaimer of liability applies to any damages or injury caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction or unauthorised access to, alteration of, or use of record, whether for breach of contract, tortious behaviour, negligence, or under any other cause of action. User specifically acknowledges that Leading Brands will not be liable for the defamatory, offensive or illegal conduct of other users or third-parties and that the risk of injury from the foregoing rests entirely with User.</li>

                         <li>Force majeure. www.tapiapp.com will not be responsible for any failure or delay in performance due to circumstances beyond its reasonable control, including, without limitation, acts of god, war, riot, embargoes, acts of civil or military authorities, fire, floods, accidents, service outages resulting from equipment and/or software failure and/or telecommunications failures, power failures, network failures, failures of third party service providers (including providers of Internet services and telecommunications). The performance of this agreement shall be suspended for as long as any such event shall prevent the affected party from performing its obligations under this agreement.</li>
                    </ol>

                    <p><strong>MONITORING</strong></p>
                    <p>Leading Brands shall have the right, but not the obligation, to monitor the performance of www.tapiapp.com and to determine compliance with this Agreement and any operating rules established by Leading Brands, and to satisfy any law, regulation or authorised government request. Leading Brands shall have the right, in its sole discretion, to edit or remove any material submitted on www.tapiapp.com that is deemed inappropriate or that has been reported.</p>

                    <p><strong>INDEMNIFICATION</strong></p>

                    <p>User agrees to defend, indemnify and hold harmless Leading Brands, its affiliates and their respective directors, officers, employees and agents from and against all claims and expenses, including attorneys' fees, arising out of the use of www.tapiapp.com by User or User's account. </p>

                    <p><strong>TERMINATION</strong></p>
                    <p> www.tapiapp.com or User may terminate this Agreement at any time. Without limiting the foregoing, www.tapiapp.com shall have the right to immediately terminate User's Account in the event of any conduct by User which the site, in its sole discretion, considers to be unacceptable, or in the event of any breach by User of this Agreement. Termination by Leading Brands or User is subject to the clauses outlined in ONLINE SUBSCRIPTION AGREEMENT.</p>

                    <p><strong>PRIVACY</strong></p>

                    <p>Signing up to use Services requires your email address (as username) and password, which will act as your login credentials. We will also require additional information pertaining to your business and company. User access on www.tapiapp.com is, in most cases, viewed only by Users that have access to &ldquo;Services&rdquo;. Leading Brands may, however, need to review communications and system access. Users should therefore not expect to have a right to privacy in any of their communications. We also collect and store IP addresses and login details for statistical and security purposes. This information will not be shared with and third parties.</p>

                    <p><strong>SECURITY</strong></p>

                    <p>www.tapiapp.com uses physical and technical safeguards to protect the security of User&rsquo;s personal information and Services data. We cannot, however, guarantee that unauthorized third parties will never be able to bypass our security measures and obtain personal information or tamper with data posted as part of Services. User acknowledges that personal data and information used on www.tapiapp.com are done so entirely at their own risk. </p>

                    <p><strong>UNSOLICITED IDEA SUBMISSION POLICY</strong></p>

                    <p>Leading Brands or any of its employees do not accept or consider unsolicited ideas, including ideas for new advertising campaigns, new promotions, new products or technologies, add-features, processes, materials, marketing plans or new product names. Please do not send any original creative artwork, samples, demos or other works. The purpose of this policy is to avoid potential misunderstandings or disputes when Leading Brands&rsquo; products or marketing strategies might seem similar to ideas submitted to the stakeholders of www.tapiapp.com.</p>

                    <p><strong>COPYRIGHT NOTICE</strong></p>

                    <p>The logos for Leading Brands and TapiApp are trademarks of Leading Brands. All rights reserved. All other trademarks uploaded by Users and appearing on www.tapiapp.com are the property of their respective owners. </p>

                    <p><strong>ACCESS, SERVICES &amp; TERMINATION</strong></p>

                    <p>  Access to Service depends on the plan User selects, and each registration process will take on the following process:</p>

                    <ul>
                    <li>Owner will register a company account; as registrar, owner will be granted administrator rights for the company account. </li> 
                    <li>For evaluation purposes, all new accounts will be granted a 30-day, no obligation trial period for Plan 1.</li>   
                    <li>Administrator may add Users to their account, as per the allocation set out in Plan 1, and these new Users will be granted access to Services in the roles defined by the Administrator.</li>  
                    <li>Administrator can at any point, prior to the termination of the trail period, cancel their account, or proceed with a payment for the plan of their choice, which is available to them in the settings page.</li>   
                    <li>Upon successful payment, Users will be granted with access to Services, and the billing cycle will commence. Once a payment has been made, it is not refundable &ndash; please see below.</li>  
                    <li>Account administrators may upgrade, downgrade or discontinue their account at any time, and changes will take effect at the end of each billing cycle (monthly).</li>   
                    <li>www.tapiapp.com reserves the right to modify, suspend or terminate access to &ldquo;Service&rdquo; at any time and for any reason without notice or refund, including the right to require you to change your login password.</li>   
                    <li>www.tapiapp.com reserves the right to delete all data and information associated with your account and/or other information you have on our system.</li>  
                    <li>Furthermore, when administrator adds new Users to its plan, the administrator, as the company representative, represents and warrants that each new added User has consented to be added to www.tapiapp.com and to avail of its Services.</li>
                    </ul>
                    <p></p>

                    <p><strong>FEES &amp; PAYMENTS</strong></p>

                    <p>Users will be charged a standard monthly fee for access to &ldquo;Service&rdquo; and the fee will be based on the plan that User has selected. User should review the complete and current price list for each of the plans available before signing up for &ldquo;Services&rdquo;. User will be given the opportunity to pay by credit card for Services upon completion or during trial period. All payments for www.tapiapp.com are processed by Avangate &ndash; www.avangate.com &ndash; a credible, recognised online payment processing company with head offices in the United States and The Netherlands. Leading Brands does not store or keep credit card details for transactions made on www.tapiapp.com. </p>

                    <p><strong>NO REFUNDS</strong></p>

                    <p>As stipulated above, Users may cancel their subscription to www.tapiapp.com at any time, but Users will not be entitled to a refund for the remaining period of the billing cycle. There will be no refunds or credits for partial months of Service. In the best interests of fairness, no exceptions will be made. In the event that www.tapiapp.com terminates User&rsquo;s account(s), Users understand and agree that they shall not receive refunds or credits for unused time.</p>

                    <p><strong>CLOSING SUMMARY</strong></p>
                    <p>This Agreement and any operating rules for www.tapiapp.com constitute the entire agreement of the parties with respect to the subject matter hereof, and supersedes all previous written or oral agreements between the parties with respect to such subject matter. This Agreement shall be construed and governed in accordance with the laws of the United Arab Emirates, without regard to its conflict of laws rules. No waiver by either party of any breach or default hereunder shall be deemed to be a waiver of any preceding or subsequent breach or default. The section headings used throughout the Agreement are for convenience only and shall not be given any legal import.</p>

                    <p style="font-size: 10px !important;">Last amendments to Terms of Use: February 2, 2017</p>

                    <p></p>

                    <p></p>

                    <p></p>

                    <p></p>
                    </div>
                    <div class="modal-footer clearfix pull-left width-full">
                        <!-- <button class="btn btn-default" data-dismiss="modal">Close</button> -->
                    </div>

                </div>

            </div>
        </div>