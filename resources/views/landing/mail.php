<?php

require 'PHPMailer-master/PHPMailerAutoload.php';
require_once "recaptchalib.php";

$servername = "127.0.0.1";
$username = "tapiapp_admin";
$password = "LB@JLTjbc1502";
$dbname = "tapiapp_v2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = "6LcJGw0UAAAAAHp_0DAE_rFTXXmThqFkUwwh_rXQ";
$secret = "6LcJGw0UAAAAAOasRCj3d2k3bNT4eE6UKtTKzuHL";
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang = "en";

// The response from reCAPTCHA
$resp = null;
// The error code from reCAPTCHA, if any
$error = null;

$reCaptcha = new ReCaptcha($secret);

// Was there a reCAPTCHA response?
if ($_POST["g-recaptcha-response"]) {
    $resp = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
    );
}

if ($resp != null && $resp->success) {

$ip = get_ip();

$fname = mysqli_real_escape_string($conn,$_POST['fname']);
$lname = mysqli_real_escape_string($conn,$_POST['lname']);
$email = mysqli_real_escape_string($conn,$_POST['email']);
$subject = mysqli_real_escape_string($conn,$_POST['subject']);
$message = mysqli_real_escape_string($conn,$_POST['message']);
 
if(isset($_POST['fname']) && isset($_POST['lname']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message'])){

    $sql = "INSERT INTO inquiries (first_name, last_name, email, message, ip, created_at, updated_at) VALUES ('".$fname."','".$lname."','".$email."','".$message."','".$ip."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."')";

    if ($conn->query($sql) === TRUE) {
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = '8b55dcc41bfbd107ed02326d1cac4eae';                 // SMTP username
        $mail->Password = '234dad900ac53dd60fd81b8fede0c83e';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2525;                                    // TCP port to connect to

        $mail->setFrom('contact@tapiapp.com', 'Contact');
        $mail->addAddress('mars@leadingbrands.me', 'Mars');     // Add a recipient
        $mail->addAddress('gene@leadingbrands.me', 'Gene');     // Add a recipient
        $mail->addReplyTo('no-reply@tapiapp.me', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'TapiApp Contact Form - '.subject;

        ob_start();

        ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width">
        <title>TapiApp Support</title>
    </head>
    <body>
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            width: 100% !important;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
            background-color: #F7F7F7;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .bodytbl {
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            display: block;
            max-width: 100%;
        }

        a img {
            border: none;
        }

        a {
            color: #ca0b7a;
        }

        p {
            margin: 1em 0;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        table td {
            border-collapse: collapse;
        }

        .o-fix table, .o-fix td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        body {
        }

        table {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 12px;
            color: #585858;
        }

        td, p {
            line-height: 24px;
            color: #000 /*Text*/;
            font-size: 18px;
            font-family: Helvetica;
            font-weight: 300;
        }

        td, tr {
            padding: 0;
        }

        ul, ol {
            margin-top: 24px;
            margin-bottom: 24px;
        }

        li {
            line-height: 24px;
        }

        a {
            color: #585858 /*Contrast*/;
            text-decoration: none;
            padding: 2px 0px;
        }

        a:link {
            color: #585858;
        }

        a:visited {
            color: #585858;
        }

        a:hover {
            color: #585858;
        }

        .h1 {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 26px;
            letter-spacing: -1px;
            margin-bottom: 16px;
            margin-top: 2px;
            line-height: 30px;
        }

        .h2 {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 20px;
            letter-spacing: 0;
            margin-top: 2px;
            line-height: 30px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: Helvetica, Arial, sans-serif;
            font-weight: normal;
        }

        h1 {
            font-size: 20px;
            letter-spacing: -1px;
            margin-bottom: 16px;
            margin-top: 4px;
            line-height: 24px;
        }

        h2 {
            font-size: 18px;
            margin-bottom: 12px;
            margin-top: 2px;
            line-height: 24px;
        }

        h3 {
            font-size: 14px;
            margin-bottom: 12px;
            margin-top: 2px;
            line-height: 24px;
        }

        h4 {
            font-size: 14px;
            font-weight: bold;
        }

        h5 {
            font-size: 12px;
        }

        h6 {
            font-size: 12px;
            font-weight: bold;
        }

        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
            color: #585858;
        }

        h1 a:active, h2 a:active, h3 a:active, h4 a:active, h5 a:active, h6 a:active {
            color: #585858 !important;
        }

        h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
            color: #585858 !important;
        }

        .wrap.header {
            border-top: 0px solid #E8E8E8 /*Content Border*/;
        }

        .wrap.footer {
            border-bottom: 1px solid #E8E8E8;
        }

        .wrap.body, .wrap.header, .wrap.footer {
            background-color: #FFFFFF /*Body Background*/;
            border-right: 0px solid #E8E8E8;
            border-left: 0px solid #E8E8E8;
        }

        .padd {
            width: 24px;
        }

        .small {
            font-size: 11px;
            line-height: 18px;
        }

        .separator {
            border-top: 1px dotted #E1E1E1 /*Separator Line*/;
        }

        .btn {
            margin-top: 10px;
            display: block;
        }

        .btn img {
            display: inline;
        }

        .subline {
            line-height: 18px;
            font-size: 16px;
            letter-spacing: -1px;
        }

        table.textbutton td {
            background: #efefef /*Text Button Background*/;
            padding: 1px 14px 4px 14px;
            color: #585858;
            display: block;
            height: 22px;
            border: 1px solid #E8E8E8;
            vertical-align: top;
        }

        table.textbutton a {
            color: #585858;
            font-size: 13px;
            font-weight: normal;
            line-height: 22px;
            width: 100%;
            display: inline-block;
        }

        div.preheader {
            line-height: 0px;
            font-size: 0px;
            height: 0px;
            display: none !important;
            display: none;
            visibility: hidden;
        }

        .lb-logo {
            display: inline-block;
            vertical-align: middle;
        }

        .bg {
            background-color: #F7F7F7;
        }

        .dark-gray {
            background-color: #909090;
        }

        @media only screen and (max-device-width: 480px) {
            body {
                -webkit-text-size-adjust: 120% !important;
                -ms-text-size-adjust: 120% !important;
            }

            table[class=bodytbl] .subline {
                float: left;
            }

            table[class=bodytbl] .padd {
                width: 12px !important;
            }

            table[class=bodytbl] .wrap {
                width: 470px !important;
            }

            table[class=bodytbl] .wrap table {
                width: 100% !important;
            }

            table[class=bodytbl] .wrap img {
                max-width: 100% !important;
                height: auto !important;
            }

            table[class=bodytbl] .wrap .m-100 {
                width: 100% !important;
            }

            table[class=bodytbl] .m-0 {
                width: 0;
                display: none;
            }

            table[class=bodytbl] .m-b {
                display: block;
                width: 100% !important;
            }

            table[class=bodytbl] .m-b-b {
                margin-bottom: 24px !important;
            }

            table[class=bodytbl] .m-1-2 {
                max-width: 264px !important;
            }

            table[class=bodytbl] .m-1-3 {
                max-width: 168px !important;
            }

            table[class=bodytbl] .m-1-4 {
                max-width: 120px !important;
            }

            table[class=bodytbl] .m-1-2 img {
                max-width: 264px !important;
            }

            table[class=bodytbl] .m-1-3 img {
                max-width: 168px !important;
            }

            table[class=bodytbl] .m-1-4 img {
                max-width: 120px !important;
            }
        }

        @media only screen and (max-device-width: 320px) {
            table[class=bodytbl] .wrap {
                width: 310px !important;
            }
        }

    </style>
    <table class="bodytbl bg" width="100%" cellspacing="0" cellpadding="0" >
        <tbody>
        <tr>
            <td background="" align="center">

                <table width="100%" height="100" cellspacing="0" cellpadding="0"
                       class="wrap">
                    <tbody>
                    <tr>
                        <td height="24" align="center" valign="middle">
                            <div class="preheader"></div>
                            <a name="top"></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table width="600" cellspacing="0" cellpadding="0"
                       class="wrap header">
                    <tbody>
                    <tr>
                        <td height="40" colspan="3" class="bg"></td>
                    </tr>
                    <tr>
                        <td valign="top" align="center">
                            <table width="100%" cellpadding="0"
                                   cellspacing="0" class="o-fix">
                                <tbody>
                                <tr>
                                    <td valign="top" align="left">
                                        <table width="600"
                                               cellpadding="0"
                                               cellspacing="0"
                                               align="left"
                                               class="m-b">
                                            <tbody>
                                            <tr>
                                                <td class="small m-b"
                                                    align="center"
                                                    valign="middle">
                                                    <img alt="logo"
                                                         title=""
                                                         height="100"
                                                         border="0"
                                                         editable=""
                                                         label="Logo"
                                                         src="http://tapiapp.com/app/public/images/Tapi-Logo.png">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <modules class="ui-sortable">
                    <module label="1/1 Column" auto="" class="active"
                            style="display: block;">
                        <table width="600" cellspacing="0" cellpadding="0"
                               class="wrap body">
                            <tbody>
                            <tr>
                                <td width="24" class="padd">&nbsp;</td>
                                <td valign="top" align="left">

                                    <multi label="Body" style="text-align: center;">
                                        <h2>TapiApp Contact Form Submission</h2>
                                        <p>From: <?php echo $_POST['fname'].' '.$_POST['lname']; ?> </p>
                                        <p>Subject: <?php echo $_POST['subject'] ?></p>
                                        <p>Email: <?php echo $_POST['email'] ?><p>
                                        <p>Message: <?php echo $_POST['message'] ?></p>
                                    </multi>

                                </td>
                                <td width="24" class="padd">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </module>
                </modules>

                <table cellpadding="0" cellspacing="0" class="wrap m-b-b">
                    <tbody>
                    <tr>
                        <td valign="top"><img
                                    src="http://tapiapp.com/app/public/images/placeholders/shadow.png"
                                    class="m-100" width="600"
                                    height="15" alt="" border="0"></td>
                    </tr>
                    </tbody>
                </table>
                <table width="600" cellspacing="0" cellpadding="0"
                       class="wrap bg">
                    <tbody>
                    <tr>
                        <td valign="top" align="left">

                            <multi label="Body">
                                <p style="line-height: 15px; margin-top: 0; margin-bottom: 0;
    ">
                                    <span style="font-family: Helvetica; font-size: 14px; display: block;text-align: center;width: 100%"> Copyright © 2016-2017 TapiApp, All rights reserved. </span>
                                </p>
                            </multi>
                            <div class="btn">
                            </div>

                        </td>
                        <td width="24" class="padd">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="32" colspan="3"></td>
                    </tr>
                    </tbody>
                </table>
                <table width="100%" height="100" cellspacing="0" cellpadding="0"
                       class="wrap">
                    <tbody>
                    <tr>
                        <td height="24" align="center" valign="middle">
                            <div class="preheader"></div>
                            <a name="top"></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    </body>
    </html>




    <?php

        $message = ob_get_contents(); 

        ob_end_clean();  
        $mail->Body    = $message; 

        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            header('Location: http://tapiapp.com?contact_success=1#contact');
        }
        
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();

} else {
    header('Location: https://www.tapiapp.com/');
}

} else {
    echo 'Invalid Request';
}

function get_ip() {
    //Just get the headers if we can or else use the SERVER global
    if ( function_exists( 'apache_request_headers' ) ) {
        $headers = apache_request_headers();
    } else {
        $headers = $_SERVER;
    }
    
    //Get the forwarded IP if it exists
    if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
        $the_ip = $headers['X-Forwarded-For'];
    } elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
    ) {
        $the_ip = $headers['HTTP_X_FORWARDED_FOR'];
    } else {
        
        $the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
    }
    return $the_ip;
}