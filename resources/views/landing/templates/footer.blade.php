<div id="termsModal" class="modal fade" role="dialog">
    @include('landing.terms')
</div>

<footer class="clearfix">
    <!-- <a href="https://leadingbrands.me" target="_blank">
        <img src="img/lb.png" height="40" alt="Leading Brands Logo" title="Leading brands Logo"></a> -->
    <!-- <span class="divider"></span> -->
    <a href="#">
        <img src="{{ asset('public/landing/img/tapi.png') }}" alt="Tapi Logo" title="Tapi Logo" height="40">
    </a><br>
    <span id="copyright">© 2017 Copyright</span>
    <div class="container">
        <a class="btn btn-link pull-left padding-0 text-white"  style="display: inline-block;float: none !important;" href="#" data-toggle="modal" data-target="#termsModal">Terms of Use</a><br>
    </div>
</footer>

<script type="text/javascript" src="{{ asset('public/landing/js/vendor/jquery-1.11.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/landing/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/landing/html5lightbox/html5lightbox.js') }}"></script>
<script src="https://tapiapp.com/app/public/plugins/bootstrap-select-1.10.0/js/bootstrap-select.min.js"></script>
<script>
    [
        '{{ asset('public/landing/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}',
        '{{ asset('public/landing/js/main.js') }}',
        '{{ asset('public/landing/js/vendor/bootstrap.min.js') }}',

    ].forEach(function(src) {
        var script = document.createElement('script');
        script.src = src;
        script.async = false;
        document.head.appendChild(script);
    });

    var slickOn = false;
    var resizeFlag = false;

    $(document).ready(function(){
        // $('.slider').slick({
        //     fade : true,
        //     arrows : false,
        //     autoplay: true.
        //     draggable: false,
        // });
    });

    function activateSLiders() {
        windowsize = $(window).width();
        //if the window is greater than 440px wide then turn on jScrollPane..
        if(slickOn==false){
            if(windowsize < 992 && windowsize > 620){

                $('.plan-slider').slick({
                    autoplay: true,
                    arrows : false,
                    slidesPerRow : 2,
                    slidesToShow : 2,
                    dots : true
                });
                slickOn = true;

            } else if (windowsize < 620) {

                $('.plan-slider').slick({
                    autoplay: true,
                    arrows : false,
                    slidesPerRow : 1,
                    slidesToShow : 1,
                    dots : true
                });
                slickOn = true;
            }
        } else {
            console.log('slick is on');
            if(windowsize < 992 && windowsize > 620){

                $('.plan-slider').slick('slickSetOption', 'slidesPerRow', 2, true);
                $('.plan-slider').slick('slickSetOption', 'slidesToShow', 2, true);

            } else if (windowsize < 620) {

                $('.plan-slider').slick('slickSetOption', 'slidesPerRow', 1, true);
                $('.plan-slider').slick('slickSetOption', 'slidesToShow', 1, true);
            }
        }
    }

    $(window).resize(function(){

        if(resizeFlag==false){

            resizeFlag = true;

            setTimeout(function(){
                windowsize = $(window).width();
                if (windowsize < 992) {
                    //if the window is greater than 440px wide then turn on jScrollPane..
                    activateSLiders();
                } else if(windowsize > 991) {

                    if(slickOn)
                        $('.plan-slider').slick('unslick');

                    slickOn = false;
                }

                resizeFlag = false;
            },500);
        }
    });

    $(window).load(function(){
        windowsize = $(window).width();
        if (windowsize < 992) {
            //if the window is greater than 440px wide then turn on jScrollPane..
            activateSLiders();
        }
    });

    setInterval(function(){
        if($(window).width()>991){
            updateVCenter()
        }
    },300);


    $(window).resize(function(){
        if($(window).width()>991){
            setTimeout(function(){
                updateVCenter();
            },1000);
        }
    });

    function updateVCenter() {
        $('.vcenter').each(function(){
            height = -($(this).outerHeight() / 2);
            width = -($(this).outerWidth() / 2);

            $(this).css({
                'margin-top' : height+'px',
                'top' : '50%',
            })
        });
    }

    $('#termsBt').on('click', function(){
        $('#termsModal').modal('show');
    });

    $('#mobile-bt').on('click', function(){
        $('#mobileable').toggle();
    });


</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-1910012-65', 'auto');
    ga('send', 'pageview');
</script>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  $('#contactForm').on('submit', function (e) {
  var response = grecaptcha.getResponse();
    //recaptcha failed validation
           if(response.length == 0) {
               e.preventDefault();
               alert('Please verify through captcha');
            }
            //recaptcha passed validation
            else {
               $('#html_element').hide();
            }
            if (e.isDefaultPrevented()) {
              return false;
            } else {
              return true;
            }
        });
</script>

@yield('js')
</body>
</html>