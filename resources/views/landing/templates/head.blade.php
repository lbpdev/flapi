<!doctype html lang="en-GB">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Online team scheduling and project management and planning software - TapiApp</title>
    <meta name="description" content="TapiApp is a customizable online team scheduling, project management and planning software.">
    <meta name="keywords" content="TapiApp,scheduling,production,project,management,spreadsheets,software,Excel,app">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Dublin Core -->
    <meta name="DC.Title" content="Tapi App - Official Website">
    <meta name="DC.Creator" content="Leading Brands JLT">
    <meta name="DC.Description" content="TapiApp is a customizable online team scheduling, project management and planning software.">
    <meta name="DC.Language" content="English">

    <!-- Open Graph data -->
    <meta property="og:title" content="Tapi App - Official Website" />
    <meta property="og:type" content="web page" />
    <meta property="og:url" content="http://www.tapiapp.com/" />
    <meta property="og:description" content="TapiApp is a customizable online team scheduling, project management and planning software." />
    <meta property="og:site_name" content="http://www.tapiapp.com/" />

    <link rel="stylesheet" media="print" href="{{ asset('public/landing/css/print.css') }}">
    <link rel="stylesheet" media="all" href="{{ asset('public/landing/css/main.css') }}">
    <link rel="stylesheet" media="all" href="{{ asset('public/landing/css/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://tapiapp.com/app/public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css">

    <link rel="shortcut icon" type="image/png" href="{{ asset('public/favicon.ico') }}"/>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('public/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('public/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <style>

        body {
            font-family: 'Source Sans Pro', sans-serif;
            /*font-family: Helvetica;*/
            /*font-weight: lighter;*/
            text-rendering: optimizeLegibility;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            font-size: 20px;
            line-height: 24px;
        }

        footer a, footer a:hover {
            font-size: 14px !important;
        }

        .modal-body p {
            font-size: 18px;
            line-height: 23px;
            padding: 0 10px;
            margin-bottom: 0;
        }

        .full-width {
            width: 100%;
        }

        .full-98 {
            width: 98%;
        }

        .margin-t-30 {
            margin-top: 30px;
        }
        .padding-t-0 {
            padding-top: 0 !important;
        }
        .padding-l-30 {
            padding-left: 30px;
        }
        .width-700 {
            width: 700px;
        }
        .text-left {
            text-align: left;
        }
        .text-white {
            color: #fff;
        }

        .g-recaptcha {
            display: inline-block;
        }

        a:hover, a:focus {
            color: #fff !important;
        }

        #more-contact {
            background-color: transparent;
            color: #158da6 !important;
        }

        h2 {
            font-size: 40px;
            line-height: 42px;
            margin-bottom: 30px;
            margin-top: 40px;
            font-weight: lighter;
        }

        .padding-lr-40 {
            padding-left: 40px;
            padding-right: 40px;
        }

        .padding-tb-40 {
            padding-top: 40px;
            padding-bottom: 40px;
        }

        .padding-tb-60 {
            padding-top: 60px;
            padding-bottom: 60px;
        }

        .margin-l-n30 {
            margin-left: -30px;
        }


        .margin-l-n60 {
            margin-left: -60px;
        }

        .margin-r-n30 {
            margin-right: -30px;
        }

        .margin-r-60 {
            margin-right: 60px;
        }

        .slick-slider {
            min-height: 230px;
        }

        #html5-watermark {
            display: none !important;
        }

        #html5-elem-box {
            height: auto !important
        }

    </style>
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->

    @yield('css')
    <noscript>
        <style type="text/css">
            .nojs {display:none !important;}
        </style>
    </noscript>
</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<nav class="navbar" id="nav">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a class="" href="{{ url('/') }}">
                    <img src="{{ asset('public/flapielements/Falpi-Logo.png') }}" height="61" alt="Tapi Logo" title="Tapi Logo">
                </a>
            </div>

            <button type="button" class="navbar-toggle collapsed" id="mobile-bt">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="col-md-9 col-sm-9 col-xs-9 text-right mobile-nav">
                <div class="main-nav trial">
                    <a href="{{ url('/pricing') }}#register-panel" class="pink">Free Trial</a>
                </div>
                <div class="main-nav" id="mobileable">
                    <a class="dark <?php echo strlen($_SERVER['REQUEST_URI']) < 3 ? 'active' : '' ;?>" href="{{ url('/') }}">Features</a>
                    <a class="dark <?php echo strpos($_SERVER['REQUEST_URI'], 'pricing') ? 'active' : '' ;?>" href="{{ url('/pricing') }}">Pricing</a>
                    <a class="dark" href="{{ route('login') }}">Login</a>
                    <a href="{{ url('/pricing') }}#register-panel" class="pink trial-mobile">Free Trial</a>
                </div>
            </div><!--/.navbar-collapse -->
        </div>
    </div>
</nav>