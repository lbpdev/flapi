<style>
    .layout-page {
        width:70px;
        height: 100px;
        background-color: #ccc;
        position: relative;
    }
    
    .layout-page .section {
        position: absolute;
        background-color: red;
        box-sizing: border-box;
        border:1px solid;
    }

    .layout-page .section:hover {
        border-color: red !important;
    }
</style>

<div class="layout-page">
    @foreach($layout->sections as $section)
        <div class="section" style="width: {{$section->width}}%; height: {{$section->height}}%; right:{{ $section->right>=0 ? $section->right.'px' : ''}}; left:{{ $section->left>=0 ? $section->left.'px' : ''}};  bottom:{{ $section->bottom>=0 ? $section->bottom.'px' : ''}};  top:{{ $section->top>=0 ? $section->top.'px' : ''}}; border-color:red; background-color: #ccc;"></div>
    @endforeach
</div>