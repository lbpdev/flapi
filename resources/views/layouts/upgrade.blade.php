<div id="plansModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center" style="width: 700px">
        <!-- Modal content-->
        <div class="modal-content clearfix">
            <div class="modal-header clearfix">
                <h4 class="modal-title">Subscription Plans</h4>
            </div>
            <div class="modal-body clearfix width-full">
                @inject('subService','App\Services\SubscriptionService')

                <?php $subscription = $subService->getSubscription(); ?>

                <div id="plans" class="plan-slider">
                    <div class="col-md-3 text-center">
                        <div class="row">
                            <div class="item {{ $subscription->plan->code!='TAPI_PLAN_1' ? '' : ( $subscription->SubscriptionReference ? "active" : '' ) }}" style="">
                                <h1>Plan 1</h1>
                                <div class="content">
                                    <p><strong>5</strong> users</p>
                                    <p><strong>5</strong> flatplans <br>(can be for multiple publications)</p>
                                    <p>Maximum <strong>80+4</strong>  <br>pages per flatplan</p>
                                    <p>$15/month</p>
                                </div>
                                <p>
                                    @if($subscription->plan->code!='plan-1'&&!$subscription->SubscriptionReference)
                                        <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4708750&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                    @elseif($subscription->plan->code=='plan-1'&&$subscription->SubscriptionReference)
                                        @if($subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone))
                                            <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&DOTEST=1&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                            <span class="current">Current</span>
                                        @else
                                            <a href="#">Active</a>
                                            <span class="current">Current</span>
                                        @endif
                                    @else
                                        @if($subscription->SubscriptionReference)
                                            <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&DOTEST=1&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @else
                                            <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4708750&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @endif
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="row">
                            <div class="item {{ $subscription->plan->code!='plan-2' ? '' : "active" }}" style="">
                                <h1>Plan 2</h1>
                                <div class="content">
                                    <p><strong>10</strong> users</p>
                                    <p><strong>5</strong> flatplans <br> (can be for multiple publications)</p>
                                    <p>Maximum <strong>160 + 4</strong>  <br>pages per flatplan</p>
                                    <p>$25/month</p>
                                </div>
                                <p>
                                    @if($subscription->plan->code!='plan-2')
                                        @if($subscription->SubscriptionReference)
                                            <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&DOTEST=1&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @else
                                        <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4708751&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @endif
                                    @else
                                        <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&DOTEST=1&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                        <span class="current">Current</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="row">
                            <div class="item {{ $subscription->plan->code!='plan-3' ? '' : "active" }}" style="">
                                <h1>Plan 3</h1>
                                <div class="content">
                                    <p><strong>15</strong> users</p>
                                    <p><strong>5</strong> flatplans <br> (can be for multiple publications)</p>
                                    <p>Maximum <strong>240 + 4</strong>  <br>pages per flatplan</p>
                                    <p>$35/month</p>
                                </div>
                                <p>
                                    @if($subscription->plan->code!='plan-3')
                                        @if($subscription->SubscriptionReference)
                                            <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&DOTEST=1&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @else
                                        <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4708752&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @endif
                                    @else
                                        <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&DOTEST=1&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                        <span class="current">Current</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="row">
                            <div class="item {{ $subscription->plan->code!='plan-4' ? '' : "active" }}" style="border: 0;">
                                <h1>Plan 4</h1>
                                <div class="content">
                                    <p><strong>20</strong> users</p>
                                    <p><strong>5</strong> flatplans <br> (can be for multiple publications)</p>
                                    <p>Maximum <strong>400+4</strong>  <br>pages per flatplan</p>
                                    <p>$60/month</p>
                                </div>
                                <p>
                                    @if($subscription->plan->code!='plan-4')
                                        @if($subscription->SubscriptionReference)
                                            <a target="_blank" href="https://secure.avangate.com/order/upgrade.php?LICENSE={{$subscription->SubscriptionReference}}&DOTEST=1&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @else
                                            <a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4708753&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID={{ Auth::user()->id }}&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you">Buy Now</a>
                                        @endif
                                    @else
                                        <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&DOTEST=1&ADDITIONAL_USER_ID={{Auth::user()->id}}">Renew</a>
                                        <span class="current">Current</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix pull-left width-full">
            </div>
        </div>

    </div>
</div>