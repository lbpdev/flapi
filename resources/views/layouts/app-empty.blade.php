<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>


@if(isset($_GET['print']))
    <body onload="window.print();">
@else
    <body>
@endif

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tapi App</title>

    <!-- Fonts -->
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>--}}

            <!-- Styles -->
    <link href="{{ asset('public/css/bootstrap.no-print.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">
    <link href='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}' rel='stylesheet' />

    <style>
        html,
        body {
            height: 100%;
            min-height: 100%;
            padding: 0 !important;
        }

        body {
            font-family: 'Helvetica Neue';
            font-weight: 100;
        }

        .fa-btn {
            margin-right: 6px;
        }

        #addProject {
            font-size: 18px;
            margin-top: 5px;
        }

        #subscription-overlay {
            background-color: rgba(0,0,0,0.8);
            height: 100%;
            width: 100%;
            position: fixed;
            top: 22px;
            left: 0;
            z-index:3000;
        }

        #subscription-overlay .message {
            width: 400px;
            height: 100px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -200px;
            margin-top: -50px;
            background-color: transparent;
            font-size: 24px;
            color: #fff;
            text-align: center;
            line-height: 30px;
        }

        #mobile-overlay {
            display: none;
        }

        @media screen and (max-width: 991px){
            #mobile-overlay {
                display: block;
                background-color: rgba(0,0,0,0.9);
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                z-index:3000;
            }

            #mobile-overlay .message {
                width: 400px;
                height: 100px;
                position: absolute;
                top: 20%;
                left: 50%;
                margin-left: -200px;
                margin-top: -50px;
                background-color: transparent;
                font-size: 19px;
                color: #fff;
                text-align: center;
                line-height: 30px;
                padding: 30px;
            }
        }

        #plansModal {
            z-index: 30001;
        }
    </style>

    <style>
        .supportButton {
            line-height: 100%;
            font-size: 14px;
            font-weight: 300;
            background-color: #cf2982;
            color: #fff;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(191, 28, 115);
            border-image: initial;
            padding: 0;
            border-radius: 999rem;
            z-index: 999998;
            transform: translateZ(0px);
            position: fixed;
            opacity: 1;
            right: 0px;
            bottom: 0px;
            width: 80px;
            height: 30px;
            margin: 10px 20px;
            font-family: Helvetica;
        }
    </style>

    @yield('style')

</head>


<body id="app-layout" style="">
<div id="main-content" class="relative">
    @yield('content')
</div>
</body>

</html>
