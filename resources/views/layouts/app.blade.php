<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Flapi App') }}</title>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tapi App</title>

    <link rel="shortcut icon" type="image/png" href="{{ asset('public/favicon.ico') }}"/>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('public/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('public/safari-pinned-tab.svg') }}" color="#5bbad5">

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/fonts/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/bootstrap-overrides.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/global.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/bootstrap-select-1.10.0/css/bootstrap-select.css') }}">
    <link href='{{ asset('public/plugins/jquery-daterangepicker/jquery.comiseo.daterangepicker.css') }}' rel='stylesheet' />
    @yield('style')

    <style>
        html,
        body {
            height: 100%;
            min-height: 100%;
            padding: 0 !important;
        }

        body {
            font-family: 'Helvetica Neue';
            font-weight: 100;
        }

        .fa-btn {
            margin-right: 6px;
        }

        #addProject {
            font-size: 18px;
            margin-top: 5px;
        }

        #subscription-overlay {
            background-color: rgba(0,0,0,0.8);
            height: 100%;
            width: 100%;
            position: fixed;
            top: 22px;
            left: 0;
            z-index:3000;
        }

        #subscription-overlay .message {
            width: 400px;
            height: 100px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -200px;
            margin-top: -50px;
            background-color: transparent;
            font-size: 24px;
            color: #fff;
            text-align: center;
            line-height: 30px;
        }

        #mobile-overlay {
            display: none;
        }

        @media screen and (max-width: 991px){
            #mobile-overlay {
                display: block;
                background-color: rgba(0,0,0,0.9);
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                z-index:3000;
            }

            #mobile-overlay .message {
                width: 400px;
                height: 100px;
                position: absolute;
                top: 20%;
                left: 50%;
                margin-left: -200px;
                margin-top: -50px;
                background-color: transparent;
                font-size: 19px;
                color: #fff;
                text-align: center;
                line-height: 30px;
                padding: 30px;
            }
        }

        #plansModal {
            z-index: 30001;
        }
    </style>

    <style>
        .supportButton {
            line-height: 100%;
            font-size: 14px;
            font-weight: 300;
            background-color: #cf2982;
            color: #fff;
            border-width: 1px;
            border-style: solid;
            border-color: rgb(191, 28, 115);
            border-image: initial;
            padding: 0;
            border-radius: 999rem;
            z-index: 999998;
            transform: translateZ(0px);
            position: fixed;
            opacity: 1;
            right: 0px;
            bottom: 0px;
            width: 80px;
            height: 30px;
            margin: 10px 20px;
            font-family: Helvetica;
        }
    </style>
</head>


@if(!isset($is_public))
    @inject('subService','App\Services\SubscriptionService')
    <?php $subscription = $subService->getSubscription(); ?>
@endif


@if(!isset($is_public))
    <body id="app-layout" style="{{ !is_null($subscription) ? ( $subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone) ? 'overflow:hidden' : '' ) :  '' }}">
@else
    <body id="app-layout">
@endif

<div id="globalAlert" class="alert"></div>
<div id="overlay-modal" style="display: block">
    <div id="spinner" style="display: block">
        <img src="{{ asset('public/img/ripple.svg') }}">
    </div>
</div>
@if(!isset($is_public))

<div class="col-md-12 top-nav no-print">
    <div class="row">
        <div class="container">

            @if(count($subscription) && !Auth::user()->isClient)
                <div class="pull-left nav-menu">
                        <span class="pull-left">
                            <span class="font-12" style="margin-right: 5px">{{ $subscription->plan->name }} {{ $subscription->SubscriptionReference ? '' : ' - Trial' }}
                                (Expires on {{ $subscription->ExpirationDate->format('M d Y') }})</span>

                            @if(Auth::user()->isAdmin || Auth::user()->isScheduler)
                                <span class="font-12"> |
                                    @if($subscription->SubscriptionReference)
                                        <a href="https://secure.avangate.com/renewal/?LICENSE={{ $subscription->SubscriptionReference }}&DOTEST=1&ADDITIONAL_USER_ID={{Auth::user()->id}}"><button>Renew</button></a> |
                                        <a href="#" class="show-plans"><button>Change Plan</button></a> |
                                        <a href="{{ route('api.company.toggleAutoRenew',$subscription->SubscriptionReference) }}">{!! $subscription->RecurringEnabled ? '<button>Disable Auto-renew</button>' : '<button>Enable Auto-renew </button>'  !!}</a>
                                    @else
                                        <a href="#" class="show-plans"><button>Subscribe Now</button></a>
                                    @endif
                                 </span>
                            @endif

                        </span>
                </div>
            @endif

                <div class="pull-right nav-menu">
                    @if(Auth::user()->isAdmin)
                        <a href="{{ route('settings.info') }}" id="settings-page-Link"></a>
                    @endif
                    <a href="{{ route('logout') }}" id="logout-page-Link" class="margin-r-0"></a>
                </div>
        </div>
    </div>
</div>
@endif

<nav class="navbar navbar-default navbar-static-top no-print">
    <div class="container">
            @if(!isset($is_public))
                <div class="col-md-6 text-left paddring-r-0">
                    @inject('company', 'App\Services\CompanyService')

                    @if(Request::path()!="settings")
                        <a class="navbar-brand" href="{{ route('flatplans.index') }}">
                    @endif
                            @if($company->getLogo())
                                <img src="{{ $company->getLogo() }}" id="lb-logo" height="82">
                            @else
                                <div id="lb-logo" class="logo-placeholder"></div>
                            @endif
                    @if(Request::path()!="settings")
                        </a>
                    @endif
                </div>
            @else
                <div class="col-md-6 text-left paddring-r-0">
                    <a class="navbar-brand" href="#">
                        @if($company->logo)
                            <img src="{{ $company->logo }}" id="lb-logo" height="82">
                        @else
                            <div id="lb-logo" class="logo-placeholder"></div>
                        @endif
                    </a>
                </div>
            @endif

            <div class="col-md-6 text-right padding-r-0">
                <a class="navbar-brand padding-b-0 padding-r-0" href="{{ url('/') }}">
                    <img src="{{ asset('public/img/Flapi-Logo-Dark.png') }}" id="lb-logo" height="78">
                </a>
            </div>
    </div>
    @if(!isset($is_public))
        <div class="container" id="app-nav">
            <a  class="{{ Request::is('flat-plans/*') || Request::is('flat-plans') ? 'active' : '' }}" href="{{ route('flatplans.index') }}" id="sched-link">
                Flatplans
            </a>
            @if(in_array('view-archives',Auth::user()->permissionsArray) && !Auth::user()->isClient)
                <a class="{{ Request::is('archive/*') || Request::is('archive') ? 'active' : '' }}"  href="{{ route('flatplans.archived') }}">
                    Archive
                </a>
            @endif
        </div>
    @endif
</nav>

<div id="main-content" class="relative">
    @yield('content')
</div>


@if(!isset($is_public))
    @if($subscription->ExpirationDate <= \Carbon\Carbon::now()->setTimezone(Auth::user()->companyTimezone))
        @if(Auth::user()->isAdmin || Auth::user()->isScheduler)
            <div id="subscription-overlay">
                <div class="message">
                    Your subscription has expired. Please renew your subscription to continue using TapiApp.<br><br>
                    <a href="#" class="show-plans"><button>Subscribe Now</button></a>
                </div>
            </div>

        @endif
    @endif
@endif

<div id="mobile-overlay">
    <div class="message">
        Sorry but this app is only currently compatible on desktop. If you are on mobile then please download the mobile app below: <br><br>
        <img src="{{ asset('public/img/Apple-Store-min.png') }}" width="160">
        <img src="{{ asset('public/img/Play-Store-min.png') }}" width="160">
    </div>
</div>

@include('layouts.upgrade')

<!-- JavaScripts -->
<script src="{{ asset('public/js/jquery.min.js') }}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{ asset('public/js/moment.js') }}"></script>

<script type="text/javascript">
    var globalAlert = $('#globalAlert');
    var rightOffset = ($('#main-content .container')[0].offsetLeft ) - 50;

    $(window).load(function(){
        $('#overlay-modal').fadeOut(500);
    });

    $(window).resize(function(){
        setTimeout(function(){
            rightOffset = ($('#main-content .container')[0].offsetLeft ) - 50;
        },500);
    });

    var my_timer;

    function alertFade() {
        my_timer = setTimeout(function () {
            globalAlert.animate({
                'top' : '-150%',
                'opacity' : '0',
                'display' : 'none',
            }, 1000,function(){ $(this).attr('class','alert').text(''); });
        }, 5000);
    };

    function doAlert(response){

        clearTimeout(my_timer);

        $(globalAlert).html(response.message);

        nudge = $(globalAlert).outerWidth() / 2;

        $(globalAlert).dequeue().stop(true,true).finish().css('margin-right','-'+nudge+'px');

        if(response.status==200)
            globalAlert.addClass('alert-success').stop(true,true).animate({
                'top' : '15px',
                'opacity' : '1',
                'display' : 'block',
            },50);
        else
            globalAlert.addClass('alert-danger').stop(true,true).animate({
                'top' : '15px',
                'opacity' : '1',
                'display' : 'block',
            },50);

        alertFade();
    }

    $('.show-plans').on('click',function(){
        $('#plansModal').modal('show');
    });

    var appUrl = '{{ url('/') }}';


    function showLoader() {
        $('#overlay-modal').show();
        $('#spinner').show();
    }


    function hideLoader() {
        $('#overlay-modal').hide();
        $('#spinner').hide();
    }

    function addLoader(parent,color,size){
        size = size ? size : '';
        color = color ? color : '';

        parent.append('<div class="loader '+size+' '+color+'"></div>');
    }

    function removeLoader(parent){
        setTimeout(function(){
            parent.find('.loader').remove();
        },200);
    }
</script>

@yield('js')

<script>

    $('a').on('click',function(e){
        if($(this).attr('href')=='#')
        e.preventDefault();
    });

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-1910012-70', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
