<?php namespace App\Services;

use App\Models\Color;
use App\Models\Option;
use App\Models\User;
use Carbon\Carbon;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Support\Facades\Auth;

class PushNotificationService {

    public function send($user,$message){

        if($user){
            $devices = $user->devices;
            foreach ($devices as $device){
                PushNotification::app( $device->os == "ios" ? 'appNameIOS' : 'appNameAndroid')
                    ->to($device->push_token)
                    ->send($message);
            }
        }
    }
}
