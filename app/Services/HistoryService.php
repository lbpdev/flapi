<?php namespace App\Services;

use App\Models\ActionHead;
use App\Models\ActionHistory;
use App\Models\Flatplans\FlatPlanVersion;

class HistoryService {

    public function undo($flat_plan_version_id){

        $head = ActionHead::where('flat_plan_version_id',$flat_plan_version_id)->first();

        $data = [];

        if($head){
            $actions = $head->actions()->get();

            foreach ($actions as $action){
                if($action->type=='delete'){
                    $target = $this->getTarget($action);
                    $target->deleted_at = null;
                    $target->save();

                    $data[] = $target;
                }
                elseif($action->type=='create'){
                    $target = $this->getTarget($action);
                    $target->delete();

                    $data[] = $target;
                }
                elseif($action->type=='layout-change'){
                    $target = $this->getTarget($action);
                    $target->layout_id = $action->value_old;
                    $target->save();

                    foreach($target->sections as $index=>$section){
                        foreach($action->pivots as $pivot){
                            if($pivot->actionable_id == $section->id){

                                if(!$pivot->value_old) {
                                    $section->delete();
                                }
                                else {
                                    $section->layout_section_id = $pivot->value_old;
                                    $section->save();
                                }
                            }
                        }
                    }


                    $data[] = $target;
                }
                elseif($action->type=='section-data-change'){
                    $model = $action->actionable()->getModel();
                    $target =  $model->find($action->actionable_id);
                    $target->layout_id = $action->value_old;

                    foreach($target->data as $index=>$datas){
                        foreach($action->pivots as $pivot){

                            if($pivot->actionable_id == $datas->id){
                                $datas->value = $pivot->value_old;
                                $datas->save();

                            }
                        }
                    }

                    $data[] = $target;
                }
            }

            $flag = false;

            $actions = $head->version->actionsByHead;
            foreach ($actions as $action){

                if($flag){
                    $head->action_head_id = $action->head;
                    $head->save();
                    break;
                }

                if($action->head == $head->action_head_id)
                    $flag = true;
            }
        }

        return $data;
    }

    public function redo($flat_plan_version_id){

        $head = ActionHead::where('flat_plan_version_id',$flat_plan_version_id)->first();

        $data = [];

        if($head){
            $actions = $head->actions()->get();

            foreach ($actions as $action){
                if($action->type=='delete'){
                    $target = $this->getTarget($action);
                    $target->delete();

                    $data[] = $target;
                }
                elseif($action->type=='create'){
                    $target = $this->getTarget($action);
                    $target->deleted_at = null;
                    $target->save();

                    $data[] = $target;
                }
            }

            $flag = false;

            $actions = $head->version->actionsByHeadReverse;

            foreach ($actions as $index=>$action){

                if($flag){
                    $head->action_head_id = $action->head;
                    $head->save();
                    break;
                }

                if($action->head == $head->action_head_id)
                    $flag = true;
            }
        }
        return $data;
    }

    private function getTarget($action){
        $model = $action->actionable()->getModel();
        return $model->withTrashed()->find($action->actionable_id);
    }
}
