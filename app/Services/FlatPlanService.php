<?php namespace App\Services;

use App\Models\Flatplans\FlatPlan;
use App\Models\Flatplans\FlatPlanPage;
use App\Models\Flatplans\FlatPlanVersion;

class FlatPlanService {

    public function getStatus(FlatPlanVersion $version){

        foreach ($version->pages as $index=>$page){
            $version->pages[$index]['completed'] = false;
            $version->pages[$index]['approved'] = false;

            $totalCompletes = $page->toComplete;
            $completed = 0;
            $totalApproves = $page->toApprove;
            $approved = 0;

            foreach ($page->sections as $section){
                if($section->isComplete)
                    $completed++;

                if($section->isApproved)
                    $approved++;
            }

            if($totalCompletes == $completed)
                $version->pages[$index]['completed'] = true;

            if($totalApproves == $approved)
                $version->pages[$index]['approved'] = true;
        }

        return $version;
    }

    public function getPageStatus(FlatPlanPage $page){

        $page['completed'] = false;
        $page['approved'] = false;

        $totalCompletes = $page->toComplete;
        $completed = 0;
        $totalApproves = $page->toApprove;
        $approved = 0;

        foreach ($page->sections as $section){
            if($section->isComplete)
                $completed++;

            if($section->isApproved)
                $approved++;
        }

        if($totalCompletes == $completed)
            $page['completed'] = true;

        if($totalApproves == $approved)
            $page['approved'] = true;

        return $page;
    }
}
