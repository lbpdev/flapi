<?php namespace App\Services;

use App\Models\Color;
use App\Models\Option;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserService {

    public function getAllGuests(){
        return $this->currentCompany()->users()->clientsOnly()->pluck('name','id');
    }

    public function getAllEmployees(){
        return $this->currentCompany()->users()->employeesOnly()->where('id','!=',Auth::user()->id)->pluck('name','id');
    }

    public function getAllRoles(){
        return Role::with('permissions')->where('company_id',$this->currentCompany()->id)->withGlobals()->pluck('name','id');
    }

    public function currentCompany(){
        return Auth::user()->company;
    }

    public function getRoleById($id){
        $user = User::find($id);

        if($user)
            return $user->role ? $user->role->id : null;

        return null;
    }

}