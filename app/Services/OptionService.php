<?php namespace App\Services;

use App\Models\Option;
use Illuminate\Support\Facades\Auth;

class OptionService {

    public function getOption($name){
        $option = Option::where('name',$name)->where('company_id',Auth::user()->company->id)->first();
        return $option;
    }
}