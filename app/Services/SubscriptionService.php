<?php namespace App\Services;

use App\Models\Color;
use App\Models\Subscription;
use Illuminate\Support\Facades\Auth;

class SubscriptionService {

    public function getSubscription(){

        $data = Subscription::with('plan')->where('company_id',Auth::user()->company->id)->orderBy('created_at','DESC')->first();

        return $data ? $data : 'null';
    }
}