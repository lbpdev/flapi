<?php namespace App\Services;

use App\Models\Color;
use App\Models\Option;
use Illuminate\Support\Facades\Auth;

class CompanyService {

    public function getLogo(){

        $companyLogo = Option::where('name','logo')->where('company_id',Auth::user()->company->id)->first();

        return $companyLogo ? asset('public/uploads/logo/'.$companyLogo->value) : null;

    }
}