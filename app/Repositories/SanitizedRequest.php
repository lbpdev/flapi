<?php namespace App\Repositories;

trait SanitizedRequest {

    private $clean = false;

    public function all(){
        return $this->sanitize(parent::all());
    }


    protected function sanitize(Array $inputs){

        foreach($inputs as $i => $item){
            $inputs[$i] = strip_tags(trim($item));
        }

        return $inputs;
    }
}
