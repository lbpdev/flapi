<?php namespace App\Repositories;

use App\Models\ActionHead;
use App\Models\Flatplans\FlatPlanPageMerge;
use Illuminate\Support\Str;
trait CanHandleHistory {

    public function createHistory( $model , $type , $flatplan_version_id , $oldValue = null , $newValue = null ){

        $currentHead = ActionHead::where('flat_plan_version_id',$flatplan_version_id)->first();

        if($type=='delete'){
            $history = $model->actions()->create([
                'type' => $type,
                'head' => Str::random(32),
                'flat_plan_version_id' => $flatplan_version_id,
            ]);
        }
        elseif($type=='layout-change'){
            $history = $model->actions()->create([
                'type' => $type,
                'head' => Str::random(32),
                'value_old' => $oldValue['page'],
                'value_new' => $newValue,
                'flat_plan_version_id' => $flatplan_version_id,
            ]);
            foreach($model->sections as $index=>$section)
                $section->actions()->create([
                    'value_old' => isset($oldValue['sections'][$index]) ? $oldValue['sections'][$index]->layout_section_id : null,
                    'value_new' => $section->layout_section_id,
                    'action_history_id' => $history->id,
                ]);
        }
        elseif($type=='section-data-change'){

            $history = $model->action()->create([
                'type' => $type,
                'head' => Str::random(32),
                'value_old' => null,
                'value_new' => null,
                'flat_plan_version_id' => $flatplan_version_id,
            ]);

            if($history){
                foreach($model->data as $index=>$data){
                    if(isset($oldValue[$index])){
                        $data->actions()->create([
                            'value_old' => $oldValue[$index],
                            'value_new' => $data->value,
                            'action_history_id' => $history->id,
                        ]);
                    }
                }
            }
        }

        if($currentHead)
            $currentHead->update(['action_head_id' => $history->head]);
        else
            ActionHead::create([
                'flat_plan_version_id' => $flatplan_version_id,
                'action_head_id' => $history->head,
            ]);
    }

    public function createHistories($model,$type,$flatplan_version_id){

        $currentHead = ActionHead::where('flat_plan_version_id',$flatplan_version_id)->first();

        $head = Str::random(32);

        if($type=='create'){

            foreach ($model as $row){
                $history = $row->actions()->create([
                    'type' => $type,
                    'head' => $head,
                    'flat_plan_version_id' => $flatplan_version_id,
                ]);
            }
        }

        if($currentHead)
            $currentHead->update(['action_head_id' => $head]);
        else
            ActionHead::create([
                'flat_plan_version_id' => $flatplan_version_id,
                'action_head_id' => $head,
            ]);
    }
}

