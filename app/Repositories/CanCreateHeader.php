<?php namespace App\Repositories;

use App\Models\Flatplans\FlatPlanPageMerge;
use Illuminate\Support\Str;
trait CanCreateHeader {

    public function generateHeaderId(){
        $existFlag = true;
        $index = 1;
        $temp = Str::random(32);

        while($existFlag==true){
            $existFlag=false;
            $check = FlatPlanPageMerge::where('merge_id' , $temp)->first();
            if(count($check)>0) {
                $existFlag = true;
                $temp = Str::random(32);
            }

            $index++;
        }
        return $temp;
    }
}