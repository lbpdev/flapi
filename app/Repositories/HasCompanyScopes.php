<?php namespace App\Repositories;

use Illuminate\Support\Facades\Auth;

trait HasCompanyScopes {

    public function scopeFromCompany($query,$company_id)
    {
        return $query->where('company_id', $company_id);
    }

    public function scopeFromCurrentCompany($query)
    {
        return $query->where('company_id', Auth::user()->company->id);
    }

    public function scopeWithGlobals($query)
    {
        return $query->orWhere('company_id', 0);
    }

    public function scopeFromCurrentCompanyAndGlobals($query)
    {
        return $query->where(function($query){
            $query->fromCompany(Auth::user()->company->id)->withGlobals();
        });
    }

    public function scopeFromCompanyAndGlobals($query,$company_id)
    {
        return $query->where(function($query) use($company_id){
            $query->fromCompany($company_id)->withGlobals();
        });
    }
}