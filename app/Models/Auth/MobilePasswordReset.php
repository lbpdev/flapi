<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class MobilePasswordReset extends Model
{
    protected $fillable = ['email','temp_password','status'];
}
