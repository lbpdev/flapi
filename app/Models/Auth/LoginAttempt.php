<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class LoginAttempt extends Model
{
    protected $fillable = ['email','password','token','ip'];
}
