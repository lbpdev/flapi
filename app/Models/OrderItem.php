<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['order_id','IPN_PID','IPN_PCODE','IPN_INFO','IPN_QTY','IPN_PRICE','IPN_TOTAL','plan_id'];

}
