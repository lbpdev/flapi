<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanSectionData extends Model
{

    public $table = 'flat_plan_section_data';

    protected $fillable = ['content_field_id','flat_plan_section_id','value'];

    public function section()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlanSection');
    }

    public function field()
    {
        return $this->hasOne('App\Models\Content\ContentField','id','content_field_id');
    }

    /**
     * Define a polymorphic, inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function actionable()
    {
        return $this->morphTo();
    }

    public function actions()
    {
        return $this->morphOne('App\Models\ActionHistoryPivot', 'actionable');
    }

    public function getFieldAttribute()
    {
        return $this->field()->first();
    }
}
