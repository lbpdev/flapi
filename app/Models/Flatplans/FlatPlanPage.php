<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlatPlanPage extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['order','flat_plan_version_id','layout_id','is_cover'];

    public function version()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlanVersion','id','flat_plan_version_id');
    }

    public function layout()
    {
        return $this->hasOne('App\Models\Layouts\Layout','id','layout_id');
    }

    public function sections()
    {
        return $this->hasMany('App\Models\Flatplans\FlatPlanSection');
    }

    public function merge()
    {
        return $this->hasMany('App\Models\Flatplans\FlatPlanPageMerge');
    }

    /**
     * Define a polymorphic, inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function actionable()
    {
        return $this->morphTo();
    }
    
    public function actions()
    {
        return $this->morphOne('App\Models\ActionHistory', 'actionable');
    }

    public function getToCompleteAttribute()
    {
        $sections =  $this->sections()->with('content.fields')->get();
        $data = 0;

        foreach ($sections as $index=>$section)
            $data += $section->content ? $section->content->fields()->where('slug','design-complete')->count() : 0;

        return $data;
    }

    public function getToApproveAttribute()
    {
        $sections =  $this->sections()->with('content.fields')->get();
        $data = 0;

        foreach ($sections as $index=>$section)
            $data += $section->content ? $section->content->fields()->where('slug','article-approved')->count() : 0;

        return $data;
    }

    public function getSectionsAttribute()
    {
        return $this->sections()->get();
    }

    public function getMergedPagesAttribute()
    {
        $merged = $this->merge()->first();

        if($merged)
            return FlatPlanPage::whereHas('merge',function($query) use($merged){
                $query->where('merge_id',$merged->merge_id);
            })->get();

        return [];
    }

    public function getCompletedAttribute()
    {
        $totalCompletes = $this->toComplete;
        $completed = 0;

        foreach ($this->sections as $section){
            if($section->isComplete)
                $completed++;
        }

        if(( $this->checkContents($this) && count($this->toComplete) == 0))
            return true;

        elseif($this->checkContents($this) && $totalCompletes == $completed)
            return true;

        return false;
    }

    public function getApprovedAttribute()
    {
        $totalApproves = $this->toApprove;
        $approved = 0;

        foreach ($this->sections as $section){
            if($section->isApproved)
                $approved++;
        }

        if(( $this->checkContents($this)  && count($this->toApprove) == 0))
            return true;

        elseif($this->checkContents($this) && $totalApproves == $approved)
            return true;

        return false;
    }

    public function checkContents($page){
        $contents = 0;

        foreach ($page->sections as $sections)
            if($sections->content)
                $contents++;

        return $contents;
    }
}
