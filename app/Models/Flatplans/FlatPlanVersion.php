<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanVersion extends Model
{
    protected $fillable = ['flat_plan_id','version'];

    /**
     * A flatplan has pages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pages()
    {
        return $this->hasMany('App\Models\Flatplans\FlatPlanPage');
    }

    public function flatplan()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlan','id','flat_plan_id');
    }

    public function actions()
    {
        return $this->hasMany('App\Models\ActionHistory');
    }

    public function getActionsByHeadAttribute()
    {
        return $this->actions()->orderBy('created_at','DESC')->distinct()->get(['head']);
    }

    public function getActionsByHeadReverseAttribute()
    {
        return $this->actions()->orderBy('created_at','ASC')->distinct()->get(['head']);
    }


    public function getCoversAttribute()
    {
        return $this->pages()->where('is_cover',1)->orderBy('order','ASC')->get();
    }

    public function getInnerPagesAttribute()
    {
        return $this->pages()->where('is_cover',0)->orderBy('order','ASC')->get();
    }

    public function getAllPagesAttribute()
    {
        return $this->pages()->where('is_cover',0)->orderBy('order','ASC')->get();
    }

    public function getAllPagesCountAttribute()
    {
        return $this->pages()->count();
    }

}
