<?php

namespace App\Models\Flatplans;

use App\Repositories\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class FormatType extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','company_id'];

    public $timestamps = false;
}
