<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanSection extends Model
{
    protected $fillable = ['flat_plan_page_id','layout_section_id','content_id'];

    public function page()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlanPage','id','flat_plan_page_id');
    }

    public function content()
    {
        return $this->hasOne('App\Models\Content\Content','id','content_id');
    }

    public function layout()
    {
        return $this->hasOne('App\Models\Layouts\LayoutSection','id','layout_section_id');
    }

    public function data()
    {
        return $this->hasMany('App\Models\Flatplans\FlatPlanSectionData','flat_plan_section_id');
    }

    /**
     * Define a polymorphic, inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function actionable()
    {
        return $this->morphTo();
    }

    public function actions()
    {
        return $this->morphOne('App\Models\ActionHistoryPivot', 'actionable');
    }

    public function action()
    {
        return $this->morphOne('App\Models\ActionHistory', 'actionable');
    }

    public function getIsCompleteAttribute(){
        return $this->data()->whereHas('field',function($query){
            $query->where('slug','design-complete');
        })->where('value',1)->count();
    }

    public function getIsApprovedAttribute(){
        return $this->data()->whereHas('field',function($query){
            $query->where('slug','article-approved');
        })->where('value',1)->count();
    }

    public function getDataAttribute(){
        return $this->data()->get();
    }

    public function getTitleAttribute(){
        $data = $this->data()->where('content_field_id',1)->first();

        if($data)
            return $data->value;

        return '';
    }

    public function getContentAttribute(){
        return $this->content()->first();
    }

    public function scopeGetData($query,$slug){

        $data = $this->data()->whereHas('field',function($q) use($slug) {
            $q->where('slug',$slug);
        })->first();

        if($data)
            return $data;

        return [];
    }
}
