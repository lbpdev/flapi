<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanParticipant extends Model
{
    protected $fillable = ['flat_plan_id','user_id','role_id'];

    public $timestamps = false;

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function flatplan(){
        return $this->hasOne('App\Models\Flatplans\FlatPlan','id','flat_plan_id');
    }

    public function role(){
        return $this->hasOne('App\Models\Role','id','role_id');
    }
    
}
