<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanView extends Model
{
    protected $fillable = ['flat_plan_id','token','is_live'];

    public function users(){
        return $this->hasMany('App\Models\Flatplans\FlatPlanViewUser');
    }

    public function flatplan()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlan','id','flat_plan_id');
    }

    public function getEmployeesAttribute(){
        $data = [];
        $users = $this->users()->get();

        foreach ($users as $user){
            if($user->user->username)
                $data[] = $user->user;
        }

        return $data;
    }

    public function getGuestsAttribute(){
        $data = [];
        $users = $this->users()->get();

        foreach ($users as $user){
            if(!$user->user->username)
                $data[] = $user->user;
        }

        return $data;
    }

    public function getEmployeeUserIdsAttribute(){
        $data = [];
        $users = $this->users()->get();

        foreach ($users as $user){
            if($user->user->username)
                $data[] = $user->user->id;
        }

        return $data;
    }

    public function getGuestUserIdsAttribute(){
        $data = [];
        $users = $this->users()->get();

        foreach ($users as $user){
            if(!$user->user->username)
                $data[] = $user->user->id;
        }

        return $data;
    }
}
