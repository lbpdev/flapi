<?php

namespace App\Models\Flatplans;

use App\Repositories\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FlatPlan extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','slug','issue','company_id','template_id','deadline','archived_at'];

    public $dates = ['archived_at'];

    public function meta(){
        return $this->hasOne('App\Models\Flatplans\FlatPlanMeta','flat_plan_id');
    }

    /**
     * A user has a profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uploads()
    {
        return $this->morphOne('App\Models\Upload', 'uploadable');
    }
    /**
     * A flatplan has pages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function versions()
    {
        return $this->hasMany('App\Models\Flatplans\FlatPlanVersion');
    }
    /**
     * A flatplan has pages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function participants()
    {
        return $this->hasMany('App\Models\Flatplans\FlatPlanParticipant');
    }

    /**
     * A flatplan has pages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne('App\Models\Company','id','company_id');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCompanyAttribute()
    {
        return $this->company()->first();
    }

    public function view()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlanView');
    }

    /**
     * get Thumbnail Attrib
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getThumbnailAttribute()
    {
        $thumb = $this->uploads()->where('template', 'thumb')->first();

        return $thumb ? asset('public/'.$thumb->url) : null;
    }

    public function getVersionsAttribute(){
        return $this->versions()->get();
    }


    public function getMetaAttribute(){
        return $this->meta()->first();
    }

    public function getParticipantsAttribute(){
        return $this->participants()->get();
    }

    public function getTemplateAttribute(){
        return $this->template()->first();
    }

    public function scopeCurrentVersion($query){

        $latesVersion = $this->versions()->orderBy('version','DESC')->first();

        if($latesVersion)
            return $query->whereHas('versions',function($query) use($latesVersion){
                $query->where('id',$latesVersion->id);
            });

        return $query;
    }

    public function scopeActive($query){

        $query->whereNull('archived_at');
        return $query;
    }

    public function scopeArchived($query){

        $query->whereNotNull('archived_at');
        return $query;
    }

    public function scopeVersionIs($query,$version){

        $data = $this->versions()->where('version',$version)->first();

        if($data)
            return $query->whereHas('versions',function($query) use($data){
                $query->where('id',$data->id);
            });

        return $query;
    }

    public function getCurrentVersionAttribute(){

        $latesVersion = $this->versions()->orderBy('version','DESC')->first();

        if($latesVersion)
            return $latesVersion;

        return [];
    }

    public function getIsCurrentUserParticipantAttribute(){
        return $this->participants()->where('user_id',Auth::user()->id)->count();
    }
}
