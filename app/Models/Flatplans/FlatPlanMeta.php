<?php

namespace App\Models\Flatplans;

use App\Repositories\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class FlatPlanMeta extends Model
{

    public $table = 'flat_plan_meta';

    protected $fillable = [
        'name',
        'media_type_id',
        'format_type_id',
        'pagination',
        'inside_pages',
        'size_x',
        'size_y',
        'units',
        'flat_plan_id',
        'start_page',
        'style',
        'print_approved'
    ];

    public $timestamps = false;

    public function media()
    {
        return $this->hasOne('App\Models\Flatplans\MediaType','id','media_type_id');
    }

    public function format()
    {
        return $this->hasOne('App\Models\Flatplans\FormatType','id','format_type_id');
    }

    public function flatplan()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlan','id','flat_plan_id');
    }

}
