<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanViewUser extends Model
{
    protected $fillable = ['flat_plan_view_id','actionable_id','actionable_type','token'];

    public function view(){
        return $this->hasOne('App\Models\Flatplans\FlatPlanView','id','flat_plan_view_id');
    }

    public function viewable()
    {
        return $this->morphTo();
    }

    public function getViewAttribute()
    {
        return $this->view()->first();
    }

    public function getUserAttribute()
    {
        $model = $this->viewable()->getModel();
        return $model->find($this->viewable_id);
    }
}
