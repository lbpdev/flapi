<?php

namespace App\Models\Flatplans;

use Illuminate\Database\Eloquent\Model;

class FlatPlanPageMerge extends Model
{
    protected $fillable = ['flat_plan_page_id','merge_id'];

    public $timestamps = false;

    public function page()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlanPage','id','flat_plan_page_id');
    }

}
