<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicUser extends Model
{
    protected $fillable = ['name','email'];

    public function viewable()
    {
        return $this->morphTo();
    }

    public function views()
    {
        return $this->morphOne('App\Models\Flatplans\FlatPlanViewUser', 'viewable');
    }
}
