<?php

namespace App\Models;

use App\Repositories\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasCompanyScopes;

    protected $fillable = [
        'name',
        'value',
        'company_id'
    ];

    public $timestamps = false;

    public function avatar()
    {
        return $this->morphOne('MEDoctors\Models\Upload', 'uploadable');
    }
}
