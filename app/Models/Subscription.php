<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['SubscriptionReference','PurchaseDate','SubscriptionStartDate','ExpirationDate','RecurringEnabled','company_id','plan_id'];

    public $dates = ['PurchaseDate','SubscriptionStartDate','ExpirationDate'];

    public function company(){
        return $this->hasOne('App\Models\Company','id');
    }

    public function plan(){
        return $this->hasOne('App\Models\Plan','id','plan_id');
    }
}
