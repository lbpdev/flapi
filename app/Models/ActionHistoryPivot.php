<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionHistoryPivot extends Model
{
    public $fillable = ['actionable_id','actionable_type','action_history_id','value_new','value_old'];

    public function actionable()
    {
        return $this->morphTo();
    }
}
