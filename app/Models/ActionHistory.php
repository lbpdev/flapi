<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionHistory extends Model
{
    public $fillable = ['type','actionable_id','actionable_type','flat_plan_version_id','value_new','value_old','head'];

    public function actionable()
    {
        return $this->morphTo();
    }

    public function pivots()
    {
        return $this->hasMany('App\Models\ActionHistoryPivot');
    }

    public function getPivotsAttribute(){
        return $this->pivots()->get();
    }
}
