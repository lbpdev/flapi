<?php

namespace App\Models\Pivots;

use App\Repositories\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasCompanyScopes;

    protected $fillable = ['name','slug','type','company_id'];

    public function scopeModifyPermissions($query)
    {
        return $query->where('type','modify');
    }

    public function scopeViewPermissions($query)
    {
        return $query->where('type','view');
    }
}
