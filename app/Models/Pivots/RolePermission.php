<?php

namespace App\Models\Pivots;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $fillable = ['role_id','permission_id'];

    public $timestamps = false;
}
