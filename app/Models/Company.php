<?php

namespace App\Models;

use App\Models\Flatplans\FlatPlan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model
{
    protected $fillable = ['name'];

    public function users(){
        return $this->belongsToMany('App\Models\User','user_companies');
    }

    public function guests(){
        return $this->HasMany('App\Models\PublicUser','company_id');
    }

    public function subscriptions(){
        return $this->hasMany('App\Models\Subscription','company_id');
    }

    public function getSubscriptionAttribute(){
        return $this->subscriptions()->orderBy('created_at','DESC')->first();
    }

    public function getActiveUsersAttribute(){
        return $this->users()->where('is_active',1)->get();
    }

    public function getUserLimitAttribute(){
        return $this->subscription ? $this->subscription->plan->max_users : 5;
    }

    public function getWorkHoursAttribute(){
        $workHours = $this->options()->where('name','workhours')->first();

        if($workHours)
            return $workHours->value;

        return 8;
    }

    public function getWorkDaysCountAttribute(){
        $data = 5;

        $workDays = $this->options()->where('name','workdays')->first();

        if($workDays)
            $data = count(json_decode($workDays->value));

        return $data;
    }

    public function getWorkDaysAttribute(){
        $data = ["Mon","Tue","Wed","Thu","Fri"];

        $workDays = $this->options()->where('name','workdays')->first();

        if($workDays)
            $data = json_decode($workDays->value);

        return $data;
    }

    public function getTimezoneAttribute(){
        $timezone = 'America/Chicago';

        $option = $this->options()->where('name','timezone')->first();

        if($option)
            $timezone = $option->value;

        return $timezone;
    }

    public function getFirstDayAttribute(){

        $data = $this->options()->where('name','first-day')->first();

        return $data ? $data->value : 'Sun';
    }


    public function getAdminCountAttribute(){
        return $this->users()->whereHas('role',function($query){
            $query->where('slug','admin');
        })->count();
    }


    public function getLogoAttribute(){
        $logo = $this->options()->where('name','logo')->first();

        if($logo)
            return $logo->value;

        return '';
    }


    public function getActiveAdminCountAttribute(){
        return $this->users()->whereHas('role',function($query){
            $query->where('slug','admin');
        })->where('is_active',1)->count();
    }

    public function options(){
        return $this->hasMany('App\Models\Option');
    }

    public function reports(){
        return $this->hasMany('App\Models\Report');
    }

//
//    public function scopeGetOption($query,$option_name){
//        return $this->options()->where('name',$option_name)->whereHas('users',function($query){
//            $query->where('company_id',$this->id);
//        })->get();
//    }

    public function scopeGetOption($query,$option_name){
        return $this->options()->where('name',$option_name);
    }

    public function getMaxFlatplansReachedAttribute(){

        $subscription = $this->subscription;

        $max = $subscription->plan->max_flatplans;

        $currentFP = FlatPlan::fromCurrentCompany()->whereNull('archived_at')->count();

        if($currentFP < $max)
            return false;

        return true;
    }

    public function getMaxFlatplansAttribute(){

        $subscription = $this->subscription;

        $max = $subscription->plan->max_flatplans;

        return $max;
    }

    public function getActiveFlatPlansAttribute(){
        return FlatPlan::where('company_id',$this->id)->whereNull('archived_at')->get();
    }

    public function getMaxPagesAttribute(){

        $subscription = $this->subscription;

        $max = $subscription->plan->max_pages;

        return $max;
    }

    public function scopeMaxPagesReached($query, $flatplan_id){

        $subscription = $this->subscription;

        $max = $subscription->plan->max_pages;

        $flatplan = FlatPlan::find($flatplan_id);

        if($flatplan->currentVersion->pages->count() < $max)
            return false;

        return true;
    }

}
