<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['REFNO','ORDERNO','SALEDATE','PAYMENTDATE','ORDERSTATUS','PAYMETHOD','CUSTOMEREMAIL','IPADDRESS','COMPLETE_DATE','user_id','AVANGATE_CUSTOMER_REFERENCE'];

    public $dates = ['PAYMENTDATE','SALEDATE','COMPLETE_DATE'];

    public function items(){
        return $this->hasMany('App\Models\OrderItem','order_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User');
    }

}
