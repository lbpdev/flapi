<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $fillable = ['user_id','activity','device'];

    public function user(){
        return $this->hasOne('App\Models\User','user_id');
    }

}
