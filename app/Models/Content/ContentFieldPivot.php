<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

class ContentFieldPivot extends Model
{
    protected $fillable = ['content_id','content_field_id'];

    public $timestamps = false;
}
