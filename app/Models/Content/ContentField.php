<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\HasCompanyScopes;

class ContentField extends Model
{
    use HasCompanyScopes;

    protected $fillable = ['name','slug','content_id','content_field_type_id','is_public','company_id'];

    public function type(){
        return $this->hasOne('App\Models\Content\ContentFieldType','id','content_field_type_id');
    }

    public function company(){
        return $this->hasOne('App\Models\Company');
    }

    public function field(){
        return $this->hasOne('App\Models\ContentField','content_id');
    }

    public function getFieldAttribute(){
        dd('ss');
        return $this->field()->first();
    }
}
