<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\HasCompanyScopes;

class Content extends Model
{
    use HasCompanyScopes;
    
    protected $fillable = ['name','color','company_id'];

    public function fields(){
        return $this->belongsToMany('App\Models\Content\ContentField','content_field_pivots','content_id');
    }

    public function sections(){
        return $this->hasMany('App\Models\Flatplans\FlatPlanSection','content_id');
    }
}
