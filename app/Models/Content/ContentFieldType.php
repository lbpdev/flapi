<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;

class ContentFieldType extends Model
{
    protected $fillable = ['name','slug'];

    public $timestamps = false;
}
