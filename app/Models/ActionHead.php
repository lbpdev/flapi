<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionHead extends Model
{
    public $fillable = ['flat_plan_version_id','action_head_id'];

    public function actions()
    {
        return $this->hasMany('App\Models\ActionHistory','head','action_head_id');
    }

    public function version()
    {
        return $this->hasOne('App\Models\Flatplans\FlatPlanVersion','id','flat_plan_version_id');
    }

    public function getVersionAttribute()
    {
        return $this->version()->first();
    }
}
