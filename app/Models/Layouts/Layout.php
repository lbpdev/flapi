<?php

namespace App\Models\Layouts;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    protected $fillable = ['name','category'];

    public function sections()
    {
        return $this->hasMany('App\Models\Layouts\LayoutSection');
    }
}
