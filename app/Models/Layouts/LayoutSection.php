<?php

namespace App\Models\Layouts;

use Illuminate\Database\Eloquent\Model;

class LayoutSection extends Model
{
    protected $fillable = ['top','left','right','bottom','height','width','order','z_index','layout_id'];
}
