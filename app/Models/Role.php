<?php

namespace App\Models;

use App\Repositories\HasCompanyScopes;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasCompanyScopes;

    protected $fillable = [
        'name',
        'slug',
        'icon',
        'company_id'
    ];

    public $timestamps = false;

    public function users(){
        return $this->belongsToMany('App\Models\User','user_roles');
    }

    public function permissions(){
        return $this->belongsToMany('App\Models\Pivots\Permission','role_permissions');
    }

    public function scopeHasPermissions($query,$slug){
        return $this->permissions()->where('slug',$slug);
    }


}
