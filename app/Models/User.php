<?php

namespace App\Models;

use App\Models\Pivots\Permission;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{

    protected $primaryKey = 'id';

    use Notifiable;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsToMany('App\Models\Role','user_roles');
    }

    public function getRoleAttribute(){
        return $this->role()->first();
    }

    public function getIsAdminAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='admin')
            return true;

        return false;
    }

    public function getIsClientAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='client')
            return true;

        return false;
    }

    public function getIsEmployeeAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='employee')
            return true;

        return false;

    }

    public function getIsSchedulerAttribute(){
        $role =  $this->role()->first();

        if($role->slug=='scheduler')
            return true;

        return false;
    }

    public function company(){
        return $this->belongsToMany('App\Models\Company','user_companies');
    }

    public function getCompanyAttribute(){
        return $this->company()->first();
    }

    public function getCompanyTimezoneAttribute(){
        $timezone = 'America/Chicago';

        $option = $this->company->options()->where('name','timezone')->first();

        if($option)
            $timezone = $option->value;

        return $timezone;
    }

    protected function getLastLoginAttribute(){
        return $this->logs()->where('activity','login')->orderBy('created_at','DESC')->first();
    }

    protected function getLastLoginTimeAttribute(){
        $act = $this->logs()->where('activity','login')->orderBy('created_at','DESC')->first();

        if($act)
            return $act->created_at->format('d-m-Y H:i');

        return [];
    }

    protected function getLoggedInTodayAttribute(){
        return $this->logs()->where('activity','login')->whereDate('created_at','=',Carbon::now()->format('Y-m-d'))->get();
    }

    public function logs(){
        return $this->hasMany('App\Models\UserLog');
    }

    public function tokens(){
        return $this->hasMany('App\Models\MobileApiToken');
    }

    public function getCompanyUserLimitAttribute(){
        $company = Auth::user()->company;

        if($company){
            $subscription = $company->subscription;

            return $subscription->plan->max_users;
        }

        return null;
    }

    public function getPermissionsArrayAttribute(){

        $data = [];

        $role = Auth::user()->role;

        if($role->slug == 'admin'){
            $userPermissions = Permission::all();

            foreach ($userPermissions as $permission)
                $data[] =  $permission->slug;

        } else {
            foreach ($role->permissions as $permission)
                $data[] =  $permission->slug;
        }

        return $data;
    }

    public function viewable()
    {
        return $this->morphTo();
    }

    public function views()
    {
        return $this->morphOne('App\Models\Flatplans\FlatPlanViewUser', 'viewable');
    }

    public function scopeClientsOnly($query){
        return $this->whereHas('role',function($innerQuery){
            $innerQuery->where('slug','client');
        });
    }

    public function scopeEmployeesOnly($query){
        return $this->whereHas('role',function($innerQuery){
            $innerQuery->where('slug','!=','client');
        });
    }

    public function getHasPermissionAttribute($slug){
        $permissions = Auth::user()->permissionsArray;

        if(in_array($slug,$permissions))
            return true;

        return false;
    }
}
