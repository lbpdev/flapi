<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SiteAdminsOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $admins = explode(",",env('BACKEND_USERS'));

            if(!in_array(Auth::user()->email,$admins))
                return redirect()->back();

        } else {
            return redirect(route('login'));
        }

        return $next($request);
    }
}
