<?php

namespace App\Http\Middleware;

use App\Models\UserLog;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckLoginRecord
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()) {
            if( UserLog::where('user_id',Auth::user()->id)->
                where('activity','login')->
                where('device','web')->
                whereDate('created_at','=',Carbon::now()->format('Y-m-d'))->
                count() < 1 ){
                UserLog::create(['user_id' => Auth::user()->id,'activity' => 'login']);
            }
        }

        return $next($request);
    }
}
