<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\HistoryService;
class HistoryController extends Controller
{
    public function __construct(HistoryService $historyService){
        $this->history = $historyService;
    }

    public function undo($version_id)
    {
        $this->history->undo($version_id);

        return redirect()->back();
    }

    public function redo($version_id)
    {
        $this->history->redo($version_id);

        return redirect()->back();
    }
}
