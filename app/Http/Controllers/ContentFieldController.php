<?php

namespace App\Http\Controllers;

use App\Repositories\CanCreateResponseCode;
use App\Repositories\CanCreateSlug;
use App\Repositories\CannotAcceptWhiteSpace;
use Illuminate\Http\Request;
use App\Models\Content\ContentField;

class ContentFieldController extends Controller
{

    use CanCreateResponseCode,CannotAcceptWhiteSpace, CanCreateSlug;

    public function __construct(ContentField $contentField)
    {
        $this->model = $contentField;
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $item = $this->model->where('id',$_GET['id'])->first();

        if(!$item)
            return response()->json($this->generateResponse('field-not-exists'));

//        $data['projectsCount'] = 0;
//        $data['tasksCount'] = 0;
//
//        $projects = Project::fromCurrentCompany()->where('format_id',$format->id)->get();
//
//        foreach ($projects as $project){
//            foreach ($project->tasks as $task){
//                $data['tasksCount'] += count($task->utasks);
//            }
//        }

//        $data['projectsCount'] = count($projects);
        $data['field'] = $item;

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->fromCompany($company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('field-exists'));

        if($input['name']){

            $input['name'] = strip_tags(trim($input['name']));
            $input['slug'] = $this->generateSlug(strip_tags(trim($input['name'])));
            $input['company_id'] = $company_id;

            $this->model->create($input);
        }

        $data = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-field-success',$data));
    }

    public function update(Request $request)
    {
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $item = $this->model->where('id',$input['id'])->first();

        if(!$item)
            return response()->json($this->generateResponse('field-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($input['name']){

            if(( strtolower($item->name) != strtolower($input['name']) ) && $this->model->fromCompany($company_id)->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('field-exists'));

            else {
                $input['name'] = strip_tags(trim($input['name']));
                $item->update(array(
                    'name'=> strip_tags(trim($input['name'])),
                    'slug'=>$this->generateSlug(strip_tags(trim($input['name']))),
                    'is_public'=>$input['is_public'],
                    'content_field_type_id'=>$input['content_field_type_id'],
                ));
            }
        }

        $formats = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-field-success',$formats));
    }



    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = $this->model->where('id',strip_tags($request->input('id')))->first();

        if($data)
            $company_id = $data->company_id;
        if($data->delete()) {
            $data = $this->getAll($company_id);
            return response()->json($this->generateResponseWithData('delete-field-success',$data));
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }


    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->fromCurrentCompany()->withGlobals()->orderBy('company_id','ASC')->get()->toArray();

        return $this->model->fromCompany($company_id)->withGlobals()->orderBy('company_id','ASC')->get()->toArray();
    }
}
