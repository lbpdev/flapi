<?php

namespace App\Http\Controllers;

use App\Models\Flatplans\FlatPlan;
use App\Models\Flatplans\FlatPlanSection;
use App\Models\Flatplans\FlatPlanSectionData;
use App\Repositories\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Services\FlatPlanService;

use App\Repositories\CanHandleHistory;

class FlatPlanSectionController extends Controller
{
    use CanHandleHistory, CanCreateResponseCode;
    
    public function __construct(FlatPlanService $flatPlanService)
    {
        $this->flatplanService = $flatPlanService;
    }

    public function update(Request $request){
        $input = $request->except('_token','section_id');
        $data = [];
        $oldValues = [];
        $newValues = [];

        $values = FlatPlanSectionData::where('flat_plan_section_id',$request->input('section_id'))->get();

        foreach ($values as $value)
            $oldValues[] = $value->value;

        FlatPlanSectionData::where('flat_plan_section_id',$request->input('section_id'))->delete();

        foreach ($input as $item){
            $value = isset($item['value']) ? $item['value'] : null;

            FlatPlanSectionData::create([
                'content_field_id' => $item['content_field_id'],
                'flat_plan_section_id' => $request->input('section_id'),
                'value' => $value,
            ]);

            $newValues[] = $value;
        }

        $section = FlatPlanSection::with('page')->find($request->input('section_id'));

//        $this->createHistory($section,'section-data-change',$section->page->version->id,$oldValues,$newValues);

        if($section->page)
            $data['page'] = $this->flatplanService->getPageStatus($section->page);

        $data['section'] = $section->getData('title');

        return response()->json($this->generateResponseWithData('page-update-success',$data));
    }
}
