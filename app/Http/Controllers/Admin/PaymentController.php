<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use App\Services\Avengate\src\SoapClient;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use GuzzleHttp\Exception\ClientException;
use App\Services\Avengate\src\AvengateClient;
use PHPUnit_Framework_TestCase as TestCase;

use Illuminate\Contracts\Logging\Log;

class PaymentController extends Controller
{
    public function __construct(Log $log)
    {
        $this->log = $log;
        $this->log->useDailyFiles(storage_path().'/logs/payments.log');
    }

    public function post(Request $request){

        $this->log->info('POST REQUEST RECEIVED');

        $input = $request->input();

        $this->log->info($request->input());

        $order = Order::where('ORDERNO',$input['ORDERNO'])->first();

        $data['REFNO'] = $input['REFNO'];
        $data['ORDERNO'] = $input['ORDERNO'];
        $data['SALEDATE'] = $input['SALEDATE'];
        $data['PAYMENTDATE'] = $input['PAYMENTDATE'];
        $data['ORDERSTATUS'] = $input['ORDERSTATUS'];
        $data['PAYMETHOD'] = $input['PAYMETHOD'];
        $data['CUSTOMEREMAIL'] = $input['CUSTOMEREMAIL'];
        $data['IPADDRESS'] = $input['IPADDRESS'];
        $data['COMPLETE_DATE'] = $input['COMPLETE_DATE'];
        $data['AVANGATE_CUSTOMER_REFERENCE'] = $input['AVANGATE_CUSTOMER_REFERENCE'];

        if($order){
            $this->log->info('Updating order: '.$order->id);
            $order->update($data);
        }
        else {
            $data['user_id'] = $input['IPN_CUSTOM_VALUE'][0];
            $this->log->info('Creating new order');
            $order = Order::create($data);
        }

        $plan = Plan::where('code',$input['IPN_PCODE'][0])->first();

        $item = OrderItem::where('order_id',$order->id)->where('IPN_PCODE',$input['IPN_PCODE'][0])->first();

        $itemData['IPN_PID'] = $input['IPN_PID'][0];
        $itemData['IPN_PCODE'] = $input['IPN_PCODE'][0];
        $itemData['IPN_INFO'] = $input['IPN_INFO'][0];
        $itemData['IPN_QTY'] = $input['IPN_QTY'][0];
        $itemData['IPN_PRICE'] = $input['IPN_PRICE'][0];
        $itemData['IPN_TOTAL'] = $input['IPN_TOTAL'][0];
        $itemData['plan_id'] = $plan->id;

        if(!$item)
            $item = $order->items()->create($itemData);

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/orders/{OrderReference}/status/

        try {

            $this->log->info('FETCHING ORDER: '.$order->id);

            $response = $client->get('orders/'.$order->REFNO.'/');

            $responsedata = json_decode($response->getBody()->getContents());

            $this->log->info(json_encode($responsedata));
//            $this->log->info(json_encode($responsedata->Items[0]->ProductDetails->Subscriptions[0]));

            $data = $responsedata->Items[0]->ProductDetails->Subscriptions[0];

            if($order->user_id)
                $user = User::where('id',$order->user_id)->first();
            else {
                $pastOrder = Order::where('AVANGATE_CUSTOMER_REFERENCE',$order->AVANGATE_CUSTOMER_REFERENCE)->first();
                $user = User::where('id',$pastOrder->user_id)->first();
            }

            if($user){

                $this->log->info('User found: '.$user->id);
                // 'SubscriptionReference','PurchaseDate','SubscriptionStartDate','ExpirationDate','RecurringEnabled','company_id'

//              if($order->ORDERSTATUS=='COMPLETE')

                $subscription = Subscription::where('company_id',$user->company->id)->where('plan_id',$plan->id)->orderBy('created_at','desc')->first();

                $subscriptionData['SubscriptionReference'] = $data->SubscriptionReference;
                $subscriptionData['PurchaseDate'] = $data->PurchaseDate;
                $subscriptionData['SubscriptionStartDate'] = $data->SubscriptionStartDate;
                $subscriptionData['ExpirationDate'] = $data->ExpirationDate;
                $subscriptionData['RecurringEnabled'] = $data->RecurringEnabled;

                if(!$subscription) {
                    $subscriptionData['plan_id'] = $plan->id;
                    $this->log->info('Creating new subscription :'.$plan->id);
                    $subscriptionData['company_id'] = $user->company->id;


                    $subscription = Subscription::create($subscriptionData);
                    $this->log->info($subscription);

                    $activeUsers = $user->company->activeUsers;
                    if($plan->max_users > count($activeUsers))
                        $this->reduceUsers($activeUsers,$plan->max_users);

                } else {

                    $subscription->update($subscriptionData);

                    $this->log->info('Update Subscription :'.$subscription->id);
                    $this->log->info('New expiration date:'.$data->ExpirationDate);
                }
            }

        } catch (ClientException $e) {
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }

    public function reduceUsers($activeUsers,$max_users){
        $employees = 0;

        if($max_users < count($activeUsers)){
            foreach($activeUsers as $employee){

                if($employees < $max_users){
                    $employees++;
                }
                else {
                    $employee->is_active = 0;
                    $employee->save();
                }
            }
        }

    }

//    public function disableAutoRenew($user,$SubscriptionReference){
//
//        $subscription = Subscription::where('SubscriptionReference',$SubscriptionReference)->where('company_id',$user->company->id)->first();
//
//        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
//        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');
//
//        try {
//            $this->log->info('Current Status: '.$subscription->RecurringEnabled);
//
//
//            try {
//                $this->log->info('Disabling Recurring : '.$subscription->id);
//                $response = SoapClient::disableRecurringBilling($SubscriptionReference);
//
//                if($response)
//                    $this->log->info('Recurring Payments disabled: '.$subscription->id);
//
//                $subscription->RecurringEnabled = 0;
//
//            } catch (\Exception $e) {
//
//            }
//
//            $subscription->save();
//
//            $this->log->info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);
//
//        } catch (ClientException $e) {
//        }
//        return redirect()->back();
//    }
//
//    public function disableSubscription($user,$SubscriptionReference){
//
//        $subscription = Subscription::where('SubscriptionReference',$SubscriptionReference)->where('company_id',$user->company->id)->first();
//
//        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
//        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');
//
//        try {
//            $disableSubscription = SoapClient::cancelSubscription($SubscriptionReference);
//            $this->log->info('DISABLED: '. $disableSubscription);
//        }
//        catch (SoapFault $e) {
//            $this->log->info('ERROR:' . $e->getMessage());
//        }
//
//        return true;
//    }

    public function process(){}

    public function get(){

        $client = new AvengateClient([
            'code' => 'LEADINGB',
            'key' => '@dJvyoEH3O1]w&|Aqk4!',
            'base_uri' => 'https://api.avangate.com/rest/3.0/'
        ]);

        // https://api.avangate.com/rest/3.0/orders/{OrderReference}/status/

        try {
            $response = $client->get('orders/53025368/');
            $data = json_decode($response->getBody()->getContents());

            $this->log->info(json_encode($data->Items[0]));

            dd($data->Items[0]);
        } catch (ClientException $e) {
            dd($e->getResponse()->getBody()->getContents());
            TestCase::assertEquals(401, $e->getResponse()->getStatusCode());
            $sentDetails = json_decode($e->getResponse()->getBody()->getContents());

            TestCase::assertEquals('AUTHENTICATION_ERROR', $sentDetails->error_code);
            TestCase::assertEquals('Authentication needed for this resource', $sentDetails->message);
        }
    }
}
