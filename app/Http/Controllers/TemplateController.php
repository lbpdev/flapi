<?php

namespace App\Http\Controllers;

use App\Models\Template\Template;
use Illuminate\Http\Request;

class TemplateController extends Controller
{

    public function __construct(Template $model)
    {
        $this->model = $model;
    }

    public function get($id){

        $data = $this->model->find($id);

        if($data)
            return response()->json(['data' => $data,'status'=>200]);

        return [];
    }
}
