<?php

namespace App\Http\Controllers;

use App\Models\ActionHead;
use App\Models\Content\Content;
use App\Models\Flatplans\FlatPlan;
use App\Models\Flatplans\FlatPlanPage;
use App\Models\Flatplans\FlatPlanSection;
use App\Models\Flatplans\FlatPlanVersion;
use App\Models\Layouts\Layout;
use App\Repositories\CanCreateResponseCode;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Repositories\CanCreateHeader;
use App\Repositories\CanHandleHistory;

class FlatPlanPagesController extends Controller
{
    use CanCreateHeader,CanHandleHistory,CanCreateResponseCode;

    public function get($id = null){

        if(!$id)
            $id = $_GET['id'];

        $data = FlatPlanPage::with(['sections.data'=>function($q){
            $q->where('content_field_id',1);
        },'layout.sections','sections.content'])->find($id);

        if($data){
            $data['merged'] = $data->mergedPages;
            return response()->json(['data' => $data,'status'=>200]);
        }

        return [];
    }

    public function add(Request $request){

        $input = $request->input();
        $page_type = $input['page_type'] == 'coverPages' ? 1 : 0;

        $version = FlatPlanVersion::with(['pages' => function($query) use($page_type){
            $query->where('is_cover',$page_type)->orderBy('order','ASC');
        }])->find($input['id']);

        if($version) {

            $newPages = [];

            if(($version->AllPagesCount + (int)$input['pages']) > Auth::user()->company->maxPages)
                return response()->json($this->generateResponse('max-pages-exceed'));

            foreach ($version->pages as $page) {
                if($page->order > $input['after']){
                    $page->order += $input['pages'];
                    $page->save();
                }
            }

            for($x=0;$x<$input['pages'];$x++)
                $newPages[] = $version->pages()->create([
                    'order'    => ( isset($input['after']) ? $input['after'] : 1 )  +$x,
                    'is_cover' => $page_type,
                ]);

            $this->createHistories($newPages,'create',$version->id);

            return response()->json($this->generateResponseWithData('create-success',$newPages));
        }

        return [];
    }

    public function merge(Request $request){

        $input = $request->input();
        $page_type = $input['page_type'] == 'coverPages' ? 1 : 0;
        $reverse = $input['page'] > $input['to'] ? true : false;

        $pageToMerge = FlatPlanPage::where('flat_plan_version_id',$input['id'])->where('is_cover',$page_type)->where('order',$input['page'])->first();

        if($pageToMerge){

            $version = FlatPlanVersion::with(['pages' => function($query) use($input,$page_type,$reverse){

                if($reverse)
                    $query->where('is_cover',$page_type)->where('order','<',$input['page'])->where('order','>=',$input['to'])->orderBy('order','ASC');
                else
                    $query->where('is_cover',$page_type)->where('order','>',$input['page'])->where('order','<=',$input['to'])->orderBy('order','ASC');

            }])->where('id',$input['id'])->first();

            $merge_head = $this->generateHeaderId();

            $pageToMerge->merge()->create([
                'merge_id' => $merge_head
            ]);

            foreach ($version->pages as $page){

                $page->sections()->delete();

                foreach ($pageToMerge->sections as $section){

                    $newSection = $page->sections()->create([
                        'layout_section_id' => $section->layout_section_id,
                        'content_id' => $section->content_id,
                    ]);

                    foreach ($section->data as $data){
                        $newSection->data()->create([
                            'content_field_id' => $data->content_field_id,
                            'value' => $data->value,
                        ]);
                    }
                }

                $page->layout_id = $pageToMerge->layout_id;
                $page->save();

                $page->merge()->create([
                    'merge_id' => $merge_head
                ]);
            }

            $version->pages->load('sections');
            $data['pages'] = $version->pages;
            $data['type'] = $input['page_type'];

            return response()->json($this->generateResponseWithData('update-success',$data));
        }

        return $this->generateResponse('error');
    }

    public function delete($id = null){

        if(!$id)
            $id = $_GET['id'];

        $flatplanPage = FlatPlanPage::find($id);
        $flat_plan_id = $flatplanPage->version->flat_plan_id;

        $this->createHistory($flatplanPage,'delete',$flatplanPage->version->id);

        if($flatplanPage->delete()){

            $data['contents'] = Content::whereHas('sections.page.version.flatplan',function($query) use($flat_plan_id) {
                $query->where('id',$flat_plan_id);
            })->get();

            return response()->json($this->generateResponseWithData('delete-success',$data));
        }

        return [];
    }

    public function getSection($id = null){

        if(!$id)
            $id = $_GET['id'];

        $data = FlatPlanSection::with('data')->find($id);

        if($data){
            return response()->json(['data' => $data,'status'=>200]);
        }

        return [];
    }

    public function assignLayout()
    {
        $page = FlatPlanPage::find($_GET['id']);
        $newSectionIds = [];
        $oldValue['page'] = $page->layout_id;

        if($page){
            $page->update(['layout_id' => $_GET['layout_id']]);

            $layout = Layout::with('sections')->find($_GET['layout_id']);

            $sections = $page->sections()->with('data')->get();
            $oldValue['sections'] = $sections;

            $content = Content::fromCurrentCompanyAndGlobals()->first();

            foreach ($layout->sections as $index=>$section){
                $newSection = $page->sections()->create(['layout_section_id'=>$section->id]);

                if(isset($sections[$index])){
                    $newSection->content_id = $sections[$index]->content_id;
                    $newSection->save();

                    $data = $sections[$index]->data;

                    foreach ($data as $datum)
                        $datum->update(['flat_plan_section_id' => $newSection->id]);
                } else {
                    $newSection->content_id = $content->id;
                    $newSection->save();
                }

                $newSectionIds[] = $newSection->id;
            }

            $page->sections()->whereNotIn('id',$newSectionIds)->delete();
            $page->load('layout.sections','sections.content','sections.data');

            $newValue = $_GET['layout_id'];

            $this->createHistory($page,'layout-change',$page->version->id,$oldValue,$newValue);

            return response()->json(['status'=>200,'data'=>$page]);
        }

        return response()->json($this->generateResponse('error'));
    }

    public function reorder(Request $request)
    {
        $input = $request->input('data');

        foreach ($input as $row){
            $page = FlatPlanPage::find($row['id']);

            if($page)
                $page->update(['order'=>$row['order']]);
        }

        return response()->json($request->input());
    }

}
