<?PHP

namespace App\Http\Controllers\Customers;

use App\Models\Company;
use App\Models\Option;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserLog;
use App\Repositories\CanCreateResponseCode;
use App\Services\Avengate\src\AvengateClient;
use App\Services\Avengate\src\SoapClient;
use App\Services\ColorServer;
use App\Services\TimezoneService;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CompanyController extends Controller
{
    use CanCreateResponseCode;
    use TimezoneService;
    use ColorServer;

    public function __construct(Company $company,Option $option, User $user, UserLog $userLog)
    {
        $this->model = $company;
        $this->option = $option;
        $this->user = $user;
        $this->userLog = $userLog;
    }

    public function index()
    {
        $timezones = $this->getTimezones();
        return view('pages.customers.index', compact('timezones'));
    }

    public function register(Request $request)
    {
        $subscription = $request->input('subscription');

        $redirect = route('login');
        $option['option']['employees'] = '1-5';

        $options = $request->only('option');

        $user = $request->only('user');

        if(User::where('email',$user['user']['email'])->count()){
            Session::flash('error','Email address is already in use. Please use a different one.');
            return redirect(url('/pricing').'#register-panel');
        }

        if(Company::where('name',$options['option']['name'])->count()){
            Session::flash('error','Company name is already registered. Please use a different one.');
            return redirect(url('/pricing').'#register-panel');
        }

        $company = $this->model->create(array('name'=>$options['option']['name']));

        if($company){
            foreach ($options['option'] as $key => $value) {
                $this->option->create(array('name'=>$key,'value'=>$value,'company_id'=>$company->id));
            }
        }

        $user['user']['password'] = Hash::make($user['user']['password']);
        $user['user']['company_id'] = $company->id;
        $user['user']['username'] = $user['user']['email'];

        $user = User::create($user['user']);

        $user->role()->sync([1]);
        $user->company()->sync([$company->id]);

        $now = Carbon::now();
        $subscriptionData['SubscriptionStartDate'] = $now;
        $subscriptionData['ExpirationDate'] = $now->addDays(30);
        $subscriptionData['plan_id'] = 1;

        $company->subscriptions()->create($subscriptionData);

        if($user){

            if($subscription){
                switch ($subscription) {
                    case 1:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4708750&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID='.$user->id.'&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        break;
                    case 2:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4708751&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID='.$user->id.'&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        $option['option']['employees'] = '6-15';
                        break;
                    case 3:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4708752&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID='.$user->id.'&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        $option['option']['employees'] = '16-30';
                        break;
                    case 4:
                        $redirect = 'https://secure.avangate.com/order/checkout.php?PRODS=4708753&QTY=1&DOTEST=1&NOTIFICATION_URL=FlapiappPayments&ADDITIONAL_USER_ID='.$user->id.'&BACK_REF=https%3A%2F%2Fflapiapp.com%2Fapp%2Fsettings%3Fm%3Dthank-you';
                        $option['option']['employees'] = '31-50';
                        break;
                }
            }

            Session::flush();
            Auth::login($user);

            return redirect($redirect);
        }
    }

    public function getUserLimit(){

        if(!isset($_GET['id']))
            return null;

        $company = Company::with('subscriptions.plan')->where('id',$_GET['id'])->first();

        $data['current_users'] = count($company->users()->where('is_active',1)->get());
        $data['max_users'] = null;

        if($company->subscription)
            if($company->subscription->plan)
                $data['max_users'] = $company->subscription->plan->max_users;

        return response()->json($data);
    }

    public function toggleAutoRenew($SubscriptionReference){

        $subscription = Subscription::where('SubscriptionReference',$SubscriptionReference)->where('company_id',Auth::user()->company->id)->first();

        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');

        try {
            Log::info('Current Status: '.$subscription->RecurringEnabled);


            try {
                Log::info('Disabling Recurring : '.$subscription->id);
                $response = SoapClient::disableRecurringBilling($SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments disabled: '.$subscription->id);

                $subscription->RecurringEnabled = 0;

            } catch (\Exception $e) {

                Log::info('Enabling Recurring : '.$subscription->id);
                $response = SoapClient::enableRecurringBilling($SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments enabled: '.$subscription->id);

                $subscription->RecurringEnabled = 1;
            }

            $subscription->save();

            Log::info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);

        } catch (ClientException $e) {
        }
        return redirect()->back();
    }

    public function disableAutoRenew($SubscriptionReference){

        $subscription = Subscription::where('SubscriptionReference',$SubscriptionReference)->where('company_id',Auth::user()->company->id)->first();

        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');

        try {
            Log::info('Current Status: '.$subscription->RecurringEnabled);


            try {
                Log::info('Disabling Recurring : '.$subscription->id);
                $response = SoapClient::disableRecurringBilling($SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments disabled: '.$subscription->id);

                $subscription->RecurringEnabled = 0;

            } catch (\Exception $e) {

            }

            $subscription->save();

            Log::info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);

        } catch (ClientException $e) {
        }
        return redirect()->back();
    }
}
