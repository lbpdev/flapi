<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Department;
use App\Models\Invite;
use App\Models\MobileApiToken;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\CanCreateResponseCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Repositories\CannotAcceptWhiteSpace;

class UsersController extends Controller
{
    use CanCreateResponseCode, CannotAcceptWhiteSpace;

    public function __construct(User $user,Invite $invite){
        $this->model = $user;
        $this->invites = $invite;
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $data = $this->model->with('role')->where('id',$_GET['id'])->first();

        if(!$data)
            return response()->json($this->generateResponse('user-not-exists'));

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if(!$this->checkWhiteSpaces($input['email']))
            return response()->json($this->generateResponse('white-spaces'));

        if(!filter_var($input['email'], FILTER_VALIDATE_EMAIL))
            return response()->json($this->generateResponse('invalid-email'));

        if(strlen($input['password'])<6)
            return response()->json($this->generateResponse('password-too-short'));

        if(isset($input['user_id'])){
            $user = User::where('id',$input['user_id'])->first();

            if($this->model->where('email',$input['email'])->count() && $user->email != $input['email']){
                $error['email'] = "User with email ".$input['email']." is already registered";
                return redirect()->back();
            }

            if($this->model->where('username',$input['username'])->count() && $user->username != $input['username']){
                return redirect()->back()->with('error',"User with username ".$input['username']." is already registered");
            }

            if($user){
                $input['name'] = strip_tags(trim($input['name']));

                $user->update(array(
                    'name'=>strip_tags($input['name']),
                    'email'=>$input['email'],
                    'username'=>$input['username'],
                    'password'=>Hash::make($input['password']),
                ));

                if($user){
                    Auth::login($user);
                    return response()->json([
                        'status' => 200
                    ]);
                }
            }
        }

        Session::flash('error',"Sorry but only invited users may register.");
        return redirect(url('login'));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $userData = $request->only('name','email');

        $record = $this->model->where('id',$request->input('id'))->first();

        if(!$record)
            return response()->json($this->generateResponse('user-not-exist'));

        if(!$this->checkWhiteSpaces($userData['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if(!$this->checkWhiteSpaces($userData['email']))
            return response()->json($this->generateResponse('white-spaces'));

        if(!filter_var($userData['email'], FILTER_VALIDATE_EMAIL))
            return response()->json($this->generateResponse('invalid-email'));

        if ($record->isAdmin && $request->input('role_id')!=1){
            if($this->currentCompany()->adminCount<2)
                return response()->json($this->generateResponse('one-admin-required'));
        }

        if($userData['email']){

            if($userData['email']==$record->email){
                $userData['name'] = strip_tags(trim($userData['name']));
                $record->update(array('name'=>$userData['name']));
            }

            elseif($this->model->where('email',$userData['email'])->first())
                return response()->json($this->generateResponse('user-email-exists'));

            else
                $record->update($userData);

            $record->role()->sync(array('role_id'=>$request->input('role_id')));
        }

        $dataRoles = Role::fromCurrentCompanyAndGlobals($record->company->id)->orderBy('name','ASC')->get();
        $dataArray[0] = $dataRoles->toArray();

        $roles = Role::fromCurrentCompanyAndGlobals($record->company->id)->get()->toArray();

        $data = [];
        foreach($roles as $index=>$role) {
            $data[0][$index]['role'] = $role;
            $data[0][$index]['users'] = User::whereHas('company',function($query) use($record){
                $query->where('id',$record->company->id);
            })->whereHas('role',function($query) use($role){
                $query->where('id',$role['id']);
            })->get();
        }

        foreach($data[0] as $depIndex=>$role)
            foreach ($role['users'] as $userIndex=>$user)
                $data[0][$depIndex]['users'][$userIndex]['last_login_activity'] = $user->lastLoginTime;

        $data[1][0]['users'] = User::with('role')->whereDoesntHave('role')->whereHas('company',function($query) use($record){
            $query->where('company_id',$record->company->id);
        })->get();

        return response()->json($this->generateResponseWithData('update-user-success',$data));
    }


    public function invite(Request $request){
        $input = $request->input();

        $company = $this->currentCompany();

        if(isset($input['company_id']))
            $company = Company::find($input['company_id']);

        if($company->userLimit <= count($company->activeUsers))
            return response()->json($this->generateResponse('user-limit-exeeded'));

        if($this->model->where('email',$input['email'])->count())
            return response()->json($this->generateResponseWithData('user-email-exists',$input['email']));

        if($this->model->where('email',$input['email'])->count())
            return response()->json($this->generateResponseWithData('user-email-exists',$input['email']));

        if(!filter_var($input['email'], FILTER_VALIDATE_EMAIL))
            return response()->json($this->generateResponse('invalid-email'));

        if($input['name'] && $input['role_id']){

            $input['name'] = strip_tags(trim($input['name']));

            $user = $this->model->create(array(
                'name'=>strip_tags($input['name']),
                'email'=>$input['email'],
                'username'=>$input['email'],
                'token'=> Str::random(32),
                'is_active'=> 1,
            ));

            $user->role()->attach($input['role_id']);
            $user->company()->attach($company->id);

            $role = Role::find($input['role_id']);

            $user->load('role');

//            Mail::send('emails.invite', ['token' => $user->token,'company'=>$this->currentCompany()->name], function($message) use($user)
//            {
//                $message->to($user['email'], $user['name'])->subject('Invitation');
//            });

            return response()->json($this->generateResponseWithData('invite-success',$user));
        }

        return response()->json($this->generateResponse('error'));
    }


    public function toggleStatus(Request $request){

        $data = $this->model->where('id',strip_tags($_GET['user_id']))->first();

        $company = $data->company;

        if($data){
            if($data->is_active){

                if($data->role->slug=="admin"){
                    if($data->company->activeAdminCount <= 1)
                        return response()->json($this->generateResponse('cannot-deactivate-last-admin'));
                }

                $data->is_active = 0;
            }
            else {

                if($company->userLimit <= count($company->activeUsers))
                    return response()->json($this->generateResponse('user-limit-exeeded'));

                $data->is_active = 1;
            }

            $data->save();

//            if($data->is_active==0){
//                MobileApiToken::where('user_id',$data->id)->delete();
//            }

            return response()->json($this->generateResponseWithData('update-success',$data->is_active));


        } else
            return response()->json($this->generateResponse('record-not-exists'));
    }

//
//    public function invite(Request $request){
//        $input = $request->input();
//
//        if($this->invites->where('email',$input['email'])->count())
//            return response()->json($this->generateResponseWithData('user-email-exists',$input['email']));
//
//        if($input['name'] && $input['role_id'] && $input['department_id']){
//            $this->invites->create(array(
//                'name'=>$input['name'],
//                'email'=>$input['email'],
//                'role_id'=>$input['role_id'],
//                'department_id'=>$input['department_id'],
//                'token'=>Str::random('32')
//            ));
//        }
//
//        return response()->json($this->generateResponseWithData('invite-sent-success',$input['email']));
//    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(){

        $data = $this->model->where('id',strip_tags($_GET['user_id']))->first();

        if($data->role->slug=="admin"){
            if($data->is_active == 1){
                if($data->company->activeAdminCount <= 1){
                    return response()->json($this->generateResponse('cannot-delete-last-admin'));
                }
            }
        }

        if($data)
            if($data->delete())
                return response()->json($this->generateResponseWithData('delete-user-success',$_GET['user_id']));

        return response()->json($this->generateResponse('record-not-exists'));
    }

    public function validateEmail(){
        if($this->model->where('email',$_GET['email'])->count())
            return response()->json($this->generateResponseWithData('user-email-exists',$_GET['email']));

        return response()->json(null);
    }

    public function validateRegisterCompany(){

        if(Company::where('name',$_GET['name'])->count()){

            $error_data = [
                'message'=>'Company name is already registered.',
                'code'=>'name-exists',
                'data'=> $_GET['name']
            ];

            return response()->json($this->generateResponseWithData('company-name-exists',$error_data));
        }

        if($this->model->where('email',$_GET['email'])->count()){

            $error_data = [
                'message'=>'Email is already registered.',
                'code'=>'email-exists',
                'data'=> $_GET['email']
            ];

            return response()->json($this->generateResponseWithData('user-email-exists',$error_data));
        }

        if(strlen($_GET['password'])<8){

            $error_data = [
                'message'=>'Password should at least be 8 characters.',
                'code'=>'password-too-short',
                'data'=> $_GET['password']
            ];

            return response()->json($this->generateResponseWithData('password-too-short',$error_data));
        }

        return response()->json(null);
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll(){
        return $this->model->fromCurrentCompany()->get()->toArray();
    }
}
