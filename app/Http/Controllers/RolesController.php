<?php

namespace App\Http\Controllers;

use App\Repositories\CanCreateResponseCode;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Role;
use App\Repositories\CanCreateSlug;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller
{
    use CanCreateSlug, CanCreateResponseCode;

    public function __construct(Role $role){
        $this->model = $role;
    }

    /*
    * CHECK IF ROLE EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();

        $permissions = array_merge(
            count($request->input('views')) ? $request->input('views') : [],
            count($request->input('modifies')) ? $request->input('modifies') : []
        );

        if(Role::where('name',$input['name'])->count())
            return response()->json($this->generateResponse('role-exists'));

        if($input['name']){
            $slug  = $this->generateSlug(strip_tags($input['name']));

            $role = Role::create(array(
                'name'=>strip_tags($input['name']),
                'slug'=>$slug,
                'icon'=>$input['icon'],
                'company_id'=>Auth::user()->company->id
            ));

            $role->permissions()->sync($permissions);
        }


        $roles = $this->getAll();

        return response()->json($this->generateResponseWithData('create-success',$roles));
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $role = $this->model->with('permissions')->find($_GET['id']);

        if(!$role)
            return response()->json($this->generateResponse('stage-not-exists'));

        return response()->json($this->generateResponseWithData('create-success',$role));
    }

    public function update(Request $request){
        $input = $request->input();

        $permissions = array_merge(
            count($request->input('views')) ? $request->input('views') : [],
            count($request->input('modifies')) ? $request->input('modifies') : []
        );

        $role = $this->model->find($input['id']);

        if(!$role)
            return response()->json($this->generateResponse('stage-not-exist'));

        if($input['name']){
            if(strtolower($input['name'])==strtolower($role->name))
                $role->update(array(
                    'icon'=>$input['icon'],
                ));

            elseif($this->model->fromCompany($this->currentCompanyId())->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('stage-exists'));

            else {
                $slug  = $this->generateSlug(strip_tags($input['name']));
                $role->update(array(
                    'name'=>strip_tags($input['name']),
                    'slug'=>$slug,
                    'icon'=>$input['icon'],
                ));
            }

            $role->permissions()->sync($permissions);
        }

        $roles = $this->getAll();

        return response()->json($this->generateResponseWithData('create-success',$roles));
    }


    /*
    * DELETE A ROLE THROUGH ID
    */
    public function delete(Request $request){

        $role = Role::where('id',strip_tags($request->input('id')))->first();

        if($role)
            if($role->delete()) {
                $roles = $this->getAll();
                return response()->json($this->generateResponseWithData('delete-success',$roles));
        }

        return response()->json($this->generateResponse('role-not-exists'));
    }

    /*
    * GET ALL ROLES AND RETURN AS ARRAY
    */

    private function getAll(){
        return Role::get()->toArray();
    }

}
