<?php

namespace App\Http\Controllers\Backend;

use App\Models\Company;
use App\Models\Content\Content;
use App\Models\Content\ContentField;
use App\Models\Content\ContentFieldType;
use App\Models\Flatplans\FormatType;
use App\Models\Flatplans\MediaType;
use App\Models\Option;
use App\Models\Pivots\Permission;
use App\Models\Role;
use App\Models\Subscription;
use App\Models\User;
use App\Services\Avengate\src\SoapClient;
use App\Services\ColorServer;
use App\Services\TimezoneService;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class CompanyController extends Controller
{

    use ColorServer;
    use TimezoneService;

    public function __construct(Company $model,Option $option, Filesystem $filesystem){
        $this->model = $model;
        $this->option = $option;
        $this->filesystem = $filesystem;
        $this->userIconDirectory = 'public/img/icons/user-icons';
    }


    public function index(){
        $data = $this->model->get();

        return view('backend.companies.index', compact('data'));
    }

    public function create(){
        return view('backend.companies.create');
    }

    public function edit($company_id){

        $userIcons = $this->getFileNames($this->filesystem->files($this->userIconDirectory));

        $options = $this->option->fromCompany($company_id)->pluck('value','name');

        $options['workdays'] = isset($options['workdays']) ? json_decode($options['workdays']) : null;

        $roles = Role::fromCurrentCompanyAndGlobals($company_id)->get()->toArray();

        $data = [];
        foreach($roles as $index=>$role){
            $data[$role['id']]['role'] = $role;
            $data[$role['id']]['users'] = User::whereHas('company',function($query) use($company_id){
                $query->where('id',$company_id);
            })->whereHas('role',function($query) use($role){
                $query->where('id',$role['id']);
            })->get();
        }

        $roles = $data;

        $roleList = Role::fromCompanyAndGlobals($company_id)->orderBy('name','ASC')->pluck('name','id');
        $colors = $this->getColors();
        $timezones = $this->getTimezones();

        $company = Company::find($company_id);

//        $this->reduceUsers($company->activeUsers,$company->subscription->plan->max_users);

        $unassigned_users = User::whereDoesntHave('role')->whereHas('company',function($query) use($company_id){
            $query->where('company_id',$company_id);
        })->get();

        $admins = User::whereHas('role', function($query){
            $query->where('role_id',1);
        })->get();

        $permissions['view'] = Permission::fromCompanyAndGlobals($company_id)->viewPermissions()->orderBy('name','ASC')->get();
        $permissions['modify'] = Permission::fromCompanyAndGlobals($company_id)->modifyPermissions()->orderBy('name','ASC')->get();

        $contents = Content::with('fields.type')->fromCompanyAndGlobals($company_id)->orderBy('name','ASC')->get();
        $contentFields = ContentField::with('type')->fromCompanyAndGlobals($company_id)->orderBy('name','ASC')->get();

        $fieldTypes = ContentFieldType::orderBy('name','ASC')->pluck('name','id');
        $fields = ContentField::fromCompanyAndGlobals($company_id)->orderBy('id','ASC')->pluck('name','id');

        $mediaTypes = MediaType::fromCompanyAndGlobals($company_id)->get();
        $formatTypes = FormatType::fromCompanyAndGlobals($company_id)->get();

        return view('backend.companies.view',compact('users','company_id','formatTypes','mediaTypes','fields','fieldTypes','contents','contentFields','permissions','company','unassigned_users','options','userIcons','roles','roleList','timezones','colors'));
    }

    public function store(Request $request){
        $article = $this->articles->store($request);

        if($article){
            Session::flash('success','Saved Successfully');
            return redirect(route('backend.companies.edit',$article->id));
        }
        
        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->articles->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){

        $company = $this->model->where('id',$id)->first();

        $company->subscriptions()->delete();
        $company->users()->delete();

//        if(count($subscriptions)){
//            foreach ($subscriptions as $subscription)
//                $this->disableAutoRenew($subscription);
//        }

        if($company)
            $company->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }

    private function getFileNames($files){
        $data = [];

        foreach($files as $key=>$file){
            $fileName = str_replace($this->userIconDirectory.'/',"",$file);
            $data[$fileName] = '';
        }

        return $data;
    }


    public function disableAutoRenew($subscription){

        SoapClient::setBaseUrl('https://api.avangate.com/soap/3.0/');
        SoapClient::setCredentials('LEADINGB', '@dJvyoEH3O1]w&|Aqk4!');

        try {
            Log::info('Current Status: '.$subscription->RecurringEnabled);

            try {
                Log::info('Disabling Recurring : '.$subscription->id);
                $response = SoapClient::disableRecurringBilling($subscription->SubscriptionReference);

                if($response)
                    Log::info('Recurring Payments disabled: '.$subscription->id);

                $subscription->RecurringEnabled = 0;

            } catch (\Exception $e) {

            }

            $subscription->save();

            Log::info('Database Updated RecurringEnabled: '.$subscription->RecurringEnabled);

        } catch (ClientException $e) {
        }
        return redirect()->back();
    }
}
