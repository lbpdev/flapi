<?php

namespace App\Http\Controllers\Backend;

use App\Models\Invite;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;

use App\Repositories\CannotAcceptWhiteSpace;
use App\Repositories\CanCreateResponseCode;

class UsersController extends Controller
{
    use CannotAcceptWhiteSpace;

    public function __construct(User $model, Role $role){
        $this->model = $model;
        $this->roles = $role;
    }


    public function index(){
        $data = $this->model->get();

        return view('backend.users.index', compact('data'));
    }

    public function edit($id){
        $data = $this->model->find($id);
        $roleList = $this->roles->pluck('name','id');

        return view('backend.users.show', compact('data','roleList'));
    }

    public function delete($id){


        $data = $this->model->where('id',$id)->first();

        if($data->role->slug=="admin"){
            if($data->is_active == 1){
                if($data->company->activeAdminCount <= 1){
                    Session::flash('error','This user account cannot be deleted until a new administrator has been added or appointed.');
                }
            }
        }

        Session::flash('success','Delete successful');
        $data->delete();

        return redirect(route('backend.users.index'));
    }


    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $userData = $request->only('name','email');

        $record = $this->model->where('id',$request->input('id'))->first();

        Session::flash('alert','danger');

        if(!$record){
            Session::flash('message','User does not exist');
            return redirect()->back();
        }

        if(!$this->checkWhiteSpaces($userData['name'])){
            Session::flash('message','Invalid  name format');
            return redirect()->back();
        }

        if(!$this->checkWhiteSpaces($userData['email'])){
            Session::flash('message','Invalid email format');
            return redirect()->back();
        }

        if(!filter_var($userData['email'], FILTER_VALIDATE_EMAIL)){
            Session::flash('message','Invalid  email format');
            return redirect()->back();
        }

        if ($record->isAdmin && $request->input('role_id')!=1){
            if($this->currentCompany()->adminCount<2){
                Session::flash('message','Cannot remove all admins from this company');
                return redirect()->back();
            }
        }

        if($userData['email']){

            if($userData['email']==$record->email){
                $userData['name'] = strip_tags(trim($userData['name']));
                $record->update(array('name'=>$userData['name']));
            }

            elseif($this->model->where('email',$userData['email'])->first()){
                Session::flash('message','Email address already in use.');
                return redirect()->back();
            }

            else
                $record->update($userData);

            $record->role()->sync(array('role_id'=>$request->input('role_id')));
        }

        Session::flash('alert','success');
        Session::flash('message','User updated successfully.');
        return redirect()->back();
    }


}
