<?php

namespace App\Http\Controllers\Backend;

use App\Models\Plan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{

    public function __construct(Plan $model){
        $this->model = $model;
    }

    public function index(){
        $data = $this->model->get();
        return view('backend.plans.index', compact('data'));
    }

    public function create(){
        return view('backend.plans.create');
    }

    public function edit($id){

        $data = $this->model->find($id);

        return view('backend.plans.view',compact('data'));
    }

    public function store(Request $request){
        $article = $this->articles->store($request);

        if($article){
            Session::flash('success','Saved Successfully');
            return redirect(route('backend.plans.edit',$article->id));
        }


        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $this->articles->update($request);

        Session::flash('success','Updated Successfully');
        return redirect()->back();
    }

    public function delete($article_id){

        $company = $this->model->where('id',$article_id)->first();

        $subscriptions = $company->subscriptions()->get();

        if(count($subscriptions)){
            foreach ($subscriptions as $subscription)
                $this->disableAutoRenew($subscription);
        }

//        $this->model->where('id',$article_id)->delete();

        Session::flash('success','Deleted Successfully');
        return redirect()->back();
    }

    private function getFileNames($files){
        $data = [];

        foreach($files as $key=>$file){
            $fileName = str_replace($this->userIconDirectory.'/',"",$file);
            $data[$fileName] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
        }

        return $data;
    }
}
