<?php

namespace App\Http\Controllers;

use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OptionController extends Controller
{

    public function update(){

        $option = Option::where('company_id',Auth::user()->company->id)->where('name',$_GET['name'])->first();


        if($option)
            $option->update(['value'=>$_GET['value']]);
        else
            Option::create([
                'value'=>$_GET['value'],
                'name'=>$_GET['name'],
                'company_id'=>Auth::user()->company->id
            ]);

    }
}
