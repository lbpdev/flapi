<?php

namespace App\Http\Controllers;

use App\Models\Content\Content;
use App\Models\Flatplans\FlatPlan;
use App\Models\Flatplans\FlatPlanPage;
use App\Models\Flatplans\FlatPlanPageMerge;
use App\Models\Flatplans\FlatPlanSection;
use App\Models\Flatplans\FlatPlanSectionData;
use App\Models\Flatplans\FlatPlanVersion;
use App\Models\Flatplans\FlatPlanView;
use App\Models\Flatplans\FlatPlanViewUser;
use App\Models\Layouts\Layout;
use App\Models\Pivots\Permission;
use App\Models\PublicUser;
use App\Models\Role;
use App\Models\Flatplans\FlatPlanMeta;
use App\Models\Flatplans\FormatType;
use App\Models\Flatplans\MediaType;
use App\Models\User;
use App\Repositories\CanCreateResponseCode;
use App\Repositories\CanCreateSlug;
use App\Repositories\SanitizedRequest;
use App\Services\Uploaders\FlatplanThumbnailUploader;
use Carbon\Carbon;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

use App\Services\FlatPlanService;
use App\Services\HistoryService;

use App\Events\EventName;
use App\Services\OptionService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Mail;

class FlatPlanController extends Controller
{
    use CanCreateResponseCode, CanCreateSlug, SanitizedRequest;

    public function __construct(
        HistoryService $historyService,
        FlatPlan $flatPlan,
        FlatPlanMeta $meta,
        FlatplanThumbnailUploader $uploader,
        FlatPlanService $flatPlanService,
        OptionService $optionService,
        Log $log)
    {
        $this->model = $flatPlan;
        $this->meta = $meta;
        $this->uploader = $uploader;
        $this->flatPlanService = $flatPlanService;
        $this->history = $historyService;
        $this->option = $optionService;
        $this->log = $log;
    }

    public function index(){
        $flatPlans = $this->model->fromCurrentCompany()->active()->distinct('version')->get();

        $formats = FormatType::fromCurrentCompany()->withGlobals()->pluck('name','id');
        $media = MediaType::fromCurrentCompany()->withGlobals()->pluck('name','id');

        $users['employees'] = $this->currentCompany()->users()->employeesOnly()->where('id','!=',Auth::user()->id)->pluck('name','id');
        $users['public'] = $this->currentCompany()->users()->clientsOnly()->pluck('name','id');
        $roles = Role::with('permissions')->fromCurrentCompany()->withGlobals()->pluck('name','id');

        $view = Auth::user()->isClient ? 'flatplans.index-client' : 'flatplans.index';

        return view($view,compact('roles','users','contents','formats','media','flatPlans'));
    }

    public function archived(){
        $flatPlans = $this->model->fromCurrentCompany()->archived()->distinct('version')->get();

        $users['employees'] = $this->currentCompany()->users()->where('id','!=',Auth::user()->id)->pluck('name','id');
        $users['public'] = $this->currentCompany()->guests()->pluck('name','id');
        $roles = Role::with('permissions')->fromCurrentCompany()->withGlobals()->pluck('name','id');
        
        $archivePage = true;

        if(!Auth::user()->isAdmin)
            return redirect()->route('flatplans.index');

        return view('flatplans.index',compact('archivePage','roles','users','contents','formats','media','flatPlans'));
    }

    public function store(Request $request){

        if($this->currentCompany()->maxFlatplansReached)
            return redirect()->back();

        $guests = $request->input('guests');
        $employees = $request->input('employees');

        $flatPlan = $request->only('flatplan')['flatplan'];

        $metaData = $request->input('meta');
        $client_role_id = Role::where('slug','client')->pluck('id')->first();

        $flatPlan['name'] = strip_tags(trim($flatPlan['name']));
        $flatPlan['slug'] = $this->generateSlug($flatPlan['name']);
        $flatPlan['company_id'] = $this->currentCompanyId();
        $flatPlan['deadline'] = Carbon::parse($flatPlan['deadline'])->format('Y-m-d');

        $fp = $this->model->create($this->sanitize($flatPlan));

        $fp->meta()->create($this->sanitize($metaData));

        $file = $request->file('cover');

        if($file){
            $photo = ($file != null ? $this->uploader->upload($file) : false);
            $fp->uploads()->createMany($photo);
        }

        $version = $fp->versions()->create([]);
        $view = $fp->view()->create(['token'=>Str::random(64)]);

        if($guests){
            foreach ($guests as $user){
                if($user){
                    $userExists = User::find($user);

                    if($userExists)
                        $fp->participants()->create(['user_id'=>$user,'role_id'=>$client_role_id]);
                }
            }
        }

        if($employees){
            foreach ($employees as $user){
                if(isset($user['user_id'])){
                    $userExists = User::find($user['user_id']);

                    if($userExists)
                        $fp->participants()->create($user);
                }
            }
        }

        $fp->participants()->create(['user_id'=>Auth::user()->id , 'role_id'=>1]);

        for($x=1;$x<=$metaData['inside_pages'];$x++)
            $version->pages()->create(['order'=>$x]);

        for($x=1;$x<=$metaData['pagination'];$x++)
            $version->pages()->create([
                'order'=>$x,
                'is_cover'=>1
            ]);

        return redirect()->route('flatplans.index');
    }

    public function update(Request $request){

        $flatPlan = $request->only('flatplan')['flatplan'];
        $metaData = $request->input('meta');

        $guests = $request->input('guests');
        $employees = $request->input('employees');


        $flatPlan['company_id'] = $this->currentCompanyId();
        $flatPlan['deadline'] = Carbon::parse($flatPlan['deadline'])->format('Y-m-d');

        $data = FlatPlan::find($request->input('flatplan_id'));
        $client_role_id = Role::where('slug','client')->pluck('id')->first();

        if($data){
            $data->update($this->sanitize($flatPlan));

            $file = $request->file('cover');

            if($file){
                $data->uploads()->delete();

                $photo = ($file != null ? $this->uploader->upload($file) : false);
                $data->uploads()->createMany($photo);
            }

            $data->meta->update($metaData);

            $view = FlatPlanView::find($data->view->id);

            $data->participants()->delete();

            if($guests){
                foreach ($guests as $user){
                    if($user){
                        $userExists = User::find($user);

                        if($userExists)
                            $data->participants()->create(['user_id'=>$user,'role_id'=>$client_role_id]);
                    }
                }
            }

            if($employees){
                foreach ($employees as $user){
                    if(isset($user['user_id'])){
                        $userExists = User::find($user['user_id']);
                        if($userExists)
                            $data->participants()->create($user);
                    }
                }
            }

            $data->participants()->create(['user_id'=>Auth::user()->id , 'role_id'=>1]);

            Session::flash('success','Flatplan has been updated.');
            return redirect()->back();
        }

        return response()->json($this->generateResponse('error'));
    }

    public function revert(Request $request){

        $id = $request->input('id');

        if($id){
            $version = FlatPlanVersion::find($id);

            if($version->flatplan->company_id != Auth::user()->company->id)
                return "This flatplan does not belong to your company. This will be reported to the developers.";

            if($version){
                $version->pages()->delete();
                $templateData = $version->flatplan->meta;

                for($x=1;$x<=$templateData->inside_pages;$x++)
                    $version->pages()->create(['order'=>$x]);

                for($x=1;$x<=$templateData->pagination;$x++)
                    $version->pages()->create([
                        'order'=>$x,
                        'is_cover'=>1
                    ]);
            }
        }

        return redirect()->back();
    }

    public function deleteVersion(Request $request){

        $id = $request->input('id');

        if($id){
            $version = FlatPlanVersion::find($id);

            if($version){

                if($version->flatplan->company_id != Auth::user()->company->id)
                    return "This flatplan does not belong to your company. This will be reported to the developers.";

                $flatplan = $version->flatplan;

                if(count($flatplan->versions)==1){
                    $flatplan->delete();
                    return redirect()->route('flatplans.index');
                }
                else{
                    $version->delete();
                    $lastVersion = $flatplan->currentVersion;
                    return redirect()->route('flatplans.show',[$flatplan->slug,$lastVersion->version]);
                }

            }
        }

        return redirect()->back();
    }

    public function get($id = null){

        if(!$id)
            $id = $_GET['id'];

        $flatplan = FlatPlan::find($id);

        $data['flatplan'] = $flatplan;
        $data['cover'] = $flatplan->thumbnail;

        $flatplan->load('meta');

        $data['meta'] = $flatplan->meta;

        if($data)
            return response()->json(['data' => $data,'status'=>200]);

        return [];
    }

    public function show($slug,$version){

        $version = FlatPlanVersion::with(['pages' => function($query){
            $query->orderBy('order', 'ASC');
        }],'flatplan','pages.sections.layout','pages.sections.content','pages.sections.data')->whereHas('flatplan',function($query) use($slug){
            $query->where('company_id',$this->currentCompanyId())->where('slug',$slug);
        })->where('version',$version)->first();

        if(!$version)
            return view('page404');

        $contents = Content::whereHas('sections.page.version.flatplan',function($query) use($version) {
            $query->where('id',$version->flatplan->id);
        })->get();

        $layouts = Layout::get();
        $layouts->load('sections');
        $contentList = Content::pluck('name','id');

        $users['employees'] = Auth::user()->company->users()->pluck('name','id');
        $users['public'] = Auth::user()->company->guests()->pluck('name','id');

        $version = $this->flatPlanService->getStatus($version);

        $formats = FormatType::fromCurrentCompany()->withGlobals()->pluck('name','id');
        $media = MediaType::fromCurrentCompany()->withGlobals()->pluck('name','id');

        $flatplan = $version->flatplan;

        $versions = $flatplan->versions()->where('id','!=',$version->id)->pluck('version','version');
        $style = $this->option->getOption('flatplan_style');
        $style = $style ? $style->value : 'traditional';

        $flatplan->load('view.users');

        $permissions = $this->getPermissions($flatplan, Auth::user()->id);

        return view('flatplans.show.index',compact('permissions','flatplan','users','style','versions','media','formats','contentList','version','contents','layouts'));
    }

    public function archive(Request $request){

        $id = $request->input('id');
        $item = FlatPlan::find($id);

        if($item){

            if($item->company_id != Auth::user()->company->id)
                return "This flatplan does not belong to your company. This will be reported to the developers.";

            $item->update(['archived_at'=>Carbon::now()]);

            return redirect()->route('flatplans.archived');
        }

        return redirect()->back();
    }

    public function showArchive($slug,$version){

        $version = FlatPlanVersion::with(['pages' => function($query){
            $query->orderBy('order', 'ASC');
        }],'flatplan','pages.sections.layout','pages.sections.content','pages.sections.data')->whereHas('flatplan',function($query) use($slug){
            $query->where('company_id',$this->currentCompanyId())->where('slug',$slug);
        })->where('version',$version)->first();

        if(!$version)
            return view('page404');

        $contents = Content::whereHas('sections.page.version.flatplan',function($query) use($version) {
            $query->where('id',$version->flatplan->id);
        })->get();

        $layouts = Layout::get();
        $layouts->load('sections');
        $contentList = Content::pluck('name','id');

        $version = $this->flatPlanService->getStatus($version);

        $flatplan = $version->flatplan;
        $style = $this->option->getOption('flatplan_style');
        $style = $style ? $style->value : 'traditional';

        return view('flatplans.show.archived',compact('flatplan','style','version','contents','contentList','layouts'));
    }

    public function publicView($viewToken,$userToken){

        $view = FlatPlanView::with('flatplan')->where('token',$viewToken)->first();

        if(!$view)
            return view('page404');

        $version = FlatPlanVersion::with(['pages' => function($query){
            $query->orderBy('order', 'ASC');
        }],'flatplan','pages.sections.layout','pages.sections.content','pages.sections.data')->where('id',$view->flatplan->currentVersion->id)->first();

        $contents = Content::whereHas('sections.page.version.flatplan',function($query) use($version) {
            $query->where('id',$version->flatplan->id);
        })->get();

        $layouts = Layout::get();
        $layouts->load('sections');
        $contentList = Content::pluck('name','id');

        $version = $this->flatPlanService->getStatus($version);

        $formats = FormatType::pluck('name','id');
        $media = MediaType::pluck('name','id');

        $flatplan = $version->flatplan;
        $versions = $flatplan->versions()->where('id','!=',$version->id)->pluck('version','version');

        $style = $version->flatplan->company->getOption('flatplan_style')->first();
        $style = $style ? $style->value : 'traditional';

        $contents = Content::whereHas('sections.page.version.flatplan',function($query) use($version) {
            $query->where('id',$version->flatplan->id);
        })->get();

        $flatplan = $version->flatplan;
        $is_public = $view->is_live;

        $currentUser = FlatPlanViewUser::where('token',$userToken)->first();
        $company = $flatplan->company;

        return view('flatplans.public',compact('flatplan','currentUser','company','is_public','style','versions','contentList','contents','version'));
    }

    public function preview($slug,$version){

        $version = FlatPlanVersion::with(['pages' => function($query){
            $query->orderBy('order', 'ASC');
        }],'flatplan','pages.sections.layout','pages.sections.content','pages.sections.data')->whereHas('flatplan',function($query) use($slug){
            $query->where('company_id',$this->currentCompanyId())->where('slug',$slug);
        })->where('version',$version)->first();

        if(!$version)
            return view('page404');

        $version = $this->flatPlanService->getStatus($version);

        return view('flatplans.preview',compact('version'));
    }

    public function pdf($slug,$version){

        $version = FlatPlanVersion::with(['pages' => function($query){
            $query->orderBy('order', 'ASC');
        }],'flatplan','pages.sections.layout','pages.sections.content','pages.sections.data')->whereHas('flatplan',function($query) use($slug){
            $query->where('company_id',$this->currentCompanyId())->where('slug',$slug);
        })->where('version',$version)->first();

        if(!$version)
            return view('page404');

        $version = $this->flatPlanService->getStatus($version);

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('flatplans.preview',compact('version'))->setPaper('a4')->setOrientation('landscape')->setOption('margin-bottom', 0);
        return $pdf->inline();
    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = $this->model->where('id',strip_tags($request->input('id')))->first();

        if($data){
            if($data->company_id == $this->currentCompanyId()){
                if($data->delete()) {
                    Session::flash('success','Flatplan has been deleted.');
                    return redirect()->back();
                }
            }
        }

        Session::flash('error','Either the flatplan is already deleted, or this flatplan does not belong to you.');
        return redirect()->back();
    }


    /*
    * Make a new version out of current item
    */
    public function createVersion($id){

        $version = FlatPlanVersion::find($id);

        if($version){

            $newVersion = $this->makeVersion($version);

            return redirect()->route('flatplans.show',[$version->flatplan->slug,$newVersion->version]);
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    public function duplicate($flatplan_id){

        $flatplan = FlatPlan::find($flatplan_id);

        // Replicate Flatplan
        $newFlatplan = $flatplan->replicate();
        $newFlatplan->slug = $this->generateSlug($flatplan->name,true);
        $newFlatplan->save();

        // Replicate Thumbnail
        foreach ($flatplan->uploads()->get() as $upload){
            $file = base_path('public/'.$upload->path.'/'.$upload->file_name);
            $fileExtension = \File::extension($file);
            $newFileName = Str::random(32).'.'.$fileExtension;
            $newFile = base_path('public/'.$upload->path.'/'.$newFileName);

            \File::copy($file , $newFile);

            $newThumbnail = $upload->replicate();
            $newThumbnail->uploadable_id = $newFlatplan->id;
            $newThumbnail->file_name = $newFileName;
            $newThumbnail->save();
        }

        $latestVersion = FlatPlanVersion::where('flat_plan_id',$flatplan->id)->orderBy('version','DESC')->first();

        // Replicate Version
        $newVersion = $latestVersion->replicate();
        $newVersion->flat_plan_id = $newFlatplan->id;
        $newVersion->save();


        // Replicate Merges
        $newMergeId = Str::random(32);

        // Replicate Pages
        foreach ($latestVersion->pages as $page){
            $newPage  = $page->replicate();
            $newPage->flat_plan_version_id  = $newVersion->id;
            $newPage->save();

            // Replicate Sections
            foreach ($page->sections as $section){
                $newSection  = $section->replicate();
                $newSection->flat_plan_page_id  = $newPage->id;
                $newSection->save();

                // Replicate Section data
                foreach ($section->data as $datum){
                    $newData  = $datum->replicate();
                    $newData->flat_plan_section_id  = $newSection->id;
                    $newData->save();
                }
            }

            foreach ($page->merge()->get() as $mergepage){
                $newPage->merge()->create(['merge_id'=>$newMergeId]);
            }
        }

        // Replicate Participants
        foreach ($flatplan->participants as $participant){
            $newParticipant  = $participant->replicate();
            $newParticipant->flat_plan_id = $newFlatplan->id;
            $newParticipant->save();
        }

        // Replicate View
        $newView = $newFlatplan->view()->create(['token'=>Str::random(64)]);

        // Replicate View Users
        foreach ($flatplan->view->users as $user){
            $newUser  = $user->replicate();
            $newUser->flat_plan_view_id = $newView->id;
            $newUser->save();
        }

        $copyMeta = $flatplan->meta;
        $newMeta = $copyMeta->replicate();
        $newMeta->flat_plan_id = $newFlatplan->id;
        $newMeta->save();

        $newFlatplan->update(['archived_at' => null]);

        Session::flash('success','Flatplan has been duplicated.');
        return redirect()->route('flatplans.index');
    }

    public function makeVersion($version){

        $latestVersion = FlatPlanVersion::where('flat_plan_id',$version->flat_plan_id)->orderBy('version','DESC')->pluck('version')->first();
        // Replicate Flatplan
        $newVersion = $version->replicate();
        $newVersion->version = $latestVersion+1;
        $newVersion->save();

        // Replicate Pages
        foreach ($version->pages as $page){
            $newPage  = $page->replicate();
            $newPage->flat_plan_version_id  = $newVersion->id;
            $newPage->save();

            // Replicate Sections
            foreach ($page->sections as $section){
                $newSection  = $section->replicate();
                $newSection->flat_plan_page_id  = $newPage->id;
                $newSection->save();

                // Replicate Section data
                foreach ($section->data as $datum){
                    $newData  = $datum->replicate();
                    $newData->flat_plan_section_id  = $newSection->id;
                    $newData->save();
                }
            }

            // Replicate Merges
            $newMergeId = Str::random(32);
            $page->load('merge');

            if($page->merge){
                $merges = FlatPlanPageMerge::where('merge_id',$page->merge->merge_id)->get();

                foreach ($merges as $page){
                    $newMerge  = $page->replicate();
                    $newMerge->merge_id  = $newMergeId;
                    $newMerge->flat_plan_page_id  = $newPage->id;
                    $newMerge->save();
                }
            }
        }

        return $newVersion;
    }

    /*
    * Make a new version out of current item
    */
    public function createVersionFromHistory($flat_plan_id,$version){

        $flatplan = FlatPlan::find($flat_plan_id);
        $version = $flatplan->versions()->where('version',$version)->first();

        if($version){

            $newVersion = $this->makeVersion($version);

            return redirect()->route('flatplans.show',[$version->flatplan->slug,$newVersion->version]);
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }



    /*
    * Make a new version out of current item
    */
    public function inviteUsers(Request $request){

        $input = $request->input();
        $flatplan = FlatPlan::find($request->input('flatplan_id'));

        $view = $flatplan->view()->first();

        if(!$view)
            $view = $flatplan->view()->create(['token'=>Str::random(64)]);

        $flatplan->participants()->where('user_id','!=',Auth::user()->id)->delete();
        $flatplan->view->users()->delete();

        if(isset($input['employees'])){
            foreach($input['employees'] as $row){
                if(isset($row['user_id'])){
                    $user = User::find($row['user_id']);

                    $row['token'] = Str::random(32);

                    if($user)
                        $flatplan->participants()->create($row);
                }
            }
        }

        if(isset($input['guests'])){
            foreach($input['guests'] as $row){
                $user = PublicUser::find($row);
                if($user)
                    $user->views()->create([
                        'flat_plan_view_id' => $view->id,
                        'token' => Str::random(32)
                    ]);
            }
        }

        return redirect()->back();
    }

    public function validateNames(Request $request){
        $flatplan = $request->input('flatplan');

        if(!trim($flatplan['name']))
            return response()->json($this->generateResponse('blank-input-title'));

        $fp = FlatPlan::fromCurrentCompany()->where('name',$flatplan['name'])->first();

        if($request->input('flatplan_id')){
            $existingFP = FlatPlan::find($request->input('flatplan_id'));

            if($existingFP){
                if($existingFP->name != $flatplan['name']){
                    if($fp)
                        return response()->json($this->generateResponse('fp-name-exists'));
                }
                $fp = $existingFP;
            }
        } elseif ($fp)
                return response()->json($this->generateResponse('fp-name-exists'));

        if($fp)
            $fp->load('view.users','participants');

        return response()->json(['status'=>200,'data'=>$fp]);
    }

    public function getViewers(Request $request){

        $fp = FlatPlan::fromCurrentCompany()->where('id',$request->input('flatplan_id'))->first();

        if($fp)
            $fp->load('view.users','participants');

        return response()->json(['status'=>200,'data'=>$fp]);
    }

    public function getPermissions($flatplan, $user_id){

        $participant = $flatplan->participants()->with('role.permissions')->where('user_id',Auth::user()->id)->first();

        $data = [];

        if($participant){
            $role = $participant->role;

            if($role->slug == 'admin'){
                $userPermissions = Permission::all();

                foreach ($userPermissions as $permission)
                    $data[] =  $permission->slug;

            } else {
                foreach ($role->permissions as $permission)
                    $data[] =  $permission->slug;
            }
        }

        return $data;
    }

    public function toggleLive(Request $request){

        $this->log->useDailyFiles(storage_path().'/logs/mail.log');

        $item = FlatPlanView::find($request->input('id'));

        if($item){
            $status = $item->is_live;

            $version = $item->flatplan->currentVersion ? $item->flatplan->currentVersion->version : [];

            foreach ($item->flatplan->participants as $participant){

                if($participant->user){
                    Mail::send('emails.flatplans.live', ['flatplan'=>$item->flatplan,'version'=>$version], function($message) use($participant)
                    {
                        $message->to($participant->user->email,$participant->user->name)->subject('Invitation');
                    });
                }
            }

            $item->update(['is_live'=>($status ? 0 : 1)]);
        }

        return response()->json(['status'=>200,'data'=>$status ? 0 : 1,'message'=>($status ? 'Flat plan is now offline' : 'Flat plan is now live' )]);
    }
    
}
