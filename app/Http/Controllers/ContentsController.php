<?php

namespace App\Http\Controllers;

use App\Models\Content\Content;
use App\Models\Flatplans\FlatPlanSection;
use App\Repositories\CanCreateSlug;
use App\Repositories\CanCreateResponseCode;
use App\Repositories\SanitizedRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\CannotAcceptWhiteSpace;

class ContentsController extends Controller
{
    use CanCreateResponseCode,CanCreateSlug,CannotAcceptWhiteSpace, SanitizedRequest;

    public function __construct(Content $format){
        $this->model = $format;
    }


    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $item = $this->model->with('fields')->where('id',$_GET['id'])->first();

        if(!$item)
            return response()->json($this->generateResponse('content-not-exists'));

//        $data['projectsCount'] = 0;
//        $data['tasksCount'] = 0;
//
////        $projects = Project::fromCurrentCompany()->where('format_id',$format->id)->get();
//
//        foreach ($projects as $project){
//            foreach ($project->tasks as $task){
//                $data['tasksCount'] += count($task->utasks);
//            }
//        }

//        $data['projectsCount'] = count($projects);
        $data['content'] = $item;

        return response()->json($this->generateResponseWithData('create-success',$data));
    }


    /*
    * GET RECORD IF EXISTS
    */

    public function getFields(){

        $item = $this->model->where('id',$_GET['id'])->first();

        if(!$item)
            return response()->json($this->generateResponse('content-not-exists'));

        $data = $item;
        $data['fields'] = $item->fields()->with('type')->orderBy('id','ASC')->get();
        $data['approved'] = $item->isApproved;
        $data['completed'] = $item->isCompleted;

        $section = FlatPlanSection::find($_GET['section_id']);

        $section->update(['content_id' => $_GET['id']]);
        $section->load('data','page.version.flatplan');

        foreach ($data['fields'] as $index=>$field){
            foreach ($section->data as $datum){
                if($field->id == $datum->content_field_id)
                    $data['fields'][$index]['value'] = $datum->value;
            }
        }

        $data['contents'] = Content::whereHas('sections.page.version.flatplan',function($query) use($section) {
            $query->where('id',$section->page->version->flatplan->id);
        })->get();

        return response()->json($this->generateResponseWithData('create-success',$data));
        
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function getForSelect(){

        $format = $this->model->fromCurrentCompany()->get();

        return response()->json($format);
    }

    /*
    * CHECK IF RECORD EXISTS AND STORE
    */

    public function store(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->fromCompany($company_id)->where('name',$input['name'])->count())
            return response()->json($this->generateResponse('content-exists'));

        if($input['name']){

            $input['name'] = strip_tags(trim($input['name']));

            $data = array(
                'name'=>$input['name'],
                'color'=>$input['color'],
                'company_id'=> $company_id
            );

            $content = $this->model->create($this->sanitize($data));

            if(isset($input['fields']))
                $content->fields()->sync($input['fields']);
        }

        $data = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-content-success',$data));
    }

    /*
    * UPDATE EXISTING AND RECORD
    */

    public function update(Request $request){
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $content = $this->model->where('id',$input['id'])->first();

        if(!$content)
            return response()->json($this->generateResponse('content-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));
        
        if($input['name']){

            if(( strtolower($content->name) != strtolower($input['name']) ) && $this->model->fromCompany($company_id)->where('name',$input['name'])->first())
                return response()->json($this->generateResponse('content-exists'));

            else {
                $input['name'] = strip_tags(trim($input['name']));

                $data = array(
                    'color'=>$input['color'],
                    'name'=>$input['name'],
                );

                $content->update($this->sanitize($data));

                $content->fields()->sync($input['fields']);
            }
        }

        $formats = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-content-success',$formats));
    }


    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = Content::where('id',strip_tags($request->input('id')))->first();

        if($data)
            $company_id = $data->company_id;
            if($data->delete()) {
//                Project::where('format_id',$request->input('id'))->delete();
                $data = $this->getAll($company_id);
                return response()->json($this->generateResponseWithData('delete-content-success',$data));
            }

        return response()->json($this->generateResponse('record-not-exists'));
    }

    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->fromCurrentCompany()->withGlobals()->orderBy('name','ASC')->get()->toArray();

        return $this->model->fromCompany($company_id)->withGlobals()->orderBy('name','ASC')->get()->toArray();
    }

}
