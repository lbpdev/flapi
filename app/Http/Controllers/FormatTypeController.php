<?php

namespace App\Http\Controllers;

use App\Models\Flatplans\FormatType;
use App\Repositories\CanCreateResponseCode;
use App\Repositories\CannotAcceptWhiteSpace;
use Illuminate\Http\Request;

class FormatTypeController extends Controller
{

    use CanCreateResponseCode,CannotAcceptWhiteSpace;

    public function __construct(FormatType $contentField)
    {
        $this->model = $contentField;
    }

    /*
    * GET RECORD IF EXISTS
    */

    public function get(){

        $item = $this->model->where('id',$_GET['id'])->first();

        if(!$item)
            return response()->json($this->generateResponse('format-type-not-exists'));

        $data['formatType'] = $item;

        return response()->json($this->generateResponseWithData('create-success',$data));
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($this->model->where('name',$input['name'])->where(function($query) use ($company_id){
            $query->fromCompany($company_id)->withGlobals();
        })->count())
            return response()->json($this->generateResponse('format-type-exists'));

        if($input['name']){

            $input['name'] = strip_tags(trim($input['name']));
            $input['company_id'] = $company_id;

            $this->model->create($input);
        }

        $data = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('create-format-type-success',$data));
    }

    public function update(Request $request)
    {
        $input = $request->input();
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        $item = $this->model->where('id',$input['id'])->first();

        if(!$item)
            return response()->json($this->generateResponse('format-type-not-exist'));

        if(!$this->checkWhiteSpaces($input['name']))
            return response()->json($this->generateResponse('white-spaces'));

        if($input['name']){

            if(( strtolower($item->name) != strtolower($input['name']) ) &&
                $this->model->where('name',$input['name'])->where(function($query) use ($company_id){
                    $query->fromCompany($company_id)->withGlobals();
                })->count())
                return response()->json($this->generateResponse('format-type-exists'));

            else {
                $input['name'] = strip_tags(trim($input['name']));
                $item->update(array(
                    'name'=> strip_tags(trim($input['name'])),
                ));
            }
        }

        $formats = $this->getAll($company_id);

        return response()->json($this->generateResponseWithData('update-format-type-success',$formats));
    }



    /*
    * DELETE A RECORD THROUGH ID
    */
    public function delete(Request $request){

        $data = $this->model->where('id',strip_tags($request->input('id')))->first();

        if($data)
            $company_id = $data->company_id;
        if($data->delete()) {
            $data = $this->getAll($company_id);
            return response()->json($this->generateResponseWithData('delete-field-success',$data));
        }

        return response()->json($this->generateResponse('record-not-exists'));
    }


    /*
    * GET ALL RECORDS AND RETURN AS ARRAY
    */

    private function getAll($company_id = null){

        if(!$company_id)
            return $this->model->fromCurrentCompany()->withGlobals()->orderBy('id','ASC')->get()->toArray();

        return $this->model->fromCompany($company_id)->withGlobals()->orderBy('id','ASC')->get()->toArray();
    }
}
