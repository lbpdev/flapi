<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Models\Content\Content;
use App\Models\Content\ContentField;
use App\Models\Content\ContentFieldType;
use App\Models\Flatplans\FormatType;
use App\Models\Flatplans\MediaType;
use App\Models\Pivots\Permission;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Option;
use Illuminate\Filesystem\Filesystem;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Services\ColorServer;
use App\Services\TimezoneService;

use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    use ColorServer;
    use TimezoneService;

    public $pageTitle = "Settings";

    public function __construct(Option $option, Filesystem $filesystem){
        $this->option = $option;
        $this->filesystem = $filesystem;
        $this->userIconDirectory = 'public/img/icons/user-icons';

    }

    public function index(){
        $company = Auth::user()->company;

        $userIcons = $this->getFileNames($this->filesystem->files($this->userIconDirectory));

        $options = $this->option->fromCurrentCompany()->pluck('value','name');

        $options['workdays'] = isset($options['workdays']) ? json_decode($options['workdays']) : null;

        $roles = Role::fromCurrentCompanyAndGlobals($company->id)->get()->toArray();

        $data = [];
        foreach($roles as $index=>$role){
            $data[$role['id']]['role'] = $role;
            $data[$role['id']]['users'] = User::whereHas('company',function($query) use($company){
                $query->where('id',$company->id);
            })->whereHas('role',function($query) use($role){
                $query->where('id',$role['id']);
            })->get();
        }

        $roles = $data;
        $roleList = Role::fromCurrentCompanyAndGlobals($company->id)->orderBy('name','ASC')->pluck('name','id');
        $colors = $this->getColors();
        $timezones = $this->getTimezones();

        $this->reduceUsers($company->activeUsers,$company->subscription->plan->max_users);

        $unassigned_users = User::whereDoesntHave('role')->whereHas('company',function($query){
            $query->where('company_id',Auth::user()->company->id);
        })->get();

        $admins = User::whereHas('role', function($query){
            $query->where('role_id',1);
        })->get();

        $permissions['view'] = Permission::fromCurrentCompanyAndGlobals($company->id)->viewPermissions()->orderBy('name','ASC')->get();
        $permissions['modify'] = Permission::fromCurrentCompanyAndGlobals($company->id)->modifyPermissions()->orderBy('name','ASC')->get();

        $contents = Content::with('fields.type')->fromCurrentCompanyAndGlobals($company->id)->orderBy('name','ASC')->get();
        $contentFields = ContentField::with('type')->fromCurrentCompanyAndGlobals($company->id)->orderBy('company_id','ASC')->get();

        $fieldTypes = ContentFieldType::orderBy('name','ASC')->pluck('name','id');
        $fields = ContentField::fromCurrentCompanyAndGlobals($company->id)->orderBy('id','ASC')->pluck('name','id');

        $mediaTypes = MediaType::fromCurrentCompanyAndGlobals($company->id)->get();
        $formatTypes = FormatType::fromCurrentCompanyAndGlobals($company->id)->get();

        return view('settings.index',compact('users','formatTypes','mediaTypes','fields','fieldTypes','contents','contentFields','permissions','company','unassigned_users','options','userIcons','roles','roleList','timezones','colors'));
    }

    public function samplePDF(){

        $userIcons = $this->getFileNames($this->filesystem->files($this->userIconDirectory));

        $options = $this->option->fromCurrentCompany()->pluck('value','name');

        $options['workdays'] = isset($options['workdays']) ? json_decode($options['workdays']) : null;

        $roles = Role::get();
        $formats = Format::fromCurrentCompany()->get();
        $roleList = Role::pluck('name','id');
        $departments = Department::fromCurrentCompany()->with('users')->get();
        $departmentList = Department::fromCurrentCompany()->pluck('name','id');
        $stages = Stage::with('department')->fromCurrentCompany()->orderBy('order','ASC')->get();
        $colors = $this->getColors();
        $timezones = $this->getTimezones();
        $clients = Client::fromCurrentCompany()->get();

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pages.settings.index', compact('options','userIcons','roles','roleList','departments','departmentList','stages','formats','timezones','colors','clients'));
        return $pdf->inline();
    }

    public function store(Request $request){

        $input = $request->except('_token','logo','workdays','crop');
        $logo = $request->file('logo');
        $company_id = $this->currentCompanyId();

        if(isset($input['company_id']))
            $company_id = $input['company_id'];

        foreach ($input as $key => $value) {

            if($value){
                $option = $this->option->fromCompany($company_id)->where('name',$key)->first();

                if($option)
                    $option->update(array('value'=>$value));
                else
                    $option = $this->option->create(array('name'=>$key,'value'=>$value,'company_id'=>$company_id));
            }
        }

        $workDays = $request->input('workdays');

        if(count($workDays)){
            $option = $this->option->where('name','workdays')->fromCompany($company_id)->first();

            if($option)
                $option->update(array('value'=>json_encode($workDays)));
            else
                $this->option->create(array('name'=>'workdays','value'=>json_encode($workDays),'company_id'=>$company_id));
        }


        if($logo)
            $this->uploadLogo($logo,$company_id,$request);

        Session::flash('message','Company details updated.');
        return redirect()->back();
    }

    private function getFileNames($files){
        $data = [];

        foreach($files as $key=>$file){
            $fileName = str_replace($this->userIconDirectory.'/',"",$file);
            $data[$fileName] = '';
//            $data[$fileName] = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
        }

        return $data;
    }

    private function uploadLogo($logo,$company_id = null,$request = null){

        if(!$company_id)
            $company_id = $this->currentCompanyId();

        $imageName = Str::random('36') . '.' . $logo->getClientOriginalExtension();

        $logo = Image::make($logo->getRealPath());
        $path = public_path('uploads/logo/' . $imageName);

        $crop = $request->input('crop');

        $uploadedLogo = $logo->resize($crop['max_width'],$crop['max_height'])->crop(intval($crop['w']), intval($crop['h']), intval($crop['x']), intval($crop['y']))->save($path);

        if($uploadedLogo){
            $existingLogo = Option::where('name','logo')->where('company_id',$company_id)->first();

            if($existingLogo)
                $existingLogo->update(array('value'=>$imageName));
            else
                $this->option->create(array('name'=>'logo','value'=>$imageName,'company_id'=>$company_id));
        }
    }


    public function reduceUsers($activeUsers,$max_users){
        $employees = 0;

        foreach($activeUsers as $employee){
            if(!$employee->department && $employee->isEmployee){
                $employee->is_active = 0;
                $employee->save();
            }
        }

        if($max_users < count($activeUsers)){
            foreach($activeUsers as $employee){

                if($employees < $max_users){
                    $employees++;
                }
                else {
                    $employee->is_active = 0;
                    $employee->save();
                }
            }
        }

    }

    public function storeReports(Request $request){

        $input = $request->input();

        $company = Auth::user()->company;
        $now = Carbon::now()->setTimezone(Auth::user()->company->timezone)->format('Y-m-d');

        $report = Report::where('type','email-reports')->where('company_id',$company->id)->first();
        $time = Carbon::parse($now.' '.$input['time'] ,$company->timezone);
        $time = $time->setTimezone('UTC');
        $day = null;
        $month = null;
        $week_day = null;

        if($input['interval']=='w'){
            for($x=0;$x<7;$x++){
                if(Carbon::now()->addDays($x)->setTimezone(Auth::user()->company->timezone)->format('D')==$input['week-day']){
                    $now = Carbon::now()->addDays($x)->setTimezone(Auth::user()->company->timezone)->format('Y-m-d');
                    $week_day = $input['week-day'];
                    break;
                }
            }
        }

        elseif($input['interval']=='m'){
            for($x=0;$x<32;$x++){
                if(intval(Carbon::now()->addDays($x)->setTimezone(Auth::user()->company->timezone)->format('d'))==intval($input['month-day'])){
                    $now = Carbon::parse(Carbon::now()->format('Y').'-'.Carbon::now()->addDays($x)->setTimezone(Auth::user()->company->timezone)->format('m-d'))->format('Y-m-d');
                    $day = Carbon::parse(Carbon::now()->format('Y').'-'.Carbon::now()->addDays($x)->setTimezone(Auth::user()->company->timezone)->format('m-d'))->format('d');
                    break;
                }
            }
        }

        elseif($input['interval']=='y'){

            $next = Carbon::parse(Carbon::now()->format('Y').'-'.$input['month'].'-'.$input['day']);

            if($next->diffInDays(Carbon::now(),false) > 0)
                $next->addYear();

            $now = $next->format('Y-m-d');
            $day = $input['day'];
            $month = $input['month'];
        } else {
            $now = Carbon::now()->setTimezone(Auth::user()->company->timezone)->addDay()->format('Y-m-d');
        }

        $next_send = Carbon::parse($now.' '.$time->format('H:i') ,$company->timezone);

        if(!$report)
            $company->reports()->create([
                'interval' => $input['interval'],
                'time' => $time->format('H:i'),
                'type' => 'email-reports',
                'day' => $day,
                'week_day' => $week_day,
                'month' => $month,
                'next_send' => $next_send,
            ]);
        else
            $report->update([
                'time' => $time->format('H:i'),
                'interval' => $input['interval'],
                'day' => $day,
                'week_day' => $week_day,
                'month' => $month,
                'next_send' => $next_send,
            ]);

        $productivityReport = $company->options()->where('name','productivity-report')->first();

        if($productivityReport && !isset($input['productivity'])){
            $productivityReport->delete();
        } else {
            if(!$productivityReport && isset($input['productivity']))
                $company->options()->create([
                    'name' => 'productivity-report',
                    'value' => $input['interval'],
                ]);
        }

        $deadlineReport = $company->options()->where('name','deadline-report')->first();

        if($deadlineReport && !isset($input['deadline'])){
            $deadlineReport->delete();
        } else {
            if(!$deadlineReport && isset($input['deadline']))
                $company->options()->create([
                    'name' => 'deadline-report',
                    'value' => $input['interval'],
                ]);
        }

        $timeReport = $company->options()->where('name','time-report')->first();

        if($timeReport && !isset($input['booked'])){
            $timeReport->delete();
        } else {
            if(!$timeReport && isset($input['booked']))
                $company->options()->create([
                    'name' => 'time-report',
                    'value' => $input['interval'],
                ]);
        }

        $deliverableReport = $company->options()->where('name','deliverable-report')->first();

        if($deliverableReport && !isset($input['deliverables'])){
            $deliverableReport->delete();
        } else {
            if(!$deliverableReport && isset($input['deliverables']))
                $company->options()->create([
                    'name' => 'deliverable-report',
                    'value' => $input['interval'],
                ]);
        }

        return redirect()->back();
    }

}

