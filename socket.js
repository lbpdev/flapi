var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var users = {};
var roomPrefix = 'flapi-room-';

var socket = http.listen(3001, function(){
    console.log('Listening on Port 3001');
});

// socket.io connection establishment
io.sockets.on('connection', function(socket){
    console.log("Connection Made");

    socket.on('subscribe', function(data) {

        room = roomPrefix+data.room;
        socket.nickname = data.nickname;
        socket.userId = data.userId;
        socket.type = data.type;
        socket.join(room);
        editting = false;

        clientIds = Object.keys(io.nsps['/'].adapter.rooms[room].sockets);

        console.log(data.nickname+" subscribed to "+room);

        someoneEditting = false;

        if(data.type=='employee'){
            for(x=0;x<clientIds.length;x++){
                if(users[clientIds[x]]){
                    if(users[clientIds[x]].editting){
                        someoneEditting = true;
                        break;
                    }
                }
            }

            if(!someoneEditting)
                editting = true;
        }

        users[data.clientId] = {
            nickname : data.nickname,
            clientId : data.clientId,
            room : room,
            userId : data.userId,
            type : data.type,
            editting : editting,
        };

        console.log(users[data.clientId]);
        io.sockets.in(room).emit('updateUsers', users);
    });

    socket.on('disconnect', function() {

        if(users[socket.id].editting)
            io.sockets.in(room).emit('noOneIsEditting');

        io.sockets.in(room).emit('statusUpdate',{ 'message' : 'Editor has been kicked out for being idle for too long.' });

        delete users[undefined];
        delete users[socket.id];

        io.sockets.in(room).emit('updateUsers', users);
    });

    socket.on('getActiveUsers', function(data) {
        io.sockets.in(roomPrefix+data.room).emit('updateUsers', users);
    });

    socket.on('kicked', function(data) {
        console.log('disconnecting');
        io.sockets.in(roomPrefix+data.room).emit('kicked');
        io.sockets.in(roomPrefix+data.room).emit('updateUsers', users);

        socket.disconnect();
    });

    socket.on('getActiveUsersInRoom', function(data) {
        clientIds = Object.keys(io.nsps['/'].adapter.rooms[data.room].sockets);
        rows = [];

        for(x=0;x<clientIds.length;x++){
            rows[x] = users[clientIds[x]];
        }

        console.log(rows);

        io.sockets.in(data.room).emit('getActiveUsersInRoom', rows);
    });
});