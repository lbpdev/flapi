<?php

use Illuminate\Database\Seeder;
use App\Models\Plan;

class PlanTableSeeder extends Seeder
{

    public function __construct(Plan $plan){
        $this->model = $plan;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['name' => 'Plan 1' , 'code' => 'flapi_starter', 'max_users' => '5', 'max_flatplans' => '5','max_pages' => '80'],
            ['name' => 'Plan 2' , 'code' => 'flapi_optimum', 'max_users' => '10','max_flatplans' => '5','max_pages' => '160'],
            ['name' => 'Plan 3' , 'code' => 'flapi_monster', 'max_users' => '15','max_flatplans' => '5','max_pages' => '240'],
            ['name' => 'Plan 4' , 'code' => 'flapi_gold', 'max_users' => '20','max_flatplans' => '5','max_pages' => '400'],
        ];

        foreach($data as $drow)
            Plan::create($drow);
    }
}
