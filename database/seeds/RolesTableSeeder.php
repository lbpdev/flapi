<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Pivots\Permission;

class RolesTableSeeder extends Seeder
{

    public function __construct(Role $role){
        $this->roles = $role;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->roles->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $roles = [
            ['name' => 'Administrator' , 'slug' => 'admin'      ,'icon' => 'Star.png'   , 'company_id' => 0],
            ['name' => 'Production'    , 'slug' => 'production' ,'icon' => 'Star.png' , 'company_id' => 0],
            ['name' => 'Editor'        , 'slug' => 'editor'     ,'icon' => 'Star.png' , 'company_id' => 0],
        ];

        $viewOnly = [
            ['name' => 'Designer'      , 'slug' => 'designer'   ,'icon' => 'Ribbon.png' , 'company_id' => 0],
            ['name' => 'Client'        , 'slug' => 'client'     ,'icon' => 'Ribbon.png' , 'company_id' => 0],
        ];

        $permissions = Permission::where('company_id',0)->pluck('id');
        $viewPermissions = Permission::where('company_id',0)->where('type','view')->pluck('id');

        foreach($roles as $row){
            $role = Role::create($row);
            $role->permissions()->sync($permissions);
        }

        foreach($viewOnly as $row){
            $role = Role::create($row);
            $role->permissions()->sync($viewPermissions);
        }
    }
}
