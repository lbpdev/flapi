<?php

use Illuminate\Database\Seeder;
use App\Models\Content\ContentField;
use Illuminate\Support\Str;

class ContentFieldSeeder extends Seeder
{

    public function __construct(ContentField $data){
        $this->model = $data;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['name' => 'Title' ,'content_field_type_id'=>1],
            ['name' => 'Description' , 'content_field_type_id'=>2],
            ['name' => 'Design Complete' , 'content_field_type_id'=>3],
            ['name' => 'Article Approved' , 'content_field_type_id'=>3],
            ['name' => 'Notes' , 'content_field_type_id'=>2],
            ['name' => 'Contact Number' , 'content_field_type_id'=>1],
            ['name' => 'Contact Email' , 'content_field_type_id'=>1],
        ];

        foreach($data as $datum){
            $datum['slug'] = Str::slug($datum['name']);
            $datum['is_public'] = 1;
            $this->model->create($datum);
        }

    }
}
