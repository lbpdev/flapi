<?php

use Illuminate\Database\Seeder;
use App\Models\Layouts\Layout;
use App\Models\Layouts\LayoutSection;

class LayoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Layout::truncate();
        LayoutSection::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        // 'top','left','right','bottom','height','width','order','z_index','layout_id'

        $layouts = [
            [
                'name'     => 'Full Page',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>100,'order'=>1,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column A',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>51,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>51,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column B',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>31,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>71,'width'=>100,'order'=>2,'z_index'=>1],
                ]
            ],
            [
                'name'     => 'One Column C',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>71,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>31,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column D',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>76,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>26,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column E',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>26,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>76,'width'=>100,'order'=>2,'z_index'=>1],
                ]
            ],
            [
                'name'     => 'One Column F',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>34,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 33.3,'left'=>0,'right'=>null,'bottom'=>0,'height'=>34,'width'=>100,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>34,'width'=>100,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column G',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>29,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 27,'left'=>0,'right'=>null,'bottom'=>0,'height'=>27,'width'=>100,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 53,'left'=>0,'right'=>null,'bottom'=>0,'height'=>27,'width'=>100,'order'=>3,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>27,'width'=>100,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column H',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>54,'width'=>100,'order'=>1,'z_index'=>1],
                    [ 'top' => 53,'left'=>0,'right'=>null,'bottom'=>0,'height'=>26,'width'=>100,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>26,'width'=>100,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column I',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>29,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 27,'left'=>0,'right'=>null,'bottom'=>0,'height'=>51,'width'=>100,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>26,'width'=>100,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column J',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>16,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>86,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'One Column K',
                'category' => 'one-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>86,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>16,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],

            // 2 COLUMN
            [
                'name'     => 'Two Column A',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>53,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column B',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>53,'order'=>3,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>53,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column C',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>53,'order'=>1,'z_index'=>2 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column D',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>53,'order'=>2,'z_index'=>2 ],
                ]
            ],
            [
                'name'     => 'Two Column E',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>53,'order'=>1,'z_index'=>2 ],
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column F',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>53,'order'=>2,'z_index'=>2 ],
                ]
            ],
            [
                'name'     => 'Two Column G',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>0,'bottom'=>0,'height'=>53,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column H',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>100,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column I',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column J',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column K',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>77,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>27,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>27,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column L',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>73,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>33,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>33,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column M',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>34,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>34,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 33.3,'left'=>0,'right'=>null,'bottom'=>null,'height'=>34,'width'=>53,'order'=>3,'z_index'=>1 ],
                    [ 'top' => 33.3,'left'=>null,'right'=>0,'bottom'=>null,'height'=>34,'width'=>53,'order'=>4,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>34,'width'=>53,'order'=>5,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>34,'width'=>53,'order'=>6,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column N',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>34,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 33.3,'left'=>null,'right'=>0,'bottom'=>null,'height'=>34,'width'=>53,'order'=>3,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>34,'width'=>53,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column O',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>34,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 33.3,'left'=>0,'right'=>null,'bottom'=>null,'height'=>34,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>34,'width'=>53,'order'=>3,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>100,'width'=>53,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column P',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>70,'width'=>53,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>70,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>34,'width'=>100,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column Q',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>0,'height'=>34,'width'=>53,'order'=>1,'z_index'=>2 ],
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>0,'height'=>100,'width'=>100,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Two Column R',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>0,'height'=>100,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>34,'width'=>53,'order'=>2,'z_index'=>2 ],
                ]
            ],
            [
                'name'     => 'Two Column S',
                'category' => 'two-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>70,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>33,'width'=>53,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>33,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],

            // 3 COLUMN
            [
                'name'     => 'Three Column A',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>33,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>70,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column B',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>70,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>33,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column C',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>35,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>33.3,'bottom'=>null,'height'=>100,'width'=>34,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>36,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column D',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>51,'width'=>70,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>0,'bottom'=>0,'height'=>51,'width'=>70,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>33,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column E',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>33,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>70,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>70,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column F',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>70,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>33,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>33,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column G',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>33,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>33,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>70,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column H',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>35,'order'=>2,'z_index'=>1 ],
                    [ 'top' => null,'left'=>33.3,'right'=>null,'bottom'=>0,'height'=>53,'width'=>34,'order'=>3,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>36,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column I',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>53,'width'=>35,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>33.3,'right'=>null,'bottom'=>null,'height'=>53,'width'=>34,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>53,'width'=>36,'order'=>3,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>100,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Three Column J',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>0,'right'=>null,'bottom'=>0,'height'=>53,'width'=>70,'order'=>2,'z_index'=>2 ],
                ]
            ],
            [
                'name'     => 'Three Column K',
                'category' => 'three-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>100,'order'=>1,'z_index'=>1 ],
                    [ 'top' => null,'left'=>null,'right'=>0,'bottom'=>0,'height'=>53,'width'=>70,'order'=>2,'z_index'=>2 ],
                ]
            ],

            // 4 COLUMN
            [
                'name'     => 'Four Column A',
                'category' => 'four-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>17,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>85,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Four Column B',
                'category' => 'four-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>85,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>17,'order'=>2,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Four Column C',
                'category' => 'four-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>29,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>27,'right'=>null,'bottom'=>null,'height'=>100,'width'=>27,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>53,'right'=>null,'bottom'=>null,'height'=>100,'width'=>27,'order'=>3,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>27,'order'=>4,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Four Column D',
                'category' => 'four-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>55,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>53,'right'=>null,'bottom'=>null,'height'=>100,'width'=>27,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>27,'order'=>3,'z_index'=>1 ],
                ]
            ],
            [
                'name'     => 'Four Column E',
                'category' => 'four-column',
                'sections' => [
                    [ 'top' => 0,'left'=>0,'right'=>null,'bottom'=>null,'height'=>100,'width'=>29,'order'=>1,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>27,'right'=>null,'bottom'=>null,'height'=>100,'width'=>27,'order'=>2,'z_index'=>1 ],
                    [ 'top' => 0,'left'=>null,'right'=>0,'bottom'=>null,'height'=>100,'width'=>53,'order'=>3,'z_index'=>1 ],
                ]
            ],

        ];  //End of $layouts

        foreach($layouts as $layout){
            $newLayout = Layout::create([
                'name' => $layout['name'],
                'category' => $layout['category'],
            ]);

            if($newLayout)
                $newLayout->sections()->createMany($layout['sections']);
        }

    }
}
