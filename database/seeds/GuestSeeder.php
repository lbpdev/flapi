<?php

use Illuminate\Database\Seeder;
use App\Models\PublicUser;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        PublicUser::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // 'top','left','right','bottom','height','width','order','z_index','layout_id'

        $data = [
            ['name' => 'John Petrucci', 'email' => 'john@petrucci.com' , 'company_id' => 1],
            ['name' => 'Steve Vai',     'email' => 'steve@vai.com' , 'company_id' => 1],
            ['name' => 'Darell Abbott', 'email' => 'darell@abbott.com' , 'company_id' => 1],
            ['name' => 'Mike Manggini', 'email' => 'mike@manggini.com' , 'company_id' => 1],
            ['name' => 'Tom Morello',   'email' => 'tom@morello.com' , 'company_id' => 1],
            ['name' => 'Jimmy Page',    'email' => 'jimmy@page.com' , 'company_id' => 1],
        ];

        foreach($data as $row){
            PublicUser::create($row);
        }
    }
}
