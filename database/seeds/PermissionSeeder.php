<?php

use Illuminate\Database\Seeder;
use App\Models\Pivots\Permission;
use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $viewPermissions = [
            ['name' => 'Title'],
            ['name' => 'Description'],
            ['name' => 'Design Complete'],
            ['name' => 'Article Approved'],
            ['name' => 'Notes'],
            ['name' => 'Contact Number'],
            ['name' => 'Contact Email'],
            ['name' => 'Content Private Fields'],
            ['name' => 'Content Public Fields'],
            ['name' => 'Archives'],
        ];

        $EditPermissions = [
            ['name' => 'Flat Plan'],
            ['name' => 'Project Name'],
            ['name' => 'Issue Number'],
            ['name' => 'Format'],
            ['name' => 'Deadline'],
            ['name' => 'Version'],
            ['name' => 'Merge Pages'],
            ['name' => 'Delete Pages'],
            ['name' => 'Add Pages'],
            ['name' => 'Move Pages'],
            ['name' => 'Page Layouts'],
            ['name' => 'Create Version'],
            ['name' => 'Status'],
            ['name' => 'Participants'],
            ['name' => 'Archives'],
            // -------------------------------------------------------------------------- //
            ['name' => 'Title'],
            ['name' => 'Description'],
            ['name' => 'Design Complete'],
            ['name' => 'Article Approved'],
            ['name' => 'Notes'],
            ['name' => 'Contact Number'],
            ['name' => 'Contact Email'],
            ['name' => 'Content Private Fields'],
            ['name' => 'Content Public Fields'],
        ];

        foreach($viewPermissions as $permission){
            $permission['type'] = 'view';
            $permission['company_id'] = 0;
            $permission['slug'] = 'view-'.Str::slug($permission['name']);
            Permission::create($permission);
        }

        foreach($EditPermissions as $permission){
            $permission['type'] = 'modify';
            $permission['company_id'] = 0;
            $permission['slug'] = 'modify-'.Str::slug($permission['name']);
            Permission::create($permission);
        }
    }
}
