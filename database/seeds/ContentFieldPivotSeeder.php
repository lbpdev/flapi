<?php

use Illuminate\Database\Seeder;
use App\Models\Content\ContentFieldPivot;

class ContentFieldPivotSeeder extends Seeder
{

    public function __construct(ContentFieldPivot $data){
        $this->model = $data;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            //Editorial
            ['content_field_id' => 1 , 'content_id' => 1],
            ['content_field_id' => 2 , 'content_id' => 1],
            ['content_field_id' => 3 , 'content_id' => 1],
            ['content_field_id' => 4 , 'content_id' => 1],
            ['content_field_id' => 5 , 'content_id' => 1],
            ['content_field_id' => 6 , 'content_id' => 1],
            ['content_field_id' => 7 , 'content_id' => 1],
            // Adverts
            ['content_field_id' => 1 , 'content_id' => 2],
            ['content_field_id' => 2 , 'content_id' => 2],
            ['content_field_id' => 5 , 'content_id' => 2],
            ['content_field_id' => 6 , 'content_id' => 2],
            ['content_field_id' => 7 , 'content_id' => 2],
        ];

        foreach($data as $datum)
            $this->model->create($datum);
    }
}
