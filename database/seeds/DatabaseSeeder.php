<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CompanySeeder::class);
         $this->call(PermissionSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(PlanTableSeeder::class);
         $this->call(SubscriptionTableSeeder::class);
         $this->call(ColorSeeder::class);
         $this->call(ContentFieldTypeSeeder::class);
         $this->call(ContentSeeder::class);
         $this->call(ContentFieldSeeder::class);
         $this->call(ContentFieldPivotSeeder::class);
         $this->call(MediaTableSeeder::class);
         $this->call(FormatTableSeeder::class);
         $this->call(LayoutSeeder::class);
         $this->call(GuestSeeder::class);
    }
}
