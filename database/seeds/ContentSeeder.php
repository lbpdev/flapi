<?php

use Illuminate\Database\Seeder;
use App\Models\Content\Content;

class ContentSeeder extends Seeder
{


    public function __construct(Content $data){
        $this->model = $data;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['name' => 'Editorial' , 'color' => '#40ad48','company_id'=>0],
            ['name' => 'Advert' , 'color' => '#ef6a9f','company_id'=>0],
        ];

        foreach($data as $datum)
            $this->model->create($datum);
    }
}
