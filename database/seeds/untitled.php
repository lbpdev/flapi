<?php 
 
    if(isset($_POST['array'])){
        /*
         * Example : print_r( $this->convert([[1,2,[3,4],5],6,[7,8,9],10,[11,[12,[13,14,[15,16,17]]]]]) );
         * $array = Array to convert,
         * $data = temporary data holder
         * $results = main result array holder
         */
        convert($_POST['array']);

        public function convert($array,$data = [],$results = []){
             
            // Loop throught the array
            for($x=0;$x<count($array);$x++) {
     
                // Check if row is an array
                if(count($array[$x])>1)
                {
                    /*
                     * Call back the whole function using the current row $array[$x]
                     * refresh loop data holder $data = []
                     * Continue the current results = $results
                     */
                    $d = $this->convert($array[$x],[],$results);
     
                    //loop through the returned array and get single items
                    foreach ($d as $i)
                        $data[] = $i;
                }
                // Row is not an Array
                else
                {
                    $data[] = $array[$x];
                }
            }
     
            // Append current temporary data to the results
            $results = $data;
     
            return $results;
        }

    } else {
        ?>

            <form>
                <input type="text" value="" name="array">
                <input type="submit">
            </form>

        <?php
    }