<?php

use Illuminate\Database\Seeder;
use App\Models\Color;

class ColorSeeder extends Seeder
{

    public function __construct(Color $color){
        $this->model = $color;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        $this->model->truncate();

        $data = [
            [ 'value' => '#00e3cc' ],
            [ 'value' => '#ff8083' ],
            [ 'value' => '#6fd1dc' ],
            [ 'value' => '#bfdd35' ],
            [ 'value' => '#9495a4' ],
            [ 'value' => '#38a4ff' ],
            [ 'value' => '#da72e1' ],
            [ 'value' => '#7e81ce' ],
            [ 'value' => '#ffc75b' ],
            [ 'value' => '#ff9f71' ],
            [ 'value' => '#00e388' ],
            [ 'value' => '#ffb943' ],
            [ 'value' => '#ffdb1a' ],
            [ 'value' => '#f1cd43' ],
            [ 'value' => '#858f54' ],
            [ 'value' => '#609090' ],
            [ 'value' => '#40ad48' ],
            [ 'value' => '#9fe0c1' ],
            [ 'value' => '#a3e08b' ],
            [ 'value' => '#c5aaa2' ],
            [ 'value' => '#b4b788' ],
            [ 'value' => '#addd29' ],
            [ 'value' => '#a81e61' ],
            [ 'value' => '#da0d81' ],
            [ 'value' => '#de584c' ],
            [ 'value' => '#db4f5c' ],
            [ 'value' => '#c3578b' ],
            [ 'value' => '#ef6a9f' ],
            [ 'value' => '#f9acde' ],
            [ 'value' => '#fe8083' ],
            [ 'value' => '#ff5d1c' ],
            [ 'value' => '#f17e4f' ],
            [ 'value' => '#fa665d' ],
            [ 'value' => '#f5a860' ],
            [ 'value' => '#8585ff' ],
            [ 'value' => '#59669a' ],
            [ 'value' => '#b8d7e9' ],
            [ 'value' => '#a2b4c2' ],
            [ 'value' => '#845fca' ],
            [ 'value' => '#da72e1' ],
            [ 'value' => '#ad95e1' ],
            [ 'value' => '#a17dba' ],
            [ 'value' => '#8c79b1' ],
            [ 'value' => '#7171df' ],
            [ 'value' => '#0078b3' ],
            [ 'value' => '#39a5ff' ],
            [ 'value' => '#5b77cb' ],
            [ 'value' => '#88a6d6' ],
            [ 'value' => '#11a0d8' ],
            [ 'value' => '#88d4ff' ],
            [ 'value' => '#6fd1dc' ],
            [ 'value' => '#60c4b7' ],
            [ 'value' => '#0cc7ff' ]
        ];

        foreach($data as $index=>$item){
            $this->model->create($item);
        }
    }
}
