<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{

    public function __construct(User $user){
        $this->user = $user;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->user->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        // Roles | 1 = Admin

        $users =
            [
                [
                    'data' => [
                        'username' => 'mars' ,
                        'name' => 'Mars Mlodzinski' ,
                        'email' => 'mars@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                    ],
                ],
                [
                    'data' => [
                        'username' => 'kirsten' ,
                        'name' => 'Kirsten Campbell-Morris' ,
                        'email' => 'kirsten@leadingbrands.me' ,
                        'password' => Hash::make('secret'),
                    ],
                ]
            ];

        foreach($users as $index=>$user){
            $newUser = User::create($user['data']);
            $newUser->role()->attach(1);
            $newUser->company()->attach(1);
        }
    }
}
