<?php

use Illuminate\Database\Seeder;
use App\Models\Content\ContentFieldType;
class ContentFieldTypeSeeder extends Seeder
{

    public function __construct(ContentFieldType $data){
        $this->model = $data;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->model->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['name' => 'Text Field' , 'slug' => 'text'],
            ['name' => 'Text Area' , 'slug' => 'text-area'],
//            ['name' => 'Selection' , 'slug' => 'select'],
//            ['name' => 'Multiple Choices' , 'slug' => 'radio'],
//            ['name' => 'Multiple Selection' , 'slug' => 'check'],
            ['name' => 'Yes/No' , 'slug' => 'boolean'],
        ];

        foreach($data as $datum)
            $this->model->create($datum);
    }
}
