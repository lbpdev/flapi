<?php

use Illuminate\Database\Seeder;
use App\Models\Pivots\Permission;
use Illuminate\Support\Str;
use App\Models\Template\TemplateFormat;
class FormatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\Models\Flatplans\FormatType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $data = [
            ['name' => 'Magazine'],
        ];

        foreach($data as $item){
            \App\Models\Flatplans\FormatType::create($item);
        }
    }
}
