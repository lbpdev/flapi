<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageMergesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_page_merges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merge_id')->index();
            $table->integer('flat_plan_page_id')->unsigned()->index();
            $table->foreign('flat_plan_page_id')->references('id')->on('flat_plan_pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_page_merges');
    }
}
