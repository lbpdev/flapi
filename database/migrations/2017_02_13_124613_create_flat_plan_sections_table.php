<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatPlanSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_page_id')->unsigned()->index();
            $table->foreign('flat_plan_page_id')->references('id')->on('flat_plan_pages')->onDelete('cascade');
            $table->integer('layout_section_id')->unsigned()->index();
            $table->foreign('layout_section_id')->references('id')->on('layout_sections')->onDelete('cascade');
            $table->integer('content_id')->nullable()->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_sections');
    }
}
