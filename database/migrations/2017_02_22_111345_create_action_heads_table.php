<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_heads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_version_id')->unsigned()->index();
            $table->foreign('flat_plan_version_id')->references('id')->on('flat_plan_versions')->onDelete('cascade');
            $table->string('action_head_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_heads');
    }
}
