<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionHistoryPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_history_pivots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('actionable_id')->unsigned()->index()->nullable();
            $table->string('actionable_type')->nullable();
            $table->integer('action_history_id')->unsigned()->index();
            $table->foreign('action_history_id')->references('id')->on('action_histories')->onDelete('cascade');
            $table->string('value_old')->nullable();
            $table->string('value_new')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_history_pivots');
    }
}
