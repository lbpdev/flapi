<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatPlanVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_id')->unsigned()->index();
            $table->foreign('flat_plan_id')->references('id')->on('flat_plans')->onDelete('cascade');
            $table->tinyInteger('version')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_versions');
    }
}
