<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatPlanSectionDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_section_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_section_id')->unsigned()->index();
            $table->foreign('flat_plan_section_id')->references('id')->on('flat_plan_sections')->onDelete('cascade');
            $table->integer('content_field_id')->unsigned()->index();
            $table->foreign('content_field_id')->references('id')->on('content_fields')->onDelete('cascade');
            $table->string('value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_section_data');
    }
}
