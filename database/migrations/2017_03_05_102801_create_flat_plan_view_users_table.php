<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatPlanViewUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_view_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_view_id')->unsigned()->index();
            $table->foreign('flat_plan_view_id')->references('id')->on('flat_plan_views')->onDelete('cascade');
            $table->integer('viewable_id')->unsigned()->index()->nullable();
            $table->string('viewable_type')->nullable();
            $table->string('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_view_users');
    }
}
