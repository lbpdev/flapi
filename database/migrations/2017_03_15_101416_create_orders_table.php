<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('REFNO');
            $table->string('PAYMENTDATE');
            $table->string('SALEDATE');
            $table->string('ORDERNO');
            $table->string('ORDERSTATUS');
            $table->string('PAYMETHOD');
            $table->string('CUSTOMEREMAIL');
            $table->string('IPADDRESS');
            $table->string('COMPLETE_DATE');
            $table->string('AVANGATE_CUSTOMER_REFERENCE');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
