<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatPlanPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_version_id')->unsigned()->index();
            $table->foreign('flat_plan_version_id')->references('id')->on('flat_plan_versions')->onDelete('cascade');
            $table->integer('layout_id')->nullable()->index();
            $table->smallInteger('order')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_pages');
    }
}
