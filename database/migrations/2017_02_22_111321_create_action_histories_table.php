<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('actionable_id')->unsigned()->index()->nullable();
            $table->string('actionable_type')->nullable();
            $table->integer('flat_plan_version_id')->unsigned()->index();
            $table->foreign('flat_plan_version_id')->references('id')->on('flat_plan_versions')->onDelete('cascade');
            $table->string('type');
            $table->string('value_old')->nullable();
            $table->string('value_new')->nullable();
            $table->string('head')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_histories');
    }
}
