<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFpMetaSizexSizey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flat_plan_meta', function (Blueprint $table) {
            $table->string('size_x')->change();
            $table->string('size_y')->change();
        });

    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flat_plan_meta', function (Blueprint $table) {
            $table->integer('size_x')->nullable()->default(0)->change();
            $table->integer('size_y')->nullable()->default(0)->change();
        });
    }
}
