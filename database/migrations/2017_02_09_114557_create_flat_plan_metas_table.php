<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatPlanMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_plan_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flat_plan_id')->unsigned()->index();
            $table->foreign('flat_plan_id')->references('id')->on('flat_plans')->onDelete('cascade')->onDelete('cascade');
            $table->integer('media_type_id')->unsigned()->index();
            $table->foreign('media_type_id')->references('id')->on('media_types');
            $table->integer('format_type_id')->unsigned()->index();
            $table->foreign('format_type_id')->references('id')->on('format_types');
            $table->string('pagination')->length('128')->nullable();
            $table->smallInteger('inside_pages')->default(0)->nullable();
            $table->string('size_x')->default(0)->nullable();
            $table->string('size_y')->default(0)->nullable();
            $table->string('units')->default('mm')->nullable();
            $table->string('start_page')->length('1')->default('3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_plan_meta');
    }
}
