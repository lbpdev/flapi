<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrintApprovedToMetas extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flat_plan_meta', function (Blueprint $table) {
            $table->string('print_approved')->default(0);
        });

    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flat_plan_meta', function (Blueprint $table) {
            $table->dropColumn('print_approved');
        });
    }
}
