<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layout_sections', function (Blueprint $table) {
            //'top','left','right','bottom','height','width','order','z-index','layout_id'
            $table->increments('id');
            $table->smallInteger('top')->lenth(4)->nullable();
            $table->smallInteger('left')->lenth(4)->nullable();
            $table->smallInteger('right')->lenth(4)->nullable();
            $table->smallInteger('bottom')->lenth(4)->nullable();
            $table->smallInteger('height')->lenth(4)->nullable();
            $table->smallInteger('width')->lenth(4)->nullable();
            $table->smallInteger('order')->lenth(4)->default(0);
            $table->smallInteger('z_index')->lenth(4)->nullable(null);
            $table->integer('layout_id')->unsigned()->index();
            $table->foreign('layout_id')->references('id')->on('layouts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('layout_sections');
    }
}
