<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdToMediaformat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media_types', function (Blueprint $table) {
            $table->integer('company_id')->nullable()->default(0);
        });

        Schema::table('format_types', function (Blueprint $table) {
            $table->integer('company_id')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media_types', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });

        Schema::table('format_types', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });
    }
}
